(define scene-name "poly")
(display (string-append "Constructing scene " scene-name "...\n"))

(define bright (rgb 700.0 700.0 700.0))
(define test-source (lightsource-material bright))
(define b (make-point #i(20.0 -20.0 20.0) test-source))

;;; this will construct a nice looking geodesic sphere
;;; And then its texture is also fun...

(define c
  (cons b
	(show-polystruct
	 (multiple-baricentric-subs 
	  (self-subdivide
	   (apply polylist->polystruct icosahedron)
	   (spherical-midpoint 2.0))
	  (spherical-midpoint 2.0)
	  2)
	 (plastic
	  (lambda (ray)
	    (let ((p (ray-position ray)))
	      (set! p (v+ (vscale 2.0 (fnoise3 (vscale 3.0 p) 5 0.7))))
	      (if (< (snoise (vscale 15.0 p)) 0.5)
		  (set! p (v+ p #i(12.198 18.1827 19.18277))))
	      ((color-map 0.000 pale-turquoise
			  0.300 light-sea-green
			  0.500 khaki
			  0.800 goldenrod
			  1.000 plum)
	       (snoise (vscale 6.0 p)))))))))

(display (string-append "Enclosing " scene-name "...\n"))
(set! c (make-scene c))
(display "Rendering...\n")
(gc)


(define (genimage)
  (render-image c
		(dvector 1.0 -6.5 3.2)
		(dvector 0. 0. 0.)
		(dvector 1.0 0. 0.)
		(dvector 0. 0. 1.0)
		(output-as-ppm)))

(with-output-to-file (string-append scene-name ".ppm") genimage)
(display "Finished.\n")


