
(define scene-name "tree3")
(display (string-append "Constructing scene " scene-name "...\n"))

(display "
This scene illustrates a trick with trees. Take a careful look at the
distant fractals - they are simpler than those nearby. This example
is exaggerated, the whole point is to avoid rendering huge primitives
")

(force-output)

(define bright (rgb 700.0 700.0 700.0))
(define test-source (lightsource-material bright))
(define b (make-point #i(20.0 20.0 20.0) test-source))

(define (bezier2 p1 c1 c2 p2)
  (bezier p1 (v+ p1 c1) (v+ p2 c2) p2))

(define mtranlist
  (list ident-mat
        (m* (translate #i( 0.8  0.  0. )) (unif-scale 0.4))
        (m* (translate #i( 0.0  0.  0.8)) (unif-scale 0.4))
        (m* (translate #i( 0.0  0. -0.8)) (unif-scale 0.4))
        (m* (translate #i(-0.8  0.  0. )) (unif-scale 0.4))))

(define (genmatl l)
  (let loop ((mats l)
	     (out '()))
    (if (null? mats)
	out
	(loop (cdr mats)
	      (append (map (lambda (x) (m* x (car mats)))
			   mtranlist)
		      out)))))

(define prime-node
  (make-scene
   (list
    (let ((curve
	   (join-curves
	    (bezier2 #i(0.0001 1.0 0.0)
		     #i(0.0 -0.2 0.0)
		     #i(-0.05 0.2 0.0)
		     #i(0.1 0.6 0.0))
	    (bezier2 #i(0.1 0.6 0.0)
		     #i(0.05 -0.2 0.0)
		     #i(0.0 0.4 0.0)
		     #i(0.15 0.0 0.0))
	    0.5)))
      (revolve rotate-y curve 20 20 (plastic yellow 0.1 0.2) 0.0001)
;      (make-sphere #i(0. 0. 0.) 0.5 (plastic yellow 0.1 0.2))
))))

(define (make-tree-distanced transform handle distance scene1 scene2)
  (let* ((transformed-handle (m* transform handle))
	 (selector
	  (lambda (ray)
	    (if (< (vdistance transformed-handle
				       (ray-origin ray))
			    distance)
			 0
			 1))))
    (make-tree transform selector scene1 scene2)))

(define c 
  (let* ((do-scene
	  (lambda (matrices)
	    (make-scene
	     (map (lambda (mat)
		    (make-tree mat #f prime-node))
		  matrices))))
	 (sc1 (do-scene (genmatl (genmatl (genmatl (list ident-mat))))))
	 (sc2 (do-scene (genmatl (list ident-mat)))))
    `(,(make-polygon
	'(#i(-100. 0. -100.)
	  #i( 100. 0. -100.)
	  #i( 100. 0.  100.)
	  #i(-100. 0.  100.))
	(bumps 0.2 10.
	       (metal (rgb 0.3 0.3 0.6) 0.7 0.05)))
      ,b
      ,@(do ((i -50. (+ i 5.))
	     (l '()))
	    ((>= i 50.) l)
	  (for-step j -50. 50. 5.
		    (set! l (cons (make-tree-distanced (translate (v+ (vscale i #i(1. 0. 0.))
								      (vscale j #i(0. 0. 1.))))
						       #i(0. 0. 0.) 15.0 sc1 sc2) l))))

      )))


(display (string-append "Enclosing " scene-name "...\n"))
(set! c (make-scene c))
(display "Rendering...\n")
(gc)

(define (genimage)
  (render-image c
	 (dvector 1.0 0.5001 2.2)
	 (dvector -50. 0. -90.0001)
	 (dvector 1.0 0. 0.)
	 (dvector 0. -1.0 0.0)
	 (output-as-ppm)))

(define (gentest x y)
  (mktest 320 240 c
	  (dvector 1.0 0.5001 2.2)
	  (dvector -50. 0. -90.0001)
	  (dvector 1.0 0. 0.)
	  (dvector 0. -1.0 0.0)
	  x y))

(with-output-to-file (string-append scene-name ".ppm") genimage)
(display "Finished.\n")


