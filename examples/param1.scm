(define scene-name "param1")
(display (string-append "Constructing scene " scene-name "...\n"))

(define gradmap    (color-map 0.00 black
			      0.20 dark-blue
			      0.40 green
			      0.60 yellow
			      0.80 red
			      1.00 white))

(define bright (rgb 700.0 700.0 700.0))
(define test-source (lightsource-material bright))
(define b (make-point #i(20.0 -20.0 20.0) test-source))

(define c (list b (make-parametric (lambda (x y) (dvector x (+ 0.5 (* 0.49 (sin (* 20 x)) (sin (* 20 y)))) y)) 48 48 (plastic (marble gradmap #i(0. -1. 0.)) 0.1 0.01) 0.0001)))

(display (string-append "Enclosing " scene-name "...\n"))
(set! c (make-scene c))
(display "Rendering...\n")
(gc)

(define (gentest x y)
  (mktest 320 240 c
	 (dvector 1.0 -1.5 1.2)
	 (dvector 0.5 0.5 0.5)
	 (dvector 1.0 0. 0.)
	 (dvector 0. 0. 1.0)
	 x y))

(define (genimage)
  (render-image c
	 (dvector 1.0 -1.5 1.2)
	 (dvector 0.5 0.5 0.5)
	 (dvector 1.0 0. 0.)
	 (dvector 0. 0. 1.0)
	 (output-as-ppm)))

(with-output-to-file (string-append scene-name ".ppm") genimage)
(display "Finished.\n")


