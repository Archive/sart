(define scene-name "hypertexture")
(display (string-append "Constructing scene " scene-name "...\n"))

(define bright (rgb 700.0 700.0 700.0))
(define test-source (lightsource-material bright))
(define b (make-point #i(20.0 -20.0 20.0) test-source))

(define plain (make-polygon '(#i(-100. 100. 0.)
			      #i(-100. -100. 0.)
			      #i(100. -100. 0.)
			      #i(100. 100. 0.))
			    (plastic red3)))

(define (potnoise x) ($expt (fnoise (vscale 5.0 x)) 4.0))
(define s '())
(for i -5 5
     (for j -5 5
	  (set! s
		(cons (make-sphere (dvector (* i 5.0) (* j 5.0) 2.0)
				   1.5 (plastic green))
		      s))))

(define c (append (list b plain) s))

(display (string-append "Enclosing " scene-name "...\n"))
(set! c (make-scene c))
(display "Rendering...\n")
(gc)

(define (genimage)
  (render-image c
	 (dvector 1.0 -6.5 5.2)
	 (dvector 0. 0. 0.)
	 (dvector 1.0 0. 0.)
	 (dvector 0. 0. 1.0)
	 (output-as-ppm)))

(with-output-to-file (string-append scene-name ".ppm") genimage)
(display "Finished.\n")


