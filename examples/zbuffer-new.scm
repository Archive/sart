
(define scene-name "zbuffer")
(display (string-append "Constructing scene " scene-name "...\n"))

(define bright (rgb 700.0 700.0 700.0))
(define test-source (lightsource-material bright))
(define b (make-point #i(20.0 -24.0 7.0) test-source))

(set-display-option! 'display:eps 0.01 display-options)

(define scn
  `(,(make-polygon (list #i(-1 -1 0)
			 #i(1 -1 0)
			 #i(1 1 0)
			 #i(-1 1 0))
		   (plastic red))
    ,(make-polygon (list #i(-1 0 -1)
			 #i(1 0 -1)
			 #i(1 0 1)
			 #i(-1 0 1))
		   (plastic green))
    ,(make-polygon (list #i(0 -1 -1)
			 #i(0 1 -1)
			 #i(0 1 1)
			 #i(0 -1 1))
		   (plastic blue))))

(define tree1
  (make-tree ident-mat #f
	     (make-scene
	      (list (make-polygon (list #i(0.4 -0.8 0.8)
					#i(0.40001 -0.4 0.8)
					#i(0.4 -0.8 0.4))
				  (plastic cyan))))))

(define mat (scale #i(0.8 0.6 1.0)))
  
(define x `(,b
	    ,@scn
	    ,tree1
	    ,(make-sphere #i(0 -1 0) 0.4 (plastic gray30))
	    ,(torus 0.7 0.15 40 15 (plastic magenta) 0.0001)
	    ,(hyper #i(-0.45 -0.45 0.25) #i(0.45 -0.45 0.25) #i(0 0.45 0.25)
		    #i(-1 -1 1) #i(1 -1 1) #i(0 1 1)
		    (lambda (x) (* 2.0
				   (vdistance x #i(-0.45 -0.45 0.25))
				   (vdistance x #i(0.45 -0.45 0.25))
				   (vdistance x #i(0 0.45 0.25))))
		    50 (plastic yellow))
	     ))


(define c (map (lambda (g)
		 (let ((h (copy-primitive g)))
		   (transform-primitive! h mat)
		   h))
	       x))

(display (string-append "Enclosing " scene-name "...\n"))
(set! c (make-scene c))
(gc)

(display "Zbuffering scene...\n")

;(define zb
;  (make-eye-zbuffer 1 c
;	 (dvector 0.7 -2.5001 1.9)
;	 (dvector 0. 0. 0.)
;	 (dvector 1.0 0. 0.)
;	 (dvector 0. 0. 1.0)))

;(display "Saving zbuffer...\n")
;
;(with-output-to-file "zbuffer-z.ppm"
;  (lambda ()
;    (display "P6\800 600\n255\n")
;    (spb-write (make-shared-array
;		  (car zb)
;		  (lambda (i j k) (list i j))
;		  600 800 3)
;		 (current-output-port) 0 1 0 1.0 #f)))

(display "Rendering...\n")

(define (genimage)
  (render-image c
	 (dvector 0.7 -2.5001 1.9)
	 (dvector 0. 0. 0.)
	 (dvector 1.0 0. 0.)
	 (dvector 0. 0. 1.0)
	 (output-as-ppm)
	))

(define (gentest x y)
  (mktest 800 600 c
	 (dvector 0.7 -2.5001 1.9)
	 (dvector 0. 0. 0.)
	 (dvector 1.0 0. 0.)
	 (dvector 0. 0. 1.0)
	 x y
	 (cons 'eye-zbuffer zb)))

(with-output-to-file (string-append scene-name ".ppm") genimage)

(display "Finished.\n")
