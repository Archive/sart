(define scene-name "sphere")
(display (string-append "Constructing scene " scene-name "...\n"))

(define bright (rgb 700.0 700.0 700.0))
(define test-source (lightsource-material bright))
(define b (make-point #i(20.0 -20.0 20.0) test-source))

(define m (color-map
	   0.000 white
	   0.300 cyan
	   1.000 midnight-blue
	   1.5 red
	   2.0 yellow
	   2.5 green))

(define mat1
  (plastic
   (turbulence 0.3 3.0
	       (lambda (ray)
		 (m (snoise (vscale 16. (ray-position ray))))))
   0.1 0.1))

(define clear (plastic white 0. 0. 1.))

(define (one-sided mat) ; this is dirty trick and won't always work
  (lambda (ray)
    (if (< (vdot (ray-direction ray) (ray-normal ray)) 0.0)
	(mat ray)
	(clear ray))))

(define materials
  (list
   (let ((mat1 (plastic
		(turbulence 0.3 3.0
			    (lambda (ray)
			      (m (snoise (vscale 16. (ray-position ray))))))
		0.1 0.1)))
     (make-shadow-transmitter
      (one-sided
       (lambda (ray)
	 (if (< (+ (forward-ray-distance ray (ray-direction ray))
		   (* 0.4 (fnoise (vscale 20. (ray-position ray)))))
		1.3)
	     (clear ray)
	     (mat1 ray))))))

   (let ((thehex (hex (lambda (ray tile)
			(m (* 2.8 (hex-norm (array-ref tile 0) (array-ref tile 1)))))))
	 (fade (lambda (ray)
		 (- 1.0 (range-limit
			 (* 0.7 (forward-ray-distance ray (ray-direction ray))))))))
     (make-shadow-transmitter
      (one-sided
       (plastic
	(lambda (ray)
	  (ray-displace! ray (vscale 5. (ray-position ray)))
	  (thehex ray))
	0.1 0.1 fade))))

   (let ((mat (metal gray70 0.89 0.1)))
     (lambda (ray)
       (ray-settext! ray
		     (v+ (vscale 0.7 (fnoise3
				      (vscale 8. (ray-position ray))))
			 (ray-texture ray)))
       (mat ray)))
   
   (let ((dirty
	  (lambda (ray)
	    (+ 0.01 (* 0.15 (snoise (vscale 12.0 (ray-position ray))))))))
     (metal dark-goldenrod 0.89 dirty))))

(define (buncha-spheres m mat-list pnt dx dy rad)
  (do ((l mat-list (cdr l))
       (center pnt)
       (k 0)
       (t '()))
      ((null? l) (reverse t))
    (set! t (cons (make-sphere center rad (car l)) t))
    (set! k (1+ k))
    (if (= k m)
	(begin
	  (set! k 0)
	  (set! pnt (v+ dy pnt))
	  (set! center pnt))
	(set! center (v+ dx center)))))

(define c (cons b 
		(buncha-spheres 2 materials
				#i(-1. -1. 0.)
				#i(2.1 0. 0.)
				#i(0. 2.1 0.)
				1.0)))

(display (string-append "Enclosing " scene-name "...\n"))
(set-zbuffer-mode #f)
(set! c (make-scene c))
(display "Rendering...\n")
(gc)

(define (gentest x y)
  (mktest 320 240 c
	  (dvector 1.0 -2.5 2.2)
	  (dvector 0. 0. 0.)
	  (dvector 1.0 0. 0.)
	  (dvector 0. 0. 1.0)
	  x y
	  ))

(define (genimage)
  (render-image c
	 (dvector 1.0 -2.5 2.2)
	 (dvector 0. 0. 0.)
	 (dvector 1.0 0. 0.)
	 (dvector 0. 0. 1.0)
	 (output-as-ppm)))

(with-output-to-file (string-append scene-name ".ppm") genimage)
(display "Finished.\n")


