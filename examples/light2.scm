(define scene-name "light2")
(display (string-append "Constructing scene " scene-name "...\n"))

(define source-mat
  (let ((texture (marble (color-map 0.00 (rgb 10. 5. 0.)
				    0.50 (rgb 10. 10. 0.)
				    0.500001 (rgb 10. 0. 0.)
				    1.00 (rgb 10. 5. 0.))
			 #i(0.25 0.0 0.0))))
    (make-lightsource (lambda (ray) (texture ray)))))

(display "x")

(define b (make-polygon 
	   '(#i( 2. -1. 0.5)
	     #i(-2. -1. 1.)
	     #i(-2. -1. 0.))
	   source-mat))

(define c
  `(
    ,(make-polygon
      '(#i(-500. -500. 0.)
	#i( 500. -500. 0.)
	#i( 500.  500. 0.)
	#i(-500.  500. 0.))
      (plastic (chequer cyan white 0.3)))
    ,(make-sphere #i(0. 0. 0.) 0.6 (plastic azure))
    ,b))

(display (string-append "Enclosing " scene-name "...\n"))
(set! c (make-scene c))
(display "Rendering...\n")
(gc)

(define (genimage)
  (render-image c
	 (dvector 0.4 3.5 2.2)
	 (dvector 0. -1. 0.50001)
	 (dvector 1.0 0. 0.)
	 (dvector 0. 0. -1.0)
	 (output-as-ppm)))

(define (gentest x y)
  (mktest 320 240 c
	 (dvector 0.4 3.5 2.2)
	 (dvector 0. -1. 0.50001)
	 (dvector 1.0 0. 0.)
	 (dvector 0. 0. -1.0)
	 x y))

(with-output-to-file (string-append scene-name ".ppm") genimage)
(display "Finished.\n")


