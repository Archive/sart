
(define a (make-uniform-array 1/3 6 6))
(array-index-map! a (lambda (x y) (+ 1 (* x y))))
;(define b (gaussian 128 0.5 1.0))
;(set! b (make-shared-array b (lambda (x) (list x 12)) 128))
(define b (make-uniform-array 1/3 2 2))
;(set! b (make-shared-array b (lambda (x) (list x x)) 34))
;(array-index-map! b (lambda (x) (sin (* x pi 1/17))))
;(set! b (array-complexify b))

(array-fill! b 1.0)

(display b)
(display a)
;(display (convolve-periodic b a))
(define c (apply index-expand `(,b 0.0 ,@(array-shape a))))
(display c)(newline)
(display (array-dot a c))(newline)
(array-add! a c)
(display a)(newline)
(array-mul! a c)
(display a)(newline)
(set! a (array-clone a))
(array-mask! a c 0.2)
(display a)(newline)

(with-output-to-file "test_array.spb"
  (lambda ()
    (spb-write a (current-output-port) 0 3 0 256.0 #f)))

(force-output)
(quit)