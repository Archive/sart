
(display "Rendering everything...\n")

(define (max-res)
  (assq-set! display-options #:resolution '(1024 . 768))
  (assq-set! display-options #:square 5))

(define (high-res)
  (assq-set! display-options #:resolution '(800 . 600))
  (assq-set! display-options #:square 5))

(define (med-res)
  (assq-set! display-options #:resolution '(640 . 480))
  (assq-set! display-options #:square 5))

(define (low-res)
  (assq-set! display-options #:resolution '(320 . 240))
  (assq-set! display-options #:square 6))

(define (no-aa)
  (assq-set! display-options #:anti-alias 1))
(define (med-aa)
  (assq-set! display-options #:anti-alias 2))
(define (high-aa)
  (assq-set! display-options #:anti-alias 3))
(define (max-aa)
  (assq-set! display-options #:anti-alias 5))

(low-res)
(no-aa)

(assq-set! display-options #:square 8)

(load "../examples/csg.scm")
(load "../examples/fractal.scm")
(load "../examples/fractal2.scm")
(load "../examples/param1.scm")
(load "../examples/param2.scm")
(load "../examples/param3.scm")
(load "../examples/param4.scm")
(load "../examples/geo.scm")
(load "../examples/tree.scm")
(load "../examples/tree2.scm")
(load "../examples/hyper1.scm")
(load "../examples/tree3.scm")
(load "../examples/volume.scm")
(load "../examples/volume2.scm")
(load "../examples/volume3.scm")
(load "../examples/sphere.scm")
(load "../examples/poly.scm")
(load "../examples/light1.scm")
(load "../examples/light2.scm")
(load "../examples/knot.scm")
(load "../examples/zbuffer.scm")

(quit)
