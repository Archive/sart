(define scene-name "param4")
(display (string-append "Constructing scene " scene-name "...\n"))

(define bright (rgb 700.0 700.0 700.0))
(define test-source (lightsource-material bright))
(define b (make-point #i(20.0 -20.0 20.0) test-source))

(define c (list
	   b 
	   (torus 2. 0.6 40 20 yellow-pine-texture 0.0001)
	   (let ((cmap (list -40. midnight-blue 50. cyan)))
	     (make-sphere #i(0. 0. 0.) 50.0 
			  (lambda (ray) 
			    (linear-spline (vdot #i(0. 0. 1.) (ray-position ray)) cmap))))
	   (make-sphere #i(0. 0. 0.) 1.2 (bumps 0.4 20.0 (metal gray80 0.95 0.001)))))

(display (string-append "Enclosing " scene-name "...\n"))
(set! c (make-scene c))
(display "Rendering...\n")
(gc)

(define (genimage)
  (render-image c
	 (dvector 1.0 -6.5 3.2)
	 (dvector 0. 0. 0.)
	 (dvector 1.0 0. 0.)
	 (dvector 0. 0. 1.0)
	 (output-as-ppm)))

(with-output-to-file (string-append scene-name ".ppm") genimage)
(display "Finished.\n")


