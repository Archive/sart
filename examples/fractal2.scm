
(define scene-name "fractal2")
(display (string-append "Constructing scene " scene-name "...\n"))

(define bright (rgb 6300.0 6300.0 6300.0))
(define test-source (lightsource-material bright))
(define b (make-point #i(60.0 -60.0 -60.0) test-source))

(define (tree1 count matrix mat)
  (if (= count 0)
      '()
      (lparse "ts67f(qqqpqrrrra)(ppq320a)(r160ppqqqqa)" matrix
	      (lambda (s e) (cyl-link s e 16 mat))
	      (lambda (x) (tree1 (1- count) x mat)))))

(define enclosed-tree1
  (make-scene (tree1 3 ident-mat (plastic green))))

(define (tree2 count matrix mat)
  (if (= count 0)
      (list (make-tree matrix #f enclosed-tree1))
      (lparse "ts67f(qqqpqrrrra)(ppq320a)(r160ppqqqqa)" matrix
	       (lambda (s e) (cyl-link s e 16 mat))
	       (lambda (x) (tree2 (1- count) x mat)))))

(define enclosed-tree2
  (make-scene (tree2 3 ident-mat (plastic red))))

(define (tree3 count matrix mat)
  (if (= count 0)
      (list (make-tree matrix #f enclosed-tree2))
      (lparse "ts67f(qqqpqrrrra)(ppq320a)(r160ppqqqqa)" matrix
	       (lambda (s e) (cyl-link s e 24 mat))
	       (lambda (x) (tree3 (1- count) x mat)))))

(define c `(,b
	    ,@(tree3 3 (rotate-x 3.14)
		     (plastic blue))))

(display (string-append "Enclosing " scene-name "...\n"))
(set! c (make-scene c))
(display "Rendering...\n")
(gc)

(define (genimage)
  (render-image c
	 (dvector 1.0 -40.5 20.2)
	 (dvector 0. 0. -10.)
	 (dvector 1.0 0. 0.)
	 (dvector 0. 0. 1.)
	 (output-as-ppm)))

(with-output-to-file (string-append scene-name ".ppm") genimage)

(display "Finished.\n")


