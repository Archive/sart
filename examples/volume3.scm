(define scene-name "volume3")
(display (string-append "Constructing scene " scene-name "...\n"))

(define bright (rgb 700.0 700.0 700.0))
(define test-source (lightsource-material bright))
(define b (make-point #i(20.0 -20.0 2.0) test-source))

(define s
  (make-sphere #i(0. 0. 0.) 1.0
	       (make-shadow-transmitter (plastic white 0. 0. 1.))))

(define (func1 ray)
  (vdistance (ray-position ray) #i(0. 0. 0.)))

(define test-surface
  (let* ((func (lambda (ray)
		 (let ((p (ray-position ray)))
		   (func1
		    (ray-displace!
		     ray
		     (v+ p
			 (vscale (sin (* (array-ref p 1) 16.0))
				 #i(0. 0.12 0.))
			 (vscale (sin (* (array-ref p 0) 16.0))
				 #i(0.12 0. 0.))))))))
	 (pal1 (color-map
		0.1 white
		0.2 cyan
		0.3 green
		0.4 orange
		0.5 yellow
		0.6 red
		0.7 purple))
	 (pal2 (marble (color-map
				 0.000 0.0
				 0.250 0.0
				 0.251 1.0
				 1.000 1.0) #i(0. 0. 5.)))
	 (mat1 (lambda (ray)
		 (let ((f (func ray)))
		   ((plastic (lambda (ray) (pal1 f))
			     0.1 0.15 pal2) ray))))
	 (ivol (iso-surfaces func
			     mat1
			     0.02
			     4.0
			     #i(0.1 0.2 0.3 0.4 0.5 0.6 0.7))))
    (lambda (ray . rest)
      (if (eq? (ray-flags ray) 1)
	  (forward-ray ray)
	  (apply ivol (cons ray rest))))))

(define test-surface-simple
  (let* ((func (lambda (ray)
		 (let ((p (ray-position ray)))
		   (func1
		    (ray-displace!
		     ray
		     (v+ p
			 (vscale (sin (* (array-ref p 1) 16.0))
				 #i(0. 0.12 0.))
			 (vscale (sin (* (array-ref p 0) 16.0))
				 #i(0.12 0. 0.))))))))
	 (pal2 (marble (color-map
				 0.000 0.0
				 0.300 0.0
				 0.301 1.0
				 1.000 1.0) #i(0. 0. 10.)))
	 (mat1 (plastic red 0.1 0.15))
	 (ivol (iso-surfaces func
			     mat1
			     0.02
			     4.0
			     #i(0.7))))
    ivol))

(define s1 (csg-container (list s) test-surface))

(define c (list b s))

(display (string-append "Enclosing " scene-name "...\n"))
(set! c (make-scene c))
(display "Rendering...\n")
(gc)

(define (genimage)
  (render-image c
	 (dvector .5 -1.7 0.9)
	 (dvector 0. 0. 0.)
	 (dvector 1.0 0. 0.)
	 (dvector 0. 0. 1.0)
	 (output-as-ppm)))

(define (gentest x y)
  (mktest 320 240 c
	 (dvector .5 -1.7 0.9)
	 (dvector 0. 0. 0.)
	 (dvector 1.0 0. 0.)
	 (dvector 0. 0. 1.0)
	 x y))

(with-output-to-file (string-append scene-name ".ppm") genimage)
(display "Finished.\n")


