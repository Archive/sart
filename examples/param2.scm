(define scene-name "param2")
(display (string-append "Constructing scene " scene-name "...\n"))

(define bright (rgb 700.0 700.0 700.0))
(define test-source (lightsource-material bright))
(define b (make-point #i(20.0 -20.0 20.0) test-source))

(set-tree-defaults 30 12)

(define c (list b
		(make-parametric
		 (lambda (x y)
		   (dvector (* 2 (cos (* 8 y)) (cos (* 7 x)))
			    (* 2 (cos (* 6 x)) (sin (* 3 x)))
			    (* 2 (sin (* 4 y)) (sin (* 9 x)))))
		 70 70 (plastic yellow 0.1 0.01))))

(set-tree-defaults 30 8)

(display (string-append "Enclosing " scene-name "...\n"))
(set! c (make-scene c))
(display "Rendering...\n")
(gc)

(define (genimage)
  (render-image c
	 (dvector 3.0 -3.5 3.2)
	 (dvector 0. 0. 0.)
	 (dvector 1.0 0. 0.)
	 (dvector 0. 0. 1.0)
	 (output-as-ppm)))

(with-output-to-file (string-append scene-name ".ppm") genimage)
(display "Finished.\n")


