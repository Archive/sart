
(define scene-name "tree2")
(display (string-append "Constructing scene " scene-name "...\n"))

(define bright (rgb 700.0 700.0 700.0))
(define test-source (lightsource-material bright))
(define b (make-point #i(20.0 -20.0 20.0) test-source))

(define (show x)
  (write x)
  (force-output)
  x)

(define the-torus
  (make-scene `(;; first, create a simple torus
		,@(let ((t2 (make-scene 
			     (list 
			      (torus 2. 0.15 40 20
				     (metal seashell 0.7 0.06) 0.0001))))
			(t1 (make-scene
			     (list 
			      (torus 2. 0.15 40 20 
				     (turbulence 0.2 8.
						 (plastic
						  (wood white-wood 
							#i(0. 12. 0.))
						  0.1 0.2)) 0.0001)))))
		    (list
		     (make-tree ident-mat #f t2)
		     (make-tree (rotate-x (* 0.5 pi)) #f t1)
		     (make-tree (rotate-y (* 0.5 pi)) #f t1))))))

(define c (list b
		(make-tree ident-mat #f the-torus)
		(make-tree (unif-scale 0.7) #f the-torus)
		(make-tree (unif-scale 0.49) #f the-torus)))

(override-material! (caddr c) (plastic rosewood-texture))

(display (string-append "Enclosing " scene-name "...\n"))
(set! c (make-scene c))
(display "Rendering...\n")
(gc)

(define (genimage)
  (render-image c
	 (dvector 1.0 -6.5 3.2)
	 (dvector 0. 0. 0.)
	 (dvector 1.0 0. 0.)
	 (dvector 0. 0. 1.0)
	 (output-as-ppm)))

(with-output-to-file (string-append scene-name ".ppm") genimage)
(display "Finished.\n")


