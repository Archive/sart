(define scene-name "geo")
(display (string-append "Constructing scene " scene-name "...\n"))

(define bright (rgb 700.0 700.0 700.0))
(define test-source (lightsource-material bright))
(define b (make-point #i(20.0 20.0 20.0) test-source))

(define (terrain x y)
  (* 0.37 (fnoise (vscale 10.0 (dvector x y)) 10)))

(define grass-texture
  (mixture
   (lambda (ray)
     (linear-spline (vdot (ray-position ray) #i(0. 0. 1.))
		    '(0.00 0.00
		      0.24 0.50
		      0.3  1.00
		      1.0  1.00)))
   (blobs (color-map 0.00 green2
		     1.00 green4)
	  500.0)
   (blobs (color-map 0.00 brown2
		     1.00 brown4)
	  500.0)))

(define water-stained-stone
  (blobs (color-map 0.00 gray30
		    1.00 gray60)
	 200.0))

(define stone-mixture
  (mixture
   (lambda (ray)
     (linear-spline (+ (vdot (ray-position ray) #i(0. 0. 1.))
		       (* 0.04 (snoise (vscale 320.0 (ray-position ray)))))
		    '(-10.00 0.00
		      0.28   0.00
		      0.281  1.00
		      10.00  1.00)))
   (mixture
    (lambda (ray)
      (linear-spline (+ (abs (vdot (ray-texture ray) #i(0. 0. 1.)))
			(* 0.09 (snoise (vscale 150.0 (ray-position ray)))))
		     '(0.0    0.00
		       0.5    0.20
		       0.75   1.00
		       2.0    1.00)))
    water-stained-stone
    grass-texture)
   white))

(define c (list b
		(let ((terrain-array (make-uniform-array 1/3 500 500)))
		  (array-index-map! terrain-array
				    (lambda (i j)
				      (terrain (/ i 499.0) (/ j 499.0))))
		  (make-heightfield terrain-array
				    (plastic stone-mixture 0.0 0.0)))

		(make-polygon '(#i(-60. -60. 0.14)
			        #i(60.  -60. 0.14)
				#i(60.  60.  0.14)
				#i(-60. 60.  0.14))
			      (plastic royal-blue 0.2 0.02))

))

(display (string-append "Enclosing " scene-name "...\n"))
(set! c (make-scene c))
(display "Rendering...\n")
(gc)

(define (genimage)
  (render-image c
	 (dvector 0.5 0.5 0.28)
	 (dvector 0.50001 0.4 0.25)
	 (dvector 1.0 0. 0.)
	 (dvector 0. 0. -1.0)
	 (output-as-ppm)))

(define (gentest x y)
  (mktest 320 240 c
	  (dvector 0.9 1.5 1.2)
	  (dvector 0.5 0.5 0.1)
	  (dvector 1.0 0. 0.)
	  (dvector 0. 0. -1.0)
	  x y))

(with-output-to-file (string-append scene-name ".ppm") genimage)
(display "Finished.\n")


