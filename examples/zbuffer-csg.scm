(define scene-name "zbuffer-csg")
(display (string-append "Constructing scene " scene-name "...\n"))

(define bright (rgb 700.0 700.0 700.0))
(define test-source (make-lightsource (lambda (ray) bright)))
(define b (make-point #i(20.0 20.0 20.0) test-source))

(define hexboard
  (let ((m1 (color-map
	     0.000 white
	     2.050 white
	     2.100 red)))
    (make-polygon
     '(#i(-100. 0. -100.)
	 #i( 100. 0. -100.)
	 #i( 100. 0.  100.)
	 #i(-100. 0.  100.))
     (plastic 
      (let ((t
	     (hex (lambda (ray tile)
		    (m1 (* 2.8 (hex-norm (array-ref tile 0) (array-ref tile 1)))))))
	    (transform (rotate-x-deg 90.0)))
	(lambda (ray)
	  (ray-displace! ray (m* transform (ray-position ray)))
	  (t ray)))))))

(define sky
  (let* ((cmap (color-map
		0.000 white
		0.100 yellow
		0.200 orange
		0.500 maroon2
		1.000 maroon1))
	 (mat (marble cmap #i(0. 0.01 0.))))
    (make-sphere #i(0. 0. 0.) 100. mat)))

(define mtranlist
  (list ident-mat
        (m* (translate #i( 0.8  0.  0. )) (unif-scale 0.4))
        (m* (translate #i( 0.0  0.  0.8)) (unif-scale 0.4))
        (m* (translate #i( 0.0  0. -0.8)) (unif-scale 0.4))
        (m* (translate #i(-0.8  0.  0. )) (unif-scale 0.4))))

(define (genmatl l)
  (let loop ((mats l)
	     (out '()))
    (if (null? mats)
	out
	(loop (cdr mats)
	      (append (map (lambda (x) (m* x (car mats)))
			   mtranlist)
		      out)))))

(define prime-node
  (make-scene
   (list
    (make-sphere #i(0. 0. 0.) 0.5 (plastic white 0.4 0.02 1.0)))))

(define (do-scene matrices)
  (make-scene
   (map (lambda (mat)
	  (make-tree mat #f prime-node))
	matrices)))

(define tree1
  (make-tree
   ident-mat
   #f
   (do-scene (genmatl (genmatl (genmatl (list ident-mat)))))))

(define cn2 (csg tree1))

(define sph1
  (make-sphere #i(0. 0.25 0.) 0.5 (plastic white 0.4 0.02 1.0)))

(define cn3 (csg sph1))

(define cn4 (csg-minus cn2 (list cn3) (gas yellow-green 0.9 1.6)))

(define c
  `(,b
    ,hexboard
    ,sky
    ,tree1
    ,sph1
    ))

(display (string-append "Enclosing " scene-name "...\n"))
(set-zbuffer-mode #t)
(set! c (make-scene c))
(display "Rendering...\n")
(gc)

(define zb
  (make-eye-zbuffer 1 c
		    (dvector 0.6 -2.1001 1.5)
		    (dvector 0. 0. 0.)
		    (dvector 1.0 0. 0.)
		    (dvector 0. 0. 1.0)))

(assq-set! display-options #:eye-zbuffer zb)

(define (genimage)
  (render-image c
         (dvector 1.0 3.0 3.2)
         (dvector 0.0 0.0 0.000001)
         (dvector 1. 0. 0.)
         (dvector 0. -1. 0.)
	 (output-as-ppm)))

(define (gentest x y)
  (mktest 300 120 c
	  (dvector 1.0 3.0 3.2)
	  (dvector 0.65 2.25 0.000001)
	  (dvector 1. 0. 0.)
	  (dvector 0. -1. 0.)
	  x y))

(with-output-to-file (string-append scene-name ".ppm") genimage)
(display "Finished.\n")


