(define scene-name "radiosity-test")
(display (string-append "Constructing scene " scene-name "...\n"))

(define bright (rgb 0.7 0.7 0.7))
(define test-source (lightsource-material bright))
(define b (make-point #i(2.0 4.6 1.6) test-source))
(define b2 (make-point #i(2.0 0.4 1.6) test-source))

(define c `(,b
	    ,b2
	    ,@(make-box (scale #i(5.0 5.0 2.0)) (plastic white))
	    ,@(make-box (translate #i(1. 1. 0.)) (plastic green 0.2 0.2))
	    ,(make-sphere #i(3.5 1.5 0.5) 0.5 (plastic red 0.2 0.2))))

(display (string-append "Enclosing " scene-name "...\n"))
(set! c (make-scene c))
(display "Rendering...\n")
(gc)

(assq-set! display-options #:resolution '(1024 . 768))
(assq-set! display-options #:anti-alias 2)
(assq-set! display-options #:jitter 0.8)
(assq-set! display-options #:ambient gray40)
(assq-set! display-options #:infinity blue)

(set-radiosity-depth 2)
(set-radiosity-resolution 0.2)
(set-radiosity-rays 512)

(define (genimage)
  (render-image c
	 (dvector 4.5 4.5 0.7)
	 (dvector 2.5 1.5 0.5)
	 (dvector 1. 0. 0.)
	 (dvector 0. 0. -1.)
	 (output-as-ppm)))

(define (gentest x y)
  (mktest 320 240 c
	 (dvector 4.5 4.5 0.7)
	 (dvector 2.5 1.5 0.5)
	 (dvector 1. 0. 0.)
	 (dvector 0. 0. -1.)
	 x y))

(with-output-to-file (string-append scene-name ".ppm") genimage)
(display "Finished.\n")


