(define scene-name "light1")
(display (string-append "Constructing scene " scene-name "...\n"))

(define b (make-polygon 
	   '(#i(-5. -5. 5.)
	     #i( 5. -5. 5.)
	     #i( 5.  5. 5.)
	     #i(-5.  5. 5.))
	   (lightsource-material (rgb 28. 28. 28.))))

(define c
  `(
    ,(make-polygon
      '(#i(-500. -500. 0.)
	#i( 500. -500. 0.)
	#i( 500.  500. 0.)
	#i(-500.  500. 0.))
      (plastic (chequer yellow white 0.3)))
    ,(make-sphere #i(1.0 1.0 1.0) 0.8 (plastic red))
    ,b))

(display (string-append "Enclosing " scene-name "...\n"))
(set! c (make-scene c))
(display "Rendering...\n")
(gc)

(define (genimage)
  (render-image c
	 (dvector 1.2 6.5 1.2)
	 (dvector 1. 1. 1.)
	 (dvector 1.0 0. 0.)
	 (dvector 0. 0. -1.0)
	 (output-as-ppm)))

(with-output-to-file (string-append scene-name ".ppm") genimage)
(display "Finished.\n")


