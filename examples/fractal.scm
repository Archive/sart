(define scene-name "fractal")
(display (string-append "Constructing scene " scene-name "...\n"))

(display "
This scene contains very large number of objects. I hope you have
enough resources to tackle it.

")

(define bright (rgb 700.0 700.0 700.0))
(define test-source (lightsource-material bright))
(define b (make-point #i(20.0 -20.0 20.0) test-source))

(define mtranlist
  (list (cons ident-mat 1.0)
        (cons (m* (translate #i(0. 0. -1.9)) (unif-scale 0.4)) 0.4)
	(cons (m* (rotate-x 1) (translate #i(0. 0. -1.9)) (unif-scale 0.4)) 0.4)
	(cons (m* (rotate-x -1) (translate #i(0. 0. -1.9)) (unif-scale 0.4)) 0.4)
	(cons (m* (rotate-y 1) (translate #i(0. 0. -1.9)) (unif-scale 0.4)) 0.4)
	(cons (m* (rotate-y -1) (translate #i(0. 0. -1.9)) (unif-scale 0.4)) 0.4)
        (cons (m* (translate #i(0. 0. 1.9)) (unif-scale 0.4)) 0.4)
	(cons (m* (rotate-x 1) (translate #i(0. 0. 1.9)) (unif-scale 0.4)) 0.4)
	(cons (m* (rotate-x -1) (translate #i(0. 0. 1.9)) (unif-scale 0.4)) 0.4)
	(cons (m* (rotate-y 1) (translate #i(0. 0. 1.9)) (unif-scale 0.4)) 0.4)
	(cons (m* (rotate-y -1) (translate #i(0. 0. 1.9)) (unif-scale 0.4)) 0.4)))


;; This routine takes a list of points and sizes, and constructs a new
;; list by copying each pair with each object from the transformation list.
;; Calling this several times will generate fractal. Mind you, it could
;; also explode your heap.

(define (genmatl l)
  (let loop ((balls l)
	     (out '()))
    (if (null? balls)
	out
	(loop (cdr balls)
	      (let* ((b (car balls))
		     (c (car b))
		     (r (cdr b)))
		(append (map (lambda (x) (cons (m* (car x) c) (* (cdr x) r)))
			     mtranlist)
			out))))))

(define c (cons b
		(let ((m (plastic yellow 0.05 0.02)))
		  (map (lambda (x) (make-sphere (car x) (cdr x) m))
		       (genmatl (genmatl (genmatl (genmatl '((#i(0. 0. 0.) . 1.))))))))))


(display (string-append "Enclosing " scene-name "...\n"))
(set! c (make-scene c))
(display "Rendering...\n")
(gc)

(define (genimage)
  (render-image c
	 (dvector 1.0 -6.5 3.2)
	 (dvector 0. 0. 0.)
	 (dvector 1.0 0. 0.)
	 (dvector 0. 0. 1.0)
	 (output-as-ppm)))

(with-output-to-file (string-append scene-name ".ppm") genimage)
(display "Finished.\n")


