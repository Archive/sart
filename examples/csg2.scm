(define scene-name "csg2")
(display (string-append "Constructing scene " scene-name "...\n"))

(define bright (rgb 700.0 700.0 700.0))
(define test-source (list (lambda (ray) bright) lightsource))

(define b (make-point #i(20.0 -20.0 20.0) test-source))

(define a1 (make-sphere #i(1.0 0.0 0.0) 2.0 (plastic yellow)))
(define a2 (make-sphere #i(-1.0 0.0 0.0) 2.0 (plastic blue)))
(define a3 (make-sphere #i(0.0 1.0 0.0) 2.0 (plastic magenta)))
(define a4 (make-sphere #i(0.0 -1.0 0.0) 2.0 (plastic green)))
(define a5 (make-sphere #i(0.0 0.0 1.0) 2.0 (plastic cyan)))
(define a6 (make-sphere #i(0.0 0.0 -1.0) 2.0 (plastic red)))
(define a7 (make-sphere #i(0.0 0.0 0.0) 2.75 (plastic white)))

(define a (csg- (csg a7)
		(csg+ (csg a4) (csg a5) (csg a1) (csg a2) (csg a6) (csg a3))))

(define c (list b a1 a2 a3 a4 a5 a6 a7))

(display (string-append "Enclosing " scene-name "...\n"))
(set! c (make-scene c))
(display "Rendering...\n")
(gc)

(define (genimage)
  (render-to-ppm c
         (dvector 6.0 -3.0 5.2)
         (dvector 0.0 0.0 0.000001)
         (dvector 0. 1. 0.)
         (dvector 0. 0. 1.0)))

(define (gentest x y)
  (mktest 320 240 c
	  (dvector 6.0 -3.0 5.2)
	  (dvector 0.0 0.0 0.000001)
	  (dvector 0. 1. 0.)
	  (dvector 0. 0. 1.00)
	  x y))

(with-output-to-file (string-append scene-name ".ppm") genimage)
(display "Finished.\n")


