(define scene-name "knot")
(display (string-append "Constructing scene " scene-name "...\n"))

(define bright (rgb 700.0 700.0 700.0))
(define test-source (lightsource-material bright))
(define b (make-point #i(20.0 -20.0 20.0) test-source))

(define c (list b
		(let* ((curve (lambda (x)
				(let* ((f (* 2.0 pi x))
				       (c (cos f))
				       (s (sin f))
				       (r 0.35))
				  (dvector (* r c) (* r s) 0.0))))
		       (transf (lambda (x)
				 (let* ((f (* 4.0 pi x)))
				   (m*
				    (rotate-y f)
				    (translate (dvector -1.8 0.0 0.0))
				    (rotate-z (* 1.5 f))
				    (translate (dvector 0.7 0.0 0.0)))))))
		  (make-transpath curve transf 20 180
				  (plastic red 0.4 0.2) 0.0001))))

(display (string-append "Enclosing " scene-name "...\n"))
(set! c (make-scene c))
(display "Rendering...\n")
(gc)

(define (genimage)
  (render-image c
	 (dvector 1.0 -4.5 5.8)
	 (dvector 0. 0. 0.)
	 (dvector 1.0 0. 0.)
	 (dvector 0. 0. 1.0)
	 (output-as-ppm)))

(define (gentest x y)
  (mktest 320 240 c
	 (dvector 1.0 -6.5 3.2)
	 (dvector 0. 0. 0.)
	 (dvector 1.0 0. 0.)
	 (dvector 0. 0. 1.0)
	 x y))

(with-output-to-file (string-append scene-name ".ppm") genimage)
(display "Finished.\n")


