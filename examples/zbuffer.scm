
(define scene-name "zbuffer")
(display (string-append "Constructing scene " scene-name "...\n"))

(define bright (rgb 700.0 700.0 700.0))
(define test-source (lightsource-material bright))
(define b (make-point #i(20.0 -24.0 7.0) test-source))

(define scn
  `(,(make-polygon (list #i(-1. -1. 0.)
			 #i(1. -1. 0.)
			 #i(1. 1. 0.)
			 #i(-1. 1. 0.))
		   (plastic indian-red))
    ,(make-polygon (list #i(-1. 0. -1.)
			 #i(1. 0. -1.)
			 #i(1. 0. 1.)
			 #i(-1. 0. 1.))
		   (plastic lime-green))
    ,(make-polygon (list #i(0. -1. -1.)
			 #i(0. 1. -1.)
			 #i(0. 1. 1.)
			 #i(0. -1. 1.))
		   (plastic cornflower-blue))))

(define tree1
  (make-tree ident-mat #f
	     (make-scene
	      (list (make-polygon (list #i(0.4 -0.8 0.8)
					#i(0.40001 -0.4 0.8)
					#i(0.4 -0.8 0.4))
				  (plastic light-sea-green))))))
  
(define c `(,b
	    ,(make-sphere #i(0. -1. 0.) 0.3 (plastic gray50))
;	     ,(make-parametric
;	       (lambda (u1 v1)
;		 (let ((u (* pi u1))
;		       (v (* pi2 v1)))
;		   (let ((su (sin u))
;			 (cu (cos u))
;			 (sv (sin v))
;			 (cv (cos v)))
;		     (v+ #i(0. -1. 0.)
;			 (vscale 0.3 (dvector (* cv su) (* sv su) cu))))))
;	       16 32 (plastic gray50)  0.0001)
	    ,@scn
	    ,tree1
	    ,(torus 0.7 0.15 40 15 (plastic deep-pink) 0.0001)
	    ,(hyper #i(-0.45 -0.45 0.25) #i(0.45 -0.45 0.25) #i(0. 0.45 0.25)
		    #i(-1. -1. 1.) #i(1. -1. 1.) #i(0. 1. 1.)
		    (lambda (x) (* 2.0
				   (vdistance x #i(-0.45 -0.45 0.25))
				   (vdistance x #i(0.45 -0.45 0.25))
				   (vdistance x #i(0. 0.45 0.25))))
		    50 (plastic light-yellow))))

(display (string-append "Enclosing " scene-name "...\n"))
(set-zbuffer-mode #t)
(set! c (make-scene c))
(gc)

(display "Zbuffering scene...\n")


(define zb
  (make-eye-zbuffer 1 c
		      (dvector 0.6 -2.1001 1.5)
		      (dvector 0. 0. 0.)
		      (dvector 1.0 0. 0.)
		      (dvector 0. 0. 1.0)))

(assq-set! display-options #:eye-zbuffer zb)

;(display "Saving zbuffer...\n")
;
;(with-output-to-file "zbuffer-z.ppm"
;  (lambda ()
;    (display "P6\800 600\n255\n")
;    (spb-write (make-shared-array
;		(car zb)
;		(lambda (i j k) (list i j))
;		600 800 3)
;	       (current-output-port) 0 1 0 1.0 #f)))

(display "Rendering...\n")

(define (genimage)
  (render-image c
	 (dvector 0.6 -2.1001 1.5)
	 (dvector 0. 0. 0.)
	 (dvector 1.0 0. 0.)
	 (dvector 0. 0. 1.0)
	 (output-as-ppm)))

(define (gentest x y)
  (mktest 320 240 c
         (dvector 0.6 -2.1001 1.5)
	 (dvector 0. 0. 0.)
	 (dvector 1.0 0. 0.)
	 (dvector 0. 0. 1.0)
	 x y
	 (cons 'eye-zbuffer zb)
	 ))

;(set-radiosity-depth 1)
(set-radiosity-table-size 2048)
(set-radiosity-resolution 0.1)
(set-radiosity-rays 768)

(with-output-to-file (string-append scene-name ".ppm") genimage)

(display "Finished.\n")
