(define scene-name "volume")
(display (string-append "Constructing scene " scene-name "...\n"))

(define bright (rgb 700.0 700.0 700.0))
(define test-source (lightsource-material bright))
(define b (make-point #i(20.0 -20.0 20.0) test-source))

(define s
  (make-sphere #i(0. 0. 0.) 1.0 (plastic white 0. 0. 1.)))

(define blobby-fog
  (gas-material (blobs (color-map
			0.00 (cons 2000. white)
			0.70 (cons 2000. white)
			0.75 (cons 0.3 white)
			0.90 (cons 0.1 white)
			1.00 (cons 0.01 black))
		       5.0)
		0.02))

(define s1 (csg-container (list s) blobby-fog))

(define c (list b s))

(display (string-append "Enclosing " scene-name "...\n"))
(set! c (make-scene c))
(display "Rendering...\n")
(gc)

(define (genimage)
  (render-image c
	 (dvector 1.0 -6.5 3.2)
	 (dvector 0. 0. 0.)
	 (dvector 1.0 0. 0.)
	 (dvector 0. 0. 1.0)
	 (output-as-ppm)))

(with-output-to-file (string-append scene-name ".ppm") genimage)
(display "Finished.\n")


