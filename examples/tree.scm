
(define scene-name "tree")
(display (string-append "Constructing scene " scene-name "...\n"))

(define bright (rgb 700.0 700.0 700.0))
(define test-source (lightsource-material bright))
(define b (make-point #i(20.0 -20.0 20.0) test-source))

(define (show x)
  (write x)
  (force-output)
  x)

(define csg-list '()) ;to protect from gc

(define the-torus
  (make-scene `(;; first, create a simple torus
		,(torus 2. 0.15 40 20 
			(turbulence 0.2 8.
			  (plastic
			   (wood white-wood #i(0. 8. 0.)) 0.2 0.05)) 0.0001)
		;; Then add a bunch of spheres around it
		,@(let ((cntsphere
			 (make-scene
			  (list 
			   (make-sphere
			    #i(0. 0. 0.) 0.7
			    (make-shadow-transmitter (plastic orange 0.1 0.1 0.95))))))
			(cntsphere-csg
			 (let* ((s (make-sphere
				    #i(0. 0. 0.) 0.7
                                    (make-shadow-transmitter (plastic orange 0.25 0.1 0.95))))
				(c (csg-container (list s) (gas green 50.0 1.6))))
			   (make-scene (list s))))
			(n 0))
		    (map (lambda (x)
			   (set! n (- 1 n))
			   (if (= n 0)
			       (make-tree x #f cntsphere)
			       (make-tree x #f cntsphere-csg)))
			 (matrix-path (m* (scale #i(1. 0.5 1.0))
					  (translate #i(2. 0. 0.)))
				      (rotate-z (/ pi 8))
				      16))))))


;make one more tree to see if anything breaks
(define c (list b
		(make-tree ident-mat #f the-torus)))

(display (string-append "Enclosing " scene-name "...\n"))
(set! c (make-scene c))
(display "Rendering...\n")
(gc)

(define (genimage)
  (render-image c
	 (dvector 1.0 -6.5 3.2)
	 (dvector 0. 0. 0.)
	 (dvector 1.0 0. 0.)
	 (dvector 0. 0. 1.0)
	 (output-as-ppm)))

(define (gentest x y)
  (mktest 320 240 c
	  (dvector 1.0 -6.5 3.2)
	  (dvector 0. 0. 0.)
	  (dvector 1.0 0. 0.)
	  (dvector 0. 0. 1.0)
	  x y))

(with-output-to-file (string-append scene-name ".ppm") genimage)
(display "Finished.\n")


