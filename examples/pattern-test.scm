(define scene-name "pattern-test")
(display (string-append "Constructing scene " scene-name "...\n"))

(define bright (rgb 700.0 700.0 700.0))
(define test-source (lightsource-material bright))
(define b (make-point #i(20.0 20.0 20.0) test-source))

(define (terrain x y)
  (* 0.37 (fnoise (vscale 10.0 (dvector x y)) 10)))

(define c (list b
		(make-polygon '(#i(-60. -60. 0.14)
			        #i(60.  -60. 0.14)
				#i(60.  60.  0.14)
				#i(-60. 60.  0.14))
			      (plastic (mipmap-texture
					(create-mipmap
					 (load-ppm "test-pattern.ppm"))
					(unif-scale 10.0)
					(mip-from-origin #i(0.5 0.5 0.28)
							 10.0))
				       0.0 0.0))))

(display (string-append "Enclosing " scene-name "...\n"))
(set! c (make-scene c))
(display "Rendering...\n")
(gc)

(define (genimage)
  (render-image c
	 (dvector 0.5 0.5 0.28)
	 (dvector 0.50001 0.4 0.25)
	 (dvector 1.0 0. 0.)
	 (dvector 0. 0. -1.0)
	 (output-as-ppm)))

(define (gentest x y)
  (mktest 320 240 c
	  (dvector 0.9 1.5 1.2)
	  (dvector 0.5 0.5 0.1)
	  (dvector 1.0 0. 0.)
	  (dvector 0. 0. -1.0)
	  x y))

(with-output-to-file (string-append scene-name ".ppm") genimage)
(display "Finished.\n")


