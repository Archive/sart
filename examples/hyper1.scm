(define scene-name "hyper1")
(display (string-append "Constructing scene " scene-name "...\n"))

(define bright (rgb 700.0 700.0 700.0))
(define test-source (lightsource-material bright))
(define b (make-point #i(15.5 5.5 20.0) test-source))

(define c
  (let ((fun
	 (lambda (x)
;	   (let* ((y (v- x #i(0.5 0.5 0.0))))
;	     (array-set! y 0.0 2)
;	     (set! y (vnorm y))
;	     (* 0.7 (exp (* -30.0 y y))))))
	   (* 0.5 (fnoise (vscale 10. x)))))
;	     (* 0.25
;		(+ 1.0
;		   (* (cos (vdot #i(15.0 0.0 0.0) x))
;		      (cos (vdot #i(0.0 15.0 0.0) x))
;		      (cos (vdot #i(0.0 0.0 15.0) x)))))))
	(txt
	 (color-map 0.000 deep-sky-blue
		    0.100 cadet-blue
		    0.200 sea-green
		    0.300 chartreuse
		    0.400 green-yellow
		    0.500 orange
		    0.600 purple
		    0.700 white)))
       (list b
	     (hyper #i(0. 0. 0.) #i(1. 0. 0.) #i(0. 1. 0.)
		    #i(0. 0. 1.) #i(0. 0. 1.) #i(0. 0. 1.)
		    fun 250 (plastic (marble txt #i(0. 0. 1.4)) 0.05 0.1))
	     (hyper #i(1. 1. 0.) #i(1. 0. 0.) #i(0. 1. 0.)
		    #i(0. 0. 1.) #i(0. 0. 1.) #i(0. 0. 1.)
		    fun 250 (plastic (marble txt #i(0. 0. 1.4)) 0.05 0.1)))))

(display (string-append "Enclosing " scene-name "...\n"))
(set! c (make-scene c))
(display "Rendering...\n")
(gc)

(define (genimage)
  (render-image c
	 (dvector 0.8 0.9 0.5)
	 (dvector 0.5 0.5 0.25)
	 (dvector 1.0 0. 0.)
	 (dvector 0. 0. -1.0)
	 (output-as-ppm)))

(define (gentest x y)
  (mktest 640 480 c
	 (dvector 1.8 1.9 0.8)
	 (dvector 0.5 0.5 0.5)
	 (dvector 1.0 0. 0.)
	 (dvector 0. 0. -1.0)
	 x y))

(with-output-to-file (string-append scene-name ".ppm") genimage)
(display "Finished.\n")


