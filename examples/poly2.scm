(define scene-name "poly2")
(display (string-append "Constructing scene " scene-name "...\n"))

(define bright (rgb 700.0 700.0 700.0))
(define test-source (list (lambda (ray) bright) lightsource))
(define b (make-point #i(20.0 -20.0 20.0) test-source))

;;; this will construct a nice looking geodesic sphere
;;; And then its texture is also fun...

(define blob
  (list
   (cons 1. (translate #i( 0.9 0. 0.)))
   (cons 1. (translate #i(-0.9 0. 0.)))))

(define c
  (cons b
	(show-polystruct
	 (multiple-baricentric-subs 
	  (self-subdivide
	   (apply polylist->polystruct tetrahedron)
	   (blob-midpoint blob))
	  (blob-midpoint blob)
	  4)
	 (plastic red 0.1 0.1))))

(display (string-append "Enclosing " scene-name "...\n"))
(set! c (make-scene c))
(display "Rendering...\n")
(gc)

(define (genimage)
  (render-to-ppm c
	 (dvector 1.0 -6.5 3.2)
	 (dvector 0. 0. 0.)
	 (dvector 1.0 0. 0.)
	 (dvector 0. 0. 1.0)))

(with-output-to-file (string-append scene-name ".ppm") genimage)
(display "Finished.\n")


