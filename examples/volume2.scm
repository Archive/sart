(define scene-name "volume2")
(display (string-append "Constructing scene " scene-name "...\n"))

(define bright (rgb 700.0 700.0 700.0))
(define test-source (lightsource-material bright))
(define b (make-point #i(20.0 -20.0 20.0) test-source))

(define blobby-fog-material
  (blobs
   (color-map
    0.00 (cons 4000. ghost-white)
    0.70 (cons 4000. ghost-white)
    0.75 (cons  800. ghost-white)
    1.00 (cons  400. ghost-white))
   300.0))

(define smoke
  (lambda (ray)
    (let* ((p (ray-position ray))
	   (z (array-ref p 2)))
	   (if (< z 0.0)
	       10000.0
	       (let* ((swirled (m* (rotate-z (* z 4.0)) p))
		      (r (begin (array-set! p 0.0 2) (vnorm p)))
		      (dump (exp (+ (* 1.5 z)
				    (* (+ r .3) (+ z 1.) 2.5)
				    (/ r z)))))
		 (* 0.2 (snoise (vscale 5.0 swirled)) dump))))))

(define s
  (make-sphere #i(0. 0. 0.) 3.0
	       (make-shadow-transmitter (plastic white 0. 0. 1.))))

(define s1 (csg-container (list s)
			  (gas-material (lambda (ray)
					  (cons (smoke ray) green))
					.1)))

(define mat1 (plastic gray40 0.2 0.1))

(define (bezier2 p1 c1 c2 p2)
  (bezier p1 (v+ p1 c1) (v+ p2 c2) p2))

(define c (list b
		s
		(revolve rotate-z
			 (join-curves
			  (bezier2
			   #i(0.0 0.0 0.0)
			   #i(0.2 0.0 0.0)
			   #i(-0.2 0.0 -0.2)
			   #i(0.8 0.0 0.35))
			  (join-curves
			   (bezier2
			    #i(0.8 0.0 0.35)
			    #i(0.05 0.0 0.05)
			    #i(-0.05 0.0 -0.05)
			    #i(0.9 0.0 0.29))
			   (join-curves
			    (bezier2
			     #i(0.9 0.0 0.29)
			     #i(-0.2 0.0 -0.2)
			     #i(0.2 0.0 0.0)
			     #i(0.15 0.0 -0.1))
			    (line
			     #i(0.15 0.0 -0.3)
			     #i(0.15 0.0 -1.2))
			    0.8)
			   0.4)
			  0.3)
			 60 20 mat1 0.0001)))

(display (string-append "Enclosing " scene-name "...\n"))
(set! c (make-scene c))
(display "Rendering...\n")
(gc)

(define (genimage)
  (render-image c
	 (dvector 3.5 -4.6 1.2)
	 (dvector 0. 0. 0.)
	 (dvector 1.0 0. 0.)
	 (dvector 0. 0. -1.0)
	 (output-as-ppm)))

(define (gentest x y)
  (mktest 320 240 c
	  (dvector 3.5 -4.6 1.2)
	  (dvector 0. 0. 0.)
	  (dvector 1.0 0. 0.)
	  (dvector 0. 0. 1.0)
	  x y))

(with-output-to-file (string-append scene-name ".ppm") genimage)
(display "Finished.\n")


