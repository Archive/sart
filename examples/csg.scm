(define scene-name "csg")
(display (string-append "Constructing scene " scene-name "...\n"))

(define bright (rgb 700.0 700.0 700.0))
(define test-source (lightsource-material bright))

(define a1 (make-sphere #i(0.0 2. 0.) 1.0 (turbulence 0.2 8. (plastic (wood white-wood #i(0. 8. 0.)) 0.2 0.05))))
(define a2 (make-sphere #i(0.9 2. 0.) 1.0 (plastic red 0.2 0.05)))
(define a6 (make-sphere #i(0.45 2.45 0.) 1.0 (plastic royal-blue 0.2 0.05)))
(define a7 (make-sphere #i(0.45 1.55 0.) 1.0 (plastic dark-green 0.2 0.05)))
(define a3 (make-sphere #i(0.45 2.0 0.) 1.35 (turbulence 0.2 8. (plastic (wood dark-wood #i(0. 10. 0.)) 0.2 0.05))))
(define a4 (make-sphere #i(0.45 2. 0.45) 1.0 (turbulence 0.2 8. (plastic (wood tan-wood #i(0. 20. 0.)) 0.2 0.05))))
(define a5 (make-sphere #i(0.45 2. -0.45) 1.0 (turbulence 0.2 8. (plastic (wood cherry-wood #i(0. 30. 0.)) 0.2 0.05))))
(define b (make-point #i(20.0 -20.0 20.0) test-source))

(define (warped-cyl trans warp vec mat)
  (let* ((curve (lambda (x)
		  (let* ((f (* 2.0 pi x))
			 (c (cos f))
			 (s (sin f))
			 (r (+ 0.12 (* 0.01 (sin (* 8 f))))))
		    (dvector (* r c) (* r s) 0.0))))
	 (transf (lambda (x)
		   (let* ((f (* 2.0 pi x)))
		     (m*
		      trans
		      (translate (vscale x vec))
		      (rotate-z (* warp f)))))))
    (make-transpath curve transf 40 20 mat 0.0001)))


;; The csg object must be stored, to protect it from gc until we
;; enclose the scene

(define a (csg- (csg+ (csg a4) (csg a5) (csg a1) (csg a2) (csg a6) (csg a7))
		(csg a3)))

(define c (list b a1 a2 a3 a4 a5 a6 a7
		(warped-cyl (translate #i(0.45 2.0 -1.6))
			    1.5 #i(0. 0. 3.2)
			    (metal lavender 0.90 0.01))
		(warped-cyl (m* (translate #i(0.45 2.0 0.))
				(rotate-x (/ pi 2))
				(translate #i(0. 0. -1.6)))
			    1.5 #i(0. 0. 3.2)
			    (metal seashell 0.90 0.01))
		(warped-cyl (m* (translate #i(0.45 2.0 0.0))
				(rotate-y (/ pi 2))
				(translate #i(0. 0. -1.6)))
			    1.5 #i(0. 0. 3.2)
			    (metal pale-green 0.90 0.01))))

(display (string-append "Enclosing " scene-name "...\n"))
(set! c (make-scene c))
(display "Rendering...\n")
(gc)

(define (genimage)
  (render-image c
         (dvector 1.0 -1.0 3.2)
         (dvector 0.45 2.0 0.000001)
         (dvector 0.500001 0.500001 0.500001)
         (dvector 0. 0. 1.0)
	 (output-as-ppm)))

(define (gentest x y)
  (mktest 160 120 c
	  (dvector 1.0 -1.0 3.2)
	  (dvector 0.45 2.0 0.000001)
	  (dvector 0.500001 0.500001 0.500001)
	  (dvector 0. 0. 1.0)
	  x y))

(with-output-to-file (string-append scene-name ".ppm") genimage)
(display "Finished.\n")


