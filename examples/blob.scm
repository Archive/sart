(define scene-name "blob")
(display (string-append "Constructing scene " scene-name "...\n"))

(define bright (rgb 700.0 700.0 700.0))
(define test-source (lightsource-material bright))
(define b (make-point #i(20.0 20.0 20.0) test-source))

(assq-set! display-options #:resolution '(1024 . 768))
(assq-set! display-options #:square 12)


(define c
  (list
   b
   (make-blob (list (list sphere-component (translate #i(0. 0. 0.)) 2.0)
		    (list sphere-component (translate #i(2.5 0. 0.)) 2.0)
		    (list sphere-component (translate #i(0. 0. 2.5)) 2.0)
		    (list sphere-component (translate #i(-1.5 0. 0.)) 0.6)
		    (list sphere-component (translate #i(3.7 0. 0.)) 0.3))
	      0.5
	      8
	      (plastic red 0.1 0.1))))

(display (string-append "Enclosing " scene-name "...\n"))
(set! c (make-scene c))
(display "Rendering...\n")
(gc)


(define (genimage)
  (render-image c
		(dvector 1.0 7.5 8.2)
		(dvector 0. 0. 0.)
		(dvector 1.0 0. 0.)
		(dvector 0. 0. 1.0)
		(output-as-ppm)))

(with-output-to-file (string-append scene-name ".ppm") genimage)
(display "Finished.\n")


