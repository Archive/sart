#include <bsp.h>

/****** PointInBox
 * NAME
 *  PointInBox
 * SYNOPSIS
 *  boolean PointInBox (Point3 *min, Point3 *max, Point3 *pt)
 * FUNCTION
 *  Check whether a point is in a given axis-aligned box.
 * INPUTS
 *  min, max - corners of the box
 *  pt       - the point
 * RESULT
 *  TRUE or FALSE
 ******
 */

boolean PointInBox (Point3 *min, Point3 *max, Point3 *pt)
{
    return pt->x>=min->x-EPS && pt->y>=min->y-EPS && pt->z>=min->z-EPS
      &&     pt->x<=max->x+EPS && pt->y<=max->y+EPS && pt->z<=max->z+EPS;
}

/****** RayBoxIntersect
 * NAME
 *  RayBoxIntersect
 * SYNOPSIS
 *  boolean RayBoxIntersect (Ray *ray, Point3 *min, Point3 *max,
 *                           double *returnMin, double *returnMax)
 * FUNCTION
 *  Find whether the ray intersects a given box.
 * INPUTS
 *  ray - the ray
 *  min, max - corners of the box
 * RESULT
 *  returns TRUE if there is an intersection. returnMin and returnMax will
 *  contain the lowest and the highest distances along the ray. (any of which
 *  may be negative).
 ******
 */

boolean RayBoxIntersect (Ray *ray, Point3 *min, Point3 *max, double *returnMin,
			 double *returnMax)
{
    double vmin, vmax, a, b;
    Point3 *dir= &ray->direction, *origin= &ray->origin;

    if (dir->x!=0.0) {
	a=(min->x-origin->x)/dir->x;
	b=(max->x-origin->x)/dir->x;
	if (dir->x>0.0) vmin=a, vmax=b;
	else vmax=a, vmin=b;
	if (vmax<0) return FALSE;
    }
    else if (origin->x < min->x || origin->x > max->x)
	return FALSE;
    else vmin= -1e8, vmax=1e8;
    if (dir->y!=0.0) {
	a=(min->y-origin->y)/dir->y;
	b=(max->y-origin->y)/dir->y;
	if (dir->y>0.0) { if (vmin<a) vmin=a; if (vmax>b) vmax=b; }
	else { if (vmin<b) vmin=b; if (vmax>a) vmax=a; }
	if (vmin>vmax || vmax<0) return FALSE;
    }
    else if (origin->y < min->y || origin->y > max->y)
	return FALSE;
    if (dir->z!=0.0) {
	a=(min->z-origin->z)/dir->z;
	b=(max->z-origin->z)/dir->z;
	if (dir->z>0.0) { if (vmin<a) vmin=a; if (vmax>b) vmax=b; }
	else { if (vmin<b) vmin=b; if (vmax>a) vmax=a; }
        if (vmin>vmax || vmax<0) return FALSE;
    }
    else if (origin->z < min->z || origin->z > max->z)
	return FALSE;
    if (vmin<0) vmin=0;
    *returnMin=vmin;
    *returnMax=vmax;
    return TRUE;
}

/****** PointBoxSqDistance
 * NAME
 *  PointBoxSqDistance
 * SYNOPSIS
 *  double PointBoxSqDistance (Point3 *o, Point3 *min, Point3 *max)
 * FUNCTION
 *  Find the squared distance between the point o and axis-aligned box
 *  with corners given with min and max. The distance is defined as distance
 *  to the closest point within (or on) the box.
 *******/

double PointBoxSqDistance (Point3 *o, Point3 *min, Point3 *max)
{
    Point3 p;

    p.x=o->x < min->x ? min->x : (o->x > max->x ? max->x : o->x);
    p.y=o->y < min->y ? min->y : (o->y > max->y ? max->y : o->y);
    p.z=o->z < min->z ? min->z : (o->z > max->z ? max->z : o->z);

    return V3SquaredDistance(o,&p);
}

/****** LineBoxIntersect
 * NAME
 *  LineBoxIntersect
 * SYNOPSIS
 *  boolean LineBoxIntersect (Point3 *p1, Point3 *p2, Point3 *min, Point3 *max)
 * FUNCTION
 *  Find whether the line between the points p1 and p2 intersects the box
 *  given with min and max. The result is TRUE if the intersection exists and
 *  is contained in the line (that is, between the endpoints).
 * SEE ALSO
 *  LinePolyIntersect
 *******
 */


boolean LineBoxIntersect (Point3 *p1, Point3 *p2, Point3 *min, Point3 *max)
{
    Ray r;
    double m1,m2;

    r.origin = *p1;
    V3Sub(p2,p1,&r.direction);
    if (RayBoxIntersect (&r, min, max, &m1, &m2))
	return m1 <= 1.0 || m2 >= 0.0;
    else
	return FALSE;
}

/****** TransformBox
 * NAME
 *  TransformBox
 * SYNOPSIS
 *  void TransformBox (Point3 *imin, Point3 *imax, Point3 *omin,
 *                     Point3 *omax, Matrix4 *mat)
 * FUNCTION
 *  This call transforms axis-aligned box given with imin and imax with
 *  matrix mat, giving a new axis-aligned box (in omin and omax). The
 *  new box is the smallest one that contains the actual transform of
 *  the box (which is not necessarily orthogonal or axis-aligned).
 ********
 */

void TransformBox (Point3 *imin, Point3 *imax, Point3 *omin, Point3 *omax,
		   Matrix4 *mat)
{
    static Point3 v={0,0,0};

    Point3 v1, v2, v3;
    Point3 vt1, vt2, vt3;
    Point3 starting;

    /* Transform cardinal vectors */

    v1=v2=v3=v;
    v1.x=imax->x-imin->x;
    v2.y=imax->y-imin->y;
    v3.z=imax->z-imin->z;
    V3MulVectorByMatrix (&v1, mat, &vt1);
    V3MulVectorByMatrix (&v2, mat, &vt2);
    V3MulVectorByMatrix (&v3, mat, &vt3);
    V3MulPointByMatrix (imin, mat, &starting);

    /* Go to the min point, by minimizing each coordinate. Do similarily
     with the max */

    *omin=starting;
    *omax=starting;

    if (vt1.x<0) omin->x+=vt1.x;
    if (vt2.x<0) omin->x+=vt2.x;
    if (vt3.x<0) omin->x+=vt3.x;
    if (vt1.y<0) omin->y+=vt1.y;
    if (vt2.y<0) omin->y+=vt2.y;
    if (vt3.y<0) omin->y+=vt3.y;
    if (vt1.z<0) omin->z+=vt1.z;
    if (vt2.z<0) omin->z+=vt2.z;
    if (vt3.z<0) omin->z+=vt3.z;

    if (vt1.x>0) omax->x+=vt1.x;
    if (vt2.x>0) omax->x+=vt2.x;
    if (vt3.x>0) omax->x+=vt3.x;
    if (vt1.y>0) omax->y+=vt1.y;
    if (vt2.y>0) omax->y+=vt2.y;
    if (vt3.y>0) omax->y+=vt3.y;
    if (vt1.z>0) omax->z+=vt1.z;
    if (vt2.z>0) omax->z+=vt2.z;
    if (vt3.z>0) omax->z+=vt3.z;
}

