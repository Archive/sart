#ifndef _poly_implicit_h_
#define _poly_implicit_h_

#include "wtree.h"
#include "hashtable.h"
#include "defs.h"
#include "linear.h"

typedef struct {
    IntPoint3 i;
    double value;
} Corner;

typedef void (*TriangleOutput)(void *data, Point3 *verts,
			       int e1, int e2, int e3);

struct st_SubdInfo;

typedef void (*VertexOutput)(void *data, struct st_SubdInfo *p);

typedef double (*SubdFunc)(Point3 *p, void *data);

typedef struct st_SubdInfo {
    void *tdata;
    void *vdata;
    void *fdata;
    int nverts;
    Point3 *verts;
    HashTable *edge_table;
    HashTable *corner_table;
    int ncorners;
    Corner *corners;
    TriangleOutput trian_out;
    VertexOutput vert_out;
    SubdFunc f;
} SubdInfo;

void
poly_mesh (TriangleOutput trian_out, void *tdata,
	   VertexOutput vert_out, void *vdata,
	   SubdFunc f, void *fdata,
	   SubdCriterium crit, void *cdata);

void init_poly_subdivision (void);


#endif

