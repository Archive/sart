
#include <stdlib.h>
#include "bsp.h"

int find_axis (Point3 *normal)
{
    double nn,n0;
    int axis;

    nn=fabs(normal->x); axis=X; 
    if ((n0=fabs(normal->y))>nn) axis=Y, nn=n0;
    if ((n0=fabs(normal->z))>nn) axis=Z, nn=n0;
    return axis;
}

boolean RayPolygonIntersection (Ray *ray, GeomPoly *poly, double *dist)
{
    double n0,d;
    double x,y,x1,y1,x2,y2,t;
    int j,k,s,axis;
    Point3 p;

    /* ray/plane intersection */
    if (fabs(n0=V3Dot(&poly->normal,&ray->direction))<EPS)
	return FALSE;
    *dist=d= -(V3Dot(&poly->normal,&ray->origin)+poly->ndist)/n0;
    if (d<0) return FALSE;
    PointAtDistance(ray,d,&p);
    /* 2D inside test */
    axis=find_axis(&poly->normal);
    CalcAxis (&p,&x,&y,axis);
    if (x<poly->min.x || x>poly->max.x ||
	y<poly->min.y || y>poly->max.y)
	return FALSE;
    for (s=j=0; j<poly->nverts; j++) {
	CalcAxis(&poly->verts[j],&x1,&y1,axis);
	k=j ? j-1 : poly->nverts-1;
	CalcAxis(&poly->verts[k],&x2,&y2,axis);
	if (y1==y2) {
	    if (y==y1 && ((x<=x1 && x>=x2) || (x>=x1 && x<=x2)))
		return 1;
	}
	else if ((y>y1 && y<=y2) || (y<=y1 && y>y2))
	    if ((t=((y2-y)*x1+(y-y1)*x2)/(y2-y1)) > x)
		s++;
	    else if (t==x) return 1;
    }
    return s&1;
}

boolean RayTriangleIntersection (Ray *ray, Point3 *p1, Point3 *p2, Point3 *p3,
				 Point3 *nPtr, double d, double *dist)
{
    double n0;
    double x,y,x1,y1,x2,y2,x3,y3,t;
    int s,axis;
    Point3 p,n,d1,d2;

    /* calculate a normal, if not already given */

    if (!nPtr) {
	nPtr= &n;
	V3Cross(V3Sub(p2,p1,&d1),V3Sub(p3,p1,&d2),&n);
	d= -V3Dot(p1,&n);
    }

    /* ray/plane intersection */
    if (fabs(n0=V3Dot(nPtr,&ray->direction))<EPS)
	return FALSE;
    *dist=d= -(V3Dot(nPtr,&ray->origin)+d)/n0;
    if (d<0)
	return FALSE;
    PointAtDistance(ray,d,&p);

    /* 2D inside test, optimized for a triangle */
    axis=find_axis(nPtr);
    CalcAxis (&p,&x,&y,axis);
    CalcAxis(p1,&x1,&y1,axis);
    CalcAxis(p2,&x2,&y2,axis);
    CalcAxis(p3,&x3,&y3,axis);
    s=0;
    if ((x<x1 && x<x2 && x<x3) || (x>x1 && x>x2 && x>x3) ||
	(y<y1 && y<y2 && y<y3) || (y>y1 && y>y2 && y>y3))
	return FALSE;
    if (y1==y2) {
	if (y==y1 && ((x<=x1 && x>=x2) || (x>=x1 && x<=x2)))
	    return 1;
    }
    else if ((y>y1 && y<y2) || (y<y1 && y>y2)) 
	if ((t=((y2-y)*x1+(y-y1)*x2)/(y2-y1)) > x)
	    s++;
	else if (t==x) return 1;
    if (y2==y3) {
	if (y==y2 && ((x<=x2 && x>=x3) || (x>=x2 && x<=x3)))
	    return 1;
    }
    else if ((y>y2 && y<y3) || (y<y2 && y>y3)) 
	if ((t=((y3-y)*x2+(y-y2)*x3)/(y3-y2)) > x)
	    s++;
	else if (t==x) return 1;
    if (y3==y1) {
	if (y==y3 && ((x<=x3 && x>=x1) || (x>=x3 && x<=x1)))
	    return 1;
    }
    else if ((y>y3 && y<y1) || (y<y3 && y>y1)) 
	if ((t=((y1-y)*x3+(y-y3)*x1)/(y1-y3)) > x)
	    s++;
	else if (t==x) return 1;
    return s&1;
}

void get_minmax (GeomPoly *p)
{
    int i;
    Point2 min, max;
    double x,y;
    int axis=find_axis(&p->normal);

    for (i=0; i<p->nverts; i++) {
	CalcAxis (&p->verts[i],&x,&y,axis);
	if (!i) {
	    min.x=max.x=x;
	    min.y=max.y=y;
	}
	else {
	    if (min.x>x) min.x=x;
	    if (min.y>y) min.y=y;
	    if (max.x<x) max.x=x;
	    if (max.y<y) max.y=y;
	}
    }
    p->min=min;
    p->max=max;
}

void get_normal (GeomPoly *p)
{
    int i;
    Point3 *p1;
    static Point3 dummy={1, 0, 0};
    Point3 t1, t2, n;
    double l;

    for (i=0,p1=p->verts; i<p->nverts-2; i++,p1++) {
	V3Cross(V3Sub(p1+1,p1,&t1),V3Sub(p1+2,p1,&t2),&n);
	if ((l=V3SquaredLength(&n))>EPS*EPS) {
	    l=sqrt(l);
	    p->normal.x=n.x/l;
	    p->normal.y=n.y/l;
	    p->normal.z=n.z/l;
	    p->ndist=-V3Dot(&p->normal,p1);
	    return;
	}
    }
    p->normal=dummy;
    p->ndist=0;
}

/* Split the poly by plane. Object will *NOT* have its min/max fields
   set. */

void SplitByPlane (GeomPoly *inpoly, GeomPoly *out1, GeomPoly *out2, Point3 *nrm, double dist, boolean twoside_flag)
{
    int i, code, lastcode, n1=0, n2=0;
    Point3 *p1, *p2;

    out1->verts=NEWARRAY(Point3,2*inpoly->nverts);
    if (twoside_flag) out2->verts=NEWARRAY(Point3,2*inpoly->nverts);
    p1=inpoly->verts;
    lastcode= V3Dot(p1,nrm) > 0;

    for (i=inpoly->nverts; i>0; i--,p1++) {

	p2=(i==1 ? inpoly->verts : p1+1);

	code = V3Dot(p2,nrm) > 0;

	if (code!=lastcode) {
	    double d;
	    Point3 s;
	    
	    d= -(V3Dot(nrm,p1)+dist)/V3Dot(nrm,V3Sub(p2,p1,&s));
	    V3Combine(p1,p2,&s,1-d,d);
	    out1->verts[n1++]=s;
	    if (twoside_flag) out2->verts[n2++]=s;
	}
	lastcode=code;

	if (code)
	    out1->verts[n1++]=*p2;
	else
	    if (twoside_flag) out2->verts[n2++]=*p2;
    }

    /* We're done, now let's setup the polies */

    /*printf (".. %d > %d %d \n",inpoly->nverts, n1, n2);

      for (i=0; i<n1; i++) {
      printf ("> %f %f %f\n",out1->verts[i].x,out1->verts[i].y,out1->verts[i].z);
      }
      */

    if (n1) {
	out1->type=inpoly->type;
	out1->verts=(Point3*)realloc(out1->verts, n1*sizeof(Point3));
	out1->nverts=n1;
	out1->normal=inpoly->normal;
	out1->ndist=inpoly->ndist;
	get_minmax (out1);
    } else {
	free (out1->verts);
	out1->nverts=0;
    }
    if (twoside_flag && n2) {
	out2->type=inpoly->type;
	out2->verts=(Point3*)realloc(out2->verts, n2*sizeof(Point3));
	out2->nverts=n2;
	out2->normal=inpoly->normal;
	out2->ndist=inpoly->ndist;
	get_minmax (out2);
    } else if (twoside_flag) {
	free (out2->verts);
	out2->nverts=0;
    }
}

void PolyFree (GeomPoly *p)
{
    free (p->verts);
}

double DistanceToLine (Point3 *t1, Point3 *t2, Point3 *t)
{
    Point3 d1, d2;
    double l, dot;

    l = V3DistanceBetween2Points (t1, t2);
    if (l < EPS) return BIG;
    V3Scale(V3Sub(t1, t2, &d1), 1 / l);
    V3Sub(t1, t, &d2);
    dot = V3Dot (&d2, &d1);
    if (dot < 0 || dot > l) return BIG;
    return sqrt(V3SquaredLength(&d2) - dot * dot);
}

double DistanceToPolygon (Point3 *t, GeomPoly *p)
{
    int i1, i2;
    double d, d1;
    Ray r;

    r.origin = *t;
    r.direction = p->normal;
    if (RayPolygonIntersection(&r, p, &d))
	return d;
    d = BIG;
    for (i1 = 0; i1 < p->nverts; i1++) {
	i2 = i1 ? i1 - 1 : p->nverts - 1;
	
	d1 = DistanceToLine(p->verts + i1, p->verts + i2, t);
	if (d1 < d)
	    d = d1;
	d1 = V3DistanceBetween2Points(p->verts + i1, t);
	if (d1 < d)
	    d = d1;
    }
    return d;
}

double PolygonViewingAngle (Point3 *t, GeomPoly *p)
{
    double s = 0;
    int i1, i2;

    for (i1 = 1; i1 < p->nverts - 1; i1++) {
	int i2 = i1 + 1;
	Point3 v1, v2, v3, d;
	double sign, k, a, b, c, z;

	V3Sub(p->verts, p->verts + i1, &v1);
	V3Sub(p->verts, p->verts + i2, &v2);
	sign = V3Dot(V3Cross(&v1, &v2, &d), &p->normal);
	if (fabs(sign) < EPS) continue;
	V3Normalize(V3Sub(t, p->verts, &v1));
	V3Normalize(V3Sub(t, p->verts + i1, &v2));
	V3Normalize(V3Sub(t, p->verts + i2, &v3));
	a = acos(V3Dot(&v2, &v3)) * 0.5;
	b = acos(V3Dot(&v3, &v1)) * 0.5;
	c = acos(V3Dot(&v1, &v2)) * 0.5;
	k = (a + b + c) * 0.5;
	z = atan(sqrt(tan(k - a) * tan(k - b) * tan(k - c) * tan(k))) * 4;
	if (sign > 0)
	    s += z;
	else
	    s -= z;
    }
    return fabs(s);
}

void smooth_triangle (Point3 *v[3], Point3 *h[3], Point3 *section, Raystruct *r)
{
    int axis,m0,m1,m2;
    double x,y,p[3],q[3],c0,c1,c2;
    double p0,p1,p2,p3,p4,d;

    axis=find_axis (&r->normal);
    CalcAxis (section,&x,&y,axis);
    CalcAxis (v[0],&p[0],&q[0],axis);
    CalcAxis (v[1],&p[1],&q[1],axis);
    CalcAxis (v[2],&p[2],&q[2],axis);
    m0=0; m1=1; m2=2;
    if (q[0]>q[1]) m1=0, m0=1;
    if (q[m1]>q[m2]) axis=m2, m2=m1, m1=axis;
    if (q[m0]>q[m1]) axis=m0, m0=m1, m1=axis;
    if (y>q[m1]) axis=m0, m0=m2, m2=axis;

    /* m[i] are now monotously ordering q, 
       and y is between q[m0] and q[m1] */

    d=q[m1]-q[m0];
    if (d==0) {
	p0=p[m1];
	p1=p[m0];
	p2=0;
	p3=1;
    }
    else {
	p0=((q[m1]-y)*p[m0]+(y-q[m0])*p[m1])/d;
	p1=((q[m2]-y)*p[m0]+(y-q[m0])*p[m2])/(q[m2]-q[m0]);
	p2=(y-q[m0])/(q[m2]-q[m0]);
	p3=(y-q[m0])/d;
    }

    if (p1==p0)
	p4=1;
    else
	p4=(p0-x)/(p0-p1);
    c2=p4*p2;
    c1=(1-p4)*p3;
    c0=1-c1-c2;
    r->pat_n.x=h[m0]->x*c0+h[m1]->x*c1+h[m2]->x*c2;
    r->pat_n.y=h[m0]->y*c0+h[m1]->y*c1+h[m2]->y*c2;
    r->pat_n.z=h[m0]->z*c0+h[m1]->z*c1+h[m2]->z*c2;
}
