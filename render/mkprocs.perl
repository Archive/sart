#!/opt/tools/bin/perl

$status = 0;
$procs = 0;
$materials = 0;
$volumes = 0;
while (<>) {
    if ($ARGV ne $oldargv) {
	rename($ARGV, $ARGV . '~');
	open(ARGVOUT, ">$ARGV");
	select(ARGVOUT);
	$oldargv = $ARGV;
    }

    if (/__PERL__/ .. /__END__/) {
	$curr=$#lines;
    } else {
	@lines=(@lines, $_);
    }
    if (/define_material\(([^,]+),([^,]+),([^,]+),([^,]+),([^,]+),([^,]+),([^,]+),([^,]+)\)/) {
	@materials = (@materials,
		      "    $2 = scm_make_subr($3,scm_tc7_subr_2,$1);\n",
		      "    scm_make_gsubr($5,$6,$7,$8,$4);\n");
	$materials++;
	next;
    }
    if (/define_volume\(([^,]+),([^,]+),([^,]+),([^,]+),([^,]+),([^,]+),([^,]+),([^,]+)\)/) {
	@volumes = (@volumes,
		    "    $2 = scm_make_subr($3,scm_tc7_lsubr_2,$1);\n",
		    "    scm_make_gsubr($5,$6,$7,$8,$4);\n");
	$volumes++;
	next;
    }
    if (/SUBR \(([a-z0-9_]*)\)/) {
	$status=1;
	$type=$1;
	next;
    }	
    if ($status==1 && /^char\s+(s_[a-z0-9_]*)/) {
	$status=2;
	$sname=$1;
	next;
    }
    if ($status==2 && /^SCM\s*([a-z0-9_]*)/) {
	$status=0;
	%tdict=(%tdict, $type, 0);
	@tlist=(@tlist, $type);
	@slist=(@slist, $sname);
	@flist=(@flist, $1);
	$procs++;
    }
}
print STDERR "Total subrs detected: $procs\n";
print @lines[0..$curr],"    /* __PERL__ This code is automatically generated. Don't modify. */\n\n";
foreach $i (keys(%tdict)) {
    print "    static scm_iproc ${i}s[] = {\n";
    foreach $j (0 .. $#tlist) {
	($tlist[$j] eq $i) && print "        {$slist[$j], $flist[$j]},\n";
    }
    print "        {0,0}\n    };\n\n";
}

foreach $i (keys(%tdict)) {
    $j=$i;
    $j =~ s/([0-9])/_$1/;
    print "    scm_init_iprocs (${i}s, scm_tc7_$j);\n";
}
if ($materials) {
    print STDERR "Total materials detected: $materials\n";
    print "\n";
    print @materials;
}
if ($volumes) {
    print STDERR "Total volumes detected: $volumes\n";
    print "\n";
    print @volumes;
}
print "\n    /* __END__ */\n",@lines[$curr+1..$#lines];
