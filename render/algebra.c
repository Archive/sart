
/****h* (render)/algebra.c [0.1]
 * NAME
 *  algebra.c -- basic linear algebra and related routines
 * COPYRIGHT
 *   Miroslav Silovic
 * MODIFICATION HISTORY
 *  
 * NOTES
 *   The currently missing features are:
 *
 *    tensor (tensor product of the two vectors)
 *    hsv <-> rgb converter - actually hsv needs a heavy overhaul
 *    hsv color palette (change its treatment in liner-spline)
 *    spectral color - there are many functions, but none are integrated
 *
 * BUGS
 *   Currently some routines allocate memory without calling
 *   DISABLE_INTERRUPTS. This is a possible core leak.
 *
 *******
*/

#include "bsp.h"

char s_badarg_err[]="bad argument";
extern long scm_tc16_array;

/*----------------------------------------------------------------------
  Simple vector functions.
*/

/****** make_dvect
 * NAME
 *  make_dvect
 * SYNOPSIS
 *  SCM make_dvect (int n, double *e)
 * FUNCTION
 *  Creates Scheme vector out of double array
 * INPUTS
 *  int n      - number of elements
 *  double *e  - vector of doubles
 * RESULT
 *  SCM vector - uniform vector of doubles
 *******
 */

SCM make_dvect (int n, double *e)
{
    SCM c;

    SCM_NEWCELL(c);
    SCM_DEFER_INTS;
    SCM_SETCHARS(c,e);
    SCM_SETLENGTH(c,n,scm_tc7_dvect);
    SCM_ALLOW_INTS;
    return c;
}

/****** make_dvect_lsubr
 * NAME
 *  make_dvect_lsubr
 * SYNOPSIS
 *  SCM make_dvect_lsubr (SCM args)
 *  dvector (lsubr)
 * FUNCTION
 *  Scheme procedure. From a list of arguments (doubles), creates
 *  a vector. Use 'scm_apply' if the arguments are in the stored list.
 * INPUTS
 *  args     - list (as provided by Scheme evaluator)
 * RESULT
 *  uniform vector
 *******
 */

/* SUBR (lsubr) */
char s_make_dvector[]="dvector";
SCM make_dvect_lsubr (SCM args)
{
    double *newvect;
    int n,i;

    n=scm_ilength(args);
    SCM_ASSERT(n>0,args,SCM_WNA,s_make_dvector);
    newvect=MNEWARRAY(double,n);
    for (i=0; i<n; i++,args=SCM_CDR(args)) {
	SCM d=SCM_CAR(args);
	SCM_ASRTGO(SCM_NIMP(d) && SCM_REALP(d),badarg);
	newvect[i]=SCM_REALPART(d);
    }
    return make_dvect(n,newvect);

  badarg:
    free(newvect);
    SCMERROR(SCM_CAR(args),s_badarg_err,s_make_dvector);
    return SCM_BOOL_F;
}

/****** vplus_lsubr
 * NAME
 *  vplus_lsubr
 * SYNOPSIS
 *  SCM vplus_lsubr (SCM args)
 *  v+ (lsubr)
 * FUNCTION
 *  Scheme procedure. Add a list of dvectors.
 *  Use 'scm_apply' if the arguments are in the stored list.
 * INPUTS
 *  args     - list (as provided by Scheme evaluator)
 * RESULT
 *  vector of doubles (a sum of args)
 *******
 */

/* SUBR (lsubr) */
char s_vplus[]="v+";
SCM vplus_lsubr (SCM args)
{
    double *newvect;
    int n=-1;
    int i;
    SCM v;

    while (SCM_NNULLP(args)) {
	double *e;

	v=SCM_CAR(args);
	SCM_ASRTGO(SCM_TYP7(v)==scm_tc7_dvect,badarg);
	e=(double*)SCM_VELTS(v);
	SCM_ASRTGO((n<0 && SCM_LENGTH(v)>0) || (n>0 && SCM_LENGTH(v)==n),badarg);
	if (n<0) {
	    newvect=MNEWARRAY(double,(n=SCM_LENGTH(v)));
	    for (i=0; i<n; i++)
		newvect[i]=e[i];
	}
	else
	    for (i=0; i<n; i++)
		newvect[i]+=e[i];
	args=SCM_CDR(args);
    }

    SCM_ASSERT(n>0,args,SCM_WNA,s_vplus);
    return make_dvect(n,newvect);

  badarg: 
    if (n>0) free(newvect);
    SCMERROR(v,s_badarg_err,s_vplus);
    return SCM_BOOL_F;
}

/****** vminus_lsubr
 * NAME
 *  vminus_lsubr
 * SYNOPSIS
 *  SCM vminus_lsubr (SCM args)
 *  v- (lsubr)
 * FUNCTION
 *  Scheme procedure. Substract a list of dvectors from the first vector
 *  in the list. Use 'scm_apply' if the arguments are in the stored list.
 * INPUTS
 *  args     - list (as provided by Scheme evaluator)
 * RESULT
 *  vector of doubles (v1 - v2 - v3 - ...)
 *******
 */

/* SUBR (lsubr) */
char s_vminus[]="v-";
SCM vminus_lsubr (SCM args)
{
    double *newvect;
    int n=-1;
    int i;
    SCM v;

    while (SCM_NNULLP(args)) {
	double *e;

	v=SCM_CAR(args);
	SCM_ASRTGO(SCM_TYP7(v)==scm_tc7_dvect,badarg);
	e=(double*)SCM_VELTS(v);
	SCM_ASRTGO((n<0 && SCM_LENGTH(v)>0) || (n>0 && SCM_LENGTH(v)==n),badarg);
	if (n<0) {
	    newvect=MNEWARRAY(double,(n=SCM_LENGTH(v)));
	    for (i=0; i<n; i++)
		newvect[i]=e[i];
	}
	else
	    for (i=0; i<n; i++)
		newvect[i]-=e[i];
	args=SCM_CDR(args);
    }

    SCM_ASSERT(n>0,args,SCM_WNA,s_vminus);
    return make_dvect(n,newvect);

  badarg: 
    if (n>0) free(newvect);
    SCMERROR(v,s_badarg_err,s_vminus);
    return SCM_BOOL_F;
}

/****** vdot_subr2
 * NAME
 *  vdot_subr2
 * SYNOPSIS
 *  SCM vdot_subr2 (SCM v1, SCM v2)
 *  vdot (subr2)
 * FUNCTION
 *  Scheme procedure. Scalar product of two vectors.
 * INPUTS
 *  v1, v2   - two dvectors
 * RESULT
 *  a SCM containing a double - scalar product of the vectors
 *******
 */

/* SUBR (subr2) */
char s_vdot[]="vdot";
SCM vdot_subr2 (SCM v1, SCM v2)
{
    int i;
    double s;

    SCM_ASSERT(SCM_TYP7(v1)==scm_tc7_dvect,v1,SCM_ARG1,s_vdot);
    SCM_ASSERT(SCM_TYP7(v2)==scm_tc7_dvect,v2,SCM_ARG2,s_vdot);
    SCM_ASSERT(SCM_LENGTH(v2)==SCM_LENGTH(v1),v2,SCM_ARG2,s_vdot);
    for (s=0,i=0; i<SCM_LENGTH(v1); i++)
	s+=((double*)SCM_VELTS(v1))[i]*((double*)SCM_VELTS(v2))[i];
    return scm_makdbl(s,0.0);
}

/****** vnorm_subr1
 * NAME
 *  vnorm_subr1
 * SYNOPSIS
 *  SCM vnorm_subr1 (SCM v)
 *  vnorm (subr1)
 * FUNCTION
 *  Scheme procedure. Returns norm of the vector
 * INPUTS
 *  v      - a dvector
 * RESULT
 *  a SCM double - norm of the vector
 *******
 */

/* SUBR (subr1) */
char s_vnorm[]="vnorm";
SCM vnorm_subr1 (SCM v)
{
    int i;
    double s;

    SCM_ASSERT(SCM_TYP7(v)==scm_tc7_dvect,v,SCM_ARG1,s_vnorm);
    for (s=0,i=0; i<SCM_LENGTH(v); i++) {
	double d=((double*)SCM_VELTS(v))[i];
	s+=d*d;
    }
    return scm_makdbl(sqrt(s),0.0);
}


/****** vdist_lsubr
 * NAME
 *  vdist_lsubr
 * SYNOPSIS
 *  SCM vdist_lsubr (SCM l)
 *  vdistance (lsubr)
 * FUNCTION
 *  Scheme procedure. Takes a list of arguments, and returns the distance
 *  between the two furthest vectors in the list.
 *  Use 'scm_apply' if the arguments are in the stored list.
 * INPUTS
 *  args     - list (as provided by Scheme evaluator)
 * RESULT
 *  a SCM double as described
 *******
 */

/* SUBR (lsubr) */
char s_vdist[]="vdistance";
SCM vdist_lsubr (SCM l)
{
    int i,n;
    double d;
    SCM l1,p;

    d=0;
    l1=l;
    SCM_ASSERT(SCM_NNULLP(l),l,SCM_WNA,s_vnorm);
    p=SCM_CAR(l1);
    SCM_ASSERT(SCM_TYP7(p)==scm_tc7_dvect,p,SCM_ARG1,s_vnorm);
    n=SCM_LENGTH(p);
    l1=SCM_CDR(l1);
    while (SCM_NNULLP(l1)) {
	p=SCM_CAR(l1);
	l1=SCM_CDR(l1);
	SCM_ASSERT(SCM_TYP7(p)==scm_tc7_dvect && SCM_LENGTH(p)==n,p,SCM_ARG1,s_vnorm);
    }
    while (SCM_NNULLP(l)) {
	double *v1;
	p=SCM_CAR(l);
	v1=(double*)SCM_VELTS(p);
	l=l1=SCM_CDR(l);
	while (SCM_NNULLP(l1)) {
	    double s,e;
	    double *v2=(double*)SCM_VELTS(SCM_CAR(l1));
	    for (s=0.0,i=0; i<n; i++) {
		e=v2[i]-v1[i];
		s+=e*e;
	    }
	    if (d<s) d=s;
	    l1=SCM_CDR(l1);
	}
    }
    return scm_makdbl(sqrt(d),0.0);
}

/****** vcross_subr2
 * NAME
 *  vcross_subr2
 * SYNOPSIS
 *  SCM vcross_subr2 (SCM v1, SCM v2)
 *  vcross (subr2)
 * FUNCTION
 *  Scheme procedure. Cross product between the two vectors
 * INPUTS
 *  v1, v2
 * RESULT
 *  vector of doubles (a sum of args)
 *******
 */

/* SUBR (subr2) */
char s_vcross[]="vcross";
SCM vcross_subr2 (SCM v1, SCM v2)
{
    double *new,*a,*b;

    SCM_ASSERT(SCM_TYP7(v1)==scm_tc7_dvect,v1,SCM_ARG1,s_vcross);
    SCM_ASSERT(SCM_LENGTH(v1)>=3,v1,SCM_ARG1,s_vcross);
    SCM_ASSERT(SCM_TYP7(v2)==scm_tc7_dvect,v2,SCM_ARG2,s_vcross);
    SCM_ASSERT(SCM_LENGTH(v2)>=3,v2,SCM_ARG2,s_vcross);
    new=MNEWARRAY(double,3);
    a=(double*)SCM_VELTS(v1);
    b=(double*)SCM_VELTS(v2);
    new[0]=a[1]*b[2]-a[2]*b[1];
    new[1]=a[2]*b[0]-a[0]*b[2];
    new[2]=a[0]*b[1]-a[1]*b[0];
    return make_dvect(3,new);
}

/****** vscale_subr2
 * NAME
 *  vscale_subr2
 * SYNOPSIS
 *  SCM vscale_subr2 (SCM v1, SCM v2)
 *  vscale (subr2)
 * FUNCTION
 *  Scheme procedure. Multiply a vector with a scalar
 * INPUTS
 *  v1     - SCM double
 *  v2     - dvector
 * RESULT
 *  dvector v1*v2
 *******
 */

/* SUBR (subr2) */
char s_vscale[]="vscale";
SCM vscale_subr2 (SCM v1, SCM v2)
{
    int i;
    double d;
    double *e;

    SCM_ASSERT(SCM_NIMP(v1) && SCM_REALP(v1),v1,SCM_ARG1,s_vscale);
    SCM_ASSERT(SCM_TYP7(v2)==scm_tc7_dvect,v2,SCM_ARG2,s_vscale);
    d=SCM_REALPART(v1);
    e=MNEWARRAY(double,SCM_LENGTH(v2));
    for (i=0; i<SCM_LENGTH(v2); i++)
	e[i]=((double*)SCM_VELTS(v2))[i]*d;
    return make_dvect(SCM_LENGTH(v2),e);
}

/*----------------------------------------------------------------------
  Matrix functions
*/

/* Matrix is represented as rank 2 array of doubles. */

extern char s_make_dmatrix[];
extern SCM scm_shap2ra (SCM args , char *what);

/****** make_dmatrix
 * NAME
 *  make_dmatrix
 * SYNOPSIS
 *  SCM make_dmatrix (int m, int n, double *e)
 * FUNCTION
 *  Creates Scheme matrix out of double array
 * INPUTS
 *  int m,n    - dimensions of the matrix
 *  double *e  - vector of doubles
 * RESULT
 *  SCM array  - uniform m x n array of doubles
 *******
 */

SCM make_dmatrix (int m, int n, double *e)
{
    SCM shape=scm_cons(SCM_MAKINUM(m),scm_cons(SCM_MAKINUM(n),SCM_EOL));
    SCM ra;
    scm_array_dim *s;

    ra=scm_shap2ra(shape, s_make_dmatrix);
    s=SCM_ARRAY_DIMS(ra);
    SCM_ARRAY_V(ra)=make_dvect(m*n,e);
    s[0].inc=n;
    s[1].inc=1;
    return ra;
}

/****** make_dmatrix_lsubr2
 * NAME
 *  make_dmatrix_lsubr2
 * SYNOPSIS
 *  SCM make_dmatrix_lsubr2 (SCM m1, SCM n1, SCM r)
 *  dmatrix (lsubr2)
 * FUNCTION
 *  Scheme procedure. From dimensions and a list of doubles, creates
 *  a matrix. Use 'scm_apply' if the arguments are in the stored list.
 * INPUTS
 *  m1, n1   - uni
 *  args     - list (as provided by Scheme evaluator)
 * RESULT
 *  uniform array formed from the args
 *******
 */

/* SUBR (lsubr2) */
char s_make_dmatrix[]="dmatrix";
SCM make_dmatrix_lsubr2 (SCM m1, SCM n1, SCM r)
{
    int m,n,k;
    int i;
    double *f,*e=NULL;

    k=scm_ilength(r);
    SCM_ASSERT(SCM_INUMP(m1),m1,SCM_ARG1,s_make_dmatrix);
    SCM_ASSERT(SCM_INUMP(n1),n1,SCM_ARG2,s_make_dmatrix);
    m=SCM_INUM(m1);
    n=SCM_INUM(n1);
    SCM_ASSERT(m*n==k,r,SCM_WNA,s_make_dmatrix);
    f=e=MNEWARRAY(double,k);
    for (i=0; i<k; i++) {
	SCM_ASRTGO(SCM_NIMP(SCM_CAR(r)) && SCM_REALP(SCM_CAR(r)),badarg);
	*(f++) = SCM_REALPART(SCM_CAR(r));
	r=SCM_CDR(r);
    }
    return make_dmatrix(m,n,e);

  badarg:
    if (e) free(e);
    SCMERROR(SCM_CAR(r),s_badarg_err,s_make_dmatrix);
    return SCM_BOOL_F;
}

/****** get_vector_dims
 * NAME
 *  get_vector_dims
 * SYNOPSIS
 *  int get_vector_dims (SCM mat, int *m)
 * FUNCTION
 *  This is an argument parsing function. It will check for a Scheme
 *  vector, either as uniform array or a standard uniform vector.
 *  (vectors of doubles are required)
 * INPUTS
 *  SCM mat    - unknown data
 *  int *m     - return pointer (see RESULT)
 * RESULT
 *  if mat is indeed a vector, function returns 1 and m contains its size.
 *  otherwise, it will return 0.
 * SEE ALSO
 *  get_dims, make_dvect
 ******
 */

int get_vector_dims (SCM mat, int *m)
{
    if (SCM_TYP7(mat) == scm_tc7_dvect) {
	*m = SCM_LENGTH(mat);
	return 1;
    }
    else if (SCM_ARRAYP(mat) && SCM_ARRAY_NDIM(mat) == 1
	     && SCM_ARRAY_DIMS(mat)[0].lbnd == 0
	     && SCM_ARRAY_DIMS(mat)[0].ubnd >= 0) {
	*m = SCM_ARRAY_DIMS(mat)[0].ubnd + 1;
	return 1;
    }
    return 0;
}
      
/****** get_dims
 * NAME
 *  get_dims
 * SYNOPSIS
 *  int get_dims (SCM mat, int *m, int *n)
 * FUNCTION
 *  This is an argument parsing function. It will check for a Scheme
 *  2-dimensional matrix of doubles.
 * INPUTS
 *  SCM mat    - unknown data
 *  int *m, *n - return pointer (see RESULT)
 * RESULT
 *  if mat is indeed a vector, function returns 1 and m,n contain its size.
 *  otherwise, it will return 0.
 * SEE ALSO
 *  get_vector_dims, make_dmatrix
 ******
 */

int get_dims (SCM mat, int *m, int *n)
{
    if (SCM_ARRAYP(mat) && SCM_ARRAY_NDIM(mat)==2 &&
	SCM_ARRAY_DIMS(mat)[0].lbnd==0 && SCM_ARRAY_DIMS(mat)[0].ubnd>=0 &&
	SCM_ARRAY_DIMS(mat)[1].lbnd==0 && SCM_ARRAY_DIMS(mat)[1].ubnd>=0) {

	*m=SCM_ARRAY_DIMS(mat)[0].ubnd+1;
	*n=SCM_ARRAY_DIMS(mat)[1].ubnd+1;
	return 1;
    }
    return 0;
}

/****** matplus_lsubr
 * NAME
 *  matplus_lsubr
 * SYNOPSIS
 *  SCM matplus_lsubr (SCM r)
 *  m+ (lsubr)
 * FUNCTION
 *  Scheme procedure. Add a list of dmatrices.
 *  Use 'scm_apply' if the arguments are in the stored list.
 * INPUTS
 *  r     - list (as provided by Scheme evaluator)
 * RESULT
 *  uniform array of doubles.
 *******
 */

/* SUBR (lsubr) */
char s_mplus[]="m+";
SCM matplus_lsubr (SCM r)
{
    int m=-1,n;
    int m1, n1;
    double *e;
    int i;

    while(!SCM_NULLP(r)) {
	SCM mat=SCM_CAR(r);

	if (!get_dims(mat, &m1, &n1)) goto badarg;
	if (m<0) {
	    m=m1;
	    n=n1;
	    e=MNEWARRAY(double,m*n);
	    for (i=0; i<m*n; i++)
		e[i]=((double*)SCM_VELTS(SCM_ARRAY_V(mat)))[i];
	}
	else {
	    SCM_ASRTGO(m==m1 && n==n1,badarg);
	    for (i=0; i<m*n; i++)
		e[i]+=((double*)SCM_VELTS(SCM_ARRAY_V(mat)))[i];
	}
	r=SCM_CDR(r);
    }

    SCM_ASSERT(m>0,r,SCM_WNA,s_mplus);
    return make_dmatrix(m,n,e);

  badarg:
    if (m>0) free(e);
    SCMERROR(SCM_CAR(r),s_badarg_err,s_mplus);
    return SCM_BOOL_F;
}

/****** matminus_lsubr
 * NAME
 *  matminus_lsubr
 * SYNOPSIS
 *  SCM matminus_lsubr (SCM r)
 *  m- (lsubr)
 * FUNCTION
 *  Scheme procedure. Substract a list of matrices from the first matrix
 *  in the list. Use 'scm_apply' if the arguments are in the stored list.
 * INPUTS
 *  args     - list (as provided by Scheme evaluator)
 * RESULT
 *  array of doubles (m1 - m2 - m3 - ...)
 *******
 */

/* SUBR (lsubr) */
char s_mminus[]="m-";
SCM matminus_lsubr (SCM r)
{
    int m=-1,n;
    int m1, n1;
    double *e;
    int i;

    while(!SCM_NULLP(r)) {
	SCM mat=SCM_CAR(r);

	if (!get_dims(mat, &m1, &n1)) goto badarg;
	if (m<0) {
	    m=m1;
	    n=n1;
	    e=MNEWARRAY(double,m*n);
	    for (i=0; i<m*n; i++)
		e[i]=((double*)SCM_VELTS(SCM_ARRAY_V(mat)))[i];
	}
	else {
	    SCM_ASRTGO(m==m1 && n==n1,badarg);
	    for (i=0; i<m*n; i++)
		e[i]-=((double*)SCM_VELTS(SCM_ARRAY_V(mat)))[i];
	}
	r=SCM_CDR(r);
    }

    SCM_ASSERT(m>0,r,SCM_WNA,s_mminus);
    return make_dmatrix(m,n,e);

  badarg:
    if (m>0) free(e);
    SCMERROR(SCM_CAR(r),s_badarg_err,s_mminus);
    return SCM_BOOL_F;
}

/****** matmult_lsubr
 * NAME
 *  matmult_lsubr
 * SYNOPSIS
 *  SCM matmult_lsubr (SCM r)
 *  m* (lsubr)
 * FUNCTION
 *  Scheme procedure. Multiply together a list, which may consist of
 *  matrices or vectors. Dimensions of adjacent list members must be
 *  compatible, and vectors are treated as column matrices. Furthermore,
 *  vector may have one element fewer than its transformation matrix.
 *  In that case, it will be expanded with 1.0 (for homogenous matrix
 *  calculation).
 * INPUTS
 *  args     - list (as provided by Scheme evaluator)
 * RESULT
 *  array of doubles as described.
 *******
 */

/* SUBR (lsubr) */
char s_mmult[]="m*";
SCM matmult_lsubr (SCM r)
{
    int m=-1,n;
    int m1, n1;
    double *e, *in, *out;
    int i;

    while(!SCM_NULLP(r)) {
	SCM mat=SCM_CAR(r);

	r=SCM_CDR(r);
	if (SCM_ARRAYP(mat) && SCM_ARRAY_NDIM(mat)==2 &&
	    SCM_ARRAY_DIMS(mat)[0].lbnd==0 && SCM_ARRAY_DIMS(mat)[0].ubnd>=0 &&
	    SCM_ARRAY_DIMS(mat)[1].lbnd==0 && SCM_ARRAY_DIMS(mat)[1].ubnd>=0) {

	    m1=SCM_ARRAY_DIMS(mat)[0].ubnd+1;
	    n1=SCM_ARRAY_DIMS(mat)[1].ubnd+1;
	    in=(double*)SCM_VELTS(SCM_ARRAY_V(mat));
	}
	else if (get_vector_dims(mat, &m1)) {
	    if (SCM_ARRAYP(mat))
	      mat=SCM_ARRAY_V(mat);
	    m1=SCM_LENGTH(mat);
	    n1=1;
	    in=(double*)SCM_VELTS(mat);
	}
	else goto badarg;

	if (m<0) {
	    m=m1;
	    n=n1;
	    e=(SCM_NULLP(r)) ? MNEWARRAY(double, m*n) : NEWARRAY(double,m*n);
	    for (i=0; i<m*n; i++)
		e[i]=in[i];
	}
	else {
	    int k,j;
	    double s;

	    SCM_ASRTGO(n==m1 || (n==m1+1 && n1==1),badarg);
	    out=(SCM_NULLP(r)) ? MNEWARRAY(double, m*n1) : NEWARRAY(double, m*n1);
	    if (n!=m1) {
		for (i=0; i<m; i++) {
		    for (s=0.0, k=0; k<m1; k++)
		      s+=e[i*n+k]*in[k];
		    s+=e[i*n+k];
		    out[i]=s;
		}
		m--;
		if (out[m]!=1.0)
		    for (i=0; i<m; i++)
			out[i]/=out[m];
		out=(double*)scm_must_realloc((char*)out, (m+1)*sizeof(double),
					  m*sizeof(double), "sart");
	    }
	    else
	      for (i=0; i<m; i++)
		for (j=0; j<n1; j++) {
		    for (s=0.0,k=0; k<n; k++)
		      s+=e[i*n+k]*in[k*n1+j];
		    out[i*n1+j]=s;
		}
	    free(e);
	    e=out;
	    n=n1;
	}
    }

    SCM_ASSERT(m>0,r,SCM_WNA,s_mmult);
    if (n==1) return make_dvect (m,e);
    else return make_dmatrix(m,n,e);

  badarg:
    if (m>0) free(e);
    SCMERROR(SCM_CAR(r),s_badarg_err,s_mmult);
    return SCM_BOOL_F;
}

/****** mattransp_subr1
 * NAME
 *  mattransp_subr1
 * SYNOPSIS
 *  SCM mattransp_subr1 (SCM mat)
 *  mtranspose (subr1)
 * FUNCTION
 *  Scheme procedure. Returns newly allocated transposed matrix
 * INPUTS
 *  mat     - a matrix
 * RESULT
 *  a matrix of doubles
 *******
 */

/* SUBR (subr1) */
char s_mtranspose[]="mtranspose";
SCM mattransp_subr1 (SCM mat)
{
    int m,n;
    double *e,*in;
    int i,j;

    if (get_dims(mat, &m, &n))
	in=(double*)SCM_VELTS(SCM_ARRAY_V(mat));
    else if (SCM_TYP7(mat)==scm_tc7_dvect && SCM_LENGTH(mat)>0) {
	m=SCM_LENGTH(mat);
	n=1;
	in=(double*)SCM_VELTS(mat);
    } else goto badarg;

    e=MNEWARRAY(double,m*n);
    for (i=0; i<m; i++)
      for(j=0; j<n; j++)
	e[j*m+i]=in[i*n+j];

    if (m==1) return make_dvect (n,e);
    else return make_dmatrix(n,m,e);

  badarg:
    SCMERROR(mat,s_badarg_err,s_mtranspose);
    return SCM_BOOL_F;
}

/****** matscale_subr2
 * NAME
 *  matscale_subr2
 * SYNOPSIS
 *  SCM matscale_subr2 (SCM d, SCM mat)
 *  mscale (subr2)
 * FUNCTION
 *  Scheme procedure. Multiply a matrix with a scalar
 * INPUTS
 *  d      - SCM double
 *  mat    - dmatrix
 * RESULT
 *  dmatrix d*mat
 *******
 */

/* SUBR (subr2) */
char s_mscalar[]="mscale";
SCM matscal_subr2 (SCM d, SCM mat)
{
    int m,n;
    double *e,*in,f;
    int i;

    if (get_dims(mat, &m, &n))
	in=(double*)SCM_VELTS(SCM_ARRAY_V(mat));
    else if (SCM_TYP7(mat)==scm_tc7_dvect && SCM_LENGTH(mat)>0) {
	m=SCM_LENGTH(mat);
	n=1;
	in=(double*)SCM_VELTS(mat);
    } else goto badarg;

    SCM_ASSERT (SCM_NIMP(d) && SCM_REALP(d),d,SCM_ARG1,s_mscalar);
    if (!get_dims(mat, &m, &n))
	SCMERROR(mat,SCM_ARG2,s_mscalar);
    f=SCM_REALPART(d);

    e=MNEWARRAY(double,m*n);
    for (i=0; i<m*n; i++)
	e[i]=in[i]*f;

    return make_dmatrix(m,n,e);
 badarg:
    SCMERROR(mat,s_badarg_err,s_mtranspose);
    return SCM_BOOL_F;
}

/*----------------------------------------------------------------------
  Color functions
*/

#define C_TYPE(x) SCM_TYP16(x)
#define C_SCM_LENGTH(x) SCM_LENGTH(SCM_CDR(x))
#define C_ELTS(x) ((double*)SCM_VELTS(SCM_CDR(x)))

/****** ColorSMOB
 * NAME
 *  tc16_RGB, tc16_HSV
 * SYNOPSIS
 *  extern long tc16_RGB, tc16_HSV;
 * FUNCTION
 *  Color SMOBs are basically type tag and a pointer to the vector of doubles
 *  (not a Scheme vector, except in the case of spectral smobs)
 *
 *  When combining the colors, the default conversion is:
 *
 *                      HSV  -> RGB -> SPC.
 * NOTES
 *
 *  Ideas on Spectral color:
 *
 *    -- ?? -- Maybe we should use an uniform subdivision, so we don't have
 *    monochromatics? (and filtering would be easier, but everything would
 *    be slower)
 *
 *    Spectral color is actually list of pairs, wavelength/amplitude, with
 *    linear interpolation between - this means that the specter is really
 *    continuous. Please avoid spectral calculation when possible, they
 *    are /not/ efficient.
 *
 *    Spectral conversion is currently supposed to be done through
 *    global variables; I don't have an idea about the value they're
 *    supposed to have.
 *
 * SOURCE
 */

int RGB_print (SCM rgb, SCM port, scm_print_state *writing)
{
    double *d=(double*)SCM_VELTS(rgb);
    char buf[256];

    sprintf(buf,"#<RGB %f %f %f>",d[0],d[1],d[2]);
    scm_puts(buf, port);
    return 1;
}

int HSV_print (SCM hsv, SCM port, scm_print_state *writing)
{
    double *d=(double*)SCM_VELTS(hsv);
    char buf[256];

    sprintf(buf,"#<RGB %f %f %f>",d[0],d[1],d[2]);
    scm_puts(buf, port);
    return 1;
}

int Channels_print (SCM chan, SCM port, scm_print_state *writing)
{
    SCM list = SCM_CDR(chan);

    scm_puts("#<Channels" , port);
    while (SCM_NIMP(SCM_CAR(list))) {
	scm_write(SCM_CAR(list), port);
	list = SCM_CDR(list);
    }
}

scm_sizet freecol (SCM c)
{
    free((void*)SCM_CDR(c));
    return 3*sizeof (double);
}

long tc16_RGB;
scm_smobfuns RGB_smob = {scm_mark0, freecol, RGB_print, 0};
long tc16_HSV;
scm_smobfuns HSV_smob = {scm_mark0, freecol, HSV_print, 0};
long tc16_Channels;
scm_smobfuns Channels_smob = {scm_markcdr, scm_free0, Channels_print, 0};

/* spectral conversion global tables */

extern SCM spec2r, spec2g, spec2b;
extern SCM r2spec, g2spec, b2spec;

/*
long tc16_Spec;
scm_smobfuns Spec_smob = {scm_markcdr, free0, 0, 0};
long tc16_CIE;
scm_smobfuns CIE_smob ={scm_markcdr, free0, 0, 0};
*/

/*********/

/****** match_spectrums
 * NAME
 *  match_spectrums
 * SYNOPSIS
 *  void match_spectrums (double *c1, int n1, double *c2, int n2,
 *                        double *d1, double *d2, int *len_out)
 * FUNCTION
 *  This is the basic utility for spectral colors. It takes two vectors
 *  and creates another couple, by merging the lists with wavelengths
 *  sorted. The amplitutes will be linearly interpolated.
 *  Equal wavelengths will be properly optimized.
 * INPUTS
 *  double *c1   - pointer to the first spectral specification
 *  int n1       - the size of the vector c1
 *  c2, n2       - same for the second vector
 *  d1, d2       - pointers to preallocated arrays for output
 *  int *len_out - pointer for the size of the output vectors
 * RESULT
 *  The new spectrums are stored into d1 and d2. The final size goes to
 *  len_out.
 * BUGS
 *  This routine is completely untested and unintegrated, like the rest
 *  of the spectral color system.
 ********
 */

void match_spectrums (double *c1, int n1, double *c2, int n2,
		      double *d1, double *d2, int *len_out)
{
    /* e1 and e2 must be premalloced, and released after use */

    int i,j,n;
    double seg, v1, v2;

    while (i<n1 && j<n2) {
	if (d1[i]==d2[j]) {
	    seg=c1[i];
	    v1=c1[i+1];
	    v2=c2[j+1];
	    i+=2;
	    j+=2;
	}
	else if (c1[i]<c2[j]) {
	    seg=c1[i];
	    v1=c1[i+1];
	    v2=j ? (c2[j-1]*c1[i+2]+c2[j+1]*c1[i])/(c1[i]+c1[i+2]) : 0;
	    i+=2;
	} else {
	    seg=c2[i];
	    v2=c2[j+1];
	    v1=i ? (c1[i-1]*c2[j+2]+c1[i+1]*c2[j])/(c2[j]+c2[j+2]) : 0;
	    j+=2;
	}
	d1[n]=d2[n]=seg;
	d1[n+1]=v1;
	d2[n+1]=v2;
	n+=2;
    }
    while (i<n1) {
	d1[n]=d2[n]=c1[i];
	d1[n+1]=c1[i+1];
	d2[n+1]=0;
	n+=2;
	i+=2;
    }
    while (j<n2) {
	d1[n]=d2[n]=c2[j];
	d2[n+1]=c2[j+1];
	d1[n+1]=0;
	n+=2;
	j+=2;
    }
    *len_out=n;
}

/****** spectral_integral
 * NAME
 *  spectral_integral
 * SYNOPSIS
 *  double spectral_integral (double *d1, int n1, double *d2, int n2)
 * FUNCTION
 *  This returns a scalar product of the spectrums. It will be
 *  \int_0^\infinity{\phi_1(x)\phi_2(x)\;dx}, where spectums
 *  are considered to be functions of wavelength.
 * INPUTS
 *  double *d1   - pointer to the first spectral specification
 *  int n1       - the size of the vector d1
 *  d2, n2       - same for the second vector
 * RESULT
 *  double       - the value of the integral
 * BUGS
 *  This routine is completely untested and unintegrated, like the rest
 *  of the spectral color system.
 * SEE ALSO
 *  match_spectrums
 ********
 */

double spectral_integral (double *d1, int n1, double *d2, int n2)
{
    int n,i;
    double s,*e1, *e2;

    /* We'll use Simpson integration, for the correct result */

    e1=MNEWARRAY(double,n1+n2);
    e2=MNEWARRAY(double,n1+n2);
    match_spectrums (d1,n1,d2,n2,e1,e2,&n);
    s=0.0;
    for (i=0; i<n-2; i+=2)
	s+=(e1[i+1]*e2[i+1]+e1[i+3]*e2[i+3]+
	    (e1[i+1]+e1[i+3])*(e2[i+1]+e2[i+3]))/
	    (e1[i+2]-e1[i]);
    free (e2);
    free (e1);
    return s*(800-450)/6;
}

/****** spectral_filter
 * NAME
 *  spectral_filter
 * SYNOPSIS
 *  void spectral_filter (double *d1, int n1, double *d2, int n2, double *d3,
 *                        int *nn)
 * FUNCTION
 *  This calculates product of two spectums, taken as functions of the
 *  wavelength. The result is approximate (since it will not be a
 *  linear spline).
 * INPUTS
 *  double *d1   - pointer to the first spectral specification
 *  int n1       - the size of the vector d1
 *  d2, n2       - same for the second vector
 *  d3, int *nn  - holders for the resulting vector
 * RESULT
 *  d3 will contain the new values, and nn the result size.
 * BUGS
 *  This routine is completely untested and unintegrated, like the rest
 *  of the spectral color system.
 * SEE ALSO
 *  match_spectrums
 ********
 */

void spectral_filter (double *d1, int n1, double *d2, int n2, double *d3,
		      int *nn)
/* d3 must be malloced to 3*(n1+n2); the result is a good approximation */
{
    int i,n, k;
    double *e1, *e2;
    
    e1=MNEWARRAY(double, n1+n2);
    e2=MNEWARRAY(double, n1+n2);
    match_spectrums (d1,n1,d2,n2,e1,e2,&n);
    k=0;
    for (i=0; i<n-2; i+=2) {
	double u1,u2,v1,v2,h1,h2;

	u1=e1[i+1];
	v1=e1[i+3];
	u2=e2[i+1];
	v2=e2[i+3];
	h1=e1[i];
	h2=e1[i+2];
	if (n<16) {
	    d3[k++]=u1*u2;
	    d3[k++]=h1;
	    d3[k++]=(0.67*u1+0.33*v1)*(0.67*u2+0.33*v2);
	    d3[k++]=0.67*h1+0.33*h2;
	    d3[k++]=(0.33*u1+0.67*v1)*(0.33*u2+0.67*v2);
	    d3[k++]=0.33*h1+0.67*h2;
	    d3[k++]=v1*v2;
	    d3[k++]=h2;
	} else if (n<32) {
	    d3[k++]=u1*u2;
	    d3[k++]=h1;
	    d3[k++]=(u1+v1)*(u2+v2)/4;
	    d3[k++]=(h1+h2)/2;
	    d3[k++]=v1*v2;
	    d3[k++]=h2;
	}
	else {
	    d3[k++]=u1*u2;
	    d3[k++]=h1;
	    d3[k++]=v1*v2;
	    d3[k++]=h2;
	}	    
    }
    *nn=k;
}
	
/****** hsv2rgb
 * NAME
 *  hsv2rgb
 * SYNOPSIS
 *  void hsv2rgb (double h, double s, double v, double *r,
 *                double *g, double *b)
 * FUNCTION
 *  Convert hsv color to rgb
 * INPUTS
 *  h,s,v  - doubles, with the hsv color
 *  *r, *g, *b - result holders.
 * BUGS
 *  This routine is presently untested.
 **********
 */

void hsv2rgb (double h, double s, double v, double *r, double *g, double *b)
{
    h=h-360*floor(h/360);

    if (h<60) *r=60, *g=h, *b=0;
    else if (h<120) *r=120-h, *g=60, *b=0;
    else if (h<180) *r=0, *g=60, *b=h-120;
    else if (h<240) *r=0, *g=240-h, *b=60;
    else if (h<300) *r=h-240, *g=0, *b=60;
    else *r=60, *g=0, *b=360-h;

    *r=v*(*r/60*s+1-s);
    *g=v*(*g/60*s+1-s);
    *b=v*(*b/60*s+1-s);
}

/* scheme support starts here */

/****** colorp
 * NAME
 *  colorp
 * SYNOPSIS
 *  SCM colorp (SCM c)
 *  color? (subr1)
 * FUNCTION
 *  Scheme predicate for color SMOBs.
 * INPUTS
 *******
 */

/* SUBR (subr1) */
char s_colorp[] = "color?";
SCM colorp (SCM c)
{
    return (SCM_NIMP(c) && (SCM_TYP16(c)==tc16_RGB || SCM_TYP16(c)==tc16_HSV
			    || SCM_TYP16(c) == tc16_Channels))
	? SCM_BOOL_T : SCM_BOOL_F;
}

/****** make_v3
 * NAME
 *  make_v3
 * SYNOPSIS
 *  SCM make_v3 (long type,double c1, double c2, double c3)
 * FUNCTION
 *  Generic constructor for 3 element SCM vectors / SMOBs.
 * INPUTS
 *  type               - the type of the smob
 *  double c1, c2, c3  - the component values
 ********
 */

SCM make_v3 (long type,double c1, double c2, double c3)
{
    double *p=MNEWARRAY(double,3);
    SCM c;

    p[0]=c1;
    p[1]=c2;
    p[2]=c3;
    SCM_NEWCELL(c);
    SCM_CDR(c)=(SCM)p;
    SCM_CAR(c)=type;
    return c;
}

/****** combine_colors
 * NAME
 *  combine_colors
 * SYNOPSIS
 *  SCM combine_colors (SCM c1, SCM c2, double t1, double t2)
 * FUNCTION
 *  Linear interpolation between two colors. Used for palettes.
 * INPUTS
 *  SCM c1, c2    - the colors
 *  double t1, t2 - the multipliers
 * RESULT
 *  returns c1*t1+c2*t2
 *******
 */

/* used in color palette's calls in mathutils */
SCM combine_colors (SCM c1, SCM c2, double t1, double t2)
{
    static double zero[3]={0,0,0};
    double *d1, *d2;
    double r1,g1,b1,r2,g2,b2;

    if (c1==SCM_BOOL_F)
	d1=zero;
    else
	d1=(double*)SCM_VELTS(c1);
    if (SCM_NIMP(c1) && SCM_TYP16(c1) == tc16_RGB)
	r1=d1[0],g1=d1[1],b1=d1[2];
    else
	hsv2rgb(d1[0],d1[1],d1[2],&r1,&g1,&b1);
    if (c2==SCM_BOOL_F)
	d2=zero;
    else
	d2=(double*)SCM_VELTS(c2);
    if (SCM_NIMP(c2) && SCM_TYP16(c2) == tc16_RGB)
	r2=d2[0],g2=d2[1],b2=d2[2];
    else
	hsv2rgb(d2[0],d2[1],d2[2],&r2,&g2,&b2);
    return make_v3(tc16_RGB,t1*r1+t2*r2,t1*g1+t2*g2,t1*b1+t2*b2);
}

/****** rgb_subr3,hsv_subr3
 * NAME
 *  rgb_subr3, hsv_subr3
 * SYNOPSIS
 *  SCM rgb_subr3 (SCM r, SCM g, SCM b)
 *  SCM hsv_subr3 (SCM r, SCM g, SCM b)
 *  rgb (subr3)
 *  hsv (subr3)
 * FUNCTION
 *  Basic color smob constructors
 * INPUTS
 *  Three SCM doubles, for each component
 * RESULT
 *  The constructed color
 ********
 */


/* SUBR (subr3) */
char s_rgb[]="rgb";
SCM rgb_subr3 (SCM r, SCM g, SCM b)
{
    SCM_ASSERT(!SCM_INUMP(r) && SCM_REALP(r),r,SCM_ARG1,s_rgb);
    SCM_ASSERT(!SCM_INUMP(g) && SCM_REALP(g),g,SCM_ARG2,s_rgb);
    SCM_ASSERT(!SCM_INUMP(b) && SCM_REALP(b),b,SCM_ARG3,s_rgb);

    return make_v3 (tc16_RGB,SCM_REALPART(r),SCM_REALPART(g),SCM_REALPART(b));
}

/* SUBR (subr3) */
char s_hsv[]="hsv";
SCM hsv_subr3 (SCM r, SCM g, SCM b)
{
    SCM_ASSERT(!SCM_INUMP(r) && SCM_REALP(r),r,SCM_ARG1,s_rgb);
    SCM_ASSERT(!SCM_INUMP(g) && SCM_REALP(g),g,SCM_ARG2,s_rgb);
    SCM_ASSERT(!SCM_INUMP(b) && SCM_REALP(b),b,SCM_ARG3,s_rgb);

    return make_v3 (tc16_HSV,SCM_REALPART(r),SCM_REALPART(g),SCM_REALPART(b));
}

/****** cplus_subr2,cfilter_subr2,cscale_subr2,cdistance_lsubr
 * NAME
 *  cplus_subr2, cfilter_subr2, cscale_subr2, cdistance_lsubr
 * SYNOPSIS
 *  SCM cscale_subr2 (SCM c1, SCM r)
 *  SCM cdistance_lsubr (SCM l)
 *  SCM cfilter_subr2 (SCM c1, SCM c2)
 *  SCM cplus_subr2 (SCM c1, SCM c2)
 *  c+ (subr2)
 *  cfilter (subr2)
 *  cdistance (lsubr)
 *  cscale (subr2)
 * FUNCTION
 *  cplus returns the sum of the two colors.
 *  cfilter returns a new color, whose components are products of the 
 *   arguments.
 *  cscale multiplies color by scalar
 *  cdistance finds the perceptive distance between the two furthest
 *   colors in the argument list.
 * INPUTS
 *  c+ and cfilter take two colors.
 *  cdistance takes at least two arguments. Each is a color
 *  cscale takes one SCM double and one color.
 *******
 */

/* SUBR (subr2) */
char s_cplus[] = "c+";
SCM cplus_subr2 (SCM c1, SCM c2)
{
    double *d1, *d2;
    double r1,g1,b1,r2,g2,b2;

    if (c1==SCM_BOOL_F) return c2;
    if (c2==SCM_BOOL_F) return c1;
    SCM_ASSERT(colorp(c1)==SCM_BOOL_T,c1,SCM_ARG1,s_cplus);
    SCM_ASSERT(colorp(c2)==SCM_BOOL_T,c2,SCM_ARG2,s_cplus);
    d1=(double*)SCM_VELTS(c1);
    d2=(double*)SCM_VELTS(c2);
    if (SCM_TYP16(c1)==tc16_RGB)
	r1=d1[0],g1=d1[1],b1=d1[2];
    else
	hsv2rgb(d1[0],d1[1],d1[2],&r1,&g1,&b1);
    if (SCM_TYP16(c2)==tc16_RGB)
	r2=d2[0],g2=d2[1],b2=d2[2];
    else
	hsv2rgb(d2[0],d2[1],d2[2],&r2,&g2,&b2);
    return make_v3(tc16_RGB,r1+r2,g1+g2,b1+b2);
}

/* SUBR (subr2) */
char s_cfilter[] = "cfilter";
SCM cfilter_subr2 (SCM c1, SCM c2)
{
    double *d1, *d2;
    double r1,g1,b1,r2,g2,b2;

    if (c1==SCM_BOOL_F) return c2;
    if (c2==SCM_BOOL_F) return c1;
    SCM_ASSERT(colorp(c1)==SCM_BOOL_T,c1,SCM_ARG1,s_cfilter);
    SCM_ASSERT(colorp(c2)==SCM_BOOL_T,c2,SCM_ARG2,s_cfilter);
    d1=(double*)SCM_VELTS(c1);
    d2=(double*)SCM_VELTS(c2);
    if (SCM_TYP16(c1)==tc16_RGB)
	r1=d1[0],g1=d1[1],b1=d1[2];
    else
	hsv2rgb(d1[0],d1[1],d1[2],&r1,&g1,&b1);
    if (SCM_TYP16(c2)==tc16_RGB)
	r2=d2[0],g2=d2[1],b2=d2[2];
    else
	hsv2rgb(d2[0],d2[1],d2[2],&r2,&g2,&b2);
    return make_v3(tc16_RGB,r1*r2,g1*g2,b1*b2);
}


extern double sq (double x);

double DistanceBetween2Cols (double r1, double g1, double b1, double r2, double g2, double b2)
{
   return 0.45*fabs(r1-r2)+0.40*fabs(g1-g2)+0.15*fabs(b1-b2);
}

/* SUBR (lsubr) */
char s_cdistance[] = "cdistance";
SCM cdistance_lsubr (SCM l)
{
    static double zeros[3]={0,0,0};
    double *d1, *d2, *mat;
    double r1,g1,b1,r2,g2,b2;
    SCM l1,p;
    double d;
    int s1, s2;
    scm_array_dim *dims;

    d=0;
    l1=l;
    SCM_ASSERT(SCM_NNULLP(l),l,SCM_WNA,s_cdistance);
    p=SCM_CAR(l1);
    if (SCM_ARRAYP(p)) goto do_special_distance;
    SCM_ASSERT(colorp(p),p,SCM_ARG1,s_cdistance);
    l1=SCM_CDR(l1);
    while (SCM_NNULLP(l1)) {
	p=SCM_CAR(l1);
	l1=SCM_CDR(l1);
	SCM_ASSERT(colorp(p),p,SCM_ARG1,s_cdistance);
    }
    while (SCM_NNULLP(l)) {
	p=SCM_CAR(l);
	d1=SCM_IMP(p) ? zeros : (double*)SCM_CDR(p);
	if (SCM_IMP(p) || SCM_TYP16(p)==tc16_RGB)
	    r1=d1[0],g1=d1[1],b1=d1[2];
	else
	    hsv2rgb(d1[0],d1[1],d1[2],&r1,&g1,&b1);
	l=l1=SCM_CDR(l);
	while (SCM_NNULLP(l1)) {
	    double s;
	    p=SCM_CAR(l1);
	    d2=SCM_IMP(p) ? zeros : (double*)SCM_CDR(p);
	    if (SCM_IMP(p) || SCM_TYP16(p)==tc16_RGB)
	        r2=d2[0],g2=d2[1],b2=d2[2];
	    else
	        hsv2rgb(d2[0],d2[1],d2[2],&r2,&g2,&b2);
	    s=DistanceBetween2Cols(r1,g1,b1,r2,g2,b2);
	    if (d<s) d=s;
	    l1=SCM_CDR(l1);
	}
    }
    return scm_makdbl(d,0.0);

do_special_distance:
    SCM_ASSERT(SCM_ARRAY_NDIM(p)==3, p, SCM_ARG1, s_cdistance);
    dims=SCM_ARRAY_DIMS(p);
    s1=dims[0].inc;
    s2=dims[1].inc;
    mat=(double*)SCM_VELTS(SCM_ARRAY_V(p))+SCM_ARRAY_BASE(p);
    l1=l=SCM_CDR(l);
    while (SCM_NNULLP(l1)) {
	p=SCM_CAR(l1);
	l1=SCM_CDR(l1);
	SCM_ASSERT(SCM_CONSP(p) && SCM_INUMP(SCM_CAR(p)) && SCM_INUMP(SCM_CDR(p)),p,SCM_ARG1,s_cdistance);
    }
    while (SCM_NNULLP(l)) {
	int i1, i2;

	p=SCM_CAR(l);
	i1=SCM_INUM(SCM_CAR(p))*s1+SCM_INUM(SCM_CDR(p))*s2;
	r1=mat[i1]; g1=mat[i1+1]; b1=mat[i1+2];
	l=l1=SCM_CDR(l);
	while (SCM_NNULLP(l1)) {
	    double s;
	    p=SCM_CAR(l1);
	    i2=SCM_INUM(SCM_CAR(p))*s1+SCM_INUM(SCM_CDR(p))*s2;
	    r2=mat[i2]; g2=mat[i2+1]; b2=mat[i2+2];
	    s=DistanceBetween2Cols(r1,g1,b1,r2,g2,b2);
	    if (d<s) d=s;
	    l1=SCM_CDR(l1);
	}
    }
    return scm_makdbl(d,0.0);
}

/* SUBR (subr2) */
char s_cscale[] = "cscale";
SCM cscale_subr2 (SCM c1, SCM r)
{
    double *d1;
    double r1,g1,b1,sc;

    if (c1==SCM_BOOL_F) return c1;
    SCM_ASSERT(colorp(c1)==SCM_BOOL_T,c1,SCM_ARG1,s_cscale);
    SCM_ASSERT(SCM_NIMP(r) && SCM_REALP(r),r,SCM_ARG2,s_cscale);
    d1=(double*)SCM_VELTS(c1);
    if (SCM_TYP16(c1)==tc16_RGB)
	r1=d1[0],g1=d1[1],b1=d1[2];
    else
	hsv2rgb(d1[0],d1[1],d1[2],&r1,&g1,&b1);
    sc=SCM_REALPART(r);
    return make_v3(tc16_RGB,r1*sc,g1*sc,b1*sc);
}

/****** rgb_vect
 * NAME
 *   rgb_vect
 * SYNOPSIS
 *   SCM rgb_vect (SCM c)
 *   rgb->vector (SUBR1)
 * FUNCTION
 *   Convert RGB color to a 3 element vector of doubles.
 ******/

/* SUBR (subr1) */
char s_rgb_vect[] = "rgb->vector";
SCM rgb_vect (SCM c)
{
    double *d, *e;

    SCM_ASSERT(SCM_NIMP(c) && SCM_TYP16(c) == tc16_RGB, c, SCM_ARG1, s_rgb_vect);
    d = (double*) SCM_VELTS(c);
    e = MNEWARRAY(double, 3);
    memcpy(e, d, 3*sizeof(double));
    return make_dvect(3, e);
}

/****** bilin_interp
 * NAME
 *  bilin_interp
 * SYNOPSIS
 *  SCM bilin_interp (SCM map)
 *  array-interpolate! (subr1)
 * FUNCTION
 *  Bilin-interp takes a m x n x k array. It will be interpreted as m x n
 *  array with k color components. The requirements is that vertice colors
 *  must be set. The inside of the array will be bilinearily interpolated
 *  by this function.
 * INPUTS
 *  map - the array
 * RESULT
 *  It will return map. Some of its elements will be overwritten.
 * BUGS
 *  colors are expected to be contiguous (but other components aren't).
 ******
 */

/* SUBR (subr1) */
char s_bilin_interp[] = "array-interpolate!";
SCM bilin_interp (SCM map)
{
    int m1, n1, ncols, colinc, k1, l1, i, j, k;
    scm_array_dim *mapdims;
    double *d;
    SCM v1;

    if (SCM_ARRAYP(map) && SCM_ARRAY_NDIM(map)==3 &&
	(mapdims=SCM_ARRAY_DIMS(map))[0].lbnd==0 && mapdims[0].ubnd>=0 &&
	mapdims[1].lbnd==0 && mapdims[1].ubnd>=0 &&
	mapdims[2].lbnd==0 && mapdims[2].ubnd>=0) {
	m1=mapdims[0].ubnd+1;
	n1=mapdims[1].ubnd+1;
	ncols=mapdims[2].ubnd+1;
	k1=mapdims[0].inc;
	l1=mapdims[1].inc;
	colinc=mapdims[2].inc;
    }
    else
      SCMERROR (map, SCM_ARG1, s_bilin_interp);

    v1=SCM_ARRAY_V(map);
    SCM_ASSERT(SCM_TYP7(v1)==scm_tc7_dvect, map, SCM_ARG1, s_bilin_interp);
    d=(double*) SCM_VELTS(v1)+SCM_ARRAY_BASE(map);

    for (k=0; k<ncols; k++) {
	double c1,c2,c3,c4;
	double *e=d+k;

	c1=e[0];
	c2=e[(m1-1)*k1];
	c3=e[(n1-1)*l1];
	c4=e[(m1-1)*k1+(n1-1)*l1];

	for (j=0; j<n1; j++)
	  for (i=0; i<m1; i++) {
	      double x,y;

	      x=m1==1 ? 0.0 : (double)i/(m1-1);
	      y=n1==1 ? 0.0 : (double)j/(n1-1);
	      e[j*l1+i*k1]=(1-y)*((1-x)*c1+x*c2)+y*((1-x)*c3+x*c4);
	  }
    }
    return map;
}

/****** get_bilin
 * NAME
 *   get_bilin
 * SYNOPSIS
 *   SCM get_bilin (SCM ra, SCM xval, SCM yval)
 *   get_pixel_interpolate (subr3)
 * FUNCTION
 *   Get a pixel from array ra - 2 or 3 dimensional array of
 *   characters or doubles. It will bilinearily interpolate between
 *   pixel values. It will For 2-dimesional array, it will return a
 *   double. For 3-dimensional, it will return the color. xval and
 *   yval are doubles.
 ******/

/* SUBR (subr3) */
char s_get_bilin[]="get-pixel-interpolate";
SCM get_bilin (SCM ra, SCM xval, SCM yval)
{
    int x, y;
    double xoffs, yoffs, x1, y1;
    int nd, index, index_x, index_y, index_xy, delta, lower, upper, inc, xinc;
    int type;
    scm_array_dim *dims;

    SCM_ASSERT(SCM_NIMP(ra)
	       && SCM_ARRAYP(ra)
	       && ((type = SCM_TYP7(SCM_ARRAY_V(ra))) == scm_tc7_dvect
		   || type == scm_tc7_string)
	       && ((nd = SCM_ARRAY_NDIM(ra)) == 2
		   || nd == 3),
	ra, SCM_ARG1, s_get_bilin);
    SCM_ASSERT (SCM_NIMP(xval) && SCM_REALP(xval),
		xval, SCM_ARG2, s_get_bilin);
    SCM_ASSERT (SCM_NIMP(yval) && SCM_REALP(yval),
		yval, SCM_ARG3, s_get_bilin);

    xoffs = SCM_REALPART(xval);
    yoffs = SCM_REALPART(yval);

    index = SCM_ARRAY_BASE(ra);
    dims = SCM_ARRAY_DIMS(ra);
    
    if (nd == 3) {
	SCM_ASSERT(dims[2].lbnd == 0 && dims[2].ubnd == 2,
		   ra, SCM_ARG1, s_get_bilin);
    }
    lower = dims[0].lbnd;
    upper = dims[0].ubnd + 1;
    xoffs *= upper;
    x = x1 = floor(xoffs);
    xoffs -= x1;
    inc = dims[0].inc;
    delta = upper - lower;
    x -= lower;
    if (x < 0) x = delta - x;
    x = x % delta;
    index += x * inc;
    if (x != delta - 1)
	xinc = inc;
    else
	xinc = (1 - delta) * inc;

    lower = dims[1].lbnd;
    upper = dims[1].ubnd + 1;
    yoffs *= upper;
    y = y1 = floor(yoffs);
    yoffs -= y1;
    inc = dims[1].inc;
    delta = upper - lower;
    y -= lower;
    if (y < 0) y = delta - y;
    y = y % delta;
    index += y * inc;
    if (y != delta - 1)
	index_y = index + inc;
    else
	index_y = index + (1 - delta) * inc;
    index_x = index + xinc;
    index_xy = index_y + xinc;

    /* at this point, the square is delimited by index, index_x, index_y
       and index_xy. Time to select the type */

    if (type == scm_tc7_string) {
	unsigned char *chars = (unsigned char*)SCM_VELTS(SCM_ARRAY_V(ra));
	if (nd == 2)
	    return scm_makdbl(((1 - yoffs) * ((1 - xoffs) * chars[index] +
					      xoffs * chars[index_x]) +
			       yoffs * ((1 - xoffs) * chars[index_y] +
					xoffs * chars[index_xy])) / 256,
			      0.0);
	else {
	    int i;
	    double d[3];
	    for (i = 0; i < 3; i++) {
		d[i] = ((1 - yoffs) * ((1 - xoffs) * chars[index] +
				       xoffs * chars[index_x]) +
			yoffs * ((1 - xoffs) * chars[index_y] +
				 xoffs * chars[index_xy])) / 256;
		chars += dims[2].inc;
	    }
	    return make_v3(tc16_RGB, d[0], d[1], d[2]);
	}
    }
    else { /* type == scm_tc7_dvect */
	double *vals = (double*)SCM_VELTS(SCM_ARRAY_V(ra));
	if (nd == 2)
	    return scm_makdbl((1 - yoffs) * ((1 - xoffs) * vals[index] +
					     xoffs * vals[index_x]) +
			      yoffs * ((1 - xoffs) * vals[index_y] +
				       xoffs * vals[index_xy]), 0.0);
	else {
	    int i;
	    double d[3];
	    for (i = 0; i < 3; i++) {
		d[i] =
		    (1 - yoffs) * ((1 - xoffs) * vals[index] +
				   xoffs * vals[index_x]) +
		    yoffs * ((1 - xoffs) * vals[index_y] +
			     xoffs * vals[index_xy]);
		vals += dims[2].inc;
	    }
	    return make_v3(tc16_RGB, d[0], d[1], d[2]);
	}
    }
}

/* *** SUBROUTINES for square iterate */

double vect_colordist (double *d1, double *d2)
{
    return DistanceBetween2Cols (d1[0],d1[1],d1[2],d2[0],d2[1],d2[2]);
}

void get_point_col (double *d, SCM *flags, int k1, int l1, SCM func,
		    int x, int y, int k2, int l2)
{
    SCM col;
    double r1, g1, b1, *d1;

    if (flags[k2*x+l2*y]==SCM_BOOL_T)
	return;
    flags[k2*x+l2*y]=SCM_BOOL_T;
    col=scm_apply (func, SCM_MAKINUM(x), scm_cons(SCM_MAKINUM(y), scm_listofnull));
    SCM_ASSERT (SCM_CONSP(col),col,
	    "Returned value is not the right type","square-iterate!");
    col = SCM_CAR(col);
    SCM_ASSERT (colorp(col)==SCM_BOOL_T,col,
	    "Returned value is not the right type","square-iterate!");
    d1=(double*)SCM_VELTS(col);
    if (SCM_TYP16(col)==tc16_RGB)
      r1=d1[0],g1=d1[1],b1=d1[2];
    else
      hsv2rgb(d1[0],d1[1],d1[2],&r1,&g1,&b1);
    d[k1*x+l1*y]=r1;
    d[k1*x+l1*y+1]=g1;
    d[k1*x+l1*y+2]=b1;
}

void subdivide_square (double *d, SCM *flags, int k1, int l1, int k2, int l2, int x, int y, int m, int n, SCM func, double limit, int ncols)
{
    int k,i,j;
    int mhalf, nhalf;
    double *p[4];
    double s,dist;
    double c1,c2,c3,c4;
    double *e;

    /* Check if we are only a single pixel */

    get_point_col (d, flags, k1, l1, func, x, y, k2, l2);

    if (m==0 && n==0) {
	return;
    }

    get_point_col (d, flags, k1, l1, func, x+m, y  , k2, l2);
    get_point_col (d, flags, k1, l1, func, x  , y+n, k2, l2);
    get_point_col (d, flags, k1, l1, func, x+m, y+n, k2, l2);
    
    if (m<2 && n<2) {
	return; /* All the pixels have been calculated */
    }

    dist=0.0;
    p[0]=d+k1*x    +l1*y;
    p[1]=d+k1*(x+m)+l1*y;
    p[2]=d+k1*x    +l1*(y+n);
    p[3]=d+k1*(x+m)+l1*(y+n);
    for (i=0; i<3; i++)
	for (j=i+1; j<4; j++) {
	    s=vect_colordist(p[i],p[j]);
	    if (s > dist) dist=s;
	    if (dist > limit) goto done;
	}

done: 
    if (dist > limit) {

	/* do the subdivision */

	mhalf=m/2;
	nhalf=n/2;

	subdivide_square (d,flags,k1,l1,k2,l2, x, y, 
			  mhalf, nhalf,func,limit, ncols);
	if (mhalf < m)
	    subdivide_square (d,flags,k1,l1,k2,l2, x+mhalf+1, y, 
			      m-mhalf-1, nhalf,func,limit, ncols);
	if (nhalf < n) {
	    subdivide_square (d,flags,k1,l1,k2,l2, x, y+nhalf+1, 
			      mhalf, n-nhalf-1,func,limit, ncols);
	    if (mhalf < m)
		subdivide_square (d,flags,k1,l1,k2,l2, x+mhalf+1, y+nhalf+1, 
				  m-mhalf-1, n-nhalf-1,func,limit, ncols);
	}
	return;
    }

    for (k=0; k<ncols; k++) {

	e=d+k;
	c1=e[x*k1+y*l1];
	c2=e[(x+m)*k1+y*l1];
	c3=e[x*k1+(y+n)*l1];
	c4=e[(x+m)*k1+(y+n)*l1];

	for (j=y; j<=y+n; j++)
	  for (i=x; i<=x+m; i++) {
	      double p,q;

	      p=m==0 ? 0.0 : (double)(i-x)/m;
	      q=n==0 ? 0.0 : (double)(j-y)/n;
	      e[i*k1+j*l1]=(1-q)*((1-p)*c1+p*c2)+q*((1-p)*c3+p*c4);
	  }
    }
}   

/****** square_iter
 * NAME
 *  square_iter
 * SYNOPSIS
 *  SCM square_iter (SCM func, SCM limit, SCM map, SCM flags, SCM z,
 *                   SCM objects, SCM materials)
 *  square-iterate! (gsubr)
 * FUNCTION
 *  This function iterates a provided function over subdivision of the
 *  square. The function is expected to return a color, and the components
 *  are stored. Subdivision ends when color distance falls below the limit
 * INPUTS
 *  func     - takes two immediate integers. Must return color.
 *  limit    - double; specifies recursion limit (the maximal difference
 *             that still permits the interpolation
 *  map      - m x n x 3 array of doubles
 *  flags    - m x n array of integer. Integers are flags,
 *             specifying whether that pixel has been evaluated already
 *             or just obtained by interpolation.
 *  z        - m x n array of doubles - zbuffer outout
 *  objects  - object output (m x n array of SCMs)
 *  materials - the materials output
 * RESULT
 *  maps are changed internally.
 * BUGS
 *  SCM_CAR maps array is assumed to be contiguous in its third index (although
 *  the increment is actually read).
 ********
 */

char s_square_iter[] = "square-iterate!";
SCM square_iter (SCM func, SCM limit, SCM map, SCM flags,
		 SCM z, SCM objects, SCM materials)
{ 
    int m1, n1, ncols, colinc, k1, l1;
    int m2, n2, k2, l2;
    scm_array_dim *mapdims;
    double *d;
    SCM *fl;
    SCM v1, v2;
    
    SCM_ASSERT (scm_procedure_p(func)==SCM_BOOL_T, func, SCM_ARG2, s_square_iter);
    SCM_ASSERT (SCM_NIMP(limit) && SCM_REALP(limit), limit, SCM_ARG3, s_square_iter);
    /*    fprintf (stderr, "%d %d %d %d\n", SCM_ARRAYP(map), SCM_ARRAY_NDIM(map), SCM_ARRAY_DIMS(map)[0].lbnd); */
    if (SCM_ARRAYP(map) && SCM_ARRAY_NDIM(map)==3 &&
	(mapdims=SCM_ARRAY_DIMS(map))[0].lbnd==0 && mapdims[0].ubnd>=0 &&
	mapdims[1].lbnd==0 && mapdims[1].ubnd>=0 &&
	mapdims[2].lbnd==0 && mapdims[2].ubnd>=0) {
	m1=mapdims[0].ubnd+1;
	n1=mapdims[1].ubnd+1;
	ncols=mapdims[2].ubnd+1;
	k1=mapdims[0].inc;
	l1=mapdims[1].inc;
	colinc=mapdims[2].inc;
    }
    else
      SCMERROR (map, SCM_ARG1, s_square_iter);

    if (SCM_ARRAYP(flags) && SCM_ARRAY_NDIM(flags)==2 &&
	(mapdims=SCM_ARRAY_DIMS(flags))[0].lbnd==0 && mapdims[0].ubnd>=0 &&
	mapdims[1].lbnd==0 && mapdims[1].ubnd>=0) {
	m2=mapdims[0].ubnd+1;
	n2=mapdims[1].ubnd+1;
	k2=mapdims[0].inc;
	l2=mapdims[1].inc;
    }
    else
      SCMERROR (flags, SCM_ARG1, s_square_iter);

    v1=SCM_ARRAY_V(map);
    SCM_ASSERT(SCM_TYP7(v1)==scm_tc7_dvect, map, SCM_ARG1, s_square_iter);
    v2=SCM_ARRAY_V(flags);
    SCM_ASSERT(SCM_TYP7(v2)==scm_tc7_vector && m1==m2 && n1==n2, flags, 
	   SCM_ARG1, s_square_iter);
    d=(double*)SCM_VELTS(v1)+SCM_ARRAY_BASE(map);
    fl=(SCM*)SCM_VELTS(v2)+SCM_ARRAY_BASE(flags);

    subdivide_square (d,fl,k1,l1,k2,l2,0,0, m1-1, n1-1, 
		      func, SCM_REALPART(limit), ncols);
    return map;
}

/****** clamp_cv
 * NAME
 *   clamp_cv
 * SYNOPSIS
 *   SCM clamp_cv (SCM invect)
 *   clamp-color-vector! (subr1)
 * FUNCTION
 *   Reduce the colors in the color vector to 0-1 interval. We try to be
 *   clever about it (i.e. preserve the hue).
 ******/

/* SUBR (subr1) */
char s_clamp_cv[]="clamp-color-vector!";
SCM clamp_cv (SCM invect)
{
    double *d, maxval;
    int step, length, i;
    scm_array_dim *s;

    SCM_ASSERT(SCM_NIMP(invect)
	       && (SCM_TYP7(invect) == scm_tc7_dvect
		   || (SCM_ARRAYP(invect)
		       && SCM_TYP7(SCM_ARRAY_V(invect)) == scm_tc7_dvect
		       && SCM_ARRAY_NDIM(invect) >= 1
		       && SCM_ARRAY_NDIM(invect) <= 2)),
	       invect, SCM_ARG1, s_clamp_cv);
    if (SCM_TYP7(invect) == scm_tc7_dvect) {
	d = (double*)SCM_VELTS(invect);
	length = SCM_LENGTH(invect);
	step = 1;
    } else {
	s = SCM_ARRAY_DIMS(invect);
	d = (double*)SCM_VELTS(SCM_ARRAY_V(invect))
	    + SCM_ARRAY_BASE(invect) + s[0].lbnd;
	length = s[0].ubnd - s[0].lbnd + 1;
	step = s[0].inc;
    }
    if (SCM_ARRAY_NDIM(invect) == 1) {
	for (i = 0; i < length; i++)
	    if (d[step * i] < 0)
		d[step * i] = 0;
	maxval = 0;
	for (i = 0; i < length; i++)
	    if (d[step * i] > maxval)
		maxval = d[step * i];
	if (maxval > 1)
	    for (i = 0; i < length; i++)
		d[step * i] /= maxval;
    } else {
	int j;
	int len2;

	d += s[1].lbnd;
	len2 = s[1].ubnd - s[1].lbnd + 1;
	for (j = 0; j < len2; j++) {
	    for (i = 0; i < length; i++)
		if (d[step * i] < 0)
		    d[step * i] = 0;
	    maxval = 0;
	    for (i = 0; i < length; i++)
		if (d[step * i] > maxval)
		    maxval = d[step * i];
	    if (maxval > 1)
		for (i = 0; i < length; i++)
		    d[step * i] /= maxval;
	    d += s[1].inc;
	}
    }
    return SCM_UNSPECIFIED;
}

/****** dvector_string
 * NAME
 *  dvector_string
 * SYNOPSIS
 *  SCM dvect_string (SCM invect, SCM length)
 *  dvector-string
 * FUNCTION
 *  Convert a dvector into a string of characters. You are allowed to
 *  convert just a part of the vector by specifying a lower length.
 *  Elements of the vector are clipped to [0.0, 1.0] interval, multiplied
 *  by 255.999 and truncated.
 * INPUTS
 *  invect - vector to be converted. It can be a uniform array, in which
 *           case it will be assumed to be contiguous, and no size checks
 *           will be performed
 *  length - the fixnum specifying the length of the output
 * RESULT
 *  a uniform vector of characters
 * SEE ALSO
 *  rgb_pixel
 * BUGS
 *  Specifying length greater than actual number of elements results in
 *  problems (SEGV included).
 ******
 */

/* SUBR (subr2) */
char s_dvect_string[]="dvector-string";
SCM dvect_string (SCM invect, SCM length)
{
    int n, l;
    char *v;
    double *d;
    SCM c, dvc;

    dvc=invect;
    if (SCM_ARRAYP(invect)) dvc=SCM_ARRAY_V(invect);
    SCM_ASSERT(SCM_TYP7(dvc)==scm_tc7_dvect, invect, SCM_ARG1, s_dvect_string);
    SCM_ASSERT(SCM_INUMP(length), length, SCM_ARG2, s_dvect_string);
    v=MNEWARRAY(char, l=SCM_INUM(length));
    d=(double*)SCM_VELTS(dvc); if (SCM_ARRAYP(invect)) d+=SCM_ARRAY_BASE(invect);
    for (n=0; n<l; n++) {
	if (d[n]<0) v[n]=0;
	else if (d[n]>1) v[n]=255;
	else v[n]=d[n]*255.999;
    }
    SCM_NEWCELL(c);
    SCM_DEFER_INTS;
    SCM_SETCHARS(c,v);
    SCM_SETLENGTH(c,l,scm_tc7_string);
    SCM_ALLOW_INTS;
    return c;
}

/****** convolve_lsubr
 * NAME
 *  convolve_lsubr
 * SYNOPSIS
 *  SCM convolve_lsubr (SCM data)
 *  convolve (lsubr)
 * FUNCTION
 *  This is convolution. It will mingle the rectangle starting at (x, y)
 *  of map with the filter. rectangle doesn't have to be contained in the
 *  map. map is a uniform array of m x n x k doubles - mn pixels, k color
 *  components. filter is uniform array of doubles. Result is plotted
 *  into a new array, as a color column. SCM_BOOL_T additional parameter means
 *  convolution will divide with total pixel weight.
 * INPUTS
 *  map   - array to be convolved. mnk array of doubles, not necessarily
 *          contiguous in any dimension. It will also work with chars
 *          instead of doubles.
 *  filter - filter array (2 dimensional array of doubles)
 *  x,y   - SCM doubles, specifying top leftmost corner of the area that
 *          will be covered by the filter. Filter doesn't have to be
 *          contained within the map
 *  output - dvector or 1-dim array of doubles, which will accept the
 *          result of the operation. It must have exactly k elements.
 *  divide_p - optional parameter. If it exists, it's value is neglected,
 *          and the elements of output will be divided by the sum of the
 *          numbers in filter that were used for the convolution (not
 *          necessarily all of the filter, because it can be disjoint with
 *          the map array.
 * RESULT
 *  array output is modified.
 ******
 */

/* SUBR (lsubr) */
char s_conv[]="convolve";
SCM convolve_lsubr (SCM data)
{
    int k, n1, n2, m1, m2, m3, ml2, nl2;
    SCM map;
    SCM filter;
    SCM x;
    SCM y;
    SCM v1, v2;
    int divide_p;
    SCM output;
    double *e, *cols, *out;
    unsigned char *cols_c, *out_c;
    int i,j,s, k1, k2, l1, l2, oinc, ncols, colinc;
    scm_array_dim *mapdims;

    k=scm_ilength(data);
    SCM_ASSERT(k>=5, data, SCM_WNA, s_conv);
    map=SCM_CAR(data); data=SCM_CDR(data);
    filter=SCM_CAR(data); data=SCM_CDR(data);
    x=SCM_CAR(data); data=SCM_CDR(data);
    y=SCM_CAR(data); data=SCM_CDR(data);
    output=SCM_CAR(data); data=SCM_CDR(data);
    divide_p=0; if (SCM_NNULLP(data)) divide_p=1;
    if (SCM_ARRAYP(map) && SCM_ARRAY_NDIM(map)==3 &&
	(mapdims=SCM_ARRAY_DIMS(map))[0].lbnd==0 && mapdims[0].ubnd>=0 &&
	mapdims[1].lbnd==0 && mapdims[1].ubnd>=0 &&
	mapdims[2].lbnd==0 && mapdims[2].ubnd>=0) {
	m1=mapdims[0].ubnd+1;
	n1=mapdims[1].ubnd+1;
	k1=mapdims[0].inc;
	l1=mapdims[1].inc;
	ncols=mapdims[2].ubnd+1;
	colinc=mapdims[2].inc;
    }
    else
      SCMERROR (map, SCM_ARG1, s_conv);

    if (SCM_ARRAYP(filter) && SCM_ARRAY_NDIM(filter)==2) {
	ml2 = SCM_ARRAY_DIMS(filter)[0].lbnd;
	nl2 = SCM_ARRAY_DIMS(filter)[1].lbnd;
	m2=SCM_ARRAY_DIMS(filter)[0].ubnd - ml2 + 1;
	n2=SCM_ARRAY_DIMS(filter)[1].ubnd - nl2 + 1;
    }
    else
	SCMERROR (filter, SCM_ARG2, s_conv);
    
    if (SCM_TYP7(output) == scm_tc7_dvect
	|| SCM_TYP7(output) == scm_tc7_string) {
	m3 = SCM_LENGTH(output);
    }
    else if (SCM_ARRAYP(output) && SCM_ARRAY_NDIM(output) == 1
	     && SCM_ARRAY_DIMS(output)[0].lbnd==0
	     && SCM_ARRAY_DIMS(output)[0].ubnd>=0) {
	m3=SCM_ARRAY_DIMS(output)[0].ubnd + 1;
    }
    else {
	SCMERROR (output, SCM_ARG5, s_conv);
    }

    if (SCM_ARRAYP(output)) {
	if (SCM_TYP7(SCM_ARRAY_V(output)) == scm_tc7_dvect)
	    out=(double*)SCM_VELTS(SCM_ARRAY_V(output)) + SCM_ARRAY_BASE(output);
	else
	    out_c = (unsigned char*)SCM_VELTS(SCM_ARRAY_V(output)) + SCM_ARRAY_BASE(output);
	oinc = SCM_ARRAY_DIMS(output)[0].inc;
    }
    else {
	if (SCM_TYP7(output) == scm_tc7_dvect)
	    out = (double*)SCM_VELTS(output);
	else
	    out_c = (unsigned char*)SCM_VELTS(output);
	oinc = 1;
    }

    SCM_ASSERT (m3==ncols, output, SCM_ARG5, s_conv);
    SCM_ASSERT (SCM_INUMP(x), x, SCM_ARG3, s_conv);
    SCM_ASSERT (SCM_INUMP(y), y, SCM_ARG4, s_conv);
    x=SCM_INUM(x);
    y=SCM_INUM(y);
    v1=SCM_ARRAY_V(map);
    v2=SCM_ARRAY_V(filter);
    SCM_ASSERT(SCM_TYP7(v1) == scm_tc7_dvect || SCM_TYP7(v1) == scm_tc7_string,
	       map, SCM_ARG1, s_conv);
    SCM_ASSERT(SCM_TYP7(v2) == scm_tc7_dvect, filter, SCM_ARG2, s_conv);
    if (SCM_TYP7(v1) == scm_tc7_dvect)
	cols=(double*)SCM_VELTS(v1)+SCM_ARRAY_BASE(map);
    else
	cols_c=(unsigned char*)SCM_VELTS(v1)+SCM_ARRAY_BASE(map);
    e=(double*)SCM_VELTS(v2)+SCM_ARRAY_BASE(filter);
    k2=SCM_ARRAY_DIMS(filter)[0].inc;
    l2=SCM_ARRAY_DIMS(filter)[1].inc;
    if (SCM_TYP7(v1) == scm_tc7_dvect) {
	for (s=0; s<ncols; s++) {
	    double totalcol=0;
	    double totalweight=0;
	
	    for (j = nl2; j < n2; j++)
		for (i = ml2; i < m2; i++) {
		    int xx=x+i, yy=y+j;
		    double f=e[i*k2+j*l2];
		    double c;
		
		    if (xx<0 || yy<0 || xx>=m1 || yy>=n1)
			c=0.0;
		    else {
			c=cols[xx*k1+yy*l1+s*colinc];
			totalweight+=f;
		    }
		    totalcol+=c*f;
		}
	    if (divide_p) totalcol=totalweight ? totalcol/totalweight : 0.0;
	    out[oinc*s]=totalcol;
	}
    }
    else {
	for (s=0; s<ncols; s++) {
	    double totalcol=0;
	    double totalweight=0;
	
	    for (j = nl2; j < n2; j++)
		for (i = ml2; i < m2; i++) {
		    int xx=x+i, yy=y+j;
		    double f=e[i*k2+j*l2];
		    double c;
		
		    if (xx<0 || yy<0 || xx>=m1 || yy>=n1)
			c=0.0;
		    else {
			c=cols_c[xx*k1+yy*l1+s*colinc];
			totalweight+=f;
		    }
		    totalcol+=c*f;
		}
	    if (divide_p) totalcol=totalweight ? totalcol/totalweight : 0.0;
	    out_c[oinc*s]=totalcol;
	}
    }
    return SCM_BOOL_F;
}

void init_algebra_c (void)
{
    /* __PERL__ This code is automatically generated. Don't modify. */

    static scm_iproc lsubrs[] = {
        {s_make_dvector, make_dvect_lsubr},
        {s_vplus, vplus_lsubr},
        {s_vminus, vminus_lsubr},
        {s_vdist, vdist_lsubr},
        {s_mplus, matplus_lsubr},
        {s_mminus, matminus_lsubr},
        {s_mmult, matmult_lsubr},
        {s_cdistance, cdistance_lsubr},
        {s_conv, convolve_lsubr},
        {0,0}
    };

    static scm_iproc lsubr2s[] = {
        {s_make_dmatrix, make_dmatrix_lsubr2},
        {0,0}
    };

    static scm_iproc subr1s[] = {
        {s_vnorm, vnorm_subr1},
        {s_mtranspose, mattransp_subr1},
        {s_colorp, colorp},
        {s_rgb_vect, rgb_vect},
        {s_bilin_interp, bilin_interp},
        {s_clamp_cv, clamp_cv},
        {0,0}
    };

    static scm_iproc subr2s[] = {
        {s_vdot, vdot_subr2},
        {s_vcross, vcross_subr2},
        {s_vscale, vscale_subr2},
        {s_mscalar, matscal_subr2},
        {s_cplus, cplus_subr2},
        {s_cfilter, cfilter_subr2},
        {s_cscale, cscale_subr2},
        {s_dvect_string, dvect_string},
        {0,0}
    };

    static scm_iproc subr3s[] = {
        {s_rgb, rgb_subr3},
        {s_hsv, hsv_subr3},
        {s_get_bilin, get_bilin},
        {0,0}
    };

    scm_init_iprocs (lsubrs, scm_tc7_lsubr);
    scm_init_iprocs (lsubr2s, scm_tc7_lsubr_2);
    scm_init_iprocs (subr1s, scm_tc7_subr_1);
    scm_init_iprocs (subr2s, scm_tc7_subr_2);
    scm_init_iprocs (subr3s, scm_tc7_subr_3);

    /* __END__ */

    scm_make_gsubr(s_square_iter, 7, 0, 0, square_iter);

    tc16_RGB=scm_newsmob(&RGB_smob);
    tc16_HSV=scm_newsmob(&HSV_smob);
}
