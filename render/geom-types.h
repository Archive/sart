
#ifndef _geom_types_h_
#define _geom_types_h_

#define TYPE_POLY 0
#define TYPE_POINT 1
#define TYPE_SPHERE 2
#define TYPE_TREE 3
#define TYPE_TESSEL 4
#define TYPE_HYPER 5
#define TYPE_HF 6
#define TYPE_PATCH 7

/****** NData
 * NAME
 *  NData
 * FUNCTION
 *  This is a pair of transformation matrices, used by Raystruct to contain
 *  transforms for Tree (and similar) objects.
 * SOURCE
 */

typedef struct {
    Matrix4 fmat, bmat; /* fmat transforms from the world coordinate system
			   into the local system. bmat is the inverse of
			   fmat */
} NData;

/*******/

/****** GeomTree
 * NAME
 *  GeomTree
 * FUNCTION
 *  This is a representation of the Tree primitive.
 * SOURCE
 */

typedef struct {
    int type;           /* TYPE_TREE */
    int nscenes;        /* number of the scenes */
    SCM *scenes;        /* pointer to the list of the scenes */
    SCM selector;       /* Scheme selector function, which takes a ray
			   structure and returns an index in the
			   scenes list
			   set selector=#f to default to scene 0 */
    Matrix4 fmat, bmat; /* transformation matrices, see NData */
} GeomTree;

/*******/

/****** GeomPoly
 * NAME
 *  GeomPoly
 * FUNCTION
 *  Representation of the polygon primitive.
 * SOURCE
 */

typedef struct {
    int type;       /* TYPE_POLY */
    int nverts;     /* number of vertices */
    Point3 *verts;  /* list of vertices */
    Point3 normal;  /* normal (of unit length) */
    Point2 min,max; /* 2d boxes, delimiting the projection of the polygon
		       along the axis corresponding to the highest component
		       of the normal */
    double ndist;   /* signed distance between the poly plane and
		       the origin */
} GeomPoly;

/*******/

/****** GeomPoint
 * NAME
 *  GeomPoint
 * FUNCTION
 *  Representation of the point primitive.
 * SOURCE
 */

typedef struct {
    int type; /* TYPE_POINT */
    Point3 p;
} GeomPoint;

/*******/

/****** GeomSphere
 * NAME
 *  GeomSphere
 * FUNCTION
 *  Representation of the Sphere primitive.
 * SOURCE
 */

typedef struct {
    int type;  /* TYPE_SPHERE */
    Point3 c;
    double r;
    NData *nd; /* transformation matrix pair. May be NULL. */
} GeomSphere;

/*******/

typedef struct {
    int type;          /* TYPE_TESSEL */
    int h,w;           /* height, width */
    Point3 *vertices;
    Point3 *normals;
    double *distances; /* for plane equations */
    Point3 *vertex_normals; /* if these are NULL, no smoothing is performed */
    struct BinTree_struct *tree; /* This is a BSP tree formed from the
				    references to the triangles */
    int *rcache_ids;             /* array of the Cache ids, one for each
				    triangle, as explained in GeomObj. */
} GeomTessel;

typedef struct {
    int type;          /* TYPE_PATCH */
    int nverts;
    Point3 *v;         /* vertices */
    Point3 *n;         /* vertex normals */
    int ntrians;
    int *verts;        /* indices to the vertex list */
    Point3 *normals;
    double *distances; /* for plane equations */
    struct BinTree_struct *tree; /* This is a BSP tree formed from the
				    references to the triangles */
    int *rcache_ids;
} GeomPatch;

typedef struct {
    int type;          /* TYPE_HF */
    int h,w;           /* height, width */
    double *heights;
    int nbounds;
    double *bounds;
    int nbranches;
    int *branches;
    NData *nd;
} GeomHF;

typedef struct {
    int type;          /* TYPE_HYPER */
    int n;             /* number of subdivisions, >1 */
    double *offsets;   /* texture offsets */
    int *lines;        /* vertex -> line table */
    struct BinTree_struct *tree; /* This is a BSP tree formed from the
				    references to the triangles */
    int *rcache_ids;             /* array of the Cache ids, one for each
				    triangle, as explained in GeomObj. */
    Point3 n1, n2, n3,           /* Three vertices and normals */
	   v1, v2, v3;
    Point3 *boundary_normals;
} GeomHyper;

/*******/

/****** GeomPrimitive
 * NAME
 *  GeomPrimitive
 * SOURCE
 */

typedef union {
    int type;
    GeomPoly polygon;
    GeomPoint point;
    GeomSphere sphere;
    GeomTree tree;
    GeomTessel tessel;
    GeomHyper hyper;
    GeomHF hf;
    GeomPatch patch;
} GeomPrimitive;

/*******/

/****** GeomObj
 * NAME
 *  GeomObj
 * FUNCTION
 *  The toplevel structure for a single raytracer object.
 * SOURCE
 */

typedef struct {
    int id;
    SCM cell;         /* cell that references to this object */
    Point3 min, max;  /* bounding box */
    int ray_cache_id; /* This is updated each time an object is stored in
			 a Cache. Later, the ray_cache_id is compared
			 to the stamp in the Cache structure to check
			 the validity of the cache reference. */
    SCM material;
    SCM csg;          /* The csg container or a pair of the containers
			 that hold this object. */
    GeomPrimitive var;
} GeomObj;

/*******/

/****** GeomObjList
 * NAME
 *  GeomObjList
 * FUNCTION
 *  List of GeomObjs. It may also contain integers, for example, triangle
 *  references in a tessel.
 * SOURCE
 */

typedef struct {
    GeomObj **list;
    int length;     /* Length of the list */
#ifdef BSP_DEBUG
    /* this was used for bug tracking in BSP subdivider */
    int axis;
    Point3 pmin;
    Point3 pmax;
#endif
} GeomObjList;

/********/

/****** SplitNode
 * NAME
 *  SplitNode
 * FUNCTION
 *  This is a pair of indices in the BSP tree data list, pointing to
 *  near_child and far_child for each node.
 * SOURCE
 */

typedef struct {
    int child[2]; /* indices */
#ifdef BSP_DEBUG
    int axis;
    Point3 pmin;
    Point3 pmax;
#endif
} SplitNode;

/********/


/****** BinNode
 * NAME
 *  BinNode
 * NOTES
 *  The child[0] decides what the node is:
 *    0   = empty node, don't follow anywhere
 *    >0  = follow to another node, and it will be a split node
 *    <0  = follow the link, to an object list
 * SOURCE
 */

typedef union uBinNode {
    SplitNode s;
    GeomObjList o;
} BinNode;

/*******/

/****** BinTree
 * NAME
 *  BinTree
 * FUNCTION
 *  The BSP tree structure
 * SOURCE
 */

typedef struct BinTree_struct {
    int         MaxDepth;      /* max allowed depth of the tree */
    int         MaxListLength; /* max primitive allowed in a leaf node */
    Point3	min,max;       /* extent of the tree */
    BinNode    *root;          /* pointer to the tree data heap */
    int         size;          /* number of BinNodes - used in free*/
    int         initial;       /* The root node */
} BinTree;

/*******/

#endif
