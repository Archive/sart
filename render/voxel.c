#include <stdio.h>
#include "bsp.h"
#include "wtree-rt.h"

/****** Voxelsmob
 * NAME
 *  Voxelsmob
 * FUNCTION
 *  Guile datatype for voxels.
 * SOURCE
 */

typedef struct {
    Matrix4 fmat;
    Wavetree *t;
} Voxel;

long tc16_Voxel;

#define VOXELSTR(s) ((Voxel*)SCM_CDR(s))

scm_sizet Voxel_free (SCM_CELLPTR v)
{
    Voxel *voxel=VOXELSTR(v);

    free_wavetree (voxel->t);
    free (voxel);
    return sizeof(Voxel);
}

int Voxel_print (SCM v, SCM port, int writing)
{
    Voxel *voxel=VOXELSTR(v);
    char buf[256];
    
    sprintf (buf, "#<Voxel %d>", v);
    scm_puts(buf, port);
    return 1;
}

static scm_smobfuns Voxelsmob = {scm_mark0, Voxel_free, Voxel_print, 0};

/*******/

typedef struct {
    int dim;
    int mindepth;
    int maxdepth;
    double eps;
    SCM func;
    SCM vector_buff;
} VoxData;

boolean subd_crit (int depth, int x[], void *data)
{
    int i, j;
    VoxData *d = (VoxData*) data;
    double *e = (double*)SCM_VELTS(d->vector_buff);
    SCM ret;

    if (depth < d->mindepth) return TRUE;
    if (depth > d->maxdepth) return FALSE;
    ret = SCM_LIST0;
    for (i = 0; i < (1 << d->dim); i++) {
	for (j = 0; j < d->dim; j++)
	    e[j] = (double)(x[j] + ((i & (1 << j)) ? 1 : 0)) / (1 << depth);
	ret = scm_cons(scm_apply(d->func, d->vector_buff, SCM_EOL), ret);
    }
    return SCM_REALPART(vdist_lsubr(ret)) > d->eps;
}

SCM_PROC(s_voxel_from_proc, "voxel_from_proc", 4, 0, 0, voxel_from_proc);
SCM voxel_from_proc(SCM trans, SCM func, SCM maxdepth, SCM eps, SCM dim)
{
    int m, n, max;
    double e;
    Voxel *voxel;
    SCM vector_buff; /* stack protected from gc */

    SCM_ASSERT(SCM_NIMP(trans) && get_dims(trans, &m, &n) &&
	       m == 4 && n == 4, trans, SCM_ARG1, s_voxel_from_proc);
    SCM_ASSERT(SCM_INUMP(maxdepth), maxdepth, SCM_ARG3, s_voxel_from_proc);
    SCM_ASSERT(SCM_NIMP(eps) && SCM_REALP(eps), eps, SCM_ARG4, s_voxel_from_proc);
    SCM_ASSERT(SCM_INUMP(dim), dim, SCM_ARG5, s_voxel_from_proc);
    
    e = SCM_REALPART(eps);
    max = SCM_INUM(maxdepth);
    
}

void init_voxel_c (void)
{ 
    /* __PERL__ This code is automatically generated. Don't modify. */


    /* __END__ */

    tc16_Voxel=scm_newsmob(&Voxelsmob);
}
