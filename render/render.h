/****h* (render)/render.h [0.1]
 * NAME
 *  render.h
 * COPYRIGHT
 *   Miroslav Silovic
 * MODIFICATION HISTORY
 *  
 * NOTES
 *  This header contains prototypes for basic rendering datastructures,
 *  as well as basic accessor macros
 ******
 */

/****** Scene
 * NAME
 *  Scene
 * FUNCTION
 *  This is the scene datastructure
 * SOURCE
 */


typedef struct {
    int n;
    SCMPTR object_list;
    SCM light_list;
    SCM csg_list;
    BinTree *object_tree;
    SCM eye_csg;         /* This is the initial csg, obtained when
			    spawning the eyeray */
    int maxid;           /* number of IDs used by this scene */
    boolean zbuffer;     /* The scene has been converted for zbuffering? */
    SCM outer_volume_mat; /* the volume material global for the scene;
			     this saves the trouble of enclosing the
			     scene into one large CSG container to
			     simulate the same thing */
} Scene;

/*******/

/****** Ray flag definitions
 * NAME
 *  RF_SHADOW RF_REFL_SPEC RF_REFL_DIFF RF_REFR_SPEC RF_REFR_DIFF RF_FORW
 * FUNCTION
 *  Each of these flag denotes purpose of the ray. Materials (supposedly)
 *  treat rays in a qualitatively different ways, depending on the flags.
 * SOURCE
 */

#define RF_SHADOW     01    /* shadow ray */
#define RF_REFL_SPEC  02    /* specular reflection trace */
#define RF_REFL_DIFF  04    /* diffuse reflection trace; this is indirect
			       lighting*/
#define RF_REFR_SPEC  010   /* specular refraction */
#define RF_REFR_DIFF  020   /* diffuse refraction - direct lighting from
			       the other side */
#define RF_FORW       040   /* forward-raytraced ray */

/*******/

/****** Ray ignore flags
 * NAME
 *  RI_DO_NOT_IGNORE RI_NOT_A_SPHERE RI_SPHERE_IGNORE_LOWER
 *  RI_SPHERE_IGNORE_HIGHER
 * FUNCTION
 *  These are used to prevent reconsidering the same primitive by
 *  to consecutive rays (as the child ray will originate at the
 *  same point where parent ray ended.
 * SOURCE
 */

#define RI_DO_NOT_IGNORE -1
#define RI_NOT_A_SPHERE 0
#define RI_SPHERE_IGNORE_LOWER 1
#define RI_SPHERE_IGNORE_HIGHER 2

/*******/

/****** Raystruct
 * NAME
 *  Raystruct
 * FUNCTION
 *  Basic ray structure
 * SOURCE
 */

typedef struct {
    /* ray data */

    Point3 origin;
    Point3 direction;
    SCM scene;
    SCM parent;     /* the parent ray cell. This is set from
		       c_get_ray_rad. It's SCM_BOOL_T for the eye ray
		       of the scene as well as for the eye rays
		       spawned by the tree primitive */
    double weight;
    int flags;

    double shadow_length; /* required length for shadows */

    NData *ndata;   /* reset to NULL when constructing the ray.  freed
		       before change. This is a pointer to a pair of
		       two matrices */
    SCM mat_override; /* override material, if needed - this allows
		         tree primitives to override the material of
		         their components */
    int nref;       /* reference for the tesselations/heightfields */

    /* intersection data */
    SCM object;     /* object the ray intersected. This is reset from
		       cast_ray call or volume material
		       function. Values: object-cell, SCM_BOOL_F
		       (meaning virgin ray or unprocessed ray) and
		       SCM_ BOOL_T (meaning ray that the ray hit
		       virtual surface generated by the volume
		       material */

    SCM csg;        /* csg for the intersection. SCM_BOOL_T means it's
		       an eye ray, while SCM_BOOL_F means that the
		       section surface is a plain surface.  Otherwise,
		       it points to the CSG container that is supposed
		       to forward the next ray. It's set in
		       RayObjectIntersect. */
    SCM csg_sector; /* the object whose CSG containment gets switched.
		       May differ from .object if .object is a tree */
    int ignore_id;  /* the ID of the object to be ignored during the
		       evaluation of this ray */
    int ignore_sphere; /* specially used for spheres */
    int ignore_id_out, ignore_sphere_out;
    Point3 section;
    double length;
    Point3 normal;
    Point3 pat_n;
    Point3 pat_pos;
} Raystruct;

/*******/

#define LIST_MARK 1
#define ORIGIN_MARK 2
#define CSGFIELD(x) ((Csg*)SCM_CDR(x))
#define CSGCONT(x)  (((GeomObj*)SCM_CDR(x))->csg)
#define CSGCONT2(x) (CSGFIELD(CSGCONT(x)))

#define RAYSTR(x)   ((Raystruct*)SCM_CDR(x))
#define SCENESTR(x) ((Scene*)SCM_CDR(x))
#define OBJSTR(x)   ((GeomObj*)SCM_CDR(x))

#define CSG_INTERSECTION 0
#define CSG_UNION        1
#define CSG_MINUS        2
#define CSG_CONTAINER    3

/****** CsgObject
 * NAME
 *  CsgObject
 * FUNCTION
 *  Reserved for future use.
 * SOURCE
 */

typedef union {
    int dummy; /* no specific data for now */
} CsgObject;

/*******/

/****** Csg
 * NAME
 *  Csg
 * FUNCTION
 *  Csg primitive data structure.
 * SOURCE
 */

typedef struct {
    CsgObject var;
    SCM parent;
    int flags;
    int type;
    int ntotal;
    int nactive;
    SCM aux;
    SCM material;
} Csg;

/*******/

Raystruct *newray(void);
void deleteray(SCM ray);
SCM check_call (SCM val, SCM ray);

#define SELFN(field) (SCM_VELTS(self)[field])
#define GET_MAT_ARG(field,predicate,convert,where) \
    p = check_call(SCM_VELTS(self)[field],rcell); \
    SCM_ASSERT(predicate,p,"Illegal material spec",where); \
    convert;
#define GET_MAT_ARG_MAYBE(field,predicate,convert,where) \
    if (SCM_VELTS(self)[field]!=SCM_UNDEFINED) { \
    	GET_MAT_ARG(field,predicate,convert,where); \
    }

extern SCM ambient_light;
extern SCM mtag_light;
extern SCM mtag_transmit_shadows;
extern SCM mtag_texture_mat;
extern SCM mtag_ior;

#define PAIR_P(x) (SCM_NIMP(x) && SCM_CONSP(x))
