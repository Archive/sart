
#include <stdlib.h>
#include "dalloc.h"
#undef FLOATS
#include "libguile.h"
#define FLOATS

void DClear (DPointer *p, int size)
{
    p->ptr=NULL;
    p->size=0;
    p->maxsize=size;
}

void *DAlloc (DPointer *p, int datasize)
{
    if (!p->ptr)
	p->ptr=malloc(datasize*p->maxsize);
    else
	if (p->size==p->maxsize) {
	    p->ptr=realloc(p->ptr, datasize*(p->maxsize<<=1));
	}
    return (void*)((char*)p->ptr+datasize*(p->size++));
}

void *DArray (DPointer *p, int datasize, int datanum)
{
    void *e;
    if (!p->ptr){
	if (p->maxsize <= datanum)
	    p->maxsize=2*datanum;
	p->ptr=malloc(datasize*p->maxsize);
	p->size=0;
    }
    else {
	if (p->size+datanum>=p->maxsize) {
	    int oldsize=p->maxsize*datasize;
	    while (p->size+datanum>=p->maxsize)
		p->maxsize<<=1;
	    p->ptr=realloc(p->ptr, datasize*p->maxsize);
	}
    }
    e=(void*)((char*)p->ptr+datasize*p->size);
    p->size+=datanum;
    return e;
}


void DFree (DPointer *p)
{
    free(p->ptr);
    p->size=0;
    p->maxsize=0;
}

