#ifndef _wtree_rt_h_
#define _wtree_rt_h_

#include "wtree.h"

/****** TraverseData
 * NAME
 *  TraverseData
 * FUNCTION
 *  These are data used by ray_wtree_traverse. This is multidimensional
 *  raytracer (i.e. you can trace 4d or even 5d voxels, if you have
 *  enough storage). 
 * SOURCE
 */

typedef int (*TraverseFun)(void *value, void *fdata);

typedef struct {
    Wavetree *t;                   /* the tree to traverse */
    double start[16];              /* starting coordinates */
    double dir[16];                /* direction */
    double pmin[16], pmax[16];     /* box corners (usually (0...) and (1...) */
    TraverseFun f;                 /* function to be called for each leaf
				      node encountered */
    void *fdata;                   /* extra data to be passed to f */
} TraverseData;

/*******/

void ray_wtree_traverse (double maxlength, TraverseData *d);

#endif
