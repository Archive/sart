/****h* (render)/primitive.c [1.0]
 * NAME
 *  primitive.c
 * COPYRIGHT
 *  Miroslav Silovic
 * MODIFICATION HISTORY
 *
 * NOTES
 *  write the geom transformation functions, at least.
 * BUGS
 *  Trees have not been fully tested.
 ******
 */

#include "bsp.h"
#include "mathutils.h"
#include "primitives/primitive.h"

extern long scm_tc16_array;

void get_normal (GeomPoly*);
void get_minmax (GeomPoly*);


/****** global_lock,global_ray
 * NAME
 *  global_lock, global_ray
 * SYNOPSIS
 *  int global_lock=0
 *  SCM global_ray
 * FUNCTION
 *  These two global variables are used to evaluate the selection of a tree.
 *  Their purpose is to ensure that the ray for which the tree selector
 *  will be evaluated is set only once.
 * BUGS
 *  global_ray is not set as it should be, on the start of the trace. This
 *  has problem potential when raytracing process is interrupted.
 * SEE ALSO
 *  find_intersecting_object, ray_eval_rec, spawn_eye_ray2
 *******
 */

int global_lock=0;
SCM global_ray; /* SCM pointer to ray struct, used for selective bsp trees */

/**********/

/****** LinePolyIntersect
 * NAME
 *  LinePolyIntersect
 * SYNOPSIS
 *  boolean LinePolyIntersect (Point3 *p1, Point3 *p2, GeomPoly *poly)
 * FUNCTION
 *  Check whether the line between p1 and p2 intersects the polygon poly.
 *  TRUE is returned only if the intersection is actually between the
 *  endpoints.
 * SEE ALSO
 *  LineBoxIntersect
 ********
 */

boolean LinePolyIntersect (Point3 *p1, Point3 *p2, GeomPoly *poly)
{
    Ray r;
    double m1;

    r.origin = *p1;
    V3Sub(p2,p1,&r.direction);
    if (RayPolygonIntersection (&r, poly, &m1))
	return m1<1.0;
    else
	return FALSE;
}

/****** ndata_ray
 * NAME
 *  ndata_ray
 * SYNOPSIS
 *  void ndata_ray (NData *nd, Ray *r, Point3 *o, Point3 *d)
 * FUNCTION
 *  This is a temporary function, returning ray origin and direction in the
 *  transformed coordinate system (using matrix nd).
 ********
 */

void ndata_ray (NData *nd, Ray *r, Point3 *o, Point3 *d)
{
    V3MulPointByMatrix(&r->origin, &nd->fmat, o);
    V3MulVectorByMatrix(&r->direction, &nd->fmat, d);
}

/****** get_tranformed_dist
 * NAME
 *  get_tranformed_dist
 * SYNOPSIS
 *  void get_tranformed_dist (NData *nd, Ray *r, Point3 *o,
 *                            Point3 *d,double *dist)
 * FUNCTION
 *  Transform the distance *dist back into the world coordinate system
 *  (the result is stored back into *dist).
 ********
 */
	
void get_tranformed_dist (NData *nd, Ray *r, Point3 *o, Point3 *d,double *dist)
{
    Point3 t, t1;

    t.x=o->x+(*dist)*d->x; t.y=o->y+(*dist)*d->y; t.z=o->z+(*dist)*d->z;
    V3MulPointByMatrix (&t, &nd->bmat, &t1);
    *dist=V3DistanceBetween2Points(&t1, &r->origin);
}

/*
-----------------------------------------------------------------------------
each of these function has primitive-dependant INDIRECT_CALL.
-----------------------------------------------------------------------------
*/

#define INDIRECT_CALL(p,c,args) ((PrimitiveClass[(p)->var.type].c) args)

/****** SetSceneIDs
 * NAME
 *  SetSceneIDs
 * SYNOPSIS
 *  int SetSceneIDs (Scene *s)
 * FUNCTION
 *  The purpose of this is to set the scene IDs for all the objects on the
 *  scenes. Included trees are specially accounted for, and each object (even
 *  the copied ones) will get its unique ID. The next free ID is returned.
 * NOTES
 *  This function is never called in the present version. It will become
 *  essential once zbuffering is implemented.
 ********
 */

int SetSceneIDs (Scene *s)
{
    int i,n=0;

    for (i=0; i<s->n; i++) {
	GeomObj *p=OBJSTR(s->object_list[i]);
	
	n = INDIRECT_CALL(p,SetSceneID,(p,n));
    }
    s->maxid = n;
    return n;
}

/****** DumpPrimitive
 * NAME
 *  DumpPrimitive
 * SYNOPSIS
 *  void DumpPrimitive (GeomObj *p)
 * FUNCTION
 *  Print the object p to stdout. The printing form is not readable by Scheme
 *  but is useful for diagnostics.
 ******
 */

void DumpPrimitive (GeomObj *p, SCM port, int writing)
{
    INDIRECT_CALL(p,Dump,(p));
}

/****** VectorPrimitive
 * NAME
 *  VectorPrimitive
 * SYNOPSIS
 *  void VectorPrimitive (GeomObj *p, VectorOutputHook f)
 * FUNCTION
 *  This call is not yet fully implemented.
 *******
 */

void VectorPrimitive (GeomObj *p, VectorOutputHook f)
{
    INDIRECT_CALL(p,Vector,(p,f));
}

/****** ZBufferPersp
 * NAME
 *  ZBufferPersp
 * SYNOPSIS
 *  void ZBufferPersp (GeomObj *p)
 * FUNCTION
 *  This call is not yet fully implemented.
 ******
 */

void ZBufferPersp (GeomObj *p)
{
    INDIRECT_CALL(p,ZBufferP,(p));
}

/****** RayPrimitiveIntersection
 * NAME
 *  RayPrimitiveIntersection
 * SYNOPSIS
 *  boolean RayPrimitiveIntersection (Ray *ray, GeomObj *o, double *dist,
 *				      double maxdist, Raystruct *r1,
 *				      GeomObj **return_o)
 * INPUTS
 *  ray       Ray being tested
 *  o         Pointer to the object to which the ray is casted
 *  maxdist   Maximal distance to which the intersection must occur
 *  r1        Full ray structure. This is used by primitives that
 *            need r1->ndata (transformed primitives and trees).
 * RESULT
 *  Return TRUE/FALSE.
 *  If TRUE is returned, the following are set:
 *      dist        distance to the intersection
 *      return_o    pointer to the lowest level primitive (in the lowest
 *                  level included tree)
 *  Additionally, global variables needed for the normal calculation are
 *  set from here. They are used by EvalWithPrimitive. No such globals are
 *  needed in the present implementation, though.
 * SEE ALSO
 *  global_lock,global_ray
 ********
 */

boolean RayPrimitiveIntersection (Ray *ray, GeomObj *o, double *dist,
				  double maxdist, Raystruct *r1,
				  GeomObj **return_o)
{
    int simple, ret;

    if (!ray)
	return (int)&o->ray_cache_id;
    if ((simple = PrimitiveClass[o->var.type].simple_primitive)
	&& o->id == r1->ignore_id)
	return FALSE;
    *return_o=o;
    ret = INDIRECT_CALL(o,RayInt,(ray,o,dist,maxdist,r1,return_o));
    if (ret && simple && *dist>0 && *dist<maxdist) {
	r1->ignore_id_out = o->id;
	r1->ignore_sphere_out = RI_NOT_A_SPHERE;
    }
    return ret;
}

/****** EvalWithPrimitive
 * NAME
 *  EvalWithPrimitive
 * SYNOPSIS
 *  void EvalWithPrimitive (Raystruct *r)
 * FUNCTION
 *  After the intersection has been found and validated, this call fills in
 *  the rest of the ray structure. Specifically, it uses r->ndata, object
 *  ndata matrices (if present) and r->section to fill up normal, pat_n and
 *  pat_pos. Smoothing calculations on the triangular meshes are performed
 *  here.
 * NOTES
 *  The normal obtained from here is not necessarily of unit length.
 ******
 */

void EvalWithPrimitive (Raystruct *r)
{
    int eqflag=0; /* says that pat_n and normal are the same. False for
		     patch objects */
    GeomObj *obj=(GeomObj*)SCM_CDR(r->object);
    Point3 t;
    Matrix4 m;
    NData *nd;
    Point3 section;

    if (r->ndata) {
	V3MulPointByMatrix(&r->section, &r->ndata->fmat, &section);
    }
    else
	section=r->section;

    eqflag=INDIRECT_CALL(obj, Eval, (obj, r, &nd, &section));

    if (nd) {
	/* object has its own transformation matrix - we will deal with it
	   first */

	TransposeMatrix4 (&nd->fmat, &m);
	V3MulVectorByMatrix(&r->normal, &m, &t);
	r->normal=t;
	if (!eqflag)
	    V3MulVectorByMatrix(&r->pat_n, &m, &t);
	r->pat_n=t;
    }
    if (r->ndata) {
	TransposeMatrix4 (&r->ndata->fmat, &m);
	V3MulVectorByMatrix(&r->normal, &m, &t);
	r->normal=t;
	if (!eqflag)
	    V3MulVectorByMatrix(&r->pat_n, &m, &t);
	r->pat_n=t;
    }
}

/****** FreePrimitive
 * NAME
 *  FreePrimitive
 * SYNOPSIS
 *  void FreePrimitive (GeomObj *o)
 * FUNCTION
 *  Deallocates pointer o as well as all the pending data.
 ******
 */

int FreePrimitive (GeomObj *o)
{
    return INDIRECT_CALL(o,Free,(o));
}

/****** CopyPrimitive
 * NAME
 *  CopyPrimitive
 * SYNOPSIS
 *  GeomObj *CopyPrimitive (GeomObj *in)
 * FUNCTION
 *  This returns verbatim copy of the object in.
 ******
 */

GeomObj *CopyPrimitive (GeomObj *in)
{
    return INDIRECT_CALL(in, Copy, (in));
}

/****** BindPrimitive
 * NAME
 *  BindPrimitive
 * SYNOPSIS
 *  void BindPrimitive (GeomObj *p)
 * FUNCTION
 *  Set up the min and max fields of the GeomObj structure. Call this before
 *  enclosing the object into any octree.
 ******
 */

void BindPrimitive (GeomObj *p)
{
    INDIRECT_CALL(p,Bind,(p));
    if (p->min.x==p->max.x) p->max.x+=EPS;
    if (p->min.y==p->max.y) p->max.y+=EPS;
    if (p->min.z==p->max.z) p->max.z+=EPS;
}

/****** TransformPrimitive
 * NAME
 *  TransformPrimitive
 * SYNOPSIS
 *  void TransformPrimitive (GeomObj *p, Matrix4 *m)
 * FUNCTION
 *  Transform the primitive p with matrix m, overwriting it.
 ******
 */

void TransformPrimitive (GeomObj *p, Matrix4 *m)
{
    INDIRECT_CALL(p,Transform,(p,m));
}

/****** PrimitiveBoxTest
 * NAME
 *  PrimitiveBoxTest
 * SYNOPSIS
 *  boolean PrimitiveBoxTest (Point3 *min, Point3 *max, GeomObj *obj)
 * FUNCTION
 *  Check whether the primitive given with obj has any point within the
 *  box given with min/max. This call is passed as pointer to the octree
 *  subdivider. Note that this call is currently VERY slow for Trees
 *  and quite imprecise for Tessels (that is, it always returns TRUE,
 *  leaving only the preliminary test of the box with the bounding box
 *  of the tessel). This routine needs a octree/box checker that would
 *  actually traverse the octree.
 ******
 */

boolean PrimitiveBoxTest (Point3 *min, Point3 *max, GeomObj *obj)
{
    return INDIRECT_CALL(obj,Box,(obj, min, max));
}

/****** Geom_mark
 * NAME
 *  Geom_mark
 * SYNOPSIS
 *  SCM Geom_mark (SCM c)
 * FUNCTION
 *  Standard mark call for GC. Only trees hold anything that needs to
 *  be marked, for now.
 ******
 */

SCM Geom_mark (SCM c)
{
    GeomObj *p=(GeomObj*)scm_markcdr(c);

    INDIRECT_CALL(p,Mark,(p));
    scm_gc_mark(p->csg);
    return p->material;
}

/* --------------------- end of primitive dependant functions --------------*/

/*-----------------------------------------------------------------
  scm interface for primitives - put constuctors here
  -----------------------------------------------------------------*/

long tc16_Geom;

scm_sizet Geom_free (SCM c)
{
    return FreePrimitive ((GeomObj*) SCM_CDR(c));
}

int Geom_print (SCM c, SCM port, int writing)
{
    DumpPrimitive ((GeomObj*) SCM_CDR(c), port, writing);
    return 1;
}

static scm_smobfuns Geomsmob={Geom_mark, Geom_free, Geom_print, 0};

/****** primitivep
 * NAME
 *   primitivep
 * SYNOPSIS
 *   SCM primitivep (SCM p)
 * FUNCTION
 *   Return boolean value saying whether p is geometric primitive.
 ******/

/* SUBR (subr1) */
char s_primitivep[]="primitive?";
SCM primitivep (SCM p)
{
    if (SCM_NIMP(p) && SCM_TYP16(p)==tc16_Geom)
	return SCM_BOOL_T;
    return SCM_BOOL_F;
}

/****** copy_primitive
 * NAME
 *   copy_primitive
 * SYNOPSIS
 *   SCM copy_primitive (SCM g)
 *   copy-primitive (subr1)
 * FUNCTION
 *   This is a Scheme interface to CopyPrimitive.
 ******/

/* SUBR (subr1) */
char s_copy_primitive[]="copy-primitive";
SCM copy_primitive (SCM g)
{
    SCM c;
    GeomObj *o;

    SCM_ASSERT(primitivep(g)==SCM_BOOL_T, g, SCM_ARG1, s_copy_primitive);
    o = CopyPrimitive (OBJSTR(g));
    SCM_NEWCELL(c);
    SCM_CDR(c) = (SCM)o;
    SCM_CAR(c) = tc16_Geom;
    return c;
}

/****** trasform_primitive
 * NAME
 *   trasform_primitive
 * SYNOPSIS
 *   SCM trasform_primitive (SCM g, SCM mat)
 * FUNCTION
 *   Scheme interface to TransformPrimitive.
 ******/

/* SUBR (subr2) */
char s_transform_primitive[]="transform-primitive!";
SCM trasform_primitive (SCM g, SCM mat)
{
    Matrix4 m;
    double *p;
    extern int get_dims (SCM, int *, int *);
    int d1, d2, i, j, k;

    if (!get_dims(mat, &d1, &d2) || d1 != 4 || d2 != 4) {
	SCMERROR (mat, SCM_ARG2, s_transform_primitive);
    }

    p = (double*)&(m.element[0][0]);
    for (i = 0; i < 16; i++)
	p[i] = ((double*)(SCM_VELTS(SCM_ARRAY_V(mat))))[i];
    TransformPrimitive(OBJSTR(g), &m);
    return SCM_UNSPECIFIED;
}

int parse_vertex (SCM d, Point3 *v)
{
    if (SCM_IMP(d) || SCM_TYP7(d) != scm_tc7_dvect || SCM_LENGTH(d) != 3)
	return 0;

    *v=*(Point3*)SCM_CDR(d);
    return 1;
}

/* SUBR (subr2) */
char s_make_poly[] = "make-polygon";
SCM make_poly (SCM verts, SCM mat)
{
    int i,l;
    GeomObj *o;
    GeomPoly *p;
    Point3 v;
    SCM data;
    SCM vrt=verts;

    l=scm_ilength(verts);
    SCM_ASSERT(l>=3, verts, SCM_ARG1, s_make_poly);

    o=NEWGEOM(GeomPoly);
    p=&o->var.polygon;
    p->type=TYPE_POLY;
    p->nverts=l;
    p->verts=MNEWARRAY(Point3,l);
    /* now we actually construct the vertex list */
    for (i=0; i<l; i++) {
	SCM_ASRTGO(parse_vertex(SCM_CAR(vrt),&v),badarg);
	p->verts[i]=v;
	vrt=SCM_CDR(vrt);
    }

    /* setup the rest of the poly data */

    get_normal(p);
    get_minmax(p);
    
    /* setup the primitive */

    o->material=mat;
    o->csg=SCM_BOOL_F;
    BindPrimitive(o);

    SCM_NEWCELL (data);
    SCM_CDR(data)=(SCM)o;
    SCM_CAR(data)=(SCM)tc16_Geom;
    return data;

  badarg:
    free(p->verts);
    free(o);
    SCMERROR(SCM_CAR(verts), "double vector expected", s_make_poly);
    return SCM_BOOL_F;
}

/* SUBR (subr2) */
char s_make_point[] = "make-point";
SCM make_point (SCM p, SCM mat)
{
    Point3 v;
    GeomObj *o;
    SCM data;

    SCM_ASSERT(parse_vertex(p,&v),p,SCM_ARG1,s_make_point);
    o=NEWGEOM(GeomPoint);
    o->var.type=TYPE_POINT;
    o->var.point.p=v;

    o->material=mat;
    o->csg=SCM_BOOL_F;
    BindPrimitive(o);

    SCM_NEWCELL (data);
    SCM_CDR(data)=(SCM)o;
    SCM_CAR(data)=(SCM)tc16_Geom;
    return data;
}

/* SUBR (subr3) */
char s_make_sphere[] = "make-sphere";
SCM make_sphere (SCM c, SCM r, SCM mat)
{
    Point3 v;
    GeomObj *o;
    SCM data;

    SCM_ASSERT(parse_vertex(c,&v),c,SCM_ARG1,s_make_sphere);
    SCM_ASSERT(SCM_NIMP(r) && SCM_REALP(r), r, SCM_ARG2, s_make_sphere);
    o=NEWGEOM(GeomSphere);
    o->var.type=TYPE_SPHERE;
    o->var.sphere.c=v;
    o->var.sphere.r=SCM_REALPART(r);
    o->var.sphere.nd=NULL;

    o->material=mat;
    o->csg=SCM_BOOL_F;
    BindPrimitive(o);

    SCM_NEWCELL (data);
    SCM_CDR(data)=(SCM)o;
    SCM_CAR(data)=(SCM)tc16_Geom;
    return data;
}

static int bind_hf_rec (GeomHF *h, int left, int right,
			int bot, int top, double *min, double *max)
{
    int cur_offs = h->nbounds, midx, midy;
    double m1, m2;
    int *br, last_nb, ind;

    h->nbounds += 2;
    h->bounds = (double*)realloc(h->bounds, h->nbounds * sizeof(double));
    if (left == right - 1 && bot == top - 1) {
	int pos = left + h->w * bot;

	*min = *max = h->heights[pos];
	if (h->heights[pos+1] < *min) *min = h->heights[pos+1];
	if (h->heights[pos+1] > *max) *max = h->heights[pos+1];
	if (h->heights[pos+h->w] < *min) *min = h->heights[pos+h->w];
	if (h->heights[pos+h->w] > *max) *max = h->heights[pos+h->w];
	if (h->heights[pos+h->w+1] < *min) *min = h->heights[pos+h->w+1];
	if (h->heights[pos+h->w+1] > *max) *max = h->heights[pos+h->w+1];
	h->bounds[cur_offs] = *min;
	h->bounds[cur_offs+1] = *max;
	return cur_offs;
    }

    h->nbranches = 2*h->nbounds;
    h->branches = (int*)realloc(h->branches, h->nbranches * sizeof(int));
    midx = (left + right) / 2;
    midy = (bot + top) / 2;
    *min = BIG;
    *max = -BIG;
    if (left < midx) {
	if (bot < midy) {
	    ind = bind_hf_rec(h, left, midx, bot, midy, &m1, &m2);
	    h->branches[2*cur_offs] = ind;
	    if (*min > m1) *min = m1;
	    if (*max < m2) *max = m2;
	}
	ind = bind_hf_rec(h, left, midx, midy, top, &m1, &m2);
	h->branches[2*cur_offs+2] = ind;
	if (*min > m1) *min = m1;
	if (*max < m2) *max = m2;
    }
    if (bot < midy) {
	ind = bind_hf_rec(h, midx, right, bot, midy, &m1, &m2);
	h->branches[2*cur_offs+1] = ind;
	if (*min > m1) *min = m1;
	if (*max < m2) *max = m2;
    }
    ind = bind_hf_rec(h, midx, right, midy, top, &m1, &m2);
    h->branches[2*cur_offs+3] = ind;
    if (*min > m1) *min = m1;
    if (*max < m2) *max = m2;
    h->bounds[cur_offs] = *min;
    h->bounds[cur_offs+1] = *max;
    return cur_offs;
}

/* SUBR (subr2) */
char s_make_hf[] = "make-heightfield";
SCM make_hf (SCM heights, SCM mat)
{
    int m1, n1;
    double *h, d1, d2;
    GeomObj *o;
    SCM data;

    SCM_ASSERT (SCM_ARRAYP(heights) && SCM_ARRAY_CONTP(heights) && SCM_ARRAY_NDIM(heights)==2 &&
		SCM_ARRAY_DIMS(heights)[0].lbnd==0 && SCM_ARRAY_DIMS(heights)[0].ubnd>0 &&
		SCM_ARRAY_DIMS(heights)[1].lbnd==0 && SCM_ARRAY_DIMS(heights)[1].ubnd>0,
		heights, SCM_ARG1, s_make_hf);
    
    m1=SCM_ARRAY_DIMS(heights)[0].ubnd+1;
    n1=SCM_ARRAY_DIMS(heights)[1].ubnd+1;

    h = MNEWARRAY(double, m1*n1);
    memcpy (h, (double*)SCM_VELTS(SCM_ARRAY_V(heights)), m1*n1*sizeof(double));

    o=NEWGEOM(GeomHF);
    o->var.type=TYPE_HF;
    o->var.hf.h=m1;
    o->var.hf.w=n1;
    o->var.hf.heights = h;
    o->var.hf.nbounds = 0;
    o->var.hf.bounds = NEWARRAY(double, 2);
    o->var.hf.nbranches = 4;
    o->var.hf.branches = NEWARRAY(int, 4);
    o->var.hf.nd = NULL;
    bind_hf_rec (&o->var.hf, 0, m1-1, 0, n1-1, &d1, &d2);

    o->material=mat;
    o->csg=SCM_BOOL_F;
    BindPrimitive(o);

    SCM_NEWCELL (data);
    SCM_CDR(data)=(SCM)o;
    SCM_CAR(data)=(SCM)tc16_Geom;
    return data;
}    

/* SUBR (subr3) */
char s_make_tessel[] = "make-tessel";
SCM make_tessel (SCM pts, SCM vnorms, SCM mat)
{
    int m1, n1, i, j;
    Point3 *verts, *vs, *ns;
    double *ds;
    int *rcids;
    double *arr1, *arr2;
    int k1;
    GeomPoly poly;
    Point3 v[3];
    GeomObj *o;
    GeomObjList l;
    int *oblist;
    SCM data;
    Point3 min, max;

    poly.verts=v;
    poly.nverts=3;
    SCM_ASSERT (SCM_ARRAYP(pts) && SCM_ARRAY_CONTP (pts) &&
	    SCM_ARRAY_NDIM(pts)==3 &&
	    SCM_ARRAY_DIMS(pts)[0].lbnd==0 && SCM_ARRAY_DIMS(pts)[0].ubnd>0 &&
	    SCM_ARRAY_DIMS(pts)[1].lbnd==0 && SCM_ARRAY_DIMS(pts)[1].ubnd>0 &&
	    SCM_ARRAY_DIMS(pts)[2].lbnd==0 && SCM_ARRAY_DIMS(pts)[2].ubnd==2,
	    pts, SCM_ARG1, s_make_tessel);

    m1=SCM_ARRAY_DIMS(pts)[0].ubnd+1;
    n1=SCM_ARRAY_DIMS(pts)[1].ubnd+1;

    if (vnorms != SCM_BOOL_F) {
	SCM_ASSERT (SCM_ARRAYP(vnorms) && SCM_ARRAY_NDIM(vnorms)==3 &&
	  SCM_ARRAY_DIMS(vnorms)[0].lbnd==0 && SCM_ARRAY_DIMS(vnorms)[0].ubnd==m1-1 &&
	  SCM_ARRAY_DIMS(vnorms)[1].lbnd==0 && SCM_ARRAY_DIMS(vnorms)[1].ubnd==n1-1 &&
	  SCM_ARRAY_DIMS(vnorms)[2].lbnd==0 && SCM_ARRAY_DIMS(vnorms)[2].ubnd==2,
	  vnorms, SCM_ARG2, s_make_tessel);
	  vs=MNEWARRAY(Point3, m1*n1);
	  arr2=(double*)SCM_VELTS(SCM_ARRAY_V(vnorms));
    }
    else {
	vs=NULL;
	arr2=NULL;
    }

    verts=MNEWARRAY(Point3, m1*n1);
    arr1=(double*)SCM_VELTS(SCM_ARRAY_V(pts));
    k1=0;
    min.x=min.y=min.z=BIG;
    max.x=max.y=max.z=-BIG;
    for (j=0; j<n1; j++)
	for (i=0; i<m1; i++) {
	    verts[k1].x=arr1[3*k1];
	    verts[k1].y=arr1[3*k1+1];
	    verts[k1].z=arr1[3*k1+2];
	    if (verts[k1].x<min.x) min.x=verts[k1].x;
	    if (verts[k1].y<min.y) min.y=verts[k1].y;
	    if (verts[k1].z<min.z) min.z=verts[k1].z;
	    if (verts[k1].x>max.x) max.x=verts[k1].x;
	    if (verts[k1].y>max.y) max.y=verts[k1].y;
	    if (verts[k1].z>max.z) max.z=verts[k1].z;
	    if (vs) {
		vs[k1].x=arr2[3*k1];
		vs[k1].y=arr2[3*k1+1];
		vs[k1].z=arr2[3*k1+2];
	    }
	    k1++;
	}
    if (min.x==max.x) max.x+=EPS;
    if (min.y==max.y) max.y+=EPS;
    if (min.z==max.z) max.z+=EPS;
    rcids=MNEWARRAY(int, 2*(m1-1)*(n1-1));
    ns=MNEWARRAY(Point3, 2*(m1-1)*(n1-1));
    ds=MNEWARRAY(double, 2*(m1-1)*(n1-1));
    o=NEWGEOM(GeomTessel);
    o->var.type=TYPE_TESSEL;
    o->var.tessel.h=m1;
    o->var.tessel.w=n1;
    for (i=0; i<2*(m1-1)*(n1-1); i++) {
	int v1, v2, v3;
	
	rcids[i]=-1;
	tessel_verts (&o->var.tessel, i, &v1, &v2, &v3);
	v[0]=verts[v1];
	v[1]=verts[v2];
	v[2]=verts[v3];
	get_normal(&poly);
	ns[i]=poly.normal;
	ds[i]=poly.ndist;
    }

    o->var.tessel.vertices=verts;
    o->var.tessel.normals=ns;
    o->var.tessel.distances=ds;
    o->var.tessel.vertex_normals=vs;
    o->var.tessel.rcache_ids=rcids;
    oblist=NEWARRAY(int, 2*(m1-1)*(n1-1));
    for (i=0; i<2*(m1-1)*(n1-1); i++)
	oblist[i]=i+16; /* Make sure no object is a NULL pointer */
    l.list=(GeomObj**)oblist;
    l.length=2*(m1-1)*(n1-1);
    o->var.tessel.tree=NEWTYPE(BinTree);
    o->var.tessel.tree->min=min;
    o->var.tessel.tree->max=max;
    tref=&o->var.tessel; /* for TesselInBox */
    InitBinTree2(o->var.tessel.tree,&l,(geominbox_type)TesselInBox);

    o->material=mat;
    o->csg=SCM_BOOL_F;
    BindPrimitive(o);

    SCM_NEWCELL (data);
    SCM_CDR(data)=(SCM)o;
    SCM_CAR(data)=(SCM)tc16_Geom;
    return data;
}

char s_make_patch[] = "make-patch";
SCM make_patch (SCM pts, SCM vnorms, SCM indices, SCM mat)
{
    int i, j;
    int nt, nv;
    Point3 *verts, *vs, *ns;
    double *ds;
    int *rcids;
    double *arr1, *arr2;
    int *inds, *arr3;
    int k1;
    GeomPoly poly;
    Point3 v[3];
    GeomObj *o;
    GeomObjList l;
    int *oblist;
    SCM data;
    Point3 min, max;
    extern GeomPatch *pref;
    extern void patch_verts (GeomPatch *p, int obj, int *v1, int *v2, int *v3);
    extern boolean PatchInBox (Point3 *min, Point3 *max, int obj);

    poly.verts=v;
    poly.nverts=3;

    SCM_ASSERT (SCM_ARRAYP(pts) &&
		SCM_ARRAY_NDIM(pts)==2 && SCM_TYP7(SCM_ARRAY_V(pts))==scm_tc7_dvect &&
		SCM_ARRAY_DIMS(pts)[0].lbnd==0 && SCM_ARRAY_DIMS(pts)[0].ubnd>0 &&
		SCM_ARRAY_DIMS(pts)[1].lbnd==0 && SCM_ARRAY_DIMS(pts)[1].ubnd==2,
		pts, SCM_ARG1, s_make_patch);
    nv=SCM_ARRAY_DIMS(pts)[0].ubnd+1;

    SCM_ASSERT (SCM_ARRAYP(vnorms) && SCM_ARRAY_NDIM(vnorms)==2 &&
		SCM_TYP7(SCM_ARRAY_V(vnorms))==scm_tc7_dvect &&
		SCM_ARRAY_DIMS(vnorms)[0].lbnd==0 && SCM_ARRAY_DIMS(vnorms)[0].ubnd== nv - 1 &&
		SCM_ARRAY_DIMS(vnorms)[1].lbnd==0 && SCM_ARRAY_DIMS(vnorms)[1].ubnd==2,
		vnorms, SCM_ARG2, s_make_patch);

    SCM_ASSERT (SCM_ARRAYP(indices) && SCM_ARRAY_NDIM(indices)==2 &&
		SCM_TYP7(SCM_ARRAY_V(indices))==scm_tc7_ivect &&
		SCM_ARRAY_DIMS(indices)[0].lbnd==0 && SCM_ARRAY_DIMS(indices)[0].ubnd > 0 &&
		SCM_ARRAY_DIMS(indices)[1].lbnd==0 && SCM_ARRAY_DIMS(indices)[1].ubnd == 2,
		indices, SCM_ARG3, s_make_patch);

    nt=SCM_ARRAY_DIMS(indices)[0].ubnd+1;

    verts=MNEWARRAY(Point3, nv);
    arr1=(double*)SCM_VELTS(SCM_ARRAY_V(pts));
    vs=MNEWARRAY(Point3, nv);
    arr2=(double*)SCM_VELTS(SCM_ARRAY_V(vnorms));
    inds=MNEWARRAY(int, 3 * nt);
    arr3=(int*)SCM_VELTS(SCM_ARRAY_V(indices));

    k1=0;
    min.x=min.y=min.z=BIG;
    max.x=max.y=max.z=-BIG;
    for (i=0; i<nv; i++) {
	verts[k1].x=arr1[3*k1];
	verts[k1].y=arr1[3*k1+1];
	verts[k1].z=arr1[3*k1+2];
	if (verts[k1].x<min.x) min.x=verts[k1].x;
	if (verts[k1].y<min.y) min.y=verts[k1].y;
	if (verts[k1].z<min.z) min.z=verts[k1].z;
	if (verts[k1].x>max.x) max.x=verts[k1].x;
	if (verts[k1].y>max.y) max.y=verts[k1].y;
	if (verts[k1].z>max.z) max.z=verts[k1].z;
	if (vs) {
	    vs[k1].x=arr2[3*k1];
	    vs[k1].y=arr2[3*k1+1];
	    vs[k1].z=arr2[3*k1+2];
	}
	k1++;
    }
    for (i = 0; i < 3 * nt; i++)
	inds[i] = arr3[i];
    if (min.x==max.x) max.x+=EPS;
    if (min.y==max.y) max.y+=EPS;
    if (min.z==max.z) max.z+=EPS;
    rcids=MNEWARRAY(int, nt);
    ns=MNEWARRAY(Point3, nt);
    ds=MNEWARRAY(double, nt);
    o=NEWGEOM(GeomPatch);
    o->var.type=TYPE_PATCH;
    o->var.patch.nverts=nv;
    o->var.patch.ntrians=nt;
    o->var.patch.verts = inds;
    for (i=0; i<nt; i++) {
	int v1, v2, v3;
	
	rcids[i]=-1;
	patch_verts (&o->var.patch, i, &v1, &v2, &v3);
	v[0]=verts[v1];
	v[1]=verts[v2];
	v[2]=verts[v3];
	get_normal(&poly);
	ns[i]=poly.normal;
	ds[i]=poly.ndist;
    }

    o->var.patch.v=verts;
    o->var.patch.n=vs;
    o->var.patch.normals=ns;
    o->var.patch.distances=ds;
    o->var.patch.rcache_ids=rcids;
    oblist=NEWARRAY(int, nt);
    for (i=0; i<nt; i++)
	oblist[i]=i+16; /* Make sure no object is a NULL pointer */
    l.list=(GeomObj**)oblist;
    l.length=nt;
    o->var.patch.tree=NEWTYPE(BinTree);
    o->var.patch.tree->min=min;
    o->var.patch.tree->max=max;
    pref=&o->var.patch; /* for PatchInBox */
    InitBinTree2(pref->tree,&l,(geominbox_type)PatchInBox);

    o->material=mat;
    o->csg=SCM_BOOL_F;
    BindPrimitive(o);

    SCM_NEWCELL (data);
    SCM_CDR(data)=(SCM)o;
    SCM_CAR(data)=(SCM)tc16_Geom;
    return data;
}

/* SUBR (subr3) */
char s_make_hyper[] = "make-hyper";
SCM make_hyper (SCM offsets, SCM vertdata, SCM mat)
{
    double *arr1, *offs;
    int i, n, ntri, n1, k1, k2;
    Point3 min, max, p;
    GeomObj *o;
    GeomHyper *h;
    extern GeomHyper *href;
    GeomObjList l;
    int *oblist;
    SCM data;
    extern boolean HyperInBox (Point3, Point3, int);

    SCM_ASSERT (SCM_NIMP(offsets) && SCM_TYP7(offsets)==scm_tc7_dvect,
	    offsets, SCM_ARG1, s_make_hyper);
    SCM_ASSERT (SCM_ARRAYP(vertdata) && SCM_ARRAY_NDIM(vertdata)==2
	    && SCM_ARRAY_DIMS(vertdata)[0].lbnd==0
	    && SCM_ARRAY_DIMS(vertdata)[0].ubnd==5
	    && SCM_ARRAY_DIMS(vertdata)[1].lbnd==0
	    && SCM_ARRAY_DIMS(vertdata)[1].ubnd==2,
	    vertdata, SCM_ARG2, s_make_hyper);
    n1=SCM_LENGTH(offsets);
    n=0;
    while (n*(n+1)/2 < n1) {
	n++;
    }
    SCM_ASSERT (n>0 && n*(n+1)/2 == n1, offsets, SCM_ARG1, s_make_hyper);

    arr1=(double*)SCM_VELTS(SCM_ARRAY_V(vertdata));
    offs=(double*)SCM_VELTS(offsets);
    min.x=min.y=min.z=BIG;
    max.x=max.y=max.z=-BIG;
    o=NEWGEOM(GeomHyper);
    o->var.type=TYPE_HYPER;
    h=&o->var.hyper;
    h->n=n;
    h->offsets=MNEWARRAY(double, n1);
    h->lines=MNEWARRAY(int, n1);
    ntri=(n-1)*(n-1);
    h->rcache_ids=MNEWARRAY(int, ntri);
    i=0;
    h->v1.x=arr1[i++]; h->v1.y=arr1[i++]; h->v1.z=arr1[i++];
    h->v2.x=arr1[i++]; h->v2.y=arr1[i++]; h->v2.z=arr1[i++];
    h->v3.x=arr1[i++]; h->v3.y=arr1[i++]; h->v3.z=arr1[i++];
    h->n1.x=arr1[i++]; h->n1.y=arr1[i++]; h->n1.z=arr1[i++];
    h->n2.x=arr1[i++]; h->n2.y=arr1[i++]; h->n2.z=arr1[i++];
    h->n3.x=arr1[i++]; h->n3.y=arr1[i++]; h->n3.z=arr1[i++];
    for (i=0, k1=k2=0; i<n1; i++) {
	h->offsets[i]=offs[i];
	h->lines[i]=k1;
	if (!(k2--))
	    k2=++k1;
	hyper_vertex (i, h, &p);
	if (p.x<min.x) min.x=p.x;
	if (p.y<min.y) min.y=p.y;
	if (p.z<min.z) min.z=p.z;
	if (p.x>max.x) max.x=p.x;
	if (p.y>max.y) max.y=p.y;
	if (p.z>max.z) max.z=p.z;
    }
    if (min.x==max.x) max.x+=EPS;
    if (min.y==max.y) max.y+=EPS;
    if (min.z==max.z) max.z+=EPS;
    h->tree=NEWTYPE(BinTree);
    h->tree->min=min;
    h->tree->max=max;
    href=h;
    oblist=NEWARRAY(int, ntri);
    for (i=0; i<ntri; i++) {
	h->rcache_ids[i]=-1;
	oblist[i]=i+16;
    }
    l.list=(GeomObj**)oblist;
    l.length=ntri;
    InitBinTree2(h->tree,&l,(geominbox_type)HyperInBox);
    h->boundary_normals = NULL;

    o->material=mat;
    o->csg=SCM_BOOL_F;
    BindPrimitive(o);

    SCM_NEWCELL (data);
    SCM_CDR(data)=(SCM)o;
    SCM_CAR(data)=(SCM)tc16_Geom;
    return data;
}

/* SUBR (subr2) */
char s_set_hyper_norms[] = "set-hyper-boundary-normals!";
SCM set_hyper_norms(SCM obj, SCM ndata)
{
    GeomObj *o;
    GeomHyper *h;
    double *arr1;
    int n, i, k;

    SCM_ASSERT(primitivep(obj)==SCM_BOOL_T && (o=OBJSTR(obj))->var.type == TYPE_HYPER,
	       obj, SCM_ARG1, s_set_hyper_norms);
    h = &o->var.hyper;
    n = h->n;
    SCM_ASSERT (SCM_ARRAYP(ndata) && SCM_ARRAY_NDIM(ndata)==2
		&& SCM_ARRAY_DIMS(ndata)[0].lbnd == 0
		&& SCM_ARRAY_DIMS(ndata)[0].ubnd == 3 * n - 3
		&& SCM_ARRAY_DIMS(ndata)[1].lbnd == 0
		&& SCM_ARRAY_DIMS(ndata)[1].ubnd == 2,
		ndata, SCM_ARG2, s_make_hyper);
    if (!h->boundary_normals)
	h->boundary_normals = MNEWARRAY(Point3, 3 * n - 3);
    arr1=(double*)SCM_VELTS(SCM_ARRAY_V(ndata));
    for (k = i = 0; i < 3 * n - 3; i++) {
	h->boundary_normals[i].x = arr1[k++];
	h->boundary_normals[i].y = arr1[k++];
	h->boundary_normals[i].z = arr1[k++];
    }
    return SCM_UNSPECIFIED;
}    

/* SUBR (lsubr2) */
char s_make_tree[]="make-tree";
SCM make_tree_lsubr2 (SCM matrix, SCM selector, SCM data)
{
    int n,i, d1, d2;
    GeomObj *o;
    extern SCM scenep (SCM);

    n=scm_ilength(data);
    SCM_ASSERT (get_dims(matrix, &d1, &d2) && d1==4 && d2==4, matrix, SCM_ARG1,
	    s_make_tree);
    SCM_ASSERT (n>0, data, SCM_WNA, s_make_tree);
    if (selector==SCM_BOOL_F) {
	SCM_ASSERT (n==1, data, SCM_WNA, s_make_tree);
    }
    for (i=data; SCM_NNULLP(i); i=SCM_CDR(i)) {
        SCM_ASSERT (scenep(SCM_CAR(i))==SCM_BOOL_T, data, SCM_ARG3, s_make_tree);
    }

    o=NEWGEOM(GeomTree);
    o->var.tree.scenes=MNEWARRAY(SCM,n);
    o->var.type=TYPE_TREE;
    o->var.tree.nscenes=n;
    for (i=0; i<n; i++) {
        o->var.tree.scenes[i]=SCM_CAR(data);
	data=SCM_CDR(data);
    }
    o->var.tree.selector=selector;
    o->var.tree.bmat=*(Matrix4*)SCM_VELTS(SCM_ARRAY_V(matrix));
    V3InvertMatrix(&o->var.tree.bmat, &o->var.tree.fmat);

    o->material=SCM_BOOL_F;
    o->csg=SCM_BOOL_F;
    BindPrimitive(o);

    SCM_NEWCELL (data);
    SCM_CDR(data)=(SCM)o;
    SCM_CAR(data)=(SCM)tc16_Geom;
    return data;
} 

/* SUBR (subr2) */
char s_override_mat[]="override-material!";
SCM override_mat (SCM obj, SCM material)
{
    GeomObj *o;

    SCM_ASSERT(primitivep(obj)==SCM_BOOL_T && (o=OBJSTR(obj))->material==SCM_BOOL_F,
	   obj, SCM_ARG1, s_override_mat);
    o->material = material;
}
    
/* initialization call */

void init_primitive_c (void)
{ 
    /* __PERL__ This code is automatically generated. Don't modify. */

    static scm_iproc lsubr2s[] = {
        {s_make_tree, make_tree_lsubr2},
        {0,0}
    };

    static scm_iproc subr1s[] = {
        {s_primitivep, primitivep},
        {s_copy_primitive, copy_primitive},
        {0,0}
    };

    static scm_iproc subr2s[] = {
        {s_transform_primitive, trasform_primitive},
        {s_make_poly, make_poly},
        {s_make_point, make_point},
        {s_make_hf, make_hf},
        {s_set_hyper_norms, set_hyper_norms},
        {s_override_mat, override_mat},
        {0,0}
    };

    static scm_iproc subr3s[] = {
        {s_make_sphere, make_sphere},
        {s_make_tessel, make_tessel},
        {s_make_hyper, make_hyper},
        {0,0}
    };

    scm_init_iprocs (lsubr2s, scm_tc7_lsubr_2);
    scm_init_iprocs (subr1s, scm_tc7_subr_1);
    scm_init_iprocs (subr2s, scm_tc7_subr_2);
    scm_init_iprocs (subr3s, scm_tc7_subr_3);

    /* __END__ */

    tc16_Geom=scm_newsmob(&Geomsmob);
    scm_make_gsubr(s_make_patch,4,0,0,make_patch);
}

void init_globals (void)
{
    global_ray=SCM_BOOL_F;
    global_lock=0;
}
