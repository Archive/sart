/*
 * GraphicsGems.h
 * Version 1.0 - Andrew Glassner
 * from "Graphics Gems", Academic Press, 1990
 */

#ifndef GG_H

#define GG_H 1

/*********************/
/* 2d geometry types */
/*********************/

typedef struct Point2Struct {	/* 2d point */
	double x, y;
	} Point2;
typedef Point2 Vector2;

typedef struct IntPoint2Struct {	/* 2d integer point */
	int x, y;
	} IntPoint2;

typedef struct Matrix3Struct {	/* 3-by-3 matrix */
	double element[3][3];
	} Matrix3;

typedef struct Box2dStruct {		/* 2d box */
	Point2 min, max;
	} Box2;


/*********************/
/* 3d geometry types */
/*********************/

typedef struct Point3Struct {	/* 3d point */
	double x, y, z;
	} Point3;
typedef Point3 Vector3;

typedef struct IntPoint3Struct {	/* 3d integer point */
	int x, y, z;
	} IntPoint3;


typedef struct Matrix4Struct {	/* 4-by-4 matrix */
	double element[4][4];
	} Matrix4;

typedef struct Box3dStruct {		/* 3d box */
	Point3 min, max;
	} Box3;



/***********************/
/* one-argument macros */
/***********************/

/* absolute value of a */
#define ABS(a)		(((a)<0) ? -(a) : (a))

/* round a to nearest int */
#define ROUND(a)	floor((a)+0.5)

/* take sign of a, either -1, 0, or 1 */
#define ZSGN(a)		(((a)<0) ? -1 : (a)>0 ? 1 : 0)

/* take binary sign of a, either -1, or 1 if >= 0 */
#define SGN(a)		(((a)<0) ? -1 : 1)

/* square a */
#define SQR(a)		((a)*(a))


/***********************/
/* two-argument macros */
/***********************/

/* find minimum of a and b */
#define MIN(a,b)	(((a)<(b))?(a):(b))

/* find maximum of a and b */
#define MAX(a,b)	(((a)>(b))?(a):(b))

/* swap a and b (see Gem by Wyvill) */
#define SWAP(a,b)	{ a^=b; b^=a; a^=b; }

/* linear interpolation from l (when a=0) to h (when a=1)*/
/* (equal to (a*h)+((1-a)*l) */
#define LERP(a,l,h)	((l)+(((h)-(l))*(a)))

/* clamp the input to the specified range */
#define CLAMP(v,l,h)	((v)<(l) ? (l) : (v) > (h) ? (h) : v)


/********************/
/* useful constants */
/********************/

#ifndef PI
#define PI		3.141592	/* the venerable pi */
#endif
#ifndef PITIMES2
#define PITIMES2	6.283185	/* 2 * pi */
#endif
#define PIOVER2		1.570796	/* pi / 2 */
#define E		2.718282	/* the venerable e */
#define SQRT2		1.414214	/* sqrt(2) */
#define SQRT3		1.732051	/* sqrt(3) */
#define GOLDEN		1.618034	/* the golden ratio */
#define DTOR		0.017453	/* convert degrees to radians */
#define RTOD		57.29578	/* convert radians to degrees */


double V2SquaredLength(Vector2 *a);
double V2Length(Vector2 *a);
Vector2 *V2Negate(Vector2 *v);
Vector2 *V2Normalize(Vector2 *v);
Vector2 *V2Scale(Vector2 *v, double newlen);
Vector2 *V2Add(Vector2 *a, Vector2 *b, Vector2 *c);
Vector2 *V2Sub(Vector2 *a, Vector2 *b, Vector2 *c);
double V2Dot(Vector2 *a, Vector2 *b);
Vector2 *V2Lerp(Vector2 *lo, Vector2 *hi, double alpha, Vector2 *result);
Vector2 *V2Combine (Vector2 *a, Vector2 *b, Vector2 *result,
		    double ascl, double bscl);
Vector2 *V2Mul (Vector2 *a, Vector2 *b, Vector2 *result);
double V2DistanceBetween2Points(Vector2 *a, Vector2 *b);
Vector2 *V2MakePerpendicular(Vector2 *a, Vector2 *ap);
Vector2 *V2New(double x, double y);
Vector2 *V2Duplicate(Vector2 *a);
Point2 *V2MulPointByProjMatrix(Vector2 *pin, Matrix3 *m, Vector2 *pout);
Matrix3 *V2MatMul(Matrix3 *a, Matrix3 *b, Matrix3 *c);
Matrix3 *TransposeMatrix3(Matrix3 *a, Matrix3 *b);
double V3SquaredLength(Vector3 *a);
double V3SquaredDistance(Vector3 *a,Vector3 *b);
double V3Length(Vector3 *a);
Vector3 *V3Negate(Vector3 *v);
Vector3 *V3Normalize(Vector3 *v);
Vector3 *V3Scale(Vector3 *v, double newlen);
Vector3 *V3Add(Vector3 *a, Vector3 *b, Vector3 *c);
Vector3 *V3Sub(Vector3 *a, Vector3 *b, Vector3 *c);
double V3Dot(Vector3 *a, Vector3 *b);
Vector3 *V3Lerp(Vector3 *lo, Vector3 *hi, double alpha, Vector3 *result);
Vector3 *V3Combine (Vector3 *a, Vector3 *b, Vector3 *result,
		    double ascl, double bscl);
Vector3 *V3Mul (Vector3 *a, Vector3 *b, Vector3 *result);
double V3DistanceBetween2Points(Vector3 *a, Vector3 *b);
Vector3 *V3Cross(Vector3 *a, Vector3 *b, Vector3 *c);
Vector3 *V3New(double x, double y, double z);
Vector3 *V3Duplicate(Vector3 *a);
Point3 *V3MulPointByMatrix(Vector3 *pin, Matrix4 *m, Vector3 *pout);
Point3 *V3MulVectorByMatrix(Vector3 *pin, Matrix4 *m, Vector3 *pout);
Point3 *V3MulPointByProjMatrix(Vector3 *pin, Matrix4 *m, Vector3 *pout);
Matrix4 *V3MatMul(Matrix4 *a, Matrix4 *b, Matrix4 *c);
Matrix4 *TransposeMatrix4(Matrix4 *a, Matrix4 *b);
int gcd(int u, int v);
int quadraticRoots(double a, double b, double c, double *roots);
double RegulaFalsi(double (*f)(double), double left, double right);
double NewtonRaphson(double (*f)(double), double (*df)(double), double x);
double findroot(double left, double right, double tolerance,
		double (*f)(double), double (*df)(double));

#endif
