
#include "bsp.h"
#include "poly-implicit.h"

void init_algebra_c(void);
void init_primitive_c(void);
void init_render_c(void);
void init_mathutils_c(void);
void init_voxel_c(void);
void init_materials_c(void);
void init_volume_c(void);
void init_nurbs_c(void);
void init_indirect_c(void);
void init_blob_c(void);

void init_sart (void)
{
    init_algebra_c();
    init_primitive_c();
    init_render_c();
    init_mathutils_c();
    init_voxel_c();
    init_io_c();
    init_materials_c();
    init_volume_c();
    init_nurbs_c();
    init_indirect_c();
    init_blob_c();
    init_poly_subdivision();
}
