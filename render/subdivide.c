/* This program uses certain pieces of code, and ideas from the bsp article in Graphics Gems IV */

#include <stdio.h>
#include <math.h>
#include "bsp.h"

/* ------------------ Build the tree --------------- */

boolean GeomInBox (Point3 *min, Point3 *max, GeomObj *obj)
{
    if (min->x > obj->max.x || max->x <= obj->min.x) return FALSE;
    if (min->y > obj->max.y || max->y <= obj->min.y) return FALSE;
    if (min->z > obj->max.z || max->z <= obj->min.z) return FALSE;

    return PrimitiveBoxTest (min, max, obj);
}

void copy_list (GeomObjList *l, GeomObjList *l1)
{
    l1->length=l->length;
    l1->list=NEWARRAY(GeomObj*,l->length);
}

void SplitBSPNode (Point3 *min, Point3 *max, GeomObjList *l, GeomObjList *l1, GeomObjList *l2, int axis, double place, int *ret_total1, int *ret_total2, geominbox_type check_geominbox)
{
    int i,*flags=(int*)malloc(l->length*sizeof(int));
    Point3 min1, max1, min2, max2;
    int n1, n2, t;

#define IN_L1 1
#define IN_L2 2

    memset(flags, 0, l->length*sizeof(int));
    min1=min2= *min;
    max1=max2= *max;
    *ret_total1=0;
    *ret_total2=0;
    COORD(&min2,axis)=COORD(&max1,axis)=place;

    /* find the members of the first and the second list */

    for (n1=n2=i=0; i<l->length; i++) {
        if ((t=(*check_geominbox)(&min1, &max1, l->list[i]))) {
	    flags[i] |= IN_L1, n1++;
	    if (t==CONTAINS_VOXEL) (*ret_total1)++;
	}
        if ((t=(*check_geominbox)(&min2, &max2, l->list[i]))) {
	    flags[i] |= IN_L2, n2++;
	    if (t==CONTAINS_VOXEL) (*ret_total2)++;
	}
    }

    /* split the list */
    
    if (n1) {
	l1->length=n1;
	l1->list=NEWARRAY(GeomObj*,n1);
	for (n1=i=0; i<l->length; i++)
	    if (flags[i] & IN_L1)
		l1->list[n1++]=l->list[i];
    } else {
	l1->length=0;
	l1->list=NULL;
    }

    if (n2) {
	l2->length=n2;
	l2->list=NEWARRAY(GeomObj*,n2);
	for (n2=i=0; i<l->length; i++)
	    if (flags[i] & IN_L2)
		l2->list[n2++]=l->list[i];
    } else {
	l2->length=0;
	l2->list=NULL;
    }
    free (flags);
}

/* list caching subroutines */

#define LCACHE_SIZE 16

static GeomObjList lcache[LCACHE_SIZE];
static int lindices[LCACHE_SIZE];
static int lhead, ltail;

void init_cache ()
{
    ltail=lhead=0;
}

void insert_cache (GeomObjList *l, int index)
{
    lcache[lhead]=*l;
    lindices[lhead++]=index;
    if (lhead==LCACHE_SIZE) lhead=0;
    if (lhead==ltail) {
	ltail++;
	if (ltail==LCACHE_SIZE) ltail=0;
    }
}

int search_cache (GeomObjList *l)
{
    int i;

    for (i=lhead; i!=ltail; i=(i==0 ? LCACHE_SIZE-1 : i-1))
	if (l->length==lcache[i].length) {
	    int j;

	    for (j=0; j<l->length; j++)
		if (l->list[j]!=lcache[i].list[j])
		    break;

	    if (j==l->length) return lindices[i];
	}
    return -1;
}

/* main subdivide */
int Subdivide (int checking_length, DPointer *heap, BinTree *tree, GeomObjList *l, int axis, int depth, Point3 *min, Point3 *max, geominbox_type check_geominbox)
{
    int r=heap->size,r1;
    BinNode *new;

    if (checking_length>tree->MaxListLength && depth<tree->MaxDepth) {
	GeomObjList l1, l2;
	double pos, temp;
	BinNode new;
	int tcnt1, tcnt2;

	DAlloc(heap, sizeof(BinNode)); /* index is already stored in r */
#ifdef BSP_DEBUG
	new.s.axis=axis; new.s.pmin=*min; new.s.pmax=*max;
#endif
	pos=0.5*(COORD(min,axis)+COORD(max,axis));
	SplitBSPNode (min, max, l, &l1, &l2, axis, pos, &tcnt1, &tcnt2, check_geominbox);
	free (l->list);
	temp=COORD(max,axis); COORD(max,axis)=pos;
	new.s.child[0]=Subdivide (l1.length-tcnt1, heap, tree, &l1, INCAXIS(axis), depth+1, min, max, check_geominbox);
	COORD(max,axis)=temp;
	temp=COORD(min,axis); COORD(min,axis)=pos;
	new.s.child[1]=Subdivide (l2.length-tcnt2, heap, tree, &l2, INCAXIS(axis), depth+1, min, max, check_geominbox);
	COORD(min,axis)=temp;
	((BinNode*)(heap->ptr))[r]=new;
	return r;
    }

    /* direct tree insertion, check the cache and emptiness first */

    if (!l->length)
	return 0;
    if ((r1=search_cache(l))>=0) {
	free (l->list);
	return -r1;
    }

    new=(BinNode*)DAlloc(heap,sizeof(BinNode));

#ifdef BSP_DEBUG
    new->s.axis=axis; new->s.pmin=*min; new->s.pmax=*max;
#endif

    new->o = *l;
    insert_cache(l, r);
    return -r;
}

/*-----------------------------------*/

/* WARNING: list l will be deallocated in the process */

void InitBinTree2 (BinTree *tree, GeomObjList *l, geominbox_type check_geominbox)
{
    DPointer heap;
    extern int global_tree_depth, global_tree_node_size;

    tree->MaxDepth = global_tree_depth;
    tree->MaxListLength = global_tree_node_size;
    DClear (&heap,128);

    init_cache();
    DAlloc (&heap,sizeof(BinNode)); /* The 0 pointer is reserved for the
				       empty node */
    tree->initial=Subdivide(l->length, &heap, tree, l, X, 0, &tree->min, 
			    &tree->max, check_geominbox);
    tree->size=heap.size;
    tree->root=(BinNode*)realloc(heap.ptr,sizeof(BinNode)*heap.size);
}    

void InitBinTree (BinTree *tree, GeomObjList *l, geominbox_type check_geominbox)
{
    int i;
    Point3 min,max;

    min.x=min.y=min.z=BIG;
    max.x=max.y=max.z=-BIG;

    for (i=0; i<l->length; i++) {
	if (l->list[i]->min.x<min.x) min.x=l->list[i]->min.x;
	if (l->list[i]->min.y<min.y) min.y=l->list[i]->min.y;
	if (l->list[i]->min.z<min.z) min.z=l->list[i]->min.z;
	if (l->list[i]->max.x>max.x) max.x=l->list[i]->max.x;
	if (l->list[i]->max.y>max.y) max.y=l->list[i]->max.y;
	if (l->list[i]->max.z>max.z) max.z=l->list[i]->max.z;
	l->list[i]->ray_cache_id=-1;
    }

    if (min.x==max.x) max.x+=EPS;
    if (min.y==max.y) max.y+=EPS;
    if (min.z==max.z) max.z+=EPS;
    tree->min=min;
    tree->max=max;

    InitBinTree2 (tree, l, check_geominbox);
}

int free_node (BinNode *root, int node)
{
    if (!node) return 0;
    if (node>0) {
	return free_node (root, root[node].s.child[0])
	     + free_node (root, root[node].s.child[1]);
    }
    else {
	if (root[-node].o.list) {
	    free (root[-node].o.list);
	    root[-node].o.list = NULL; /* same node may be multireferenced */
	    return root[-node].o.length * sizeof(GeomObj*);
	}
	else
	    return 0;
    }
}

int FreeBinTree (BinTree *tree)
{
    int size=0;

    size+=sizeof(BinNode)*(tree->size);
    size+=sizeof(tree);
    size+=free_node(tree->root, tree->initial);
    free(tree->root);
    free(tree);
    return 0; /* we won't add up the size, as nothing have actually been
		 scm_must_malloced */
}

/* ------------ This is for debugging/printing purposes -------------- */

int DumpNode (BinNode *root, int node, int depth)
{
    int i,nodes;

    printf ("%5d#%*c\n", node, depth*2, '{');
    if (!node) return 0;
    if (node>0) {
	nodes=DumpNode(root, root[node].s.child[0], depth+1);
	nodes+=DumpNode(root, root[node].s.child[1], depth+1);
    }
    else {
	nodes=root[-node].o.length;
	for (i=0; i<root[-node].o.length; i++) {
	    printf ("%d",(int)root[-node].o.list[i]);
	    if (i<root[-node].o.length-1) printf (", ");
	}
	printf ("\n");
    }
    printf ("%*c\n", depth*2+6, '}');
    return nodes;
}
