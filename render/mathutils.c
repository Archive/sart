/****h* (render)/mathutils.c [0.1]
 * NAME
 *  mathutils.c - miscellaneous math utilities
 * COPYRIGHT
 *  Miroslav Silovic
 * MODIFICATION HISTORY
 *
 ******
 */


#include <math.h>
#include "bsp.h"

#define SRTABLE_SIZE 18723


/* distance between a point and a unit cylinder from (0,0,0) to (0,0,1) */
double point_cyl_sq_distance (Point3 *p)
{
    Point3 q;
    double r;

    q = *p;
    r = q.x * q.x + q.y * q.y;
    if (r < 1)
	return p->z < 0 ? -p->z : (p->z > 1 ? p->z - 1 : 0);
    r = sqrt(r);
    if (q.z >= 0 && q.z <=1)
	return r - 1;
    q.x /= r; q.y /= r;
    if (q.z < 0)
	q.z = -q.z;
    else if (q.z > 1)
	q.z -= 1;
    return sqrt(V3SquaredDistance(p, &q));
}

void invert_matrix (int n, double *m1, double *m2)
{
    int j,i,k;
    double t;

    for (i=0; i<n; i++)
	for (j=0; j<n; j++)
	    m2[i*n+j]= i==j ? 1 : 0;

    for (j=0; j<n; j++) {
	int i0=j;
	for (i=j+1; i<n; i++)
	    if (fabs(m1[i0*n+j])<fabs(m1[i*n+j]))
		i0=i;

	for (i=0; i<n; i++) {
	    t=m1[i0*n+i];
	    m1[i0*n+i]=m1[j*n+i];
	    m1[j*n+i]=t;
	    t=m2[i0*n+i];
	    m2[i0*n+i]=m2[j*n+i];
	    m2[j*n+i]=t;
	}

	t=1/m1[j*n+j];
	for (i=0; i<n; i++) {
	    m1[j*n+i]*=t;
	    m2[j*n+i]*=t;
	}

	for (i=0; i<n; i++) {
	    double t=m1[i*n+j];
	    if (i!=j)
		for (k=0; k<n; k++) {
		    m2[i*n+k]-=t*m2[j*n+k];
		    m1[i*n+k]-=t*m1[j*n+k];
		}
	}
    }
}


/****** V3InvertMatrix
 * NAME
 *  V3InvertMatrix
 * SYNOPSIS
 *  void V3InvertMatrix (Matrix4 *m1, Matrix4 *m2)
 * FUNCTION
 *  This is standard matrix inversion.
 * INPUTS
 *  m1 - pointer to matrix to be inverted
 * RESULT
 *  Result will be written to m2.
 ******
 */

void V3InvertMatrix (Matrix4 *m1, Matrix4 *m2)
{
    Matrix4 m;

    m=*m1;
    invert_matrix (4, (double*)m.element, (double*)m2->element);
}

/****** Intersect3Planes
 * NAME
 *   Intersect3Planes
 * SYNOPSIS
 *   void Intersect3Planes (Point3 *p[3], double d[3], Point3 *out)
 * FUNCTION
 *   Find the intersection of 3 planes given by <p_i, x> = d_i.
 ******/

void Intersect3Planes (Point3 *p[3], double d[3], Point3 *out)
{
    double m1[9], m2[9];
    int i, j, k;

    for (i = k = 0; i < 2; i++)
	for (j = 0; j < 2; j++)
	    m1[k++] = ((double*)p[i])[j];
    invert_matrix(3, m1, m2);
    out->x = m2[0] * d[0] + m2[1] * d[1] + m2[2] * d[2];
    out->y = m2[3] * d[0] + m2[4] * d[1] + m2[5] * d[2];
    out->z = m2[6] * d[0] + m2[7] * d[1] + m2[8] * d[2];
}
    
/****** minvert_subr1
 * NAME
 *  minvert_subr1
 * SYNOPSIS
 *  SCM minvert_subr1 (SCM m)
 *  minvert (subr1)
 * FUNCTION
 *  A Scheme call for matrix inversion.
 ******
 */

/* SUBR (subr1) */
char s_matinvert[]="minvert";
SCM minvert_subr1 (SCM m)
{
    int m1, m2;
    double *e, *f;

    SCM_ASSERT (get_dims(m, &m1, &m2), m, SCM_ARG1, s_matinvert);
    SCM_ASSERT (m1==m2, m, SCM_ARG1, s_matinvert);
    e=MNEWARRAY(double,m1*m1);
    f=NEWARRAY(double,m1*m1);
    memcpy (f, (double*)SCM_VELTS(SCM_ARRAY_V(m)), m1*m1*sizeof(double));
    invert_matrix(m1,f,e);
    free(f);
    return make_dmatrix (m1, m1, e);
}

/****** is_isoscale_mat
 * NAME
 *  is_isoscale_mat
 * SYNOPSIS
 *  double is_isoscale_mat (Matrix4 *m)
 * FUNCTION
 *  Check whether matrix m is orthogonal, uniformly scaling matrix.
 * RESULT
 *  -1 if the matrix is not isoscale.
 *  otherwise it will return the scaling factor.
 ******
 */

double is_isoscale_mat (Matrix4 *m)
{
    static Point3 e[3]={{1,0,0}, {0,1,0}, {0,0,1}};
    Point3 te[3];
    int i,j;
    int sqnorm;

    for (i=0; i<3; i++)
	V3MulVectorByMatrix(&e[i],m,&te[i]);

    /* check orthogonality and lengths of the normed base */

    for (i=0; i<3; i++)
	for (j=0; j<3; j++) {
	    double t=V3Dot(&te[i],&te[j]);

	    if (i==j) {
		if (!i)
		    sqnorm=t;
		else
		    if (fabs(sqnorm-t) > EPS)
			return -1;
	    }
	    else
		if (fabs(t) > EPS)
		    return -1;
	}
    return sqnorm;
}

double srtable[SRTABLE_SIZE];

/****** arand
 * NAME
 *  arand
 * SYNOPSIS
 *  double arand (int n,double *f)
 * FUNCTION
 *  Return a pseudo-random spatial noise. The value depends on the vector f
 *  (which is n-dimensional).
 ******
 */

double arand (int n,double *f)
{
    int i;
    double d;
    const double c1=52.123717287181882, c2=12.8182767378123, c3=29.32128381276;

    d=0;
    for (i=0; i<n; i++) {
	d+=f[i];
	d=d*c1+c2+srtable[(int)((d*c3-floor(d*c3))*SRTABLE_SIZE)];
	d=d-floor(d);
    }
    d+=f[0]; /* This reduces smoothness in dependance on the f[n-1] */
    d=d*c1+c2+srtable[(int)((d*c3-floor(d*c3))*SRTABLE_SIZE)];
    d=d-floor(d);
    return d;
}

#define NRAND(x,s) ((s)*128398191+(x))
#define SRTABLE(x) (srtable[(unsigned)(x)%SRTABLE_SIZE])

double irand (int n, int *i)
{
    switch (n) {
    case 1:
	return SRTABLE(NRAND(i[0],i[0]));
    case 2:
	return SRTABLE(NRAND(i[0],NRAND(i[1],i[0])));
    case 3:
	return SRTABLE(NRAND(i[0],NRAND(i[1],NRAND(i[2],i[0]))));
    case 4:
	return SRTABLE(NRAND(i[0],NRAND(i[1],NRAND(i[2],NRAND(i[3],i[0])))));
    default: {
	    int s=0,k=i[0];

	    while (n--)
		s=NRAND(*(i++),s);
	    return SRTABLE(NRAND(k,s)%SRTABLE_SIZE);
	}
    }
}

/****** snrand
 * NAME
 *  snrand
 * SYNOPSIS
 *  double snrand (int n, double *f)
 * FUNCTION
 *  Returns the smoothed spatial noise. The noise is bilinearily smoothed
 *  over unity-sized cubes.
 ******
 */


double snrand_rec(int n,int k,double *f,int *i)
{
    double t, t1, d1, d2;

    if (n<=k) return irand(n,i);

    t1=f[k]-floor(f[k]);
    d1=snrand_rec(n,k+1,f,i);
    i[k]++;
    d2=snrand_rec(n,k+1,f,i);
    i[k]--;
    return t1*d2+(1-t1)*d1;
}

double snrand (int n, double *f)
{
    double d0,d1,t1;
    int i[16],k;

    if (n<1 || n>16)
        SCMERROR(SCM_INUM(n),"Illegal noise argument (1-16 expected)","<unknown>");
    for (k=0; k<n; k++)
	i[k]=floor(f[k]);
    switch (n) {
    case 1:
	t1=f[0]-i[0];
	d0=irand(n,i);
	i[0]++;
	d1=irand(n,i);
	return t1*d1+(1-t1)*d0;
    case 2: {
	double d3, d2, t2;

	t1=f[0]-i[0];
	t2=f[1]-i[1];
	d0=irand(n,i);
	i[0]++;
	d1=irand(n,i);
	i[1]++;
	d3=irand(n,i);
	i[0]--;
	d2=irand(n,i);
	return t1*(t2*d3+(1-t2)*d1)+(1-t1)*(t2*d2+(1-t2)*d0);
    }
    case 3: {
	double d2,d3,d4,d5,d6,d7,t2,t3;

	t1=f[0]-i[0];
	t2=f[1]-i[1];
	t3=f[2]-i[2];
	d0=irand(n,i);
	i[0]++;
	d1=irand(n,i);
	i[1]++;
	d3=irand(n,i);
	i[0]--;
	d2=irand(n,i);
	i[2]++;
	d6=irand(n,i);
	i[1]--;
	d4=irand(n,i);
	i[0]++;
	d5=irand(n,i);
	i[1]++;
	d7=irand(n,i);
	return t3*(t1*(t2*d7+(1-t2)*d5)+(1-t1)*(t2*d6+(1-t2)*d4))
	    + (1-t3)*(t1*(t2*d3+(1-t2)*d1)+(1-t1)*(t2*d2+(1-t2)*d0));
    }
    }
    return snrand_rec(n,0,f,i);
}

/****** turbrand
 * NAME
 *  turbrand
 * SYNOPSIS
 *  double turbrand (double *f, int n, int k, double lambda, double omega)
 * FUNCTION
 *  Fractal noise.
 * INPUTS
 *  n,f - point of evaluation. n is the length of f.
 *  k   - Number of recursions (default: 4)
 *  lambda - size multiplier for the random value in each iteration
 *           (default: 0.5, higher value makes it more bumpy)
 *  omega  - frequency multiplier (default: 2.0)
 ******
 */

double turbrand (double *f, int n, int k, double lambda, double omega)
{
    double t, s, p;
    int i,j;

    s=t=0;
    p=1;
    for (i=0; i<k; i++) {
	s+=p;
	t+=p*snrand(n,f);
	p*=lambda;
	for (j=0; j<n; j++)
	    f[j]*=omega;
    }
    return t/s;
}

SCM interpolate_data (double t, SCM l1, SCM l2)
{
    int n;

    if (scm_thunk_p(l1) == SCM_BOOL_T)
	l1 = scm_apply(l1, SCM_EOL, SCM_EOL);
    if (SCM_IMP(l1)) return l1;
    if (scm_thunk_p(l2) == SCM_BOOL_T)
	l2 = scm_apply(l2, SCM_EOL, SCM_EOL);
    if (SCM_REALP(l1))
	if (SCM_REALP(l2))
	    return scm_makdbl((1-t)*SCM_REALPART(l1)+t*SCM_REALPART(l2),0.0);
	else
	    return SCM_BOOL_F;
    if (colorp(l1)==SCM_BOOL_T)
	if (colorp(l2)==SCM_BOOL_T)
	    return combine_colors(l1,l2,1-t,t);
	else
	    return SCM_BOOL_F;
    if (SCM_TYP7(l1)==scm_tc7_dvect)
	if (SCM_TYP7(l2)==scm_tc7_dvect && (n=SCM_LENGTH(l1))==SCM_LENGTH(l2)) {
	    int i;
	    double *l=MNEWARRAY(double, n),*v1=(double*)SCM_VELTS(l1),
		*v2=(double*)SCM_VELTS(l2);
	    for (i=0; i<n; i++)
		l[i]=(1-t)*v1[i]+t*v2[i];
	    return make_dvect (n,l);
	}
	else
	    return SCM_BOOL_F;
    if (SCM_CONSP(l1))
	if (SCM_CONSP(l2))
	    return scm_cons(interpolate_data(t, SCM_CAR(l1), SCM_CAR(l2)),
			interpolate_data(t, SCM_CDR(l1), SCM_CDR(l2)));
    return SCM_BOOL_F;
}

/****** lspline_subr2
 * NAME
 *  lspline_subr2
 * SYNOPSIS
 *  SCM lspline_subr2 (SCM x, SCM l)
 *  linear-spline (subr2)
 * FUNCTION
 *  This function interpolates information in the list l using linear spline.
 * INPUTS
 *  x - spline argument
 *  l - list of the form (value data value data ...), where values are
 *      ascending SCM doubles, and data may be immediate, double,
 *      color, dvector, procedure or scm_cons of any of these. If x is
 *      less than the least value, first data is returned, and similar
 *      for the high x.
 ****** */

/* SUBR (subr2) */
char s_lspline[]="linear-spline";
SCM lspline_subr2 (SCM x, SCM l)
{
    double t;
    double t1, t2;
    SCM d, d2;
    int n;

    n=scm_ilength(l);
    SCM_ASSERT(SCM_NIMP(x) && SCM_REALP(x), x, SCM_ARG1, s_lspline);
    SCM_ASSERT(n>0 && (n&1)==0, l, SCM_ARG2, s_lspline);
    t=SCM_REALPART(x);
    SCM_ASSERT (SCM_NIMP(SCM_CAR(l)) && SCM_REALP(SCM_CAR(l)), SCM_CAR(l), SCM_ARG2, s_lspline);
    t1=SCM_REALPART(SCM_CAR(l));
    d=SCM_CAR(SCM_CDR(l));
    if (t<t1)
	return d;
    l=SCM_CDR(SCM_CDR(l));
    while (SCM_NNULLP(l)) {
	SCM ca, cd;

	ca=SCM_CAR(l); cd=SCM_CDR(l);
	SCM_ASSERT (SCM_NIMP(ca) && SCM_REALP(ca), ca, SCM_ARG2, s_lspline);
	t2=SCM_REALPART(ca);
	d2=SCM_CAR(cd);
	l=SCM_CDR(cd);
	if (t<t2)
	    return interpolate_data((t-t1)/(t2-t1),d,d2);
	t1=t2;
	d=d2;
    }
    return d;
}

/****** noise_functions
 * NAME
 *  noise functions - basic texturing library
 * SYNOPSIS
 *  SCM random_subr (void)
 *  SCM noise_subr1 (SCM l)
 *  SCM snoise_subr1 (SCM l)
 *  SCM snoise3_subr1 (SCM l)
 *  SCM fnoise_lsubr (SCM l)
 *  SCM fnoise3_lsubr (SCM l)
 *  random (subr)
 *  noise (subr1)
 *  snoise (subr1)
 *  snoise3 (subr1)
 *  fnoise (lsubr)
 *  fnoise3 (lsubr)
 * FUNCTION
 *  noise is a Scheme interface for arand. l is a dvector in each of the
 *  above, except for fnoise (see INPUTS). random is an interface to
 *  drand48. snoise is an interface to snrand, and snoise 3 returns a
 *  dvector from three uncorrelated snrand values.
 * INPUTS
 *  fnoise takes at most 4 arguments, where the first is like in the other
 *  calls, the remaining three are k, lambda and omega from turbrand call.
 *  Only the first arg is required.
 ******
 */

/* SUBR (subr1) */
char s_noise[]="noise";
SCM noise_subr1 (SCM l)
{
    SCM_ASSERT (SCM_NIMP(l) && SCM_TYP7(l)==scm_tc7_dvect,
		l, SCM_ARG1, s_noise);
    return scm_makdbl(arand(SCM_LENGTH(l),(double*)SCM_VELTS(l)),0.0);
}

/* SUBR (subr0) */
char s_random[]="random";
SCM random_subr (void)
{
    return scm_makdbl(drand48(),0.0);
}

/* SUBR (subr1) */
char s_snoise3[]="snoise3";
SCM snoise3_subr1 (SCM l)
{
    double *f,*v;
    int n,i;
    SCM r;

    SCM_ASSERT (SCM_NIMP(l) && SCM_TYP7(l)==scm_tc7_dvect,
		l, SCM_ARG1, s_snoise3);
    n=SCM_LENGTH(l);
    f=NEWARRAY(double, n);
    v=MNEWARRAY(double, 3);
    for (i=0; i<3; i++) {
	memcpy(f,(double*)SCM_VELTS(l),n*sizeof(double));
	f[0]+=i*1128.12837182;
	v[i]=snrand(n,f);
    }
    r=make_dvect(3,v);
    free(f);
    return r;
}

/* SUBR (subr1) */
char s_snoise[]="snoise";
SCM snoise_subr1 (SCM l)
{
    double *f,v;

    SCM_ASSERT (SCM_NIMP(l) && SCM_TYP7(l)==scm_tc7_dvect,
		l, SCM_ARG1, s_snoise);
    return scm_makdbl(snrand(SCM_LENGTH(l),(double*)SCM_VELTS(l)),0.0);

}

/* SUBR (lsubr) */
char s_fnoise[]="fnoise";
SCM fnoise_lsubr (SCM l)
{
    double *f;
    int n;
    int k=4;
    double lambda=0.5, omega=2.0; 
    SCM r, c, c0;

    SCM_ASSERT (SCM_NNULLP(l), l, SCM_WNA, s_fnoise);
    c0=SCM_CAR(l);
    l=SCM_CDR(l);
    SCM_ASSERT (SCM_NIMP(c0) && SCM_TYP7(c0)==scm_tc7_dvect,
		c0, SCM_ARG1, s_fnoise);
    n=SCM_LENGTH(c0);
    if (SCM_NNULLP(l)) {
	c=SCM_CAR(l);
	l=SCM_CDR(l);
	SCM_ASSERT (SCM_INUMP(c), c, SCM_ARG2, s_fnoise);
	k=SCM_INUM(c);
	if (SCM_NNULLP(l)) {
	    c=SCM_CAR(l);
	    l=SCM_CDR(l);
	    SCM_ASSERT (SCM_NIMP(c) && SCM_REALP(c), c, SCM_ARG3, s_fnoise);
	    lambda=SCM_REALPART(c);
	    if (SCM_NNULLP(l)) {
		c=SCM_CAR(l);
		l=SCM_CDR(l);
		SCM_ASSERT (SCM_NIMP(c) && SCM_REALP(c), c, SCM_ARG4, s_fnoise);
		omega=SCM_REALPART(c);
	    }
	}
    }
    f=NEWARRAY(double, n);
    memcpy(f,(double*)SCM_VELTS(c0),n*sizeof(double));
    r=scm_makdbl(turbrand(f,n,k,lambda,omega),0.0);
    free(f);
    return r;
}

/* SUBR (lsubr) */
char s_fnoise3[]="fnoise3";
SCM fnoise3_lsubr (SCM l)
{
    double *f;
    int n;
    int i;
    int k=4;
    double lambda=0.5, omega=2.0; 
    double *v;
    SCM r, c, c0;

    SCM_ASSERT (SCM_NNULLP(l), l, SCM_WNA, s_fnoise);
    c0=SCM_CAR(l);
    l=SCM_CDR(l);
    SCM_ASSERT (SCM_NIMP(c0) && SCM_TYP7(c0)==scm_tc7_dvect,
		c0, SCM_ARG1, s_fnoise);
    n=SCM_LENGTH(c0);
    if (SCM_NNULLP(l)) {
	c=SCM_CAR(l);
	l=SCM_CDR(l);
	SCM_ASSERT (SCM_INUMP(c), c, SCM_ARG2, s_fnoise);
	k=SCM_INUM(c);
	if (SCM_NNULLP(l)) {
	    c=SCM_CAR(l);
	    l=SCM_CDR(l);
	    SCM_ASSERT (SCM_NIMP(c) && SCM_REALP(c), c, SCM_ARG3, s_fnoise);
	    lambda=SCM_REALPART(c);
	    if (SCM_NNULLP(l)) {
		c=SCM_CAR(l);
		l=SCM_CDR(l);
		SCM_ASSERT (SCM_NIMP(c) && SCM_REALP(c), c, SCM_ARG4, s_fnoise);
		omega=SCM_REALPART(c);
	    }
	}
    }
    f=NEWARRAY(double, n);
    v=MNEWARRAY(double, 3);
    for (i=0; i<3; i++) {
	memcpy(f,(double*)SCM_VELTS(c0),n*sizeof(double));
	f[0]+=i*1128.12837182;
	v[i]=turbrand(f,n,k,lambda,omega);
    }
    r=make_dvect(3,v);
    free(f);
    return r;
}

/****** array_dot
 * NAME
 *   array_dot
 * SYNOPSIS
 *   SCM array_dot (SCM ra0, SCM ra1)
 *   array-dot (subr2)
 * FUNCTION
 *   Return dot-product of arrays ra0 and ra1. Indices of array 0 have to
 *   be compatible with ra1 (in array-map sense).
 ******/

int do_arrdot (SCM r0, double *d, SCM r1)
{
    int n0, inc0, n1, inc1;
    double *base0, *base1;

    r1 = SCM_CAR(r1);
    n0 = SCM_ARRAY_DIMS(r0)->ubnd - SCM_ARRAY_DIMS(r0)->lbnd + 1;
    inc0 = SCM_ARRAY_DIMS(r0)->inc;
    base0 = (double*)SCM_VELTS(SCM_ARRAY_V(r0)) + SCM_ARRAY_BASE(r0);
    inc1 = SCM_ARRAY_DIMS(r1)->inc;
    base1 = (double*)SCM_VELTS(SCM_ARRAY_V(r1)) + SCM_ARRAY_BASE(r1);
    for (; n0--; base0 += inc0, base1 += inc1)
	*d += (*base0) * (*base1);
    return 1;
}

static double *cmodind (int ndim, int *vinds1, int *vmods1, int *vincs1, int *inds, double *e)
{
    scm_sizet i;
    int k;

    i = 0;
    for (k = 0; k < ndim; k++) {
	int t;
	t=inds[k] - vinds1[k];
	while (t >= vmods1[k]) t -= vmods1[k];
	while (t < 0) t += vmods1[k];
	i += t * vincs1[k];
    }
    return &e[i];
}

static double *cclampind (int ndim, int *vinds1, int *vmods1, int *vincs1, int *inds, double *e)
{
    static double zero;
    scm_sizet i;
    int k;

    i = 0;
    for (k = 0; k < ndim; k++) {
	int t;
	t=inds[k] - vinds1[k];
	if (t >= vmods1[k] || t < 0) {
	    zero = 0;
	    return &zero;
	}
	i += t * vincs1[k];
    }
    return &e[i];
}

static double *cmirrind (int ndim, int *vinds1, int *vmods1, int *vincs1, int *inds, double *e)
{
    scm_sizet i;
    int k;

    i = 0;
    for (k = 0; k < ndim; k++) {
	int t;
	t=inds[k] - vinds1[k];
    recheck:
	if (t >= vmods1[k]) {
	    t = 2 * vmods1[k] - t - 2;
	    goto recheck;
	}
	if (t < 0) {
	    t = -t;
	    goto recheck;
	}
	i += t * vincs1[k];
    }
    return &e[i];
}

static double *cmirrind2 (int ndim, int *vinds1, int *vmods1, int *vincs1, int *inds, double *e)
{
    scm_sizet i;
    int k;

    i = 0;
    for (k = 0; k < ndim; k++) {
	int t;
	t=inds[k] - vinds1[k];
    recheck:
	if (t >= vmods1[k]) {
	    t = 2 * vmods1[k] - t - 1;
	    goto recheck;
	}
	if (t < 0) {
	    t = -t - 1;
	    goto recheck;
	}
	i += t * vincs1[k];
    }
    return &e[i];
}

static scm_sizet cind(SCM ra, int *inds)
{
    scm_sizet i;
    int k;

    if (!SCM_ARRAYP(ra))
	return *inds;
    i = SCM_ARRAY_BASE(ra);
    for (k = 0; k < SCM_ARRAY_NDIM(ra); k++)
	i += (inds[k] - SCM_ARRAY_DIMS(ra)[k].lbnd)*SCM_ARRAY_DIMS(ra)[k].inc;
    return i;
}

/* SUBR (subr2) */
char s_arrdot[]="array-dot";
SCM array_dot (SCM ra0, SCM ra1)
{
    double d;

    SCM_ASSERT(SCM_NIMP(ra0) && (SCM_TYP7(ra0)==scm_tc7_dvect || (SCM_ARRAYP(ra0) &&
	   SCM_TYP7(SCM_ARRAY_V(ra0)) == scm_tc7_dvect)), ra0, SCM_ARG1, s_arrdot);
    SCM_ASSERT(SCM_NIMP(ra1) && (SCM_TYP7(ra1)==scm_tc7_dvect || (SCM_ARRAYP(ra1) &&
	   SCM_TYP7(SCM_ARRAY_V(ra1)) == scm_tc7_dvect)), ra1, SCM_ARG2, s_arrdot);
    d = 0.0;
    scm_ramapc (do_arrdot, (int)&d, ra0, scm_cons(ra1, SCM_EOL), s_arrdot);
    return scm_makdbl(d,0.0);
}

/****** arrdot_per
 * NAME
 *   arrdot_per
 * SYNOPSIS
 *   SCM arrdot_per (SCM ra0, SCM ra1, SCM adjust_type, SCM offsets)
 *   array-dot-adjust (gsubr)
 * FUNCTION
 *   Similar to array-dot, but it will adjust indices on overflow. offsets
 *   give the position of kernel within the array.
 *   adjust_type = 0 - wing
 *                 1 - clamp
 *                 2 - mirror, without repeating last
 *                 3 - mirror, repeat last
 ******/

char s_arrdot_per[]="array-dot-adjust";
SCM arrdot_per (SCM ra0, SCM ra1, SCM adjust_type, SCM offsets)
{
    typedef double* (*index_clamp_ptr)
	(int ndim, int *vinds1, int *vmods1,
	 int *vincs1, int *inds, double *e);
    index_clamp_ptr ptrmodind;
    double d, *e0, *e1;
    SCM r0, r1, inds0, inds1, incs1, mods1;
    int k, kmax, *vinds0, *vinds1, *vmods1, *vincs1;
    scm_array_dim *the_dims;

    SCM_ASSERT(SCM_NIMP(ra0) && (SCM_TYP7(ra0)==scm_tc7_dvect || (SCM_ARRAYP(ra0) &&
	   SCM_TYP7(SCM_ARRAY_V(ra0)) == scm_tc7_dvect)), ra0, SCM_ARG1, s_arrdot_per);
    SCM_ASSERT(SCM_NIMP(ra1) && (SCM_TYP7(ra1)==scm_tc7_dvect || (SCM_ARRAYP(ra1) &&
	   SCM_TYP7(SCM_ARRAY_V(ra1)) == scm_tc7_dvect)), ra1, SCM_ARG2, s_arrdot_per);
    SCM_ASSERT(scm_ilength(offsets)==(SCM_ARRAYP(ra1) ? SCM_ARRAY_NDIM(ra1) : 1),
	   offsets, SCM_ARG4, s_arrdot_per);
    SCM_ASSERT(SCM_INUMP(adjust_type), adjust_type, SCM_ARG3, s_arrdot_per);

    switch (SCM_INUM(adjust_type)) {
    case 0: ptrmodind = cmodind; break;
    case 1: ptrmodind = cclampind; break;
    case 2: ptrmodind = cmirrind; break;
    case 3: ptrmodind = cmirrind2; break;
    default: SCMERROR(adjust_type, SCM_ARG3, s_arrdot_per);
    }

    if (!SCM_ARRAYP(ra0)) {
	r0 = scm_make_ra(1);
	SCM_ARRAY_DIMS(r0)->lbnd = 0;
	SCM_ARRAY_DIMS(r0)->ubnd = SCM_LENGTH(ra0) - 1;
	SCM_ARRAY_DIMS(r0)->inc = 1;
	SCM_ARRAY_BASE(r0) = 0;
	SCM_ARRAY_V(r0) = ra0;
	ra0 = r0;
    }
    if (!SCM_ARRAYP(ra1)) {
	r1 = scm_make_ra(1);
	SCM_ARRAY_DIMS(r1)->lbnd = 0;
	SCM_ARRAY_DIMS(r1)->ubnd = SCM_LENGTH(ra1) - 1;
	SCM_ARRAY_DIMS(r1)->inc = 1;
	SCM_ARRAY_BASE(r1) = 0;
	SCM_ARRAY_V(r1) = ra1;
	ra1 = r1;
    }
    SCM_ASSERT(SCM_ARRAY_NDIM(ra0) == SCM_ARRAY_NDIM(ra1), ra1, SCM_ARG2, s_arrdot_per);
    e0 = (double*)SCM_VELTS(SCM_ARRAY_V(ra0));
    e1 = (double*)SCM_VELTS(SCM_ARRAY_V(ra1)) + SCM_ARRAY_BASE(ra1);

    inds0 = scm_make_uve(SCM_ARRAY_NDIM(ra0)+0L, SCM_MAKINUM(-1L));
    vinds0 = (int*)SCM_VELTS(inds0);
    inds1 = scm_make_uve(SCM_ARRAY_NDIM(ra0)+0L, SCM_MAKINUM(-1L));
    vinds1 = (int*)SCM_VELTS(inds1);
    incs1 = scm_make_uve(SCM_ARRAY_NDIM(ra0)+0L, SCM_MAKINUM(-1L));
    vincs1 = (int*)SCM_VELTS(incs1);
    mods1 = scm_make_uve(SCM_ARRAY_NDIM(ra0)+0L, SCM_MAKINUM(-1L));
    vmods1 = (int*)SCM_VELTS(mods1);
    kmax=SCM_ARRAY_NDIM(ra0);
    for (k = 0; k < kmax; k++) {
	vinds0[k] = SCM_ARRAY_DIMS(ra0)[k].lbnd;
	vinds1[k] = SCM_ARRAY_DIMS(ra1)[k].lbnd;
	vmods1[k] = SCM_ARRAY_DIMS(ra1)[k].ubnd - vinds1[k] + 1;
	vincs1[k] = SCM_ARRAY_DIMS(ra1)[k].inc;
	vinds1[k] -= SCM_INUM(SCM_CAR(offsets));
	offsets = SCM_CDR(offsets);
    }
    k = kmax;
    d = 0.0;
    the_dims = SCM_ARRAY_DIMS(ra0);
    switch (kmax) {
    case 1:
	for (vinds0[0] = the_dims[0].lbnd;
	     vinds0[0] <= the_dims[0].ubnd; vinds0[0]++)
	    d += e0[cind(ra0, vinds0)] * (*ptrmodind(kmax, vinds1, vmods1, vincs1, vinds0, e1));
	break;
    case 2:
	for (vinds0[1] = the_dims[1].lbnd;
	     vinds0[1] <= the_dims[1].ubnd; vinds0[1]++)
	    for (vinds0[0] = the_dims[0].lbnd;
	     vinds0[0] <= the_dims[0].ubnd; vinds0[0]++)
		d += e0[cind(ra0, vinds0)] * (*ptrmodind(kmax, vinds1, vmods1, vincs1, vinds0, e1));
	break;
    case 3:
	for (vinds0[2] = the_dims[2].lbnd;
	     vinds0[2] <= the_dims[2].ubnd; vinds0[2]++)
	    for (vinds0[1] = the_dims[1].lbnd;
		 vinds0[1] <= the_dims[1].ubnd; vinds0[1]++)
		for (vinds0[0] = the_dims[0].lbnd;
		     vinds0[0] <= the_dims[0].ubnd; vinds0[0]++)
		    d += e0[cind(ra0, vinds0)] * (*ptrmodind(kmax, vinds1, vmods1, vincs1, vinds0, e1));
	break;
    default:
	do {
	    if (k == kmax) {
		d += e0[cind(ra0, vinds0)] * (*ptrmodind(kmax, vinds1, vmods1, vincs1, vinds0, e1));
		k--;
		continue;
	    }
	    if (vinds0[k] < the_dims[k].ubnd) {
		vinds0[k]++;
		k++;
		continue;
	    }
	    vinds0[k] = the_dims[k].lbnd - 1;
	    k--;
	} while (k >= 0);
	break;
    }
    return scm_makdbl(d, 0.0);
}



/****** scale_accumulate
 * NAME
 *   scale_accumulate
 * SYNOPSIS
 *   SCM scale_accumulate (SCM ra0, SCM ra1, SCM scale, SCM adjust_type,
 *                         SCM offsets)
 *   scale-accumulate-array! (gsubr)
 * FUNCTION
 *   Scale array ra0 and add it to ra1, obeying offsets and adjust_type.
 *   adjust_type = 0 - wing
 *                 1 - clamp
 *                 2 - mirror, without repeating last
 *                 3 - mirror, repeat last
 ******/

char s_scale_accumulate[]="scale-accumulate-array!";
SCM scale_accumulate (SCM ra0, SCM ra1, SCM scale,
		      SCM adjust_type, SCM offsets)
{
    typedef double* (*index_clamp_ptr)
	(int ndim, int *vinds1, int *vmods1,
	 int *vincs1, int *inds, double *e);
    index_clamp_ptr ptrmodind;
    double *e0, *e1, dscale;
    SCM r0, r1, inds0, inds1, incs1, mods1;
    int k, kmax, *vinds0, *vinds1, *vmods1, *vincs1;
    scm_array_dim *the_dims;

    SCM_ASSERT(SCM_NIMP(ra0) && (SCM_TYP7(ra0)==scm_tc7_dvect || (SCM_ARRAYP(ra0) &&
	   SCM_TYP7(SCM_ARRAY_V(ra0)) == scm_tc7_dvect)), ra0, SCM_ARG1, s_scale_accumulate);
    SCM_ASSERT(SCM_NIMP(ra1) && (SCM_TYP7(ra1)==scm_tc7_dvect || (SCM_ARRAYP(ra1) &&
	   SCM_TYP7(SCM_ARRAY_V(ra1)) == scm_tc7_dvect)), ra1, SCM_ARG2, s_scale_accumulate);
    SCM_ASSERT(scm_ilength(offsets)==(SCM_ARRAYP(ra1) ? SCM_ARRAY_NDIM(ra1) : 1),
	   offsets, SCM_ARG5, s_scale_accumulate);
    SCM_ASSERT(SCM_INUMP(adjust_type), adjust_type, SCM_ARG4, s_scale_accumulate);
    SCM_ASSERT(SCM_NIMP(scale) && SCM_REALP(scale), scale, SCM_ARG5,
	       s_scale_accumulate);

    switch (SCM_INUM(adjust_type)) {
    case 0: ptrmodind = cmodind; break;
    case 1: ptrmodind = cclampind; break;
    case 2: ptrmodind = cmirrind; break;
    case 3: ptrmodind = cmirrind2; break;
    default: SCMERROR(adjust_type, SCM_ARG3, s_arrdot_per);
    }

    if (!SCM_ARRAYP(ra0)) {
	r0 = scm_make_ra(1);
	SCM_ARRAY_DIMS(r0)->lbnd = 0;
	SCM_ARRAY_DIMS(r0)->ubnd = SCM_LENGTH(ra0) - 1;
	SCM_ARRAY_DIMS(r0)->inc = 1;
	SCM_ARRAY_BASE(r0) = 0;
	SCM_ARRAY_V(r0) = ra0;
	ra0 = r0;
    }
    if (!SCM_ARRAYP(ra1)) {
	r1 = scm_make_ra(1);
	SCM_ARRAY_DIMS(r1)->lbnd = 0;
	SCM_ARRAY_DIMS(r1)->ubnd = SCM_LENGTH(ra1) - 1;
	SCM_ARRAY_DIMS(r1)->inc = 1;
	SCM_ARRAY_BASE(r1) = 0;
	SCM_ARRAY_V(r1) = ra1;
	ra1 = r1;
    }
    SCM_ASSERT(SCM_ARRAY_NDIM(ra0) == SCM_ARRAY_NDIM(ra1), ra1, SCM_ARG2, s_arrdot_per);
    e0 = (double*)SCM_VELTS(SCM_ARRAY_V(ra0));
    e1 = (double*)SCM_VELTS(SCM_ARRAY_V(ra1)) + SCM_ARRAY_BASE(ra1);
    dscale = SCM_REALPART(scale);

    inds0 = scm_make_uve(SCM_ARRAY_NDIM(ra0)+0L, SCM_MAKINUM(-1L));
    vinds0 = (int*)SCM_VELTS(inds0);
    inds1 = scm_make_uve(SCM_ARRAY_NDIM(ra0)+0L, SCM_MAKINUM(-1L));
    vinds1 = (int*)SCM_VELTS(inds1);
    incs1 = scm_make_uve(SCM_ARRAY_NDIM(ra0)+0L, SCM_MAKINUM(-1L));
    vincs1 = (int*)SCM_VELTS(incs1);
    mods1 = scm_make_uve(SCM_ARRAY_NDIM(ra0)+0L, SCM_MAKINUM(-1L));
    vmods1 = (int*)SCM_VELTS(mods1);
    kmax=SCM_ARRAY_NDIM(ra0);
    for (k = 0; k < kmax; k++) {
	vinds0[k] = SCM_ARRAY_DIMS(ra0)[k].lbnd;
	vinds1[k] = SCM_ARRAY_DIMS(ra1)[k].lbnd;
	vmods1[k] = SCM_ARRAY_DIMS(ra1)[k].ubnd - vinds1[k] + 1;
	vincs1[k] = SCM_ARRAY_DIMS(ra1)[k].inc;
	vinds1[k] -= SCM_INUM(SCM_CAR(offsets));
	offsets = SCM_CDR(offsets);
    }
    k = kmax;
    the_dims = SCM_ARRAY_DIMS(ra0);
    switch (kmax) {
    case 1:
	for (vinds0[0] = the_dims[0].lbnd;
	     vinds0[0] <= the_dims[0].ubnd; vinds0[0]++)
	    *ptrmodind(kmax, vinds1, vmods1, vincs1, vinds0, e1) += dscale * e0[cind(ra0, vinds0)];
	break;
    case 2:
	for (vinds0[1] = the_dims[1].lbnd;
	     vinds0[1] <= the_dims[1].ubnd; vinds0[1]++)
	    for (vinds0[0] = the_dims[0].lbnd;
	     vinds0[0] <= the_dims[0].ubnd; vinds0[0]++)
		*ptrmodind(kmax, vinds1, vmods1, vincs1, vinds0, e1) += dscale * e0[cind(ra0, vinds0)];
	break;
    case 3:
	for (vinds0[2] = the_dims[2].lbnd;
	     vinds0[2] <= the_dims[2].ubnd; vinds0[2]++)
	    for (vinds0[1] = the_dims[1].lbnd;
		 vinds0[1] <= the_dims[1].ubnd; vinds0[1]++)
		for (vinds0[0] = the_dims[0].lbnd;
		     vinds0[0] <= the_dims[0].ubnd; vinds0[0]++)
		    *ptrmodind(kmax, vinds1, vmods1, vincs1, vinds0, e1) += dscale * e0[cind(ra0, vinds0)];
	break;
    default:
	do {
	    if (k == kmax) {
		*ptrmodind(kmax, vinds1, vmods1, vincs1, vinds0, e1) += dscale * e0[cind(ra0, vinds0)];
		k--;
		continue;
	    }
	    if (vinds0[k] < the_dims[k].ubnd) {
		vinds0[k]++;
		k++;
		continue;
	    }
	    vinds0[k] = the_dims[k].lbnd - 1;
	    k--;
	} while (k >= 0);
	break;
    }
    return SCM_UNSPECIFIED;
}



/****** fft
 * NAME
 *   fft
 * SYNOPSIS
 *   void fft (double *xr, double *xi, int n, int dir)
 * FUNCTION
 *   Fast Fourier Transform the data. xr and xi are real and imaginary
 *   components of the vector, n is the number of elements in the vector
 *   (it must be power of 2), and dir is direction (non zero -> inverse
 *   transform. Result is stored in the same vectors.
 ******/

void fft (double *xr, double *xi, int n, int dir)
{
    long i,i1,j,k,n2,l1,l2;
    double c1,s1,tr,ti,ur,ui,c2,s2,t;

    n2 = n >> 1;
    j = 0;
    for (i=0; i<n-1; i++) {
	if (i < j) {
	    tr = xr[i];
	    ti = xi[i];
	    xr[i] = xr[j];
	    xi[i] = xi[j];
	    xr[j] = tr;
	    xi[j] = ti;
	}
	k = n2;
	while (k <= j) {
	    j -= k;
	    k >>= 1;
	}
	j += k;
    }

    c1 = -1.0; 
    s1 = 0.0;
    for (l2=1; l2<n;) {
	l1 = l2;
	l2 <<= 1;
	c2 = 1.0;
	s2 = 0.0;
	for (j=0; j<l1; j++) {
	    for (i=j; i<n; i+=l2) {
		i1 = i + l1;
		ur = c2 * xr[i1] - s2 * xi[i1];
		ui = c2 * xi[i1] + s2 * xr[i1];
		xr[i1] = xr[i] - ur; 
		xi[i1] = xi[i] - ui;
		xr[i] += ur;
		xi[i] += ui;
	    }
	    t =  c2 * c1 - s2 * s1;
	    s2 = c2 * s1 + s2 * c1;
	    c2 = t;
	}
	s1 = sqrt((1.0 - c1) / 2.0);
	if (dir) 
	    s1 = -s1;
	c1 = sqrt((1.0 + c1) / 2.0);
    }

    if (dir) {
	for (i=0; i<n; i++) {
	    xr[i] /= n;
	    xi[i] /= n;
	}
    }
}

/****** fft1
 * NAME
 *   fft1
 * SYNOPSIS
 *   SCM fft1 (SCM ra, SCM dir)
 *   fft! (subr2)
 * FUNCTION
 *   FFT the 1-dimensional array in direction dir (SCM_BOOL_T for inverse).
 ******/

/* SUBR (subr2) */
char s_fft1[]="fft!";
SCM fft1 (SCM ra, SCM dir)
{
    int n, n1, i, b;
    double *re, *im, (*p)[2], (*p1)[2];

    SCM_ASSERT(SCM_NIMP(ra) && (SCM_TYP7(ra)==scm_tc7_cvect ||
			(SCM_ARRAYP(ra) &&
			 SCM_TYP7(SCM_ARRAY_V(ra)) == scm_tc7_cvect &&
			 SCM_ARRAY_NDIM(ra)==1)),
	   ra, SCM_ARG1, s_fft1);

    if (SCM_TYP7(ra) == scm_tc7_cvect) {
	n = SCM_LENGTH(ra);
	p = (double (*)[2])SCM_VELTS(ra);
	b = 1;
    } else {
	n = SCM_ARRAY_DIMS(ra)[0].ubnd - SCM_ARRAY_DIMS(ra)[0].lbnd + 1;
	p = (double (*)[2])SCM_VELTS(SCM_ARRAY_V(ra)) + SCM_ARRAY_BASE(ra) + 
	    SCM_ARRAY_DIMS(ra)[0].lbnd;
	b = SCM_ARRAY_DIMS(ra)[0].inc;
    }
    for (n1=1; n1<n;) n1 <<= 1;
    re = NEWARRAY(double, n1);
    im = NEWARRAY(double, n1);
    for (p1=p,i=0; i<n; i++) {
	re[i] = (*p1)[0];
	im[i] = (*p1)[1];
	p1 += b;
    }
    for (; i<n1; i++)
	re[i] = im[i] = 0;

    fft (re, im, n1, dir == SCM_BOOL_T);

    for (p1=p,i=0; i<n; i++) {
	(*p1)[0] = re[i];
	(*p1)[1] = im[i];
	p1 += b;
    }
    free (re);
    free (im);
    return SCM_UNSPECIFIED;
}

void init_mathutils_c (void)
{ 
    int i;

    /* __PERL__ This code is automatically generated. Don't modify. */

    static scm_iproc lsubrs[] = {
        {s_fnoise, fnoise_lsubr},
        {s_fnoise3, fnoise3_lsubr},
        {0,0}
    };

    static scm_iproc subr0s[] = {
        {s_random, random_subr},
        {0,0}
    };

    static scm_iproc subr1s[] = {
        {s_matinvert, minvert_subr1},
        {s_noise, noise_subr1},
        {s_snoise3, snoise3_subr1},
        {s_snoise, snoise_subr1},
        {0,0}
    };

    static scm_iproc subr2s[] = {
        {s_lspline, lspline_subr2},
        {s_arrdot, array_dot},
        {s_fft1, fft1},
        {0,0}
    };

    scm_init_iprocs (lsubrs, scm_tc7_lsubr);
    scm_init_iprocs (subr0s, scm_tc7_subr_0);
    scm_init_iprocs (subr1s, scm_tc7_subr_1);
    scm_init_iprocs (subr2s, scm_tc7_subr_2);

    /* __END__ */

    scm_make_gsubr(s_arrdot_per,4,0,0,arrdot_per);
    scm_make_gsubr(s_scale_accumulate,5,0,0,scale_accumulate);

    srand48 (128198273);
    for (i=0; i<SRTABLE_SIZE; i++)
	srtable[i]=drand48();
}
