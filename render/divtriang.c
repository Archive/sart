
static int divs;

double *recurse_offsets (int d, double *data)
{
    if (d) {
	int n=1<<d;
	double *mins=data;
	double *maxs=data+4*d*d;
	double *newmins=data+8*d*d;
	double *newmaxs=data+9*d*d;

	for (i=0; i<n; i++) {
	    int k1=4*i*i;
	    int k2=k2+4*i+1;
	    int kk=i*i;

	    for (j=0; j<2*i+1; j++) {

		int k=2*j;
		double min, max;

		min=mins[k1+k];
		if (mins[k2+k]<min) min=mins[k2+k];
		if (mins[k2+k+1]<min) min=mins[k2+k+1];
		if (mins[k2+k+2]<min) min=mins[k2+k+2];

		max=maxs[k1+k];
		if (maxs[k2+k]>max) max=maxs[k2+k];
		if (maxs[k2+k+1]>max) max=maxs[k2+k+1];
		if (maxs[k2+k+2]>max) max=maxs[k2+k+2];

		newmins[kk+j]=min;
		newmaxs[kk+j]=max;
	    }
	}
	return recurse_offsets (d-1, data+10*d*d);
    }
    return data;
}

int disptable_size (int d)
{
    int i,s, n=1<<d;

    for (i=0, s=(n+1)*(n+2)/2; i<n; i++) n/=4, s+=n*n*2;
    return s;
}

double (*dispfunc_type) (double, double);
double *create_displacements (int d, double *data, dispfunc_type f)
{
    int n=1<<d;
    int i,j;
    double *ld=data, *ld1, *ld2;

    for (i=0; i<=n; i++)
	for (j=0; j<=i; j++)
	    *(ld++)=f((double)i,i==0 ? 0.0 : (double)j/i);

    ld1=ld;
    ld2=ld+n*n;
    for (i=0; i<n; i++) {
	for (j=0; j<2*i+1; j++) {
	    double p1, p2, p3, min, max;
	    int k1=i*(i+1)/2;
	    int k2=(i+1)*(i+2)/2;
	    if (j&1==0) { /* even triangles*/
		p1=data[k1+(j/2)];
		p2=data[k2+(j/2)];
		p3=data[k2+(j/2)+1];
	    } else {
		p1=data[k1+(j/2)];
		p2=data[k2+(j/2)+1];
		p3=data[k1+(j/2)+1];
	    }
	    min=p1;
	    if (p2<min) min=p2;
	    if (p3<min) min=p3;
	    max=p1;
	    if (p2>max) max=p2;
	    if (p3>max) max=p3;
	    *(ld1++)=min;
	    *(ld2++)=max;
	}
    }
    return recurse_offsets(d-1,ld1);
}
