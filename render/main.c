#include <libguile.h>
#include "libpath.h"

static void inner_main (void *closure, int argc, char **argv)
{
    char **new_argv;
    SCM parsed_opts;
    SCM *load_path, path;

    init_sart();

    new_argv = scm_get_meta_args (argc, argv);
    if (new_argv) {
	argv = new_argv;
	argc = scm_count_argv(new_argv);
    }

    parsed_opts = scm_compile_shell_switches(argc, argv);
    load_path = SCM_CDRLOC(scm_sysintern0("%load-path"));
    path = scm_cons(scm_makfrom0str(SART_DATA_DIR),*load_path);
    *load_path = path;
    scm_primitive_load(scm_makfrom0str(SART_INIT_FILE));
    exit(scm_exit_status(scm_eval_x(parsed_opts)));
}

int main (int argc, char **argv)
{
    scm_boot_guile (argc, argv, inner_main, 0);
    return 0;
}
