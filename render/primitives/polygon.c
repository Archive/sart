#include "primitive.h"

void Dump_Polygon (GeomObj *p)
{
    int i;

    printf ("#<polygon");
    for (i=0; i<p->var.polygon.nverts; i++) {
	printf (" (%f %f %f)", p->var.polygon.verts[i].x, p->var.polygon.verts[i].y, p->var.polygon.verts[i].z);
	if (i<p->var.polygon.nverts-1) printf(", ");
    }
    printf (">");
}

void Vector_Polygon (GeomObj *p, VectorOutputHook f)
{
    int i;
    Point3 *p1, *p2;
    GeomPoly *poly;

    poly=&p->var.polygon;
    for (i=poly->nverts, p1=poly->verts; i>0; i--,p1++) {
	p2= i==1 ? poly->verts : p1+1;
	f (p1, p2);
    }
}

void ZBufferP_Polygon (GeomObj *p)
{
    zb_persp_poly (&p->var.polygon, p->id);
}

boolean RayInt_Polygon (Ray *ray, GeomObj *o, double *dist,
			double maxdist, Raystruct *r1,
			GeomObj **return_o)
{
    return RayPolygonIntersection (ray, &o->var.polygon, dist);
}

int Eval_Polygon (GeomObj *obj, Raystruct *r, NData **nd, Point3 *section)
{
    *nd=NULL;
    r->normal=obj->var.polygon.normal;
    r->pat_pos=*section;
    r->pat_n=r->normal;
    return 1;
}

int Free_Polygon (GeomObj *o)
{
    int size;

    size=o->var.polygon.nverts*sizeof(Point3);
    free(o->var.polygon.verts);
    size+=sizeof(GeomObj)-sizeof(GeomPrimitive)+sizeof(GeomPoly);
    free(o);
    return size;
}

GeomObj *Copy_Polygon (GeomObj *in)
{
    GeomObj *out;

    out = NEWGEOM(GeomPoly);
    memcpy (out,in,sizeof(GeomObj)-sizeof(GeomPrimitive)+sizeof(GeomPoly));
    out->var.polygon.verts=MNEWARRAY(Point3,out->var.polygon.nverts);
    memcpy(out->var.polygon.verts,in->var.polygon.verts,
	   out->var.polygon.nverts*sizeof(Point3));
    return out;
}

void Bind_Polygon (GeomObj *p)
{
    Point3 min, max;
    int i;
    GeomPoly *poly=&p->var.polygon;
	
    min=poly->verts[0];
    max=poly->verts[0];
    for (i=1; i<poly->nverts; i++) {
	if (min.x>poly->verts[i].x) min.x=poly->verts[i].x;
	if (min.y>poly->verts[i].y) min.y=poly->verts[i].y;
	if (min.z>poly->verts[i].z) min.z=poly->verts[i].z;
	if (max.x<poly->verts[i].x) max.x=poly->verts[i].x;
	if (max.y<poly->verts[i].y) max.y=poly->verts[i].y;
	if (max.z<poly->verts[i].z) max.z=poly->verts[i].z;
    }
    p->min=min;
    p->max=max;
}

void Transform_Polygon (GeomObj *p, Matrix4 *m)
{
    int i;

    for (i = 0; i < p->var.polygon.nverts; i++) {
	Point3 t;

	V3MulPointByMatrix(p->var.polygon.verts+i, m, &t);
	p->var.polygon.verts[i] = t;
    }
    get_normal (&p->var.polygon);
    get_minmax (&p->var.polygon);
    BindPrimitive (p);
}

boolean Box_Polygon (GeomObj *obj, Point3 *min, Point3 *max)
{
    int sgn1,i;
    GeomPoly *o = &obj->var.polygon;
    Point3 p;
    double dist;

    /* Check whether the polygon plane intersects the box */
	
    p = *min;
    dist = V3Dot(&o->normal, &p) + o->ndist;
    if (dist == 0.0)
	goto sect;
    sgn1 = dist >= 0;
    p.x=max->x;
    dist = V3Dot(&o->normal, &p) + o->ndist;
    if (dist == 0.0 || sgn1 != (dist >= 0))
	goto sect;
    p.y=max->y;
    dist = V3Dot(&o->normal, &p) + o->ndist;
    if (dist == 0.0 || sgn1 != (dist >= 0))
	goto sect;
    p.x=min->x;
    dist = V3Dot(&o->normal, &p) + o->ndist;
    if (dist == 0.0 || sgn1 != (dist >= 0))
	goto sect;
    p.z=max->z;
    dist = V3Dot(&o->normal, &p) + o->ndist;
    if (dist == 0.0 || sgn1 != (dist >= 0))
	goto sect;
    p.x=max->x;
    dist = V3Dot(&o->normal, &p) + o->ndist;
    if (dist == 0.0 || sgn1 != (dist >= 0))
	goto sect;
    p.y=min->y;
    dist = V3Dot(&o->normal, &p) + o->ndist;
    if (dist == 0.0 || sgn1 != (dist >= 0))
	goto sect;
    p.x=min->x;
    dist = V3Dot(&o->normal, &p) + o->ndist;
    if (dist == 0.0 || sgn1 != (dist >= 0))
	goto sect;

    return FALSE;

sect: /* intersection test succesful. Now check whether any of the poly
	 edges intersect with the box */

    for (i=0; i<o->nverts-1; i++)
	if (LineBoxIntersect (o->verts+i,o->verts+i+1,min,max))
	    return TRUE;
    if (LineBoxIntersect (o->verts+i, o->verts, min, max))
	return TRUE;
	
    /* No intersections, now check the intersection with the line between
       two box verts and the poly. If no intersection is detected, they
       are disjoint. Line connects two vertices on the different sides
       of the polygon plane */

    return LinePolyIntersect (min, &p, o);
}
