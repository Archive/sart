#include "primitive.h"

#define CACHE_SIZE 3251

typedef struct {
    int node;
    void *container;
    Point3 value;
} PointCacheEntry;

static PointCacheEntry pos_cache[CACHE_SIZE];
static PointCacheEntry normal_cache[CACHE_SIZE];

typedef struct {
    int node;
    void *container;
    Point3 p[3];
    Point3 n;
    double d;
} TriangleEntry;

static cache_touched;

static TriangleEntry trian_cache[CACHE_SIZE];

#define H_HASH(x,y) ((unsigned)(839491903*(int)(x)+235566293*(y))%CACHE_SIZE)

void startup_hyper(void)
{
    int i;

    for (i=0; i<CACHE_SIZE; i++)
	trian_cache[i].node=pos_cache[i].node=normal_cache[i].node=-12873123;
    cache_touched = 0;
}

void hyper_verts (int *lines, int n, int t, int *v1, int *v2, int *v3)
{
    int mid = n * (n - 1) / 2;
    int line;

    if (t < mid) {
	/* This is an 'upright' triangle */

	line = lines[t];
	*v1 = t;
	*v2 = *v1 + line + 1;
	*v3 = *v2 + 1;
    }
    else {
	line = lines[t - mid] + 1;
	*v1 = t - mid + line;
	*v3 = *v1 + 1;
	*v2 = *v3 + line + 1;
    }
}

void hyper_vertex (int v, GeomHyper *h, Point3 *p)
{
    int line, hash, offs;
    double d1, d2, d3;

    hash = H_HASH(h,v);
    if (cache_touched)
	startup_hyper();
    if (pos_cache[hash].node == v && pos_cache[hash].container == h) {
	*p = pos_cache[hash].value;
	return;
    }

    line = h->lines[v];
    offs = v - line * (line + 1) / 2;

    d1 = (double)line/(double)(h->n - 1);
    d3 = line ? (double)offs/(double)line : 0;
    d2 = 1 - d3;
    d2 *= d1;
    d3 *= d1;
    d1 = 1 - d1;

    p->x = h->offsets[v]*(h->n1.x*d1 + h->n2.x*d2 + h->n3.x*d3)
	   +              h->v1.x*d1 + h->v2.x*d2 + h->v3.x*d3;
    p->y = h->offsets[v]*(h->n1.y*d1 + h->n2.y*d2 + h->n3.y*d3)
	   +              h->v1.y*d1 + h->v2.y*d2 + h->v3.y*d3;
    p->z = h->offsets[v]*(h->n1.z*d1 + h->n2.z*d2 + h->n3.z*d3)
	   +              h->v1.z*d1 + h->v2.z*d2 + h->v3.z*d3;
    pos_cache[hash].node = v;
    pos_cache[hash].container = h;
    pos_cache[hash].value = *p;
}

int boundary_index (int *lines, int n, int v)
{
    int line, k;

    if (!v) return 0;
    line = lines[v];
    k = v - line * (line + 1) / 2;
    if (line == n - 1) return 2 * line - 1 + k;
    if (k == 0) return 2 * line - 1;
    if (k == line) return 2 * line;
    return -1;
}

/****** hyperderivative
 * NAME
 *   hyperderivative
 * SYNOPSIS
 *   void hyperderivative (int v, GeomHyper *h, Point3 *n)
 * FUNCTION
 *   Returns the derivative for the vertex v of Hypertexture polygon.
 *   The result is stored in n.
 ******/

void hyper_derivative (int v, GeomHyper *h, Point3 *n)
{
    int n1, n2, n3, m1, m2, m3, bound;
    int line, hash, offs;
    Point3 p1, p2, p3, q1, q2, q3, d1, d2, d3, nr1, nr2, nr3;

    hash = H_HASH(h,v); 
    if (cache_touched)
	startup_hyper();
    if (normal_cache[hash].node == v && normal_cache[hash].container == h) {
	*n = normal_cache[hash].value;
	return;
    }

    /*
      ni = previous vertex in each direction
      mi = next vertex in each direction
    */

    if (h->boundary_normals &&
	(bound = boundary_index(h->lines, h->n, v)) >= 0)
	n = &h->boundary_normals[bound];
    else {
	line = h->lines[v];
	offs = v - (line * (line + 1) / 2);

	n1 = offs > 0 ? v-1 : v;
	m1 = offs < line ? v+1 : v;
	n2 = line > 0 && offs > 0 ? v-line-1 : v;
	m2 = line < h->n-1 ? v+line+2 : v;
	n3 = line > 0 && offs < line ? v-line : v;
	m3 = line < h->n-1 ? v+line+1 : v;

	hyper_vertex (n1, h, &p1);
	hyper_vertex (n2, h, &p2);
	hyper_vertex (n3, h, &p3);
	hyper_vertex (m1, h, &q1);
	hyper_vertex (m2, h, &q2);
	hyper_vertex (m3, h, &q3);

	V3Sub (&q1, &p1, &d1);
	V3Sub (&q2, &p2, &d2);
	V3Sub (&q3, &p3, &d3);
	V3Cross (&d1, &d2, &nr1);
	V3Cross (&d2, &d3, &nr2);
	V3Cross (&d1, &d3, &nr3);
	V3Normalize (&nr1);
	V3Normalize (&nr2);
	V3Normalize (&nr3);
	V3Add(&nr1, &nr2, n);
	V3Add(n, &nr3, n);
	V3Normalize (n);
    }
    normal_cache[hash].node = v;
    normal_cache[hash].container = h;
    normal_cache[hash].value = *n;
}

/****** href
 * NAME
 *  href
 * SYNOPSIS
 *  static GeomHyper *href;  (static variable for module primitive.c)
 * FUNCTION
 *  The hypertexture which is being subdivided or checked for the ray
 *  intersection is globally passed through this pointer.
 ******** */

GeomHyper *href;

/****** check_hyper
 * NAME
 *  check_hyper
 * SYNOPSIS
 *  boolean check_hyper (Ray *ray, int tnum, double *dist,
 *                       double maxdist, Raystruct *r1,
 *                       int *return_o)
 * FUNCTION
 *  This call is passed in place of RayPrimitiveIntersection to
 *  RayTreeIntersect. Check that function for parameter/return
 *  description.
 * NOTES
 *  The hnum and return_o are integers (triangle indices). At this point
 *  it is implicitly assumed that sizeof (int) == sizeof (GeomObj*). This
 *  should work on most architectures, but is, strictly speaking, a bug.
 ********
 */

boolean check_hyper (Ray *ray, int tnum, double *dist,
		     double maxdist, Raystruct *r1,
		     int *return_o)
{
    int v1, v2, v3, hash;

    tnum -= 16;
    if (!ray)
	return (int)&href->rcache_ids[tnum];
    if (tnum == r1->ignore_id)
	return FALSE;
    *return_o = tnum+16;
    hash = H_HASH(href, tnum);
    if (cache_touched)
	startup_hyper();
    if (trian_cache[hash].node!=tnum || trian_cache[hash].container!=href) {
	Point3 d1, d2;
	
	trian_cache[hash].node = tnum;
	trian_cache[hash].container = href;
	hyper_verts (href->lines, href->n, tnum, &v1, &v2, &v3);
	hyper_vertex (v1, href, &(trian_cache[hash].p[0]));
	hyper_vertex (v2, href, &(trian_cache[hash].p[1]));
	hyper_vertex (v3, href, &(trian_cache[hash].p[2]));
	V3Cross(V3Sub(&(trian_cache[hash].p[1]),
		      &(trian_cache[hash].p[0]),&d1),
		V3Sub(&(trian_cache[hash].p[2]),
		      &(trian_cache[hash].p[0]),&d2),
		&(trian_cache[hash].n));
	trian_cache[hash].d = -V3Dot(&(trian_cache[hash].p[0]),
				     &(trian_cache[hash].n));
    }
    return RayTriangleIntersection (ray,
				    &(trian_cache[hash].p[0]),
				    &(trian_cache[hash].p[1]),
				    &(trian_cache[hash].p[2]), 
				    &(trian_cache[hash].n), 
				    trian_cache[hash].d, dist);
}

/****** HyperInBox
 * NAME
 *  HyperInBox
 * SYNOPSIS
 *  boolean HyperInBox (Point3 *min, Point3 *max, int obj)
 * FUNCTION
 *  This call is passed to BSP tree subdivision call in place of
 *  PrimitiveBoxTest. 
 *******
 */

boolean HyperInBox (Point3 *min, Point3 *max, int obj)
{
    int v1, v2, v3,hash;
    GeomObj o;

    obj-=16;
    hash = H_HASH(href, obj);
    if (cache_touched)
	startup_hyper();
    if (trian_cache[hash].node!=obj || trian_cache[hash].container!=href) {
	Point3 d1, d2;
	
	trian_cache[hash].node = obj;
	trian_cache[hash].container = href;
	hyper_verts (href->lines, href->n, obj, &v1, &v2, &v3);
	hyper_vertex (v1, href, &(trian_cache[hash].p[0]));
	hyper_vertex (v2, href, &(trian_cache[hash].p[1]));
	hyper_vertex (v3, href, &(trian_cache[hash].p[2]));
	V3Cross(V3Sub(&(trian_cache[hash].p[1]),
		      &(trian_cache[hash].p[0]),&d1),
		V3Sub(&(trian_cache[hash].p[2]),
		      &(trian_cache[hash].p[0]),&d2),
		&(trian_cache[hash].n));
	trian_cache[hash].d = -V3Dot(&(trian_cache[hash].p[0]),
				     &(trian_cache[hash].n));
    }
    o.var.type=TYPE_POLY;
    o.var.polygon.nverts=3;
    o.var.polygon.verts=trian_cache[hash].p;
    BindPrimitive (&o);
    if (min->x > o.max.x || max->x <= o.min.x) return FALSE;
    if (min->y > o.max.y || max->y <= o.min.y) return FALSE;
    if (min->z > o.max.z || max->z <= o.min.z) return FALSE;
    o.var.polygon.normal = trian_cache[hash].n;
    o.var.polygon.ndist = trian_cache[hash].d;
    get_minmax(&o.var.polygon);
    return PrimitiveBoxTest(min, max, &o);
}

int SetSceneID_Hyper (GeomObj *p, int n)
{
    p->id=n;
    n += (p->var.hyper.n-1)*(p->var.hyper.n-1);
    return n;
}

void Dump_Hyper (GeomObj *p)
{
    printf ("#<");
    printf ("hyper (%d subdivisions)>",p->var.hyper.n);
}

boolean RayInt_Hyper (Ray *ray, GeomObj *o, double *dist,
		      double maxdist, Raystruct *r1,
		      GeomObj **return_o)
{
    int tmp, obj;
    Point3 old_o, old_d;

    href=&o->var.hyper;
    if (SCM_INUMP(r1->object)) {
	int n;

	n = SCM_INUM(r1->object) - o->id;
	tmp = check_hyper ((Ray*)r1, n+16, dist, BIG, r1, &obj);
    }
    else {
	r1->ignore_id -= o->id;
	tmp=RayTreeIntersect(r1,href->tree,(GeomObj**)&obj,dist,check_hyper);
	r1->ignore_id += o->id;
    }
    if (tmp && *dist>0 && *dist<maxdist) {
	r1->ignore_id_out = obj + o->id - 16;
	r1->ignore_sphere_out = RI_NOT_A_SPHERE;
	r1->nref = obj;
    }
    return tmp;
}

int Eval_Hyper (GeomObj *obj, Raystruct *r, NData **nd, Point3 *section)
{

    /* Smoothing is not yet supported */

    int ref=r->nref-16, hash;
    GeomHyper *t=&obj->var.hyper;
    GeomPoly trian;
    Point3 *hp[3], n[3], *np[3], s1;
    int v1, v2, v3;

    *nd=NULL;
    r->pat_pos=*section;

    hash = H_HASH(t, ref);
    hyper_verts (t->lines, t->n, ref, &v1, &v2, &v3);
    if (cache_touched)
	startup_hyper();
    if (trian_cache[hash].node!=ref || trian_cache[hash].container!=t) {
	Point3 d1, d2;
	
	trian_cache[hash].node = ref;
	trian_cache[hash].container = t;
	hyper_vertex (v1, t, &(trian_cache[hash].p[0]));
	hyper_vertex (v2, t, &(trian_cache[hash].p[1]));
	hyper_vertex (v3, t, &(trian_cache[hash].p[2]));
	V3Cross(V3Sub(&(trian_cache[hash].p[1]),
		      &(trian_cache[hash].p[0]),&d1),
		V3Sub(&(trian_cache[hash].p[2]),
		      &(trian_cache[hash].p[0]),&d2),
		&(trian_cache[hash].n));
	trian_cache[hash].d = -V3Dot(&(trian_cache[hash].p[0]),
				     &(trian_cache[hash].n));
    }
    /*
    trian.nverts=3;
    trian.verts=trian_cache[hash].p;
    trian.normal=trian_cache[hash].n;
    trian.ndist=trian_cache[hash].d;
    r->normal=trian.normal;
    */

    r->normal=trian_cache[hash].n;

    hp[0]=&(trian_cache[hash].p[0]);
    hp[1]=hp[0]+1;
    hp[2]=hp[1]+1;
    hyper_derivative (v1, t, &n[0]);
    hyper_derivative (v2, t, &n[1]);
    hyper_derivative (v3, t, &n[2]);
    np[0]=&n[0];
    np[1]=&n[1];
    np[2]=&n[2];
    smooth_triangle (hp, np, section, r);

    return 0;
}

void ZBufferP_Hyper (GeomObj *o)
{
    GeomHyper *h=&o->var.hyper;
    int i, k;
    GeomPoly p;
    int v[3];
    Point3 d[3];
    Matrix4 save;
    extern Matrix4 *global_trans;

    p.nverts=3;
    p.verts=d;

    for (i=0; i<(h->n-1)*(h->n-1); i++) {
	hyper_verts(h->lines, h->n, i, v, v+1, v+2);
	for (k=0; k<3; k++)
	    hyper_vertex(v[k], h, &d[k]);
	zb_persp_poly(&p, i+o->id);
    }
}

int Free_Hyper (GeomObj *o)
{
    int size, n;

    n=o->var.hyper.n;
    size=n*(n+1)/2*sizeof(double);
    free (o->var.hyper.offsets);
    size+=n*(n+1)/2*sizeof(int);
    free (o->var.hyper.lines);
    size+=(n-1)*(n-1)*sizeof(int);
    free (o->var.hyper.rcache_ids);
    size+=FreeBinTree (o->var.hyper.tree);
    size+=sizeof(GeomObj)-sizeof(GeomPrimitive)+sizeof(GeomHyper);
    if (o->var.hyper.boundary_normals) {
	size += (3 * n - 3) * sizeof(Point3);
	free(o->var.hyper.boundary_normals);
    }
    free (o);
    return size;
}

GeomObj *Copy_Hyper (GeomObj *in)
{
    GeomObj *out;
    int size, n, n1, n2, i, *oblist;
    GeomObjList l;

    out = NEWGEOM(GeomHyper);
    memcpy (out,in,sizeof(GeomObj)-sizeof(GeomPrimitive)+sizeof(GeomHyper));
    n = out->var.hyper.n;
    n1 = n*(n+1)/2;
    out->var.hyper.offsets = MNEWARRAY(double, n1);
    memcpy(out->var.hyper.offsets,
	   in->var.hyper.offsets,
	   n1*sizeof(double));
    out->var.hyper.lines = MNEWARRAY(int, n1);
    memcpy(out->var.hyper.lines,
	   in->var.hyper.lines,
	   n1*sizeof(int));
    n2 = (n-1)*(n-1);
    out->var.hyper.rcache_ids = MNEWARRAY(int, n2);
    memcpy(out->var.hyper.rcache_ids,
	   in->var.hyper.rcache_ids,
	   n2*sizeof(int));
    out->var.hyper.tree = NEWTYPE(BinTree);
    out->var.hyper.tree->min = in->var.hyper.tree->min;
    out->var.hyper.tree->max = in->var.hyper.tree->max;
    href = &out->var.hyper;
    oblist = NEWARRAY(int, n2);
    for (i = 0; i < n2; i++) {
	out->var.hyper.rcache_ids[i] = -1;
	oblist[i]=i+16;
    }
    l.list = (GeomObj**)oblist;
    l.length = n2;
    InitBinTree2(out->var.hyper.tree,&l,(geominbox_type)HyperInBox);
    if (out->var.hyper.boundary_normals) {
	out->var.hyper.boundary_normals = MNEWARRAY(Point3, 3 * n - 3);
	memcpy(out->var.hyper.boundary_normals,
	       in->var.hyper.boundary_normals,
	       (3 * n - 3) * sizeof(Point3));
    }
    return out;
}

void Bind_Hyper (GeomObj *p)
{
    p->min=p->var.hyper.tree->min;
    p->max=p->var.hyper.tree->max;
}

void Transform_Hyper (GeomObj *p, Matrix4 *m)
{
    GeomHyper *t = &p->var.hyper;
    Matrix4 m1, m2;
    Point3 min, max;
    Point3 v;
    int i, n, n1, n2, *oblist;
    GeomObjList l;

    cache_touched = 1;  /* transformation of a tessel invalidates the
			   way caching is handled, so everything will
			   have to rehash */
    V3InvertMatrix(m, &m1);
    TransposeMatrix4(&m1, &m2);
    n = t->n;
    n1 = n*(n+1)/2;
    n2 = (n-1)*(n-1);

    V3MulVectorByMatrix(&t->n1, m, &v); t->n1 = v;
    V3MulVectorByMatrix(&t->n2, m, &v); t->n2 = v;
    V3MulVectorByMatrix(&t->n3, m, &v); t->n3 = v;
    V3MulPointByMatrix(&t->v1, m, &v); t->v1 = v;
    V3MulPointByMatrix(&t->v2, m, &v); t->v2 = v;
    V3MulPointByMatrix(&t->v3, m, &v); t->v3 = v;
    if (t->boundary_normals) {
	Point3 p;

	for (i = 0; i < 3 * n - 3; i++) {
	    V3MulPointByMatrix(&t->boundary_normals[i], &m2, &p);
	    V3Normalize(&p);
	    t->boundary_normals[i] = p;
	}
    }
    min.x=min.y=min.z=BIG;
    max.x=max.y=max.z=-BIG;
    for (i = 0; i < n1; i++) {
	hyper_vertex(i, t, &v);
	if (v.x < min.x) min.x = v.x;
	if (v.y < min.y) min.y = v.y;
	if (v.z < min.z) min.z = v.z;
	if (v.x > max.x) max.x = v.x;
	if (v.y > max.y) max.y = v.y;
	if (v.z > max.z) max.z = v.z;
    }
    FreeBinTree (t->tree);
    t->tree = NEWTYPE(BinTree);
    t->tree->min = t->tree->min;
    t->tree->max = t->tree->max;
    href = t;
    oblist = NEWARRAY(int, n2);
    for (i = 0; i < n2; i++) {
	t->rcache_ids[i] = -1;
	oblist[i]=i+16;
    }
    l.list = (GeomObj**)oblist;
    l.length = n2;
    InitBinTree2(t->tree,&l,(geominbox_type)HyperInBox);
    Bind_Hyper(p);
}

struct box {
    Point3 min, max;
};

boolean check_h_box (GeomObj *ref, void *data)
{
    struct box *b=data;

    return !HyperInBox(&b->min, &b->max, (int)ref);
}

boolean Box_Hyper (GeomObj *p, Point3 *min, Point3 *max)
{
    struct box b;

    b.min = *min;
    b.max = *max;
    href = &p->var.hyper;
    return !BoxTreeCall (href->tree, &b.min, &b.max, check_h_box,
			 &b, check_hyper);
}
