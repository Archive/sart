#include "primitive.h"

GeomPatch *pref;

int SetSceneID_Patch (GeomObj *p, int n)
{
    p->id=n;
    n += p->var.patch.ntrians;
    return n;
}

void Dump_Patch (GeomObj *p)
{
    printf ("#<patch %x / %d verts, %d triangles>", p, p->var.patch.nverts,
	    p->var.patch.ntrians);
}

void patch_verts (GeomPatch *p, int obj, int *v1, int *v2, int *v3)
{
    int *k = p->verts + 3*obj;

    *v1 = k[0];
    *v2 = k[1];
    *v3 = k[2];
}

boolean check_patch (Ray *ray, int tnum, double *dist,
		     double maxdist, Raystruct *r1,
		     int *return_o)
{
    int v1, v2, v3;
    tnum -= 16;
    if (!ray)
	return (int)&pref->rcache_ids[tnum];
    if (tnum == r1->ignore_id)
	return FALSE;
    *return_o = tnum+16;
    patch_verts (pref, tnum, &v1, &v2, &v3);
    return RayTriangleIntersection (ray, pref->v+v1, pref->v+v2, pref->v+v3,
				    pref->normals+tnum, pref->distances[tnum],
				    dist);
}

boolean PatchInBox (Point3 *min, Point3 *max, int obj)
{
    int v1, v2, v3;
    GeomObj o;
    Point3 v[3];

    obj-=16;
    o.var.type=TYPE_POLY;
    o.var.polygon.nverts=3;
    o.var.polygon.verts=v;
    patch_verts (pref, obj, &v1, &v2, &v3);
    v[0]=pref->v[v1];
    v[1]=pref->v[v2];
    v[2]=pref->v[v3];
    BindPrimitive (&o);
    if (min->x > o.max.x || max->x <= o.min.x) return FALSE;
    if (min->y > o.max.y || max->y <= o.min.y) return FALSE;
    if (min->z > o.max.z || max->z <= o.min.z) return FALSE;
    o.var.polygon.normal=pref->normals[obj];
    o.var.polygon.ndist=pref->distances[obj];
    get_minmax(&o.var.polygon);
    return PrimitiveBoxTest(min, max, &o);
}

boolean RayInt_Patch (Ray *ray, GeomObj *o, double *dist,
		      double maxdist, Raystruct *r1,
		      GeomObj **return_o)
{
    int tmp, obj;

    pref=&o->var.patch;
    if (SCM_INUMP(r1->object)) {
	int n;

	n=SCM_INUM(r1->object) - o->id;
	if (tmp = check_patch(ray, n+16, dist, BIG, r1, &obj))
	    r1->nref=obj;
	return tmp;
    }

    r1->ignore_id -= o->id;
    tmp=RayTreeIntersect(r1, pref->tree, (GeomObj**)&obj, dist, check_patch);
    r1->ignore_id += o->id;
    if (tmp && *dist>0 && *dist<maxdist) {
	r1->ignore_id_out = obj + o->id - 16;
	r1->ignore_sphere_out = RI_NOT_A_SPHERE;
	r1->nref = obj;
    }
    return tmp;
}

int Eval_Patch (GeomObj *obj, Raystruct *r, NData **nd, Point3 *section)
{
    int ref=r->nref-16;
    GeomPatch *p = &(obj->var.patch);
    Point3 *vertices[3], *normals[3];
    int i, v[3];

    *nd=NULL;
    r->normal = p->normals[ref];
    r->pat_pos = *section;
    patch_verts (p, ref, v, v+1, v+2);
    vertices[0] = p->v + v[0];
    vertices[1] = p->v + v[1];
    vertices[2] = p->v + v[2];
    normals[0] = p->n + v[0];
    normals[1] = p->n + v[1];
    normals[2] = p->n + v[2];
    smooth_triangle (vertices, normals, section, r);
    return 0;
}

void ZBufferP_Patch (GeomObj *o)
{
    GeomPatch *e = &o->var.patch;
    GeomPoly p;
    Point3 d[3];
    int i, k, v[3];

    p.nverts = 3;
    p.verts = d;
    for (i = 0; i < e->ntrians; i++) {
	patch_verts(e, i, v, v+1, v+2);
	for (k = 0; k < 3; k++)
	    d[k] = e->v[v[k]];
	zb_persp_poly(&p, i+o->id);
    }
}

int Free_Patch (GeomObj *o)
{
    int size;
    int nv, nt;
    GeomPatch *p = &o->var.patch;

    size = sizeof(GeomObj)-sizeof(GeomPrimitive)+sizeof(GeomPatch);
    nv = p->nverts;
    nt = p->ntrians;
    size += 2 * nv * sizeof(Point3);
    free (p->v);
    free (p->n);
    size += 3 * nt * sizeof(int);
    free (p->verts);
    size += nt * (sizeof(Point3) + sizeof(double) + sizeof(int));
    free (p->normals);
    free (p->distances);
    free (p->rcache_ids);
    size += FreeBinTree (p->tree);
    free (o);
    return size;
}

GeomObj *Copy_Patch (GeomObj *in)
{
    GeomObj *out;
    GeomPatch *pin, *pout;
    int nv, nt;
    int i, *oblist;
    GeomObjList l;

    out = NEWGEOM(GeomPatch);
    memcpy (out,in,sizeof(GeomObj)-sizeof(GeomPrimitive)+sizeof(GeomPatch));
    pin = &in->var.patch;
    pout = &out->var.patch;
    nt = pin->ntrians;
    nv = pin->nverts;
    pout->v = MNEWARRAY(Point3, nv);
    memcpy(pout->v, pin->v, nv * sizeof (Point3));
    pout->n = MNEWARRAY(Point3, nv);
    memcpy(pout->n, pin->n, nv * sizeof (Point3));
    pout->verts = MNEWARRAY(int, nv * 3);
    memcpy(pout->verts, pin->verts, nv * 3 * sizeof(int));
    pout->normals = MNEWARRAY(Point3, nt);
    memcpy(pout->normals, pin->normals, nt * sizeof(Point3));
    pout->distances = MNEWARRAY(double, nt);
    memcpy(pout->distances, pin->distances, nt * sizeof(double));
    pout->rcache_ids = MNEWARRAY(int, nt);
    memcpy(pout->rcache_ids, pin->rcache_ids, nt * sizeof(int));
    oblist=NEWARRAY(int, nt);
    for (i = 0; i < nt; i++)
	oblist[i] = i+16;
    l.list = (GeomObj**)oblist;
    l.length = nt;
    pout->tree = NEWTYPE(BinTree);
    pout->tree->min = pin->tree->min;
    pout->tree->max = pin->tree->max;
    pref = pout;
    InitBinTree2(pout->tree, &l, (geominbox_type)PatchInBox);
    return out;
}

void Transform_Patch (GeomObj *p, Matrix4 *m)
{
    GeomPatch *t = &p->var.patch;
    Matrix4 tmp, m2;
    Point3 min, max;
    Point3 v;
    int verts, triangs, i, *oblist, nt, nv;
    GeomObjList l;
    double len;

    V3InvertMatrix(m, &tmp);
    TransposeMatrix4(&tmp, &m2);
    verts = t->nverts;
    triangs = t->ntrians;
    for (i = 0; i < verts; i++) {
	V3Normalize(V3MulVectorByMatrix(&t->n[i], &m2, &v));
	t->n[i] = v;
    }
    min.x=min.y=min.z=BIG;
    max.x=max.y=max.z=-BIG;
    for (i = 0; i < verts; i++) {
	V3MulPointByMatrix(&t->v[i], m, &v);
	t->v[i] = v;
	if (v.x < min.x) min.x = v.x;
	if (v.y < min.y) min.y = v.y;
	if (v.z < min.z) min.z = v.z;
	if (v.x > max.x) max.x = v.x;
	if (v.y > max.y) max.y = v.y;
	if (v.z > max.z) max.z = v.z;
    }
    for (i = 0; i < triangs; i++) {
	V3MulVectorByMatrix(&t->normals[i], &m2, &v);
	len = V3Length(&v);
	t->distances[i] /= len;
	t->normals[i] = *V3Scale(&v, 1/len);
    }
    FreeBinTree (t->tree);
    oblist=NEWARRAY(int, triangs);
    for (i = 0; i < triangs; i++)
	oblist[i] = i+16;
    l.list = (GeomObj**)oblist;
    l.length = triangs;
    t->tree = NEWTYPE(BinTree);
    t->tree->min = min;
    t->tree->max = max;
    pref = t;
    InitBinTree2(t->tree,&l,(geominbox_type)PatchInBox);
    Bind_Patch(p);
}

void Bind_Patch (GeomObj *p)
{
    p->min=p->var.patch.tree->min;
    p->max=p->var.patch.tree->max;
}

struct box {
    Point3 min, max;
};

boolean check_p_box (GeomObj *ref, void *data)
{
    struct box *b=data;

    return !PatchInBox(&b->min, &b->max, (int)ref);
}

boolean Box_Patch (GeomObj *p, Point3 *min, Point3 *max)
{
    struct box b;

    b.min = *min;
    b.max = *max;
    pref = &p->var.patch;
    return !BoxTreeCall (pref->tree, min, max, check_p_box,
			 &b, check_patch);
}
