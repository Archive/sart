#include "primitive.h"

int SetSceneID_Tree (GeomObj *p, int n)
{
    int i;

    p->id=n;
    for (i=0; i<p->var.tree.nscenes; i++)
	n += SetSceneIDs(SCENESTR(p->var.tree.scenes[i]));
    return n;
}

void Dump_Tree (GeomObj *p)
{
    printf ("#<tree>");
}


boolean RayInt_Tree (Ray *ray, GeomObj *o, double *dist,
		     double maxdist, Raystruct *r1,
		     GeomObj **return_o)
{
    int n;
    Matrix4 tmat;
    GeomTree *t=&o->var.tree;
    Point3 origin, dir, tmpp;
    SCM return_ray;
    Raystruct *retray;
    extern boolean find_intersecting_object (int,int,Point3*,Point3*,SCM,
					     GeomObj**,SCM *);

    /* Check for zbuffered query */
    if (SCM_INUMP(r1->object)) {
	Scene *s;
	GeomObj *g;
	Point3 sect;
	Raystruct *r;
	double d;
	int i, n1;
	extern int tc16_Ray;
	SCM c;

	/* Okay, there we are. Do the query... */
	n = SCM_INUM(r1->object) - o->id;
	i=0;
	while ((s=SCENESTR(t->scenes[i++]))->maxid <= n)
	    n -= s->maxid;
	n1 = retrieve_id(s, n);
	g = OBJSTR(s->object_list[n1]);

	r=newray();
	V3MulPointByMatrix(&r1->origin, &t->fmat, &r->origin);
	V3MulVectorByMatrix(&r1->direction, &t->fmat, &r->direction);
	r->scene=r1->scene;
	SCM_NEWCELL(c);
	SCM_CDR(c)=(SCM)r;
	SCM_CAR(c)=tc16_Ray;

	V3Normalize(&r->direction);
	if (!RayPrimitiveIntersection ((Ray*)r, g, &d,
				       BIG, r, return_o)) {
	    deleteray(c);
	    return FALSE;
	}
	if (o->material != SCM_BOOL_F)
	    r1->mat_override=o->material;
	else
	    r1->mat_override=r->mat_override;
	if (g->csg == SCM_BOOL_F) {
	    r1->csg_sector = o->csg;
	    r1->csg = get_entering_csg(o->csg);
	} else {
	    r1->csg_sector = g->csg;
	    r1->csg = get_entering_csg(g->csg);
	}
	PointAtDistance ((Ray*)r, d, &sect);
	V3MulPointByMatrix (&sect, &t->bmat, &r1->section);
	*dist=V3DistanceBetween2Points(&sect, &r1->origin);
	deleteray(c);
	return TRUE;
    }

    /* Check for the selector */
    if (t->selector!=SCM_BOOL_F) {
	/* this is a bit reckless, but then, value of n is checked */
	n=SCM_INUM(scm_apply(t->selector, global_ray, scm_listofnull));
	if (n<0 || n>=t->nscenes) {
	    fprintf (stderr,"WARNING: Invalid scene selection: %d\n",n);
	    n=0;
	}
    }
    else n=0;

    V3MulPointByMatrix(&ray->origin, &t->fmat, &origin);
    V3MulVectorByMatrix(&ray->direction, &t->fmat, &dir);
    V3Normalize(&dir);

    global_lock++;

    if (!find_intersecting_object(r1->ignore_id - o->id, r1->ignore_sphere,
				  &origin, &dir, t->scenes[n],
				  return_o, &return_ray)) {
	global_lock--;
	return FALSE;
    }

    global_lock--;
    retray=RAYSTR(return_ray);

    /* Now update all the transformations */


    V3MulPointByMatrix(&retray->section, &t->bmat, &tmpp);
    *dist=V3DistanceBetween2Points(&tmpp, &ray->origin);

    /* return some rdata */

    if (*dist>EPS && *dist<maxdist) {
	int i;

	r1->nref = retray->nref; /* This makes tessels work within trees */
	r1->ignore_id_out = retray->ignore_id_out + o->id;
	r1->ignore_sphere_out = retray->ignore_sphere_out;
	if (!r1->ndata) r1->ndata=NEWTYPE(NData);
	if (!retray->ndata) {
	    r1->ndata->fmat=t->fmat;
	    r1->ndata->bmat=t->bmat;
	}
	else {
	    V3MatMul(&t->fmat,&retray->ndata->fmat,&tmat);
	    r1->ndata->fmat=tmat;
	    V3MatMul(&retray->ndata->bmat,&t->bmat,&tmat);
	    r1->ndata->bmat=tmat;
	}
	if (o->material!=SCM_BOOL_F)
	    r1->mat_override=o->material;
	else
	    r1->mat_override=retray->mat_override;
	r1->csg=retray->csg;
    }
    deleteray(retray->parent);
    deleteray(return_ray);
    return TRUE;
}

/* Tree *never* gets evaluated, since the function above never returns
   the tree itself, so Eval_Tree is unnecessary. */

int Free_Tree (GeomObj *o)
{
    free (o->var.tree.scenes);
    free (o);
    return sizeof(GeomObj)-sizeof(GeomPrimitive)+sizeof(GeomHyper)
	+  o->var.tree.nscenes*sizeof(SCM);
}

void ZBufferP_Tree (GeomObj *o)
{

    GeomTree *t=&o->var.tree;
    int i, n;
    Matrix4 save;
    extern Matrix4 *global_trans;
    Scene *s;
    extern int global_id_increment;
    int gl_id;

    save=*global_trans;
    V3MatMul(&t->fmat,&save,global_trans);
    gl_id = global_id_increment;
    global_id_increment += o->id;
    if (t->selector!=SCM_BOOL_F) {
	n=SCM_INUM(scm_apply(t->selector, global_ray, scm_listofnull));
	if (n<0 || n>=t->nscenes) {
	    fprintf (stderr,"WARNING: Invalid scene selection: %d\n",n);
	    n=0;
	}
    }
    else n=0;

    s=SCENESTR(t->scenes[n]);

    for (i=0; i<s->n; i++)
	ZBufferPersp(OBJSTR(s->object_list[i]));

    *global_trans=save;
    global_id_increment = gl_id;
}

GeomObj *Copy_Tree (GeomObj *in)
{
    GeomObj *out;

    out = NEWGEOM(GeomTree);
    memcpy (out,in,sizeof(GeomObj)-sizeof(GeomPrimitive)+sizeof(GeomTree));
    out->var.tree.scenes=MNEWARRAY(SCM, out->var.tree.nscenes);
    memcpy (out->var.tree.scenes, in->var.tree.scenes,
	    out->var.tree.nscenes*sizeof(SCM));
    return out;
}

void Bind_Tree (GeomObj *p)
{
    Point3 min, max;
    GeomTree *t=&p->var.tree;
    Scene *s;
    Point3 min1, max1;
    int i;

    /* check all the scenes */
    min.x=min.y=min.z=BIG;
    max.x=max.y=max.z=-BIG;
    for (i=0; i<t->nscenes; i++) {
	s=SCENESTR(t->scenes[i]);
	TransformBox (&s->object_tree->min, &s->object_tree->max,
		      &min1, &max1, &t->bmat);
	if (min.x > min1.x) min.x = min1.x;
	if (min.y > min1.y) min.y = min1.y;
	if (min.z > min1.z) min.z = min1.z;
	if (max.x < min1.x) max.x = max1.x;
	if (max.y < min1.y) max.y = max1.y;
	if (max.z < min1.z) max.z = max1.z;
    }
    p->min=min;
    p->max=max;
}

void Transform_Tree (GeomObj *p, Matrix4 *m)
{
    GeomTree *t=&p->var.tree;
    Matrix4 m1, m2;

    V3InvertMatrix (m, &m1);
    V3MatMul (m, &t->bmat, &m2);
    t->bmat=m2;
    V3MatMul (&m1, &t->fmat, &m2);
    t->fmat=m2;
    BindPrimitive (p);
}

struct box {
    Point3 min, max;
};

boolean check_tree_box (GeomObj *ref, void *data)
{
    struct box *b=data;

    return !PrimitiveBoxTest(&b->min, &b->max, ref);
}

boolean Box_Tree (GeomObj *obj, Point3 *min, Point3 *max)
{
    /* This one is complicated... */
    GeomTree *t=&obj->var.tree;
    int i;
    Scene *s;
    struct box b;

    /* By now we know that the bounding box is intersected. Find if
       any object is intersected. This may be recursive */

    TransformBox (min, max, &b.min, &b.max, &t->fmat);
    for (i=0; i<t->nscenes; i++) {
	s=SCENESTR(t->scenes[i]);
	if (!BoxTreeCall (s->object_tree, &b.min, &b.max, check_tree_box, &b,
			  RayPrimitiveIntersection))
	    return TRUE;
    }
    return FALSE;
}

void Mark_Tree (GeomObj *p)
{
    int i;
    
    scm_gc_mark (p->var.tree.selector);
    for (i=0; i<p->var.tree.nscenes; i++) {
	scm_gc_mark (p->var.tree.scenes[i]);
    }
}
