
#include "primitive.h"

int SetSceneID_Simple (GeomObj *p, int n)
{
    p->id=n++;
    return n;
}

void Vector_Simple (GeomObj *p, VectorOutputHook f)
{
}

void ZBufferP_Simple (GeomObj *p)
{
}

void Mark_Simple (GeomObj *p)
{
}

PClass PrimitiveClass[]={
    {SetSceneID_Simple, Dump_Polygon, Vector_Polygon, ZBufferP_Polygon,
     RayInt_Polygon, Eval_Polygon, Free_Polygon, Copy_Polygon,
     Bind_Polygon, Transform_Polygon, Box_Polygon, Mark_Simple,
     TRUE},
    {SetSceneID_Simple, Dump_Point, Vector_Point, ZBufferP_Simple,
     RayInt_Point, Eval_Point, Free_Point, Copy_Point,
     Bind_Point, Transform_Point, Box_Point, Mark_Simple,
     TRUE},
    {SetSceneID_Sphere, Dump_Sphere, Vector_Simple, ZBufferP_Sphere,
     RayInt_Sphere, Eval_Sphere, Free_Sphere, Copy_Sphere,
     Bind_Sphere, Transform_Sphere, Box_Sphere, Mark_Simple,
     FALSE},
    {SetSceneID_Tree, Dump_Tree, Vector_Simple, ZBufferP_Tree,
     RayInt_Tree, NULL, Free_Tree, Copy_Tree,
     Bind_Tree, Transform_Tree, Box_Tree, Mark_Tree,
     FALSE},
    {SetSceneID_Tessel, Dump_Tessel, Vector_Simple, ZBufferP_Tessel,
     RayInt_Tessel, Eval_Tessel, Free_Tessel, Copy_Tessel,
     Bind_Tessel, Transform_Tessel, Box_Tessel, Mark_Simple,
     FALSE},
    {SetSceneID_Hyper, Dump_Hyper, Vector_Simple, ZBufferP_Hyper,
     RayInt_Hyper, Eval_Hyper, Free_Hyper, Copy_Hyper,
     Bind_Hyper, Transform_Hyper, Box_Hyper, Mark_Simple,
     FALSE},
    {SetSceneID_HF, Dump_HF, Vector_Simple, ZBufferP_HF,
     RayInt_HF, Eval_HF, Free_HF, Copy_HF,
     Bind_HF, Transform_HF, Box_HF, Mark_Simple,
     FALSE},
    {SetSceneID_Patch, Dump_Patch, Vector_Simple, ZBufferP_Patch,
     RayInt_Patch, Eval_Patch, Free_Patch, Copy_Patch,
     Bind_Patch, Transform_Patch, Box_Patch, Mark_Simple,
     FALSE}};

