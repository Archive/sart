#include "primitive.h"

GeomHF *href;

static int object_to_ignore;

static void hf_verts (GeomHF *t, int obj, int *v1, int *v2, int *v3)
{
    int n;
    int row, col;
    int sqnum;

    sqnum=obj/2;
    row=sqnum/(t->w-1);
    col=sqnum%(t->w-1);
    n=row*t->w+col;
    if ((obj&1)==0) {
	*v1=n;
	*v2=n+1;
	*v3=n+t->w;
    }
    else {
	*v1=n+1;
	*v3=n+t->w;
	*v2=*v3+1;
    }
}    

int SetSceneID_HF (GeomObj *p, int n)
{
    p->id=n;
    n += 2 * (p->var.hf.h - 1) * (p->var.hf.w - 1);
    return n;
}

void Dump_HF (GeomObj *p)
{
    printf ("#<heightfield %dx%d>",p->var.hf.w,p->var.hf.h);
}

static void get_hf_vert (GeomHF *h, int v, Point3 *out)
{
    int row, col;

    row = v / h->w;
    col = v % h->w;
    out->x = (double)col / (h->w - 1);
    out->y = (double)row / (h->h - 1);
    out->z = h->heights[v];
}

static boolean check_triangle_pair (GeomHF *h, Ray *ray, int left, int bot,
				    int *trian_out, double *dist)
{
    Point3 p1, p2, p3;
    int t0, t1;
    double d0, d1;
    int pos, n;

    pos = left + h->w*bot;
    n = 2 * (pos - bot);

    if (object_to_ignore != n) {
	get_hf_vert(h, pos, &p1);
	get_hf_vert(h, pos+1, &p2);
	get_hf_vert(h, pos+h->w, &p3);
	t0 = RayTriangleIntersection (ray, &p1, &p2, &p3, NULL, 0.0, &d0);
    }
    else
	t0 = FALSE;
    if (object_to_ignore != n + 1) {
	get_hf_vert(h, pos+1, &p1);
	get_hf_vert(h, pos+h->w, &p2);
	get_hf_vert(h, pos+h->w+1, &p3);
	t1 = RayTriangleIntersection (ray, &p1, &p2, &p3, NULL, 0.0, &d1);
    }
    else
	t1 = FALSE;
    if (t0 && d0 < EPS) t0 = FALSE;
    if (t1 && d1 < EPS) t1 = FALSE;
    if (t0 && t1 && (d0 > d1))
	t0 = FALSE;
    if (t0) {
	*trian_out = n;
	*dist = d0;
	return TRUE;
    }
    if (t1) {
	*trian_out = n + 1;
	*dist = d1;
	return TRUE;
    }
    return FALSE;
}

static boolean check_or_subdivide (GeomHF *h, Ray *ray, int left, int right,
				   int bot, int top, int offset,
				   int *trian_out, double *dist)
{
    double x1 = (double) left / (h->w - 1);
    double x2 = (double) right / (h->w - 1);
    double y1 = (double) bot / (h->h - 1);
    double y2 = (double) top / (h->h - 1);

    double vmin, vmax, a, b, lower, upper;
    Point3 *dir= &ray->direction, *origin= &ray->origin;
    int r, midx, midy;
    int *br;
    double fmidx, fmidy;

    if (dir->x!=0.0) {
	a=(x1-origin->x)/dir->x;
	b=(x2-origin->x)/dir->x;
	if (dir->x>0.0) vmin=a, vmax=b;
	else vmax=a, vmin=b;
	if (vmax<0) return FALSE;
    }
    else if (origin->x < x1 || origin->x > x2)
	return FALSE;
    else vmin= -1e8, vmax=1e8;
    if (dir->y!=0.0) {
	a=(y1-origin->y)/dir->y;
	b=(y2-origin->y)/dir->y;
	if (dir->y>0.0) { if (vmin<a) vmin=a; if (vmax>b) vmax=b; }
	else { if (vmin<b) vmin=b; if (vmax>a) vmax=a; }
	if (vmin>vmax || vmax<0) return FALSE;
    }
    else if (origin->y < y1 || origin->y > y2)
	return FALSE;

    lower = h->bounds[offset];
    upper = h->bounds[offset+1];
    if (dir->z!=0.0) {
	a=(lower-origin->z)/dir->z;
	b=(upper-origin->z)/dir->z;
	if (dir->z>0.0) { if (vmin<a) vmin=a; if (vmax>b) vmax=b; }
	else { if (vmin<b) vmin=b; if (vmax>a) vmax=a; }
        if (vmin>vmax || vmax<0) return FALSE;
    }
    else if (origin->z < lower || origin->z > upper)
	return FALSE;

    if (left == right-1 && bot == top-1) {
	/* done subdividing, now do the hands-on test */
	return check_triangle_pair(h, ray, left, bot, trian_out, dist);
    }

    /* now subdivide */

    midx = (left + right) / 2;
    midy = (bot + top) / 2;
    fmidx = (double) midx / (h->w - 1);
    fmidy = (double) midy / (h->h - 1);
    r = 0;
    if (origin->x > fmidx) r ++;
    if (origin->y > fmidy) r += 2;

    br = h->branches + offset*2;

#define CHECK0 \
    if (left < midx && bot < midy)                             \
	if (check_or_subdivide(h, ray, left, midx, bot, midy,  \
			       br[0], trian_out, dist))   \
	    return TRUE;                                       
#define CHECK1 \
    if (bot < midy)                                            \
	if (check_or_subdivide(h, ray, midx, right, bot, midy, \
			       br[1], trian_out, dist))   \
	    return TRUE;                                       
#define CHECK2 \
    if (left < midx)                                           \
	if (check_or_subdivide(h, ray, left, midx, midy, top,  \
			       br[2], trian_out, dist))   \
	    return TRUE;                                       
#define CHECK3 \
	if (check_or_subdivide(h, ray, midx, right, midy, top, \
			       br[3], trian_out, dist))   \
	    return TRUE;                                       

    switch (r) {
    case 0:
	CHECK0;
	 if (dir->x > 0) CHECK1;
	 if (dir->y > 0) {
	    CHECK2;
	     if (dir->x > 0) 
		CHECK3;
	}
	break;
    case 1:
	CHECK1;
	 if (dir->x < 0) CHECK0;
	 if (dir->y > 0) {
	    CHECK3;
	     if (dir->x < 0) 
		CHECK2;
	}
	break;
    case 2:
	CHECK2;
	if (dir->x > 0) CHECK3;
	if (dir->y < 0) {
	    CHECK0;
	    if (dir->x > 0)
		CHECK1;
	}
	break;
    case 3:
	CHECK3;
	if (dir->x < 0) CHECK2;
	if (dir->y < 0) {
	    CHECK1;
	    if (dir->x < 0)
		CHECK0;
	}
	break;
    }
    return FALSE;
}

static void get_hf_normal (GeomHF *h, int pos, Point3 *n)
{
    int x = pos % h->w;
    int y = pos / h->w;
    Point3 d1, d2, p1, p2;

    if (x == 0)
	get_hf_vert (h, pos, &p2);
    else
	get_hf_vert (h, pos - 1, &p2);
    if (x == h->w - 1)
	get_hf_vert (h, pos, &p1);
    else
	get_hf_vert (h, pos + 1, &p1);
    V3Sub(&p1, &p2, &d1);

    if (y == 0)
	get_hf_vert (h, pos, &p2);
    else
	get_hf_vert (h, pos - h->w, &p2);
    if (y == h->h - 1)
	get_hf_vert (h, pos, &p1);
    else
	get_hf_vert (h, pos + h->w, &p1);
    V3Sub(&p1, &p2, &d2);

    V3Cross(&d1, &d2, n);
}

boolean RayInt_HF (Ray *ray, GeomObj *o, double *dist,
		   double maxdist, Raystruct *r1,
		   GeomObj **return_o)
{
    int tmp, obj;
    Point3 old_o, old_d;
    NData *nd=o->var.hf.nd;
    GeomHF *h;

    h = &o->var.hf;
    object_to_ignore = r1->ignore_id - o->id;
    if (nd) {
	old_o = r1->origin;
	old_d = r1->direction;
	ndata_ray (nd, ray, &r1->origin, &r1->direction);
    }
    if (SCM_INUMP(r1->object)) {
	int n;

	n=SCM_INUM(r1->object) - o->id;
    }
    else
	tmp=check_or_subdivide(h, (Ray*)r1,
			       0, h->w-1, 0, h->h-1, 0, &obj, dist);
    if (nd) {
	get_tranformed_dist (nd, ray, &old_o, &old_d, dist);
	r1->origin = old_o;
	r1->direction = old_d;
    }
    if (tmp && *dist>0 && *dist<maxdist) {
	r1->nref = obj + 16; /* because we didn't correct for this
				while building the BSP tree */
	r1->ignore_id_out = o->id + obj;
	r1->ignore_sphere_out = RI_NOT_A_SPHERE;
    }
    return tmp;
}

int Eval_HF (GeomObj *obj, Raystruct *r, NData **nd, Point3 *section)
{
    int ref=r->nref-16;
    GeomHF *h;
    int v1, v2, v3, i;
    Point3 *verts[3], *normals[3], s1, d1, d2;
    Point3 v[3], n[3];

    h=&(obj->var.hf);
    *nd=h->nd;
    if (*nd) {
	s1=*section;
	V3MulPointByMatrix(&s1, &(*nd)->fmat, section);
    }

    for (i=0; i<3; i++) {
	verts[i] = &v[i];
	normals[i] = &n[i];
    }
    hf_verts (h, ref, &v1, &v2, &v3);
    get_hf_vert (h, v1, v);
    get_hf_vert (h, v2, v+1);
    get_hf_vert (h, v3, v+2);
    V3Cross(V3Sub(v+1, v, &d1), V3Sub(v+2, v, &d2),
	    &r->normal);
    r->pat_pos=*section;
    get_hf_normal (h, v1, n);
    get_hf_normal (h, v2, n+1);
    get_hf_normal (h, v3, n+2);
    smooth_triangle (verts, normals, section, r);
    return 0;
}

void ZBufferP_HF (GeomObj *o)
{
    GeomHF *t=&o->var.hf;
    int i,k;
    GeomPoly p;
    int v[3];
    Point3 d[3];
    Matrix4 save;
    extern Matrix4 *global_trans;

    p.nverts=3;
    p.verts=d;

    if (t->nd) {
	save = *global_trans;
	V3MatMul(&t->nd->fmat,&save,global_trans);
    }

    for (i=0; i<2*(t->h-1)*(t->w-1); i++) {
	hf_verts (t, i, v, v+1, v+2);
	for (k=0; k<3; k++)
	    get_hf_vert(t, v[k], &d[k]);
	zb_persp_poly(&p, i+o->id);
    }

    if (t->nd) *global_trans = save;
}

int Free_HF (GeomObj *o)
{
    int size, m, n;

    m=o->var.hf.h;
    n=o->var.hf.w;
    size=sizeof(GeomObj)-sizeof(GeomPrimitive)+sizeof(GeomHF);
    if (o->var.hf.nd) {
	size+=sizeof(NData);
	free (o->var.hf.nd);
    }
    size+=m*n*sizeof(double);
    free (o->var.hf.heights);
    free (o->var.hf.bounds);
    free (o->var.hf.branches);
    free (o);
    return size;
}

GeomObj *Copy_HF (GeomObj *in)
{
    GeomObj *out;
    int m1, n1, i, *oblist;
    GeomObjList l;

    out = NEWGEOM(GeomHF);
    memcpy (out,in,sizeof(GeomObj)-sizeof(GeomPrimitive)+sizeof(GeomHF));
    m1 = in->var.hf.h;
    n1 = in->var.hf.w;
    if (out->var.hf.nd) {
	out->var.hf.nd = MNEWTYPE(NData);
	memcpy (out->var.hf.nd, in->var.hf.nd, sizeof (NData));
    }
    out->var.hf.heights = MNEWARRAY(double, m1*n1);
    memcpy(out->var.hf.heights, in->var.hf.heights, m1*n1*sizeof(double));
    out->var.hf.bounds = NEWARRAY(double, in->var.hf.nbounds);
    memcpy(out->var.hf.bounds,
	   in->var.hf.bounds,
	   in->var.hf.nbounds*sizeof(double));
    out->var.hf.branches = NEWARRAY(int, in->var.hf.nbranches);
    memcpy(out->var.hf.branches,
	   in->var.hf.branches,
	   in->var.hf.nbranches*sizeof(int));
    return out;
}

void Bind_HF (GeomObj *p)
{
    p->min.x = p->min.y = 0.0;
    p->max.x = p->max.y = 1.0;
    p->min.z = p->var.hf.bounds[0];
    p->max.z = p->var.hf.bounds[1];
    if (p->var.hf.nd) {
	Point3 m1, m2;
	TransformBox(&p->min, &p->max, &m1, &m2, &p->var.hf.nd->bmat);
	p->min=m1;
	p->max=m2;
    }
}

void Transform_HF (GeomObj *p, Matrix4 *m)
{
    GeomHF *h = &p->var.hf;
    NData *nd = h->nd;
    Matrix4 tmat, *m1;

    m1=m;
    if (nd) {
	V3MatMul (m, &nd->bmat, &tmat);
	m1 = &tmat;
    }
    else
	nd = h->nd = NEWTYPE (NData);
    nd->bmat=*m1;
    V3InvertMatrix (m1, &nd->fmat);
    BindPrimitive (p);
}

static boolean check_hf_box_node (GeomHF *h, int pos, Point3 *min, Point3 *max)
{
    GeomObj o;
    Point3 v[3];

    o.var.type=TYPE_POLY;
    o.var.polygon.nverts=3;
    o.var.polygon.verts=v;

    get_hf_vert(h, pos, v);
    get_hf_vert(h, pos+1, v+1);
    get_hf_vert(h, pos+h->w, v+2);
    BindPrimitive (&o);
    if (min->x > o.max.x || max->x <= o.min.x &&
	min->y > o.max.y || max->y <= o.min.y &&
	min->z > o.max.z || max->z <= o.min.z)
	{
	    get_normal(&o.var.polygon);
	    get_minmax(&o.var.polygon);
	    if (PrimitiveBoxTest(min, max, &o))
		return TRUE;
	}
    get_hf_vert(h, pos+1, v);
    get_hf_vert(h, pos+h->w, v+1);
    get_hf_vert(h, pos+h->w+1, v+2);
    BindPrimitive (&o);
    if (min->x > o.max.x || max->x <= o.min.x) return FALSE;
    if (min->y > o.max.y || max->y <= o.min.y) return FALSE;
    if (min->z > o.max.z || max->z <= o.min.z) return FALSE;
    get_normal(&o.var.polygon);
    get_minmax(&o.var.polygon);
    return PrimitiveBoxTest(min, max, &o);
}

static boolean check_current_box (GeomHF *h, int offset, int left,
				  int right, int bot, int top,
				  Point3 *min, Point3 *max)
{
    double x1 = (double) left / (h->w - 1);
    double x2 = (double) right / (h->w - 1);
    double y1 = (double) bot / (h->h - 1);
    double y2 = (double) top / (h->h - 1);
    
    return (x2 < min->x || x1 > max->x ||
	    y2 < min->y || y1 > max->y ||
	    h->bounds[offset+1] < min->z || h->bounds[offset] > max->z);
}

static boolean check_box_rec (GeomHF *h, int offset, int left,
			      int right, int bot, int top,
			      Point3 *min, Point3 *max)
{
    int midx, midy;
    int *br;

    if (left == right || top == bot) return FALSE;
    if (left == right - 1 && bot == top - 1)
	return check_hf_box_node (h, left+bot*h->w, min, max);
    if (!check_current_box(h, offset, left, right, bot, top, min, max))
	return FALSE;
    midx = (left + right) / 2;
    midy = (bot + top) / 2;
    br = h->branches + offset*2;
    return (check_box_rec(h, br[0], left, midx, bot, midy, min, max) ||
	    check_box_rec(h, br[1], midx, right, bot, midy, min, max) ||
	    check_box_rec(h, br[2], left, midx, midy, top, min, max) ||
	    check_box_rec(h, br[3], midx, right, midy, top, min, max));
}

boolean Box_HF (GeomObj *p, Point3 *min, Point3 *max)
{
    GeomHF *h = &p->var.hf;
    Point3 min1, max1;
    NData *nd;
   
    if ((nd=p->var.hf.nd)) {
	TransformBox (min, max, &min1, &max1, &nd->fmat);
	min = &min1;
	max = &max1;
    }
    return check_box_rec (h, 0, 0, h->w - 1, 0, h->h - 1, min, max);
}
