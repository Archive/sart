#include "primitive.h"

void Dump_Point (GeomObj *p)
{
    printf ("#<point (%f %f %f)>",p->var.point.p.x,p->var.point.p.y,p->var.point.p.z);
}

void Vector_Point (GeomObj *p, VectorOutputHook f)
{
    Point3 p1,p2,*pnt;
    static Point3 d1={0.2,0,0};
    static Point3 d2={0,0.2,0};
    static Point3 d3={0,0,0.2};
    
    pnt=&p->var.point.p;
    f (V3Sub(pnt,&d1,&p1), V3Add(pnt,&d1,&p2));
    f (V3Sub(pnt,&d2,&p1), V3Add(pnt,&d2,&p2));
    f (V3Sub(pnt,&d3,&p1), V3Add(pnt,&d3,&p2));
}

boolean RayInt_Point (Ray *ray, GeomObj *o, double *dist,
		      double maxdist, Raystruct *r1,
		      GeomObj **return_o)
{
    Point3 t,u;

    V3Sub(&o->var.point.p,&ray->origin,&t);
    V3Cross(&t,&ray->direction,&u);
    return (V3SquaredLength(&u)<EPS*EPS) ? (*dist=V3Length(&t),TRUE) : FALSE;
}

int Eval_Point (GeomObj *obj, Raystruct *r, NData **nd, Point3 *section)
{
    *nd=NULL;
    return 1;
}

int Free_Point (GeomObj *o)
{
    free(o);
    return sizeof(GeomObj)-sizeof(GeomPrimitive)+sizeof(GeomPoint);
}

GeomObj *Copy_Point (GeomObj *in)
{
    GeomObj *out;

    out = NEWGEOM(GeomPoint);
    memcpy (out,in,sizeof(GeomObj)-sizeof(GeomPrimitive)+sizeof(GeomPoint));
    return out;
}

void Bind_Point (GeomObj *p)
{
    p->min=p->var.point.p;
    p->max=p->var.point.p;
}

void Transform_Point (GeomObj *p, Matrix4 *m)
{
    	Point3 pnt;

	V3MulPointByMatrix(&p->var.point.p, m, &pnt);
	p->var.point.p=pnt;
	BindPrimitive (p);
}

boolean Box_Point (GeomObj *p, Point3 *min, Point3 *max)
{
    return TRUE;
}
