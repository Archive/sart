#include "primitive.h"
#include "../mathutils.h"

int SetSceneID_Sphere (GeomObj *p, int n)
{
    p->id = n;
    if (global_zbuffer_mode != SCM_BOOL_F)
	return n + SetSceneID_Tessel (OBJSTR(*loc_sphere_geom), 0);
    else
	return n + 1;
}

void Dump_Sphere (GeomObj *p)
{
    printf (p->var.sphere.nd ? "#<transformed sphere " : "#<sphere ");
    printf ("(%f %f %f) %f)>",p->var.sphere.c.x,p->var.sphere.c.y,p->var.sphere.c.z,p->var.sphere.r);
}

boolean RayInt_Sphere (Ray *ray, GeomObj *o, double *dist,
		       double maxdist, Raystruct *r1,
		       GeomObj **return_o)
{
    double a,b,c,d;
    Point3 orig, dir, *oo, *dd, t, u;
    NData *nd=o->var.sphere.nd;

    if (nd)
	ndata_ray (nd, ray, oo=&orig, dd=&dir);
    else
	oo=&ray->origin, dd=&ray->direction;
    V3Sub (oo, &o->var.sphere.c, &t);
    if (global_zbuffer_mode == SCM_BOOL_F) {
	boolean direction_flag;

	a=V3Dot(dd, dd);
	b=2*V3Dot(dd,&t);
	c=V3Dot(&t,&t)-o->var.sphere.r*o->var.sphere.r;
	d=b*b-4*a*c; if (d<0) return FALSE;
	d=sqrt(d);
	if ((o->id == r1->ignore_id
	     && r1->ignore_sphere == RI_SPHERE_IGNORE_LOWER)
	    || (direction_flag = RI_SPHERE_IGNORE_LOWER, *dist=-b-d) < 0)
	    direction_flag = RI_SPHERE_IGNORE_HIGHER, *dist=-b+d;
	if (r1->ignore_sphere == direction_flag && r1->ignore_id == o->id)
	    return FALSE;
	*dist=*dist/(2*a);
	if (*dist>0 && *dist<maxdist) {
	    r1->ignore_id_out = o->id;
	    r1->ignore_sphere_out = direction_flag;
	}
	if (nd)
	    get_tranformed_dist (nd, ray, oo, dd, dist);
	return TRUE;
    }
    else {
	Ray new_ray;
	Point3 s1, s2;
	int nref_save, igid_save, igs_save;

	if (SCM_INUMP(r1->object))
	    r1->object = SCM_MAKINUM(SCM_INUM(r1->object) - o->id + OBJSTR(*loc_sphere_geom)->id);

	V3Scale(&t, 1/o->var.sphere.r);
	s1 = r1->origin;
	s2 = r1->direction;
	r1->origin = new_ray.origin = t;
	r1->direction = new_ray.direction = *dd;
	nref_save = r1->nref;
	igid_save = r1->ignore_id_out;
	igs_save = r1->ignore_sphere_out;
	r1->ignore_id -= o->id;
	if (RayPrimitiveIntersection (&new_ray, OBJSTR(*loc_sphere_geom), dist,
				      BIG, r1, return_o)) {
	    Point3 s, u;

	    *dist *= o->var.sphere.r;
	    r1->origin = s1;
	    r1->direction = s2;
	    if (nd)
		get_tranformed_dist (nd, ray, oo, dd, dist);
	    if (!(*dist>0 && *dist<maxdist)) {
		r1->nref = nref_save;
		r1->ignore_id_out = igid_save;
		r1->ignore_sphere_out = igs_save;
	    }
	    else {
		r1->ignore_id_out = r1->nref + o->id - 16;
		r1->ignore_sphere_out = RI_NOT_A_SPHERE;
	    }
	    *return_o = o; /* overwritten by ray_primitive intersection */
	    r1->ignore_id += o->id;
	    return TRUE;
	}
	else {
	    r1->ignore_id += o->id;
	    r1->origin = s1;
	    r1->direction = s2;
	    return FALSE;
	}
    }
}

int Eval_Sphere (GeomObj *obj, Raystruct *r, NData **nd, Point3 *section)
{
    Point3 s1, t;

    *nd=obj->var.sphere.nd;
    if (*nd) {
	s1=*section;
	V3MulPointByMatrix(&s1, &(*nd)->fmat, section);
    }
    if (global_zbuffer_mode == SCM_BOOL_F) {
	V3Sub(section,&obj->var.sphere.c,&t);
	r->normal=t;
	r->pat_n=r->normal;
	r->pat_pos=*section;
	return 1;
    } else {
	NData *nd_dummy;
	int t;

	V3Sub(section, &obj->var.sphere.c, &s1);
	V3Scale(&s1, 1/obj->var.sphere.r);
	t = Eval_Tessel(OBJSTR(*loc_sphere_geom), r, &nd_dummy, &s1);
	return t;
    }
}

void ZBufferP_Sphere (GeomObj *o)
{
    Matrix4 save, m;
    static Matrix4 diag = {{{0,0,0,0}, {0,0,0,0}, {0,0,0,0}, {0,0,0,1}}};
    NData *nd=o->var.sphere.nd;
    int gl_id;
    extern Matrix4 *global_trans;
    extern int global_id_increment;

    save = *global_trans;
    gl_id = global_id_increment;
    global_id_increment += o->id;
    OBJSTR(*loc_sphere_geom)->id = 0;
    diag.element[0][0] = diag.element[1][1] = diag.element[2][2] =
	o->var.sphere.r;
    diag.element[0][3] = o->var.sphere.c.x;
    diag.element[1][3] = o->var.sphere.c.y;
    diag.element[2][3] = o->var.sphere.c.z;
    V3MatMul(global_trans, &diag, &m);
    if (nd)
	V3MatMul(&nd->fmat, &m, global_trans);
    else
	*global_trans = m;
    ZBufferP_Tessel(OBJSTR(*loc_sphere_geom));
    *global_trans = save;
    global_id_increment = gl_id;
}

int Free_Sphere (GeomObj *o)
{
    int size;

    size=sizeof(GeomObj)-sizeof(GeomPrimitive)+sizeof(GeomSphere);
    if (o->var.sphere.nd) {
	size+=sizeof(NData);
	free (o->var.sphere.nd);
    }
    free(o);
    return size;
}

GeomObj *Copy_Sphere (GeomObj *in)
{
    GeomObj *out;

    out = NEWGEOM(GeomSphere);
    memcpy (out,in,sizeof(GeomObj)-sizeof(GeomPrimitive)+sizeof(GeomSphere));
    if (in->var.sphere.nd) {
	out->var.sphere.nd=MNEWTYPE(NData);
	memcpy (out->var.sphere.nd, in->var.sphere.nd, sizeof (NData));
    }
    return out;
}

void Bind_Sphere (GeomObj *p)
{
    Point3 t;
	
    t.x=t.y=t.z=p->var.sphere.r;
    V3Sub(&p->var.sphere.c,&t,&p->min);
    V3Add(&p->var.sphere.c,&t,&p->max);
    if (p->var.sphere.nd) {
	Point3 m1, m2;
	TransformBox(&p->min, &p->max, &m1, &m2, &p->var.sphere.nd->bmat);
	p->min=m1;
	p->max=m2;
    }
}

void Transform_Sphere (GeomObj *p, Matrix4 *m)
{
    double scale;
    Point3 pnt;
    GeomSphere *s=&p->var.sphere;
    NData *nd=s->nd;
    Matrix4 tmat, *m1;

    m1=m;
    if (nd) {
	V3MatMul (m, &nd->bmat, &tmat);
	m1=&tmat;
    }
    if ((scale=is_isoscale_mat(m1))>0) {
	V3MulPointByMatrix(&s->c, m1, &pnt);
	s->c=pnt;
	s->r*=scale;
	if (nd) {
	    free(nd);
	    s->nd=NULL;
	}
    }
    else {
	if (!nd)
	    nd=s->nd=NEWTYPE (NData);
	nd->bmat=*m1;
	V3InvertMatrix (m1, &nd->fmat);
    }
    BindPrimitive (p);
}

boolean Box_Sphere (GeomObj *obj, Point3 *min, Point3 *max)
{
    NData *nd;

    if ((nd=obj->var.sphere.nd)) {
	Point3 m1, m2;
	TransformBox (min, max, &m1, &m2, &nd->fmat);
	return (PointBoxSqDistance(&obj->var.sphere.c,&m1,&m2) <
		obj->var.sphere.r * obj->var.sphere.r);
    }
    else
	return (PointBoxSqDistance(&obj->var.sphere.c,min,max) <
		obj->var.sphere.r * obj->var.sphere.r);
}
