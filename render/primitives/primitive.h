#include "../bsp.h"

typedef struct PClass_struct {
    int (*SetSceneID)(GeomObj*,int);
    void (*Dump)(GeomObj*);
    void (*Vector)(GeomObj*, VectorOutputHook);
    void (*ZBufferP)(GeomObj*);
    boolean (*RayInt)(Ray*, GeomObj*, double*, double, Raystruct*, GeomObj**);
    int (*Eval)(GeomObj*, Raystruct *, NData **, Point3 *);
    int (*Free)(GeomObj*);
    GeomObj *(*Copy)(GeomObj*);
    void (*Bind)(GeomObj*);
    void (*Transform)(GeomObj*,Matrix4*);
    boolean (*Box)(GeomObj*,Point3*,Point3*);
    void (*Mark)(GeomObj*);

    boolean simple_primitive; /* primitive contains a single, flat object */
} PClass;

extern PClass PrimitiveClass[];
extern SCM *loc_sphere_geom;
extern SCM global_zbuffer_mode;

void Dump_Point (GeomObj *p);
void Vector_Point (GeomObj *p, VectorOutputHook f);
boolean RayInt_Point (Ray *ray, GeomObj *o, double *dist, double, Raystruct *, GeomObj**);
int Eval_Point (GeomObj *obj, Raystruct *r, NData **nd, Point3 *section);
int Free_Point (GeomObj *o);
GeomObj *Copy_Point (GeomObj *in);
void Bind_Point (GeomObj *p);
void Transform_Point (GeomObj *p, Matrix4 *m);
boolean Box_Point (GeomObj *p, Point3 *min, Point3 *max);
void Dump_Polygon (GeomObj *p);
void Vector_Polygon (GeomObj *p, VectorOutputHook f);
void ZBufferP_Polygon (GeomObj *p);
boolean RayInt_Polygon (Ray *ray, GeomObj *o, double *dist,double, Raystruct *, GeomObj**);
int Eval_Polygon (GeomObj *obj, Raystruct *r, NData **nd, Point3 *section);
int Free_Polygon (GeomObj *o);
GeomObj *Copy_Polygon (GeomObj *in);
void Bind_Polygon (GeomObj *p);
void Transform_Polygon (GeomObj *p, Matrix4 *m);
boolean Box_Polygon (GeomObj *p, Point3 *min, Point3 *max);
int SetSceneID_Sphere (GeomObj *p, int n);
void Dump_Sphere (GeomObj *p);
boolean RayInt_Sphere (Ray *ray, GeomObj *o, double *dist,double, Raystruct *, GeomObj**);
int Eval_Sphere (GeomObj *obj, Raystruct *r, NData **nd, Point3 *section);
void ZBufferP_Sphere (GeomObj *p);
int Free_Sphere (GeomObj *o);
GeomObj *Copy_Sphere (GeomObj *in);
void Bind_Sphere (GeomObj *p);
void Transform_Sphere (GeomObj *p, Matrix4 *m);
boolean Box_Sphere (GeomObj *p, Point3 *min, Point3 *max);
int SetSceneID_Tessel (GeomObj *p, int n);
void Dump_Tessel (GeomObj *p);
boolean RayInt_Tessel (Ray *ray, GeomObj *o, double *dist,double, Raystruct *, GeomObj**);
int Eval_Tessel (GeomObj *obj, Raystruct *r, NData **nd, Point3 *section);
void ZBufferP_Tessel (GeomObj *o);
int Free_Tessel (GeomObj *o);
GeomObj *Copy_Tessel (GeomObj *in);
void Bind_Tessel (GeomObj *p);
void Transform_Tessel (GeomObj *p, Matrix4 *m);
boolean Box_Tessel (GeomObj *p, Point3 *min, Point3 *max);
int SetSceneID_Tree (GeomObj *p, int n);
void Dump_Tree (GeomObj *p);
boolean RayInt_Tree (Ray *ray, GeomObj *o, double *dist,double, Raystruct *, GeomObj**);
void ZBufferP_Tree (GeomObj *o);
int Free_Tree (GeomObj *o);
GeomObj *Copy_Tree (GeomObj *in);
void Bind_Tree (GeomObj *p);
void Transform_Tree (GeomObj *p, Matrix4 *m);
boolean Box_Tree (GeomObj *p, Point3 *min, Point3 *max);
void Mark_Tree (GeomObj *p);
int SetSceneID_HF (GeomObj *p, int n);
void Dump_HF (GeomObj *p);
boolean RayInt_HF (Ray *ray, GeomObj *o, double *dist,double, Raystruct *, GeomObj**);
int Eval_HF (GeomObj *obj, Raystruct *r, NData **nd, Point3 *section);
void ZBufferP_HF (GeomObj *o);
int Free_HF (GeomObj *o);
GeomObj *Copy_HF (GeomObj *in);
void Bind_HF (GeomObj *p);
void Transform_HF (GeomObj *p, Matrix4 *m);
boolean Box_HF (GeomObj *p, Point3 *min, Point3 *max);

extern GeomTessel *tref;
extern int global_lock;
extern SCM global_ray;
extern boolean check_tessel (Ray *ray, int tnum, double *dist,
			     double maxdist, Raystruct *r1,
			     int *return_o);
double PointBoxSqDistance (Point3 *o, Point3 *min, Point3 *max);
double PBSDistance2 (Point3 *o, Point3 *v1, Point3 *v2, Point3 *v3,
		     Point3 *e, double l1, double l2, double l3);
boolean LineBoxIntersect (Point3 *p1, Point3 *p2, Point3 *min, Point3 *max);
boolean LinePolyIntersect (Point3 *p1, Point3 *p2, GeomPoly *poly);
void TransformBox (Point3 *imin, Point3 *imax, Point3 *omin, Point3 *omax,
		   Matrix4 *mat);
void ndata_ray (NData *nd, Ray *r, Point3 *o, Point3 *d);
void get_tranformed_dist (NData *nd, Ray *r, Point3 *o, Point3 *d, 
			  double *dist);
void tessel_verts (GeomTessel *t, int obj, int *v1, int *v2, int *v3);
boolean TesselInBox (Point3 *min, Point3 *max, int obj);
int SetSceneID_Hyper (GeomObj *p, int n);
void Dump_Hyper (GeomObj *p);
boolean RayInt_Hyper (Ray *ray, GeomObj *o, double *dist,
		      double maxdist, Raystruct *r1,
		      GeomObj **return_o);
int Eval_Hyper (GeomObj *obj, Raystruct *r, NData **nd, Point3 *section);
void ZBufferP_Hyper (GeomObj *o);
int Free_Hyper (GeomObj *o);
GeomObj *Copy_Hyper (GeomObj *in);
void Bind_Hyper (GeomObj *p);
void Transform_Hyper (GeomObj *p, Matrix4 *m);
boolean Box_Hyper (GeomObj *p, Point3 *min, Point3 *max);

int SetSceneID_Patch (GeomObj *p, int n);
void Dump_Patch (GeomObj *p);
boolean RayInt_Patch (Ray *ray, GeomObj *o, double *dist,
		      double maxdist, Raystruct *r1,
		      GeomObj **return_o);
int Eval_Patch (GeomObj *obj, Raystruct *r, NData **nd, Point3 *section);
void ZBufferP_Patch (GeomObj *o);
int Free_Patch (GeomObj *o);
GeomObj *Copy_Patch (GeomObj *in);
void Bind_Patch (GeomObj *p);
void Transform_Patch (GeomObj *p, Matrix4 *m);
boolean Box_Patch (GeomObj *p, Point3 *min, Point3 *max);
