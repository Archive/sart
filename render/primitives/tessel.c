#include "primitive.h"

/****** tref
 * NAME
 *  tref
 * SYNOPSIS
 *  static GeomTessel *tref;  (static variable for module primitive.c)
 * FUNCTION
 *  The tessel which is being subdivided or checked for the ray intersection
 *  is globally passed through this pointer.
 ********
 */

GeomTessel *tref;

/****** tessel_verts
 * NAME
 *  tessel_verts
 * SYNOPSIS
 *  void tessel_verts (GeomTessel *t, int obj, int *v1, int *v2, int *v3)
 * FUNCTION
 *  This is a temporary function used by other tessel calls. For a triangle
 *  obj in the tessel t, return the indices of its three vertices.
 ********
 */

void tessel_verts (GeomTessel *t, int obj, int *v1, int *v2, int *v3)
{
    int n;
    int row, col;
    int sqnum;

    sqnum=obj/2;
    row=sqnum/(t->w-1);
    col=sqnum%(t->w-1);
    n=row*t->w+col;
    if ((obj&1)==0) {
	*v1=n;
	*v2=n+1;
	*v3=n+t->w;
    }
    else {
	*v1=n+1;
	*v3=n+t->w;
	*v2=*v3+1;
    }
}    

/****** check_tessel
 * NAME
 *  check_tessel
 * SYNOPSIS
 *  boolean check_tessel (Ray *ray, int tnum, double *dist,
 *                        double maxdist, Raystruct *r1,
 *                        int *return_o)
 * FUNCTION
 *  This call is passed in place of RayPrimitiveIntersection to
 *  RayTreeIntersect. Check that function for parameter/return
 *  description.
 * NOTES
 *  The tnum and return_o are integers (triangle indices). At this point
 *  it is implicitly assumed that sizeof (int) == sizeof (GeomObj*). This
 *  should work on most architectures, but is, strictly speaking, a bug.
 ********
 */

boolean check_tessel (Ray *ray, int tnum, double *dist,
		      double maxdist, Raystruct *r1,
		      int *return_o)
{
    int v1, v2, v3;

    tnum-=16;
    if (!ray)
	return (int)&tref->rcache_ids[tnum];
    if (tnum == r1->ignore_id)
	return FALSE;
    *return_o=tnum+16;
    tessel_verts (tref, tnum, &v1, &v2, &v3);
    return RayTriangleIntersection (ray, tref->vertices+v1,
				    tref->vertices+v2, tref->vertices+v3,
				    tref->normals+tnum, tref->distances[tnum],
				    dist);
}

/****** TesselInBox
 * NAME
 *  TesselInBox
 * SYNOPSIS
 *  boolean TesselInBox (Point3 *min, Point3 *max, int obj)
 * FUNCTION
 *  This call is passed to BSP tree subdivision call in place of
 *  PrimitiveBoxTest. 
 *******
 */

boolean TesselInBox (Point3 *min, Point3 *max, int obj)
{
    int v1, v2, v3;
    GeomObj o;
    Point3 v[3];

    obj-=16;
    o.var.type=TYPE_POLY;
    o.var.polygon.nverts=3;
    o.var.polygon.verts=v;
    tessel_verts (tref, obj, &v1, &v2, &v3);
    v[0]=tref->vertices[v1];
    v[1]=tref->vertices[v2];
    v[2]=tref->vertices[v3];
    BindPrimitive (&o);
    if (min->x > o.max.x || max->x <= o.min.x) return FALSE;
    if (min->y > o.max.y || max->y <= o.min.y) return FALSE;
    if (min->z > o.max.z || max->z <= o.min.z) return FALSE;
    o.var.polygon.normal=tref->normals[obj];
    o.var.polygon.ndist=tref->distances[obj];
    get_minmax(&o.var.polygon);
    return PrimitiveBoxTest(min, max, &o);
}

int SetSceneID_Tessel (GeomObj *p, int n)
{
    p->id=n;
    n += 2 * (p->var.tessel.h - 1) * (p->var.tessel.w - 1);
    return n;
}

void Dump_Tessel (GeomObj *p)
{
    printf ("#<tesselation %dx%d>",p->var.tessel.w,p->var.tessel.h);
}

boolean RayInt_Tessel (Ray *ray, GeomObj *o, double *dist,
		       double maxdist, Raystruct *r1,
		       GeomObj **return_o)
{
    int tmp, obj;

    tref=&o->var.tessel;
    if (SCM_INUMP(r1->object)) {
	int n;

	n=SCM_INUM(r1->object) - o->id;
	if (tmp = check_tessel(ray, n+16, dist, BIG, r1, &obj))
	    r1->nref = obj;
	return tmp;
    }

    r1->ignore_id -= o->id;
    tmp=RayTreeIntersect(r1, tref->tree, (GeomObj**)&obj, dist,
			 check_tessel);
    r1->ignore_id += o->id;
    if (tmp && *dist>0 && *dist<maxdist) {
	r1->ignore_id_out = obj + o->id - 16;
	r1->ignore_sphere_out = RI_NOT_A_SPHERE;
	r1->nref = obj;
    }
    return tmp;
}

int Eval_Tessel (GeomObj *obj, Raystruct *r, NData **nd, Point3 *section)
{
    int ref=r->nref-16;
    GeomTessel *t;
    int v[3];
    Point3 *verts[3], *normals[3];

    *nd=NULL;
    t=&(obj->var.tessel);
    r->normal=t->normals[ref];
    r->pat_pos=*section;
    if (!t->vertex_normals) {
	r->pat_n=r->normal;
	return 1;
    }
    tessel_verts (t, ref, v, v+1, v+2);
    verts[0] = t->vertices+v[0];
    normals[0] = t->vertex_normals+v[0];
    verts[1] = t->vertices+v[1];
    normals[1] = t->vertex_normals+v[1];
    verts[2] = t->vertices+v[2];
    normals[2] = t->vertex_normals+v[2];
    smooth_triangle (verts, normals, section, r);
    return 0;
}

void ZBufferP_Tessel (GeomObj *o)
{
    GeomTessel *t=&o->var.tessel;
    int i,k;
    GeomPoly p;
    int v[3];
    Point3 d[3];

    p.nverts=3;
    p.verts=d;

    for (i=0; i<2*(t->h-1)*(t->w-1); i++) {
	tessel_verts(t, i, v, v+1, v+2);
	for (k=0; k<3; k++)
	    d[k]=t->vertices[v[k]];
	zb_persp_poly(&p, i+o->id);
    }
}

int Free_Tessel (GeomObj *o)
{
    int size, m, n;

    m=o->var.tessel.h;
    n=o->var.tessel.w;
    size=sizeof(GeomObj)-sizeof(GeomPrimitive)+sizeof(GeomTessel);
    if (o->var.tessel.vertex_normals) {
	size+=m*n*sizeof(Point3);
	free (o->var.tessel.vertex_normals);
    }
    size+=m*n*sizeof(Point3);
    free (o->var.tessel.vertices);
    size+=2*(m-1)*(n-1)*(sizeof(Point3)+sizeof(double)+sizeof(int));
    free (o->var.tessel.normals);
    free (o->var.tessel.distances);
    free (o->var.tessel.rcache_ids);
    size+=FreeBinTree (o->var.tessel.tree);
    free (o);
    return size;
}

GeomObj *Copy_Tessel (GeomObj *in)
{
    GeomObj *out;
    int m1, n1, i, *oblist;
    GeomObjList l;

    out = NEWGEOM(GeomTessel);
    memcpy (out,in,sizeof(GeomObj)-sizeof(GeomPrimitive)+sizeof(GeomTessel));
    m1 = in->var.tessel.h;
    n1 = in->var.tessel.w;
    if (in->var.tessel.vertex_normals) {
	out->var.tessel.vertex_normals = MNEWARRAY(Point3, m1*n1);
	memcpy (out->var.tessel.vertex_normals,
		in->var.tessel.vertex_normals,
		m1*n1*sizeof(Point3));
    }
    out->var.tessel.vertices = MNEWARRAY(Point3, m1*n1);
    memcpy(out->var.tessel.vertices,
	   in->var.tessel.vertices,
	   m1*n1*sizeof(Point3));
    out->var.tessel.rcache_ids = MNEWARRAY(int, 2*(m1-1)*(n1-1));
    memcpy(out->var.tessel.rcache_ids,
	   in->var.tessel.rcache_ids,
	   2*(m1-1)*(n1-1)*sizeof(int));
    out->var.tessel.normals = MNEWARRAY(Point3, 2*(m1-1)*(n1-1));
    memcpy(out->var.tessel.normals,
	   in->var.tessel.normals,
	   2*(m1-1)*(n1-1)*sizeof(Point3));
    out->var.tessel.distances = MNEWARRAY(double, 2*(m1-1)*(n1-1));
    memcpy(out->var.tessel.distances,
	   in->var.tessel.distances,
	   2*(m1-1)*(n1-1)*sizeof(double));
    oblist=NEWARRAY(int, 2*(m1-1)*(n1-1));
    for (i = 0; i < 2*(m1-1)*(n1-1); i++)
	oblist[i] = i+16;
    l.list = (GeomObj**)oblist;
    l.length = 2*(m1-1)*(n1-1);
    out->var.tessel.tree = NEWTYPE(BinTree);
    out->var.tessel.tree->min = in->var.tessel.tree->min;
    out->var.tessel.tree->max = in->var.tessel.tree->max;
    tref=&out->var.tessel;
    InitBinTree2(out->var.tessel.tree,&l,(geominbox_type)TesselInBox);
    return out;
}

void Bind_Tessel (GeomObj *p)
{
    p->min=p->var.tessel.tree->min;
    p->max=p->var.tessel.tree->max;
}

void Transform_Tessel (GeomObj *p, Matrix4 *m)
{
    GeomTessel *t = &p->var.tessel;
    Matrix4 tmp, m2;
    Point3 min, max;
    Point3 v;
    int m1, n1, verts, triangs, i, *oblist;
    GeomObjList l;
    double len;

    V3InvertMatrix(m, &tmp);
    TransposeMatrix4(&tmp, &m2);
    m1 = t->h;
    n1 = t->w;
    verts = m1*n1;
    triangs = 2*(m1-1)*(n1-1);
    if (t->vertex_normals)
	for (i = 0; i < verts; i++) {
	    V3Normalize(V3MulVectorByMatrix(&t->vertex_normals[i], &m2, &v));
	    t->vertex_normals[i] = v;
	}
    min.x=min.y=min.z=BIG;
    max.x=max.y=max.z=-BIG;
    for (i = 0; i < verts; i++) {
	V3MulPointByMatrix(&t->vertices[i], m, &v);
	t->vertices[i] = v;
	if (v.x < min.x) min.x = v.x;
	if (v.y < min.y) min.y = v.y;
	if (v.z < min.z) min.z = v.z;
	if (v.x > max.x) max.x = v.x;
	if (v.y > max.y) max.y = v.y;
	if (v.z > max.z) max.z = v.z;
    }
    for (i = 0; i < triangs; i++) {
	V3MulVectorByMatrix(&t->normals[i], &m2, &v);
	len = V3Length(&v);
	t->distances[i] /= len;
	t->normals[i] = *V3Scale(&v, 1/len);
    }
    FreeBinTree (t->tree);
    oblist=NEWARRAY(int, 2*(m1-1)*(n1-1));
    for (i = 0; i < 2*(m1-1)*(n1-1); i++)
	oblist[i] = i+16;
    l.list = (GeomObj**)oblist;
    l.length = 2*(m1-1)*(n1-1);
    t->tree = NEWTYPE(BinTree);
    t->tree->min = min;
    t->tree->max = max;
    tref = t;
    InitBinTree2(t->tree,&l,(geominbox_type)TesselInBox);
    Bind_Tessel(p);
}

struct box {
    Point3 min, max;
};

boolean check_node_box (GeomObj *ref, void *data)
{
    struct box *b=data;

    return !TesselInBox(&b->min, &b->max, (int)ref);
}

boolean Box_Tessel (GeomObj *p, Point3 *min, Point3 *max)
{
    struct box b;
    extern GeomTessel *tref;

    b.min = *min;
    b.max = *max;
    tref = &p->var.tessel;
    return !BoxTreeCall (tref->tree, min, max, check_node_box,
			 &b, check_tessel);
}
