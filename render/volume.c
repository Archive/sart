
#include "bsp.h"
#include <math.h>


/****** forw_ray
 * NAME
 *  forw_ray
 * SYNOPSIS
 *  SCM forw_ray (SCM rcell)
 *  forward-ray
 * FUNCTION
 *  This call forwards the ray through volume (by calling c_eval_ray_rec
 *  with parent #f). It's used to obtain the radiance coming in the ray
 *  direction before filtering it with the volume color.
 ******
 */

/* SUBR (subr1) */
char s_forw_ray[]="forward-ray";
SCM forw_ray (SCM rcell)
{
    SCM_ASSERT (rayp(rcell), rcell, SCM_ARG1, s_forw_ray);
    return c_get_ray_rad (rcell, SCM_BOOL_F);
}

/****** univol
 * NAME
 *  univol
 * SYNOPSIS
 *  SCM univol (SCM color, SCM dump_range, SCM ior)
 *  gas (gsubr)
 * FUNCTION
 *  Result of this function propagates the ray through uniform volume
 *  and returns the radiance.
 ****** */

/* define_volume(eval_univol,f_eval_univol,s_eval_univol,univol,s_univol,1,2,0) */

static SCM f_eval_univol;

char s_eval_univol[]=" eval-uniform-volume";
SCM eval_univol (SCM self, SCM ray)
{
    SCM p, dumpcol;
    double dr=0;
    double dist;
    Raystruct *r;
    SCM col;

    SCM_ASSERT (rayp(ray),ray,SCM_ARG2,s_eval_univol);
    
    dumpcol=SELFN(1);
    if (SELFN(2)!=SCM_UNDEFINED)
	dr=SCM_REALPART(SELFN(2));

    r=RAYSTR(ray);
    if ((r->flags & RF_SHADOW)) dumpcol=SCM_BOOL_F; /* we won't add our own radiance */
    col=c_get_ray_rad(ray, SCM_BOOL_F);
    if (r->object==SCM_BOOL_F)
	col=SCM_BOOL_F;
    dist=r->length;
    if (dr!=0) {
	dr=exp(-dist/dr);
	col=cplus_subr2(cscale_subr2(dumpcol,scm_makdbl(1-dr,0.0)),
			cscale_subr2(col, scm_makdbl(dr, 0.0)));
    }
    else
	col=cplus_subr2(cscale_subr2(dumpcol,scm_makdbl(dist,0.0)),col);
    return col;
}	  

static char s_univol[] = "gas";
SCM univol (SCM color, SCM dump_range, SCM ior)
{
    SCM cclo=scm_makcclo(f_eval_univol, 3);

    SCM_ASSERT(colorp(color)==SCM_BOOL_T,color,SCM_ARG1,s_univol);
    SCM_ASSERT(dump_range==SCM_UNDEFINED||(SCM_NIMP(dump_range)&&SCM_REALP(dump_range)),
	   dump_range,SCM_ARG2,s_univol);
    SCM_ASSERT(ior==SCM_UNDEFINED||(SCM_NIMP(ior)&&SCM_REALP(ior)),ior,SCM_ARG3,s_univol);

    SCM_VELTS(cclo)[1] = color;
    SCM_VELTS(cclo)[2] = dump_range;
    if (ior != SCM_UNDEFINED)
	scm_set_procedure_property_x(cclo, mtag_ior, ior);
    return cclo;
}

/****** anyvol
 * NAME
 *  eval_anyvol
 * SYNOPSIS
 *  SCM anyvol (SCM evaler, SCM resolution, SCM ior)
 *  gas-material (gsubr)
 * FUNCTION
 *  Result of this function propagates the ray through any volume and returns
 *  the radiance. It calls the evaler function to get the dumping color
 *  and range.
 ******
 */

/* define_volume(eval_anyvol,f_eval_anyvol,s_eval_anyvol,anyvol,s_anyvol,2,1,0) */

static SCM f_eval_anyvol;

char s_eval_anyvol[]=" eval-volume";
SCM eval_anyvol (SCM self, SCM ray)
{
    SCM dumpcol, dumprange, ior=SCM_BOOL_F, mat, tmat, fun, p;
    Matrix4 *m1;
    double res;
    double dist;
    double dr, t0;
    Point3 save, p1, p2, *q1, *q2;
    Raystruct *r;
    SCM col;
    int n,i;

    SCM_ASSERT (rayp(ray),ray,SCM_ARG2,s_eval_anyvol);

    mat=SELFN(1);
    res=SCM_REALPART(SELFN(2));
    m1=NULL;

    if ((tmat=scm_procedure_property(mat, mtag_texture_mat))!=SCM_BOOL_F) {
	SCM_ASSERT(SCM_ARRAYP(tmat), tmat, "Illegal texture transformation",s_eval_anyvol);
	m1=(Matrix4*)SCM_VELTS(SCM_ARRAY_V(tmat));
    }
    fun=mat;

    r=RAYSTR(ray);
	
    col=c_get_ray_rad(ray, SCM_BOOL_F);
    save=r->pat_pos;
    if (m1) {
	V3MulPointByMatrix(&r->origin, m1, q1=&p1);
	V3MulPointByMatrix(&r->section, m1, q2=&p2);
	dist = V3DistanceBetween2Points(q1, q2);
    }
    else {
	q1=&r->origin;
	q2=&r->section;
	dist=r->length;
    }
    if (r->object==SCM_BOOL_F)
	col=SCM_BOOL_F;
    n=res > 0.0 ? (dist/res) : 32;
    if (n>100) n=100; /* we don't want too high resolution here */
    if (n<1) n=1;
    t0=0;
    for (i=0; i<n; i++) {
	double t;
	SCM ret;

	t=(0.5+i)/n;
	r->pat_pos.x=q1->x*t+q2->x*(1-t);
	r->pat_pos.y=q1->y*t+q2->y*(1-t);
	r->pat_pos.z=q1->z*t+q2->z*(1-t);
	ret=scm_apply(fun, ray, scm_listofnull);
	SCM_ASSERT (PAIR_P(ret), ret, "Wrong spec returned",s_eval_anyvol);
	dumprange=SCM_CAR(ret);
	dumpcol=SCM_CDR(ret);
	SCM_ASSERT(dumprange==SCM_BOOL_F || (SCM_NIMP(dumprange) && SCM_REALP(dumprange)),
	       dumprange,"Wrong spec returned",s_eval_anyvol);
	if ((r->flags & RF_SHADOW)) dumpcol=SCM_BOOL_F; /* we won't add our own radiance */
	if (dumprange!=SCM_BOOL_F && (dr=SCM_REALPART(dumprange))!=0.0) {
	    dr=exp(-dist/dr*(t-t0));
	    col=cplus_subr2(cscale_subr2(dumpcol,scm_makdbl(1-dr,0.0)),
			    cscale_subr2(col, scm_makdbl(dr, 0.0)));
	}
	else
	    col=cplus_subr2(cscale_subr2(dumpcol,scm_makdbl(dist,0.0)),col);
	t0=t;
    }
    r->pat_pos=save;
    return col;
}

static char s_anyvol[] = "gas-material";
SCM anyvol (SCM evaler, SCM resolution, SCM ior)
{
    SCM cclo=scm_makcclo(f_eval_anyvol, 4);

    SCM_ASSERT(SCM_NIMP(resolution)&&SCM_REALP(resolution),resolution,SCM_ARG2,s_anyvol);
    SCM_ASSERT(ior==SCM_UNDEFINED||(SCM_NIMP(ior)&&SCM_REALP(ior)),ior,SCM_ARG3,s_anyvol);

    SCM_VELTS(cclo)[1] = evaler;
    SCM_VELTS(cclo)[2] = resolution;
    if (ior != SCM_UNDEFINED)
	scm_set_procedure_property_x(cclo, mtag_ior, ior);
    return cclo;
}

/****** solve
 * NAME
 *  solve
 * SYNOPSIS
 *   typedef (double*)(double, void*) solve_arg;
 *   double solve (solve_arg func, void *data, double a, double b, double fa,
                   double fb)
 * FUNCTION
 *   Using modified regula-falsi, find the zero of the function passed to it,
 *   in the interval (a,b). fa and fb are the function values at the end
 *   of the interval (so that they don't have to be recalculated, as f
 *   is assumed to be relatively espensive.
 ******/

typedef double (*solve_arg)(double, void*);

double solve (solve_arg f, void *data, double a, double b,
	      double fa, double fb)
{
    double c, fc, c0;

    c0 = a;
    c = (-fb*a + fa*b) / (fa-fb);
    while (fabs(c-c0) > EPS/10) {
	fc = f(c, data);
	if ((fc > 0 && fa > 0) || (fc < 0 && fa < 0)) {
	    fa = fc;
	    a = c;
	} else {
	    fb = fc;
	    b = c;
	}
	c0 = c;
	c = (-fb*a + fa*b) / (fa-fb);
    }
    return c;
}

/****** eval_isosurf
 * NAME
 *  eval_isosurf
 * SYNOPSIS
 *  SCM isosurf (SCM evaler, SCM surfmat, SCM res, SCM lipsch, SCM thres, SCM ior)
 *  iso-surfaces (gsubr)
 * FUNCTION
 *  This function propagates the ray through any volume and returns
 *  the radiance. If the result of the evaler crosses one of the
 *  thresholds, the intersection parameters are calculated, and passed
 *  to the surface-mat.  Thresholds are assumed to be sorted in
 *  ascending order, and ior defaults to 1.
 * NOTES
 *  The function will evaluate the material on the other side as if
 *  the ray passed through the volume regardless of whether it did
 *  actually pass through.
 ****** */

struct slvinfo {
    Point3 *p1;
    Point3 *p2;
    Raystruct *r;
    SCM ray;
    SCM func;
    double t;
};

extern char s_eval_isosurf[];

double getfunc (double t, void *data)
{
    SCM ret;
    struct slvinfo *d=data;

    V3Combine(d->p1, d->p2, &d->r->pat_pos, 1-t, t);
    ret=scm_apply(d->func, d->ray, scm_listofnull);
    SCM_ASSERT (SCM_REALP(ret), ret, "Wrong spec returned",s_eval_isosurf);
    return SCM_REALPART(ret)-d->t;
}

int classify_point (double t, double *thres, int n)
{
    int i;

    for (i=0; i<n; i++)
	if (t < thres[i])
	    break;
    return i;
}

void gradient (SCM f, Point3 *where, double eps, Point3 *out,
	       SCM ray, Raystruct *r)
{
    Point3 *tmp;
    Point3 e1={1,0,0};
    Point3 e2={0,1,0};
    Point3 e3={0,0,1};
    double v, v1, v2, v3;
    SCM ret;

    e1.x=e2.y=e3.z=eps;
    tmp = &r->pat_pos;
    *tmp = *where;
    ret = scm_apply(f, ray, scm_listofnull);
    SCM_ASSERT (SCM_NIMP(ret) && SCM_REALP(ret),ret,"Wrong spec returned",s_eval_isosurf);
    v = SCM_REALPART(ret);
    V3Add (where, &e1, tmp);
    ret = scm_apply(f, ray, scm_listofnull);
    SCM_ASSERT (SCM_NIMP(ret) && SCM_REALP(ret),ret,"Wrong spec returned",s_eval_isosurf);
    v1 = SCM_REALPART(ret);
    V3Add (where, &e2, tmp);
    ret = scm_apply(f, ray, scm_listofnull);
    SCM_ASSERT (SCM_NIMP(ret) && SCM_REALP(ret),ret,"Wrong spec returned",s_eval_isosurf);
    v2 = SCM_REALPART(ret);
    V3Add (where, &e3, tmp);
    ret = scm_apply(f, ray, scm_listofnull);
    SCM_ASSERT (SCM_NIMP(ret) && SCM_REALP(ret),ret,"Wrong spec returned",s_eval_isosurf);
    v3 = SCM_REALPART(ret);
    out->x = v1-v;
    out->y = v2-v;
    out->z = v3-v;
}

/* define_volume(eval_isosurf,f_eval_isosurf,s_eval_isosurf,isosurf,s_isosurf,5,1,0) */

static SCM f_eval_isosurf;

char s_eval_isosurf[]=" eval-iso-surfaces";
SCM eval_isosurf (SCM self, SCM ray)
{
    SCM ior=SCM_BOOL_F, mat, tmat, smat, tsmat;
    SCM thres;
    Matrix4 *m1, *m2;
    struct slvinfo d;
    double res, lps, *vtr, t, t2;
    int ntr;
    double dist;
    double t0, pfunc, nfunc, weight;
    Point3 save, p1, p2;
    Raystruct *r;
    SCM col;
    SCM save_csg;
    int pclass, nclass;


    SCM_ASSERT (rayp(ray),ray,SCM_ARG2,s_eval_isosurf);

    mat=SELFN(1);
    smat = SELFN(2);
    res=SCM_REALPART(SELFN(3));
    lps=SCM_REALPART(SELFN(4));
    thres = SELFN(5);

    m1=NULL;
    if ((tmat=scm_procedure_property(mat, mtag_texture_mat))!=SCM_BOOL_F) {
	SCM_ASSERT(SCM_ARRAYP(tmat), tmat, "Illegal texture transformation",s_eval_isosurf);
	m1=(Matrix4*)SCM_VELTS(SCM_ARRAY_V(tmat));
    }
    d.func=mat;

    m2=NULL;
    if ((tsmat=scm_procedure_property(smat, mtag_texture_mat))!=SCM_BOOL_F) {
	SCM_ASSERT(SCM_ARRAYP(tsmat), tsmat, "Illegal surface texture transformation",s_eval_isosurf);
	m2=(Matrix4*)SCM_VELTS(SCM_ARRAY_V(tsmat));
    }

    vtr=(double*)SCM_VELTS(thres);
    ntr=SCM_LENGTH(thres);

    d.r = r = RAYSTR(d.ray=ray);
    weight = r->weight;
    r->weight = 0; /* This will prevent the material from the evaluation */
    save_csg = r->csg; /* just in case we need it */
    c_get_ray_rad(ray, SCM_BOOL_F);
    r->weight = weight;
    save = r->pat_pos;
    if (m1) {
	Point3 t;
	V3MulPointByMatrix(&r->origin, m1, d.p1 = &p1);
	V3MulPointByMatrix(&r->section, m1, d.p2 = &p2);
	V3MulVectorByMatrix(&r->direction, m1, &t);
	r->direction = t;
	dist = V3DistanceBetween2Points(d.p1, d.p2);
    }
    else {
	d.p1=&r->origin;
	d.p2=&r->section;
	dist=r->length;
    }
    if (res==0) res=dist/50;
    t=t0=0;
    d.t=0;
    while (t<dist) {
	if (!t0) t+=BIGGER_EPS;
	else
	    t+=MAX(res, (pclass == ntr ? pfunc-vtr[ntr-1]
			 : (pclass == 0 ? vtr[0] - pfunc
			    : MIN(pfunc-vtr[pclass-1],vtr[pclass]-pfunc)))/lps);

	if (t>dist) t=dist;

	nfunc = getfunc(t, &d);
	nclass = classify_point(nfunc, vtr, ntr);
	if (t0 && nclass!=pclass) {
	    while (abs(nclass-pclass)>1) {
		double t2, f2;
		int c;

		t2=(t+t0)/2;
		f2=getfunc (t2, &d);
		c=classify_point(f2, vtr, ntr);
		if (c==pfunc) {
		    pfunc=f2;
		    pclass=c;
		    t0=t2;
		} else {
		    nfunc=f2;
		    nclass=c;
		    t=t2;
		}
	    }
	    d.t=vtr[MIN(pclass,nclass)];
	    t=solve (getfunc, &d, t0, t, pfunc-d.t, nfunc-d.t);
	    V3Combine(d.p1, d.p2, &r->section, 1-t, t);
	    gradient (d.func, &r->section, res*1e-3, &r->normal, ray, r);
	    r->length=V3DistanceBetween2Points(&r->origin, &r->section);
	    r->object=SCM_BOOL_T; /* mark virtual intersection */
	    r->csg = save_csg; /* This has been overwritten */
	    V3Normalize(&r->normal);
	    if (m2){
		V3MulPointByMatrix(&r->section, m2, &r->pat_pos);
		V3MulVectorByMatrix(&r->normal, m2+16, &r->pat_n);
		V3Normalize(&r->pat_n);
	    }
	    else {
		r->pat_n=r->normal;
		r->pat_pos=r->section;
	    }
	    r->ignore_id_out = RI_DO_NOT_IGNORE;
	    return scm_apply(smat,ray,scm_listofnull);
	}
	pfunc = nfunc;
	pclass = nclass;
	t0=t;
    }
    r->pat_pos=save;
    return c_get_ray_rad(ray, SCM_BOOL_F); /* eval it for real this time */
}
	  
static char s_isosurf[] = "iso-surfaces";
SCM isosurf (SCM evaler, SCM surfmat, SCM resolution, SCM lipsch, SCM thres, SCM ior)
{
    SCM cclo=scm_makcclo(f_eval_isosurf, 7);

    SCM_ASSERT(SCM_NIMP(resolution)&&SCM_REALP(resolution),resolution,SCM_ARG3,s_isosurf);
    SCM_ASSERT(SCM_NIMP(lipsch)&&SCM_REALP(lipsch),lipsch,SCM_ARG4,s_isosurf);
    SCM_ASSERT(ior==SCM_UNDEFINED||(SCM_NIMP(ior)&&SCM_REALP(ior)),ior,"Bad ior",s_isosurf);
    SCM_ASSERT(SCM_NIMP(thres)&&SCM_TYP7(thres)==scm_tc7_dvect,thres,SCM_ARG5,s_isosurf);

    SCM_VELTS(cclo)[1] = evaler;
    SCM_VELTS(cclo)[2] = surfmat;
    SCM_VELTS(cclo)[3] = resolution;
    SCM_VELTS(cclo)[4] = lipsch;
    SCM_VELTS(cclo)[5] = thres;
    if (ior != SCM_UNDEFINED)
	scm_set_procedure_property_x(cclo, mtag_ior, ior);
    return cclo;
}

/* ---------------------- csg-scheme interface ------------------ */

/****** CSGSMOB
 * NAME
 *  CSGSMOB
 * FUNCTION
 *  Carrier for CSG container (as defined in render.h)
 * SOURCE
 */

long tc16_CSG;

SCM CSG_mark (SCM csg)
{
    Csg *c=(Csg*)scm_markcdr(csg);
    
    scm_gc_mark(c->aux);
    scm_gc_mark(c->material);
    return c->parent;
}

scm_sizet CSG_free (SCM_CELLPTR csg)
{
    Csg *c=(Csg*)SCM_CDR(csg);
    
    free(c);
    return sizeof(Csg);
}

int CSG_print (SCM csg, SCM port, int writing)
{
    Csg *c=CSGFIELD(csg);
    char buf[256];

    scm_puts ("#<CSG ", port);
    switch (c->type) {
    case CSG_CONTAINER:
	scm_puts ("Container", port);
	break;
    case CSG_UNION:
	scm_puts ("Union", port);
	break;
    case CSG_MINUS:
	scm_puts ("Minus", port);
	break;
    case CSG_INTERSECTION:
	scm_puts ("Intersection", port);
    }
    sprintf (buf, "/%x size=%d act=%d flags=%d>",
	     (int)c, c->ntotal, c->nactive, c->flags);
    scm_puts(buf, port);
    return 1;
}

static scm_smobfuns CSGsmob = {CSG_mark, CSG_free, CSG_print, 0};

/*********/

/****** csgp
 * NAME
 *  csgp
 * SYNOPSIS
 *  SCM csgp (SCM csg)
 *  csg? (subr1)
 * FUNCTION
 *  Type predicate for CSGSMOB.
 ******

/* SUBR (subr1) */
char s_csgp[] = "csg?";
SCM csgp (SCM csg)
{
    if (SCM_NIMP(csg) && SCM_TYP16(csg)==tc16_CSG)
	return SCM_BOOL_T;
    return SCM_BOOL_F;
}

/****** container_subr2,union_subr2,intersection_subr2,minus_subr3
 * NAME
 *  container_subr2
 *  union_subr2
 *  intersection_subr2
 *  minus_subr3
 * SYNOPSIS
 *  SCM container_subr2 (SCM list, SCM mat)
 *  SCM union_subr2 (SCM list, SCM mat)
 *  SCM intersection_subr2 (SCM list, SCM mat)
 *  SCM minus_subr3 (SCM aux, SCM list, SCM mat)
 *  csg-container (subr2)
 *  csg-union (subr2)
 *  csg-intersection (subr2)
 *  csg-minus (subr3)
 * FUNCTION
 *  CSG constructors. 'list' is a list of CSGs (or, for a container, objects).
 *  An object may be at most in two containers (make sure that their interiors
 *  are on the opposite sides). 'aux' in minus is the CSG being substracted
 *  from.
 * BUGS
 *  object separating TWO containers hasn't been tested yet.
 *******
 */

/* SUBR (subr2) */
char s_container[] = "csg-container";
SCM container_subr2 (SCM list, SCM mat)
{
    Csg *c;
    SCM thecell, l;

    SCM_ASSERT (scm_ilength(list)>0, list, SCM_ARG1, s_container);
    c=MNEWTYPE(Csg);
    c->type=CSG_CONTAINER;
    c->parent=SCM_BOOL_F;
    c->flags=0;
    c->ntotal=0;
    c->nactive=0;
    c->aux=SCM_BOOL_F;
    c->material=mat;
    SCM_NEWCELL(thecell);
    SCM_CDR(thecell)=(SCM)c;
    SCM_CAR(thecell)=tc16_CSG;
    while (list != SCM_EOL) {
	GeomObj *p;
	l=SCM_CAR(list);
	list=SCM_CDR(list);
	if (primitivep(l)==SCM_BOOL_F) goto bail;
	c->ntotal++;
	if ((p=OBJSTR(l))->csg==SCM_BOOL_F)
	    p->csg=thecell;
	else if (csgp(p->csg))
	    p->csg=scm_cons(thecell, p->csg);
	else goto bail;
    }
    return thecell;

  bail:
    SCMERROR (l, SCM_ARG1, s_container);
}

/* SUBR (subr2) */
char s_union[] = "csg-union";
SCM union_subr2 (SCM list, SCM mat)
{
    Csg *c;
    SCM thecell;

    SCM_ASSERT (scm_ilength(list)>0, list, SCM_ARG1, s_union);
    c=MNEWTYPE(Csg);
    c->type=CSG_UNION;
    c->parent=SCM_BOOL_F;
    c->flags=0;
    c->ntotal=0;
    c->nactive=0;
    c->aux=SCM_BOOL_F;
    c->material=mat;
    SCM_NEWCELL(thecell);
    SCM_CDR(thecell)=(SCM)c;
    SCM_CAR(thecell)=tc16_CSG;
    while (list != SCM_EOL) {
	SCM l=SCM_CAR(list);
	Csg *p;
	list=SCM_CDR(list);
	if ((csgp(l)==SCM_BOOL_F) || (p=CSGFIELD(l))->parent!=SCM_BOOL_F) {
	    SCMERROR (l, SCM_ARG1, s_union);
	}
	c->ntotal++;
	p->parent=thecell;
    }
    return thecell;
}

/* SUBR (subr2) */
char s_intersection[] = "csg-intersection";
SCM intersection_subr2 (SCM list, SCM mat)
{
    Csg *c;
    SCM thecell;

    SCM_ASSERT (scm_ilength(list)>0, list, SCM_ARG1, s_intersection);
    c=MNEWTYPE(Csg);
    c->type=CSG_INTERSECTION;
    c->parent=SCM_BOOL_F;
    c->flags=0;
    c->ntotal=0;
    c->nactive=0;
    c->aux=SCM_BOOL_F;
    c->material=mat;
    SCM_NEWCELL(thecell);
    SCM_CDR(thecell)=(SCM)c;
    SCM_CAR(thecell)=tc16_CSG;
    while (list != SCM_EOL) {
	SCM l=SCM_CAR(list);
	Csg *p;
	list=SCM_CDR(list);
	if ((csgp(l)==SCM_BOOL_F) || (p=CSGFIELD(l))->parent!=SCM_BOOL_F) {
	    SCMERROR (l, SCM_ARG1, s_intersection);
	}
	c->ntotal++;
	p->parent=thecell;
    }
    return thecell;
}

/* SUBR (subr3) */
char s_minus[] = "csg-minus";
SCM minus_subr3 (SCM aux, SCM list, SCM mat)
{
    Csg *c;
    SCM thecell;

    SCM_ASSERT (scm_ilength(list)>=1, list, SCM_ARG2, s_minus);
    SCM_ASSERT (csgp(aux)==SCM_BOOL_T && (CSGFIELD(aux))->parent==SCM_BOOL_F, aux, SCM_ARG1, s_minus);
    c=MNEWTYPE(Csg);
    c->type=CSG_MINUS;
    c->parent=SCM_BOOL_F;
    c->flags=0;
    c->ntotal=0;
    c->nactive=0;
    c->aux=aux;
    c->material=mat;
    SCM_NEWCELL(thecell);
    SCM_CDR(thecell)=(SCM)c;
    SCM_CAR(thecell)=tc16_CSG;
    (CSGFIELD(aux))->parent=thecell;
    while (list != SCM_EOL) {
	SCM l=SCM_CAR(list);
	Csg *p;
	list=SCM_CDR(list);
	if ((csgp(l)==SCM_BOOL_F) || (p=CSGFIELD(l))->parent!=SCM_BOOL_F) {
	    SCMERROR (l, SCM_ARG2, s_minus);
	}
	c->ntotal++;
	p->parent=thecell;
    }
    return thecell;
}

/* SUBR (subr1) */
char s_measure_light[] = "measure-total-light";
SCM measure_light (SCM rcell)
{

    Raystruct *ray;
    SCM light, total;
    extern long tc16_Ray;

    SCM_ASSERT (SCM_NIMP(rcell) && SCM_TYP16(rcell)==tc16_Ray,
		rcell, SCM_ARG1, s_measure_light);
    ray = RAYSTR(rcell);

    total = SCM_BOOL_F;
    for (light = SCENESTR(ray->scene)->light_list; SCM_NNULLP(light);
	 light = SCM_CDR(light)) {

	SCM l = SCM_CAR(light), c;

	if (is_light(OBJSTR(l)->material,rcell)==SCM_BOOL_T) {
            /* The second is_light checked for the directional lightsource */
	    SCM llist;

	    CreateLightList (OBJSTR(l), 0.1, ray, 0.5, &llist);
	    while (SCM_NNULLP(llist)) {
		SCM node, c, temp;
		Raystruct *r;
		double weight;

		node = SCM_CAR(llist);
		r = RAYSTR(temp = SCM_CDR(node));
		r->object = l;
		weight = SCM_REALPART(SCM_CAR(node));
		c = c_get_ray_rad(temp, rcell);
		total = combine_colors(total, c, 1, weight);
	    }
	}
    }
}

void init_volume_c (void)
{

    /* __PERL__ This code is automatically generated. Don't modify. */

    static scm_iproc subr1s[] = {
        {s_forw_ray, forw_ray},
        {s_csgp, csgp},
        {s_measure_light, measure_light},
        {0,0}
    };

    static scm_iproc subr2s[] = {
        {s_container, container_subr2},
        {s_union, union_subr2},
        {s_intersection, intersection_subr2},
        {0,0}
    };

    static scm_iproc subr3s[] = {
        {s_minus, minus_subr3},
        {0,0}
    };

    scm_init_iprocs (subr1s, scm_tc7_subr_1);
    scm_init_iprocs (subr2s, scm_tc7_subr_2);
    scm_init_iprocs (subr3s, scm_tc7_subr_3);

    f_eval_univol = scm_make_subr(s_eval_univol,scm_tc7_lsubr_2,eval_univol);
    scm_make_gsubr(s_univol,1,2,0,univol);
    f_eval_anyvol = scm_make_subr(s_eval_anyvol,scm_tc7_lsubr_2,eval_anyvol);
    scm_make_gsubr(s_anyvol,2,1,0,anyvol);
    f_eval_isosurf = scm_make_subr(s_eval_isosurf,scm_tc7_lsubr_2,eval_isosurf);
    scm_make_gsubr(s_isosurf,5,1,0,isosurf);

    /* __END__ */

    tc16_CSG=scm_newsmob(&CSGsmob);
}
