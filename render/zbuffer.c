
#include "bsp.h"

static double *ztab = NULL;
static int *itab = NULL;
static int height, width;

Matrix4 *global_trans;
int global_id_increment;

void zb_set (int i, int j, double z, int id)
{
    int n;

    if (i<0 || i>=width || j<0 || j>=height) {
	fprintf (stderr, "internal: z index overflow");
	return;
    }
    n=width*j+i;
    if (ztab[n]>z) {
        ztab[n]=z;
	itab[n]=id;
    }
}

typedef struct {
    double cx, dx, cz, dz;
    int y;
} vertex;

void advance_list (vertex **list, int *lsize, int y)
{
    int i,j;

    for (i=j=0; i<*lsize; i++)
	if (list[i]->y > y ) {
	    list[i]->cx+=list[i]->dx;
	    list[i]->cz+=list[i]->dz;
	    list[j++]=list[i];
	}
    *lsize=j;
}
		    
typedef struct {
    int starty;
    vertex v;
} edge;

void makecd (double x1, double x2, double y1, double y2, double *c, double *d)
{
    *d=(x2-x1)/(y2-y1);
    *c=x1+(*d)*(ceil(y1)-y1);
}

void zb_persp_poly (GeomPoly *p, int id) /* p is supposed to be
					    transformed already */
{
    GeomPoly p1, p2;
    Point3 t, *t1, *t2;
    int i,j,maxi;
    double miny, maxy;
    double w1=width/2.0;
    double h1=height/2.0;

    vertex **list;
    int lsize;
    edge *edgelist;

    /* Transformation, to begin with */

    id += global_id_increment;
    p1.verts=NEWARRAY(Point3, p->nverts);
    p1.nverts=p->nverts;
    for (i=0; i<p->nverts; i++) {
	V3MulPointByMatrix(&p->verts[i], global_trans, &p1.verts[i]);
    }
    get_normal(&p1);

    /* The clipping */

    t.x=t.y=0;
    t.z=1;
    SplitByPlane (&p1, &p2, NULL, &t, 0.0, 0);
    PolyFree(&p1);
    if (!p2.nverts) return;
    t.x=-1-0.2/w1; /* the addition fixes the numerical instabilities */
    t.y=0;
    SplitByPlane (&p2, &p1, NULL, &t, 0.0, 0);
    PolyFree(&p2);
    if (!p1.nverts) return;
    t.x=1-0.2/w1;
    SplitByPlane (&p1, &p2, NULL, &t, 0.0, 0);
    PolyFree(&p1);
    if (!p2.nverts) return;
    t.x=0;
    t.y=-1-0.2/h1;
    SplitByPlane (&p2, &p1, NULL, &t, 0.0, 0);
    PolyFree(&p2);
    if (!p1.nverts) return;
    t.y=1-0.2/h1;
    SplitByPlane (&p1, &p2, NULL, &t, 0.0, 0);
    PolyFree(&p1);
    if (!p2.nverts) return;

    /* Now we only have to rasterize p2 */

    /* Perspective transformation, for start */

    for (i=0; i<p2.nverts; i++) {
	double y;

	p2.verts[i].x=w1*p2.verts[i].x/p2.verts[i].z+w1;
	y=p2.verts[i].y=h1*p2.verts[i].y/p2.verts[i].z+h1;
	p2.verts[i].z=-1/p2.verts[i].z;
	if (!i || y<miny) miny=y;
	if (!i || y>maxy) maxy=y;
    }

    /* Allocate lists */

    lsize=0;
    list=NEWARRAY(vertex*,p2.nverts);

    /* Create edge list */

    edgelist=NEWARRAY(edge,p2.nverts);

    for (j=0,i=p2.nverts, t1=p2.verts; i>0; i--, t1++) {
	t2 = i==1 ? p2.verts : t1+1;

	if (t1->y<t2->y) {
	    if (ceil(t1->y) < ceil(t2->y)) {
		makecd (t1->x, t2->x, t1->y, t2->y, &edgelist[j].v.cx,
			&edgelist[j].v.dx);
		makecd (t1->z, t2->z, t1->y, t2->y, &edgelist[j].v.cz,
			&edgelist[j].v.dz);
		edgelist[j].v.y=ceil(t2->y);
		edgelist[j].starty=ceil(t1->y);
		j++;
	    }
	}
	else {
	    if (ceil(t2->y) < ceil(t1->y)) {
		makecd (t2->x, t1->x, t2->y, t1->y, &edgelist[j].v.cx,
			&edgelist[j].v.dx);
		makecd (t2->z, t1->z, t2->y, t1->y, &edgelist[j].v.cz,
			&edgelist[j].v.dz);
		edgelist[j].v.y=ceil(t1->y);
		edgelist[j].starty=ceil(t2->y);
		j++;
	    }
	}
    }

    /* y loop */

    maxi=ceil(maxy);
    for (i=(int)ceil(miny); i<maxi; i++) {
	int e,flag,lsz;

	for (e=0; e<j; e++)
	    if (edgelist[e].starty==i)
	        /* add vertex to the active list */
		list[lsize++]=&edgelist[e].v;
	
	/* re-sort segment boundaries */
	lsz=lsize;
	do {
	    int i;
	    
	    flag=0;
	    for (i=0; i<lsz-1; i++)
		if (list[i]->cx>list[i+1]->cx) {
		    vertex *t;
		    t=list[i];
		    list[i]=list[i+1];
		    list[i+1]=t;
		    flag=1;
		}
	    lsz--; /* the last element is correct */
	} while (flag);
	
	/* iterate over all the segments */
	for (e=0; e<lsize; e+=2) {
	    vertex *v1=list[e];
	    vertex *v2=list[e+1];

	    if (ceil(v1->cx)<ceil(v2->cx)) {
		double cz,dz;
		int k,l;

		makecd (v1->cz,v2->cz,v1->cx,v2->cx,&cz,&dz);
		l=(int)ceil(v2->cx);
		for (k=(int)ceil(v1->cx); k<l; k++) {
		    zb_set (k,i,cz,id);
		    cz+=dz;
		}
	    }
	}

	advance_list (list, &lsize, i+1);
    }

    free (list);
    free (edgelist);
    PolyFree (&p2);
}

void zb_init (int h, int w)
{
    int n=h*w, i;

    height=h;
    width=w;
    itab=NEWARRAY(int,n);
    ztab=NEWARRAY(double,n);
    for (i=0; i<n; i++) {
	itab[i]=-1;
	ztab[i]=BIG;
    }
}

void set_zbstruct (ZBuffer *z)
{
    height = z->height;
    width = z->width;
    itab = z->i;
    ztab = z->z;
    global_trans = z->trans;
    global_id_increment=0;
}

int retrieve_id (Scene *s, int id)
{
    int left, right, ltid, rtid, mid, mdid;

    left = 0;
    right = s->n - 1;
    if ((rtid = OBJSTR(s->object_list[right])->id) <= id)
	return right;
    ltid = OBJSTR(s->object_list[left])->id;
    while (1) {
	mid = (right + left)/2;
	if (left == mid)
	    return left;
	mdid = OBJSTR(s->object_list[mid])->id;
	if (mdid <= id)
	    left = mid, ltid = mdid;
	else
	    right = mid, rtid = mdid;
    }
}

boolean get_zbuffer_point (SCM zb, Raystruct *ray)
{
    Matrix4 *fmat, *bmat;
    SCM z, i, fm, bm;
    Point3 pix;
    int *ids, h, w, id, index;
    double *zs, rx, ry;
    GeomObj *o, *ino;

    z=SCM_CAR(zb); zb=SCM_CDR(zb);
    i=SCM_CAR(zb); zb=SCM_CDR(zb);
    fm=SCM_CAR(zb); zb=SCM_CDR(zb);
    bm=SCM_CAR(zb);
    
    fmat=(Matrix4*)SCM_VELTS(SCM_ARRAY_V(fm));
    V3MulVectorByMatrix(&ray->direction, fmat, &pix);
    if (pix.z > 0) {
	h = SCM_ARRAY_DIMS(z)[0].ubnd - SCM_ARRAY_DIMS(z)[0].lbnd + 1;
	w = SCM_ARRAY_DIMS(z)[1].ubnd - SCM_ARRAY_DIMS(z)[1].lbnd + 1;
	pix.x = (1+pix.x/pix.z)*w*0.5;
	pix.y = (1+pix.y/pix.z)*h*0.5;
	if (fabs(pix.x - (rx = floor(pix.x+0.5))) > EPS ||
	    fabs(pix.y - (ry = floor(pix.y+0.5))) > EPS)
	    return FALSE;
	ids = (int*)SCM_VELTS(SCM_ARRAY_V(i));
	if ((id = ids[(int)ry*w + (int)rx]) < 0)
	    return -1;
	index = retrieve_id(SCENESTR(ray->scene), id);
	/* tree-like objects will figure this notification */
	ray->object = SCM_MAKINUM(id);
	ino = OBJSTR(SCENESTR(ray->scene)->object_list[index]);
	ray->csg = ino->csg;
	ray->csg_sector = SCM_BOOL_F;
	if (!RayPrimitiveIntersection ((Ray*)ray, ino,
				       &ray->length, BIG, ray, &o)) {
	    fprintf (stderr,"Zbuffering went bonkers - raycast to the primitive returned FALSE");
	    return FALSE;
	}
	PointAtDistance ((Ray*)ray, ray->length, &ray->section);
	ray->object = o->cell;
	if (ray->csg_sector == SCM_BOOL_F)
	    /* only the tree primitive will set the csg_sector */
	    ray->csg_sector = o->csg;
	ray->ignore_id_out = id;
	ray->ignore_sphere_out = RI_NOT_A_SPHERE;
	return TRUE;
    }
    return FALSE;
}
