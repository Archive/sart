

typedef struct {
	void *ptr;
	int size;
	int maxsize;
} DPointer;

void DClear (DPointer *p, int size);
void *DAlloc (DPointer *p, int datasize);
void DFree (DPointer *p);

