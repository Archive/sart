
#ifndef LIGHTSOURCE_H
#define LIGHTSOURCE_H

typedef struct {
    Point3 *p0;
    Point3 *v1;
    Point3 *v2;
    Point3 *v3;
    double l1;
    double l2;
    double l3;
    double angle;
} LightSourceBox;

void CreateLightList (GeomObj *o, double angle, Raystruct *par, double w,
		      SCM *lightlist);

void get_lightsources_contrib (SCM rcell, Raystruct *ray, double tdiffuse, double tspec, double transmittance, double specularity, double roughness, double cos1, SCM light, SCM *dcol, SCM *scol, SCM *tcol);

#endif
