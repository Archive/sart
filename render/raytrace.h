
#ifndef _raytrace_h_
#define _raytrace_h_

#include "defs.h"
#include "linear.h"

#define X 0
#define Y 1
#define Z 2

#define COORD(p,axis) (((double*)(p))[axis])
#define INCAXIS(axis) ((axis)==Z ? X : (axis)+1)

/* uncomment to make the bsp process visible - spammy and memory intensive */
/* #define BSP_DEBUG 1
*/

typedef struct {
    Point3     origin;       /* ray origin */
    Point3     direction;    /* unit vector, indicating ray direction */
} Ray;

typedef struct {
    int height;
    int width;
    int *i;
    double *z;
    Matrix4 *trans;
} ZBuffer;


#define STACKSIZE 256

typedef struct {
    int node;
    double     min, max;
    double     *address, value;
    int axis;
} StackElem;

typedef struct {
    int       stackPtr;
    StackElem stack[STACKSIZE];
} Stack;

typedef boolean (*geominbox_type)(Point3 *, Point3 *, void *);

#define InitStack(stack) (stack->stack[0].node=0, stack->stackPtr=1)
#define emptyStack(stack) ((stack)->stackPtr==0)
#define push(stack,_node,_min,_max,_address,_value, _axis) { StackElem *p= &(stack->stack[stack->stackPtr++]); p->node=_node; p->min=_min; p->max=_max; p->address=_address; p->value=_value; p->axis=_axis;}
#define push2(stack,_address) { StackElem *p= &(stack->stack[stack->stackPtr++]); p->node=-1; p->address=_address; p->value=*_address;}

#endif
