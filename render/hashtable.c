
#include "hashtable.h"

int habs(int x)
{
    return x < 0 ? ~x : x;
}

/****** new_hashtable
 * NAME
 *   new_hashtable
 * SYNOPSIS
 *   HashTable *new_hashtable (int starting_alloc, int keysize, int valuesize)
 *              	       HashFunc hash, PtrEq key_eq)
 * FUNCTION
 *   Create a hashtable. You have to supply the initial number of buckets,
 *   keysize, valuesize and the pointers to hash function and key comparison
 *   function (hash should take a void* and return integer, key_eq should
 *   take a pair of void*, and return an integer).
 * NOTES
 *   This implementation automatically doubles the table size each time
 *   the number of datapairs reaches the number of buckets. This keeps
 *   the table reasonably sparse and very efficient.
 ******/

HashTable *new_hashtable (int starting_alloc, int keysize, int valuesize,
			  HashFunc hash, PtrEq key_eq)
{
    HashTable *p = NEWTYPE(HashTable);
    int i;

    p->size = starting_alloc;
    p->hash = hash;
    p->key_eq = key_eq;
    p->keysize = keysize;
    p->valuesize = valuesize;

    p->key_array = malloc(keysize * starting_alloc);
    p->value_array = malloc(valuesize * starting_alloc);
    p->entry = NEWARRAY(int, starting_alloc);
    p->next = NEWARRAY(int, starting_alloc);

    p->free = 0;
    for (i = 0; i < starting_alloc; i++) {
	p->entry[i] = -1;
	p->next[i] = i + 1;
    }
    p->next[starting_alloc - 1] = -1;

    return p;
}

/****** free_hashtable
 * NAME
 *   free_hashtable
 * SYNOPSIS
 *   void free_hashtable (HashTable *p)
 * FUNCTION
 *   Destroy the hashtable.
 ******/

void free_hashtable (HashTable *p)
{
    free(p->key_array);
    free(p->value_array);
    free(p->entry);
    free(p->next);
    free(p);
}

#define KEY(p_, i_) ((void*) (((char*)(p_)->key_array) + (i_) * p->keysize))
#define VALUE(p_, i_) ((void*) (((char*)(p_)->value_array) \
 + (i_) * p->valuesize))

/****** hash_find
 * NAME
 *   hash_find
 * SYNOPSIS
 *   void *hash_find (HashTable *p, void *key)
 * FUNCTION
 *   Return the pointer to value corresponding to the key (or NULL if there
 *   isn't one).
 ******/

void *hash_find (HashTable *p, void *key)
{
    int h = habs(p->hash(key));
    int ind = p->entry [h % p->size];

    while (ind != -1) {
	if (p->key_eq(KEY(p, ind), key))
	    return VALUE(p, ind);
	ind = p->next[ind];
    }
    return NULL;
}

/****** hash_add
 * NAME
 *   hash_add
 * SYNOPSIS
 *   HashTable *hash_add (HashTable *p, void *key, void *value)
 * FUNCTION
 *   Add a new key/value pair. You have to ensure that the key is not
 *   already present in the table (or the program will bug out).
 *   This function returns a HashTable pointer which may differ
 *   from the original.
 ******/

HashTable *hash_add (HashTable *p, void *key, void *value)
{
    int h;
    int ind;
    int tmp;
    
    if (p->free == -1) {
	HashTable *p1 = new_hashtable(p->size * 2, p->keysize, p->valuesize,
				      p->hash, p->key_eq);
	int j;

	for (j = 0; j < p->size; j++) {
	    int k = p->entry[j];
	    while (k != -1) {
		hash_add (p1, KEY(p, k), VALUE(p, k));
		k = p->next[k];
	    }
	}
	free_hashtable(p);
	p = p1;
    }
    h = habs(p->hash(key));
    ind = h % p->size;
    memcpy (KEY(p, p->free), key, p->keysize);
    memcpy (VALUE(p, p->free), value, p->valuesize);

    tmp = p->free;
    p->free = p->next[tmp];
    p->next[tmp] = p->entry[ind];
    p->entry[ind] = tmp;
    return p;
}

/****** hash_foreach
 * NAME
 *   hash_foreach
 * SYNOPSIS
 *   void *hash_foreach (HashTable *p, HashMapFunc f, void *data)
 * FUNCTION
 *   Iterate the function with key/value pairs. The function shouldn't
 *   call hash_add.
 ******/

void hash_foreach (HashTable *p, HashMapFunc f, void *data)
{
    int k;

    for (k = 0; k < p->size; k ++) {
	int ind = p->entry[k];
	while (ind != -1) {
	    f(KEY(p, ind), VALUE(p, ind), data);
	    ind = p->next[ind];
	}
    }
}
