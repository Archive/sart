#ifndef _wtree_h_
#define _wtree_h_

#include <stdlib.h>
#include "defs.h"

/****** Wavenode
 * NAME
 *  Wavenode
 * FUNCTION
 *  This is a node within a wavetree. If branches pointer is NULL,
 *  this is considered to be a leaf node. Otherwise, branch is an
 *  array of pointers to the other leaf nodes. The pointers within the
 *  array are also allowed to be NULL (signifying unused slots).
 *  value pointer should be casted to void*, as create_wtree takes
 *  care of allocation.
 * SOURCE */

typedef struct st_Wavenode {
    struct st_Wavenode **branches;    /* NULL if non-branching */
    int value[1];                     /* this will be heavily casted */
} Wavenode;

/*******/

/****** Wavetree
 * NAME
 *  Wavetree
 * FUNCTION
 *  Wavetree is extension of BSP/octree - it may have any number of
 *  dimensions. Each node is either a leaf node or branches into a
 *  number of smaller nodes, spatially represented as subcubes of the
 *  original. Each node contains a value of an unspecified type. The
 *  value size for leaf nodes can be specified separately (allowing
 *  for branch-or-store strategy).
 * SOURCE */

typedef void (*ValueDestructor)(Wavenode *);
typedef boolean (*SubdCriterium)(int depth, int x[], void *data);
typedef boolean (*ValueConstructor)(Wavenode *n, int depth,
				    int x[], boolean is_leaf, void *data);

typedef struct {
    Wavenode *root;             /* the root wavenode in the tree */
    int dim;                    /* dimensionality (3 for octree) */
    int branch_count;           /* = 1 << dim */
    SubdCriterium crit;         /* criterium to stop subdivision */
    void *critdata;             /* extra data passed to crit */
    ValueConstructor constr;    /* constructor for value (called when node
				   is created) */
    void *consdata;             /* extra data for the constructor */
    ValueDestructor d;          /* destructor for the value part of the node */
    size_t extra;               /* value size for non-leaves */
    size_t leaf_extra;          /* value size for leaves */
} Wavetree;

/*******/

void free_wavetree (Wavetree *t);
void *wavetree_ref (Wavetree *t, int depth, int x[]);
Wavenode *biggest_containing (Wavetree *t, int depth, int x[], int *depth_out);
Wavetree *create_wtree (int dim,
			ValueDestructor d,
			ValueConstructor constr, void *consdata,
			SubdCriterium crit, void *critdata,
			int extra, int leaf_extra);


#endif
