
#include "bsp.h"
#include "tmalloc.h"

static void *trays[32] = {NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
			  NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
			  NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
			  NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL};


/****** tmalloc
 * NAME
 *   tmalloc
 * SYNOPSIS
 *   void *tmalloc (size_t size)
 * FUNCTION
 *   Tray allocator. This is VERY fast allocation mechanism useful whenever
 *   you deal with lots of small chunks whose size is known in advance.
 *   tmalloc may be used in place of regular malloc. Note that for large
 *   blocks (above 128 bytes) tmalloc/tfree default to normal malloc/free.
 * SEE ALSO
 *   tfree
 ******/

void *tmalloc (size_t size)
{
    if (size > 30 * sizeof(int))
	return malloc(size);
    else {
	void *tray;
	int ind, i;

	ind = size / sizeof(int) + 1;
	size = ind * sizeof(int);
	tray = trays[ind];
	if (!tray) {
	    char *p = malloc(4000);
	    for (i = 0; i < 4000 - size; i += size)
		if (i < 4000 - size - size)
		    *(void**)(p + i) = (void*)(p + i + size);
		else
		    *(void**)(p + i) = NULL;
	    tray = trays[ind] = (void*)p;
	}
	trays[ind] = *(void**)tray;
	return tray;
    }
}



/****** tfree
 * NAME
 *   tfree
 * SYNOPSIS
 *   void tfree (void *p, size_t size)
 * FUNCTION
 *   Tfree is free function for tmalloc. In addition to the parameter
 *   required by the standard free, this one also requires the size of
 *   the block to be freed.
 ******/

void tfree (void *p, size_t size)
{
    if (size > 30 * sizeof(int))
	free(p);
    else {
	int ind;

	ind = size / sizeof(int) + 1;
	*(void**)p = trays[ind];
	trays[ind] = p;
    }
}

