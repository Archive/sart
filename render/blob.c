
#include "bsp.h"
#include "poly-implicit.h"

#define BLOB_SPHERE 20000

extern SCM make_dmatrix (int m, int n, double *e);
double blob_eval_boxed (Point3 *p, void *data);

/****** blob_value
 * NAME
 *   blob_value
 * SYNOPSIS
 *   double blob_value (Point3 *point, SCM blob)
 * FUNCTION
 *   Calculate the value of the blob function at point. Blob is
 *   of the format: (component ...) where component is one of:
 *       (sphere transform dummy) = sphere
 ******/

double blob_value (Point3 *point, SCM blob)
{
    SCM i;
    double sum = 0;

    for (i = blob; SCM_NNULLP(i); i = SCM_CDR(i)) {
	SCM component = SCM_CAR(i);
	Matrix4 *transform_mat
	    = (Matrix4*)SCM_VELTS(SCM_ARRAY_V(SCM_CADR(component)));
	int tag = SCM_INUM(SCM_CAR(component));
	double radius = SCM_REALPART(SCM_CADDR(component));
	Point3 origin;

	radius = radius * radius;
	component = SCM_CDDDR(component);
	V3MulPointByMatrix(point, transform_mat, &origin);

	switch (tag) {
	case BLOB_SPHERE: {
	    double rsq = 1 - V3SquaredLength(&origin) / radius;
	    sum += rsq > 0 ? rsq * rsq : 0;
	    break;
	}
	}
    }
    return sum;
}

/****** blob_gradient
 * NAME
 *   blob_gradient
 * SYNOPSIS
 *   void blob_gradient (Point3 *point, SCM blob, Point3 *g)
 * FUNCTION
 *   Gradient of the blob function.
 ******/

void blob_gradient (Point3 *point, SCM blob, Point3 *g)
{
    SCM i;
    double sum = 0;
    static Point3 zeropnt = {0, 0, 0};

    *g = zeropnt;
    for (i = blob; SCM_NNULLP(i); i = SCM_CDR(i)) {
	SCM component = SCM_CAR(i);
	Matrix4 *transform_mat
	    = (Matrix4*)SCM_VELTS(SCM_ARRAY_V(SCM_CADR(component)));
	int tag = SCM_INUM(SCM_CAR(component));
	double radius = SCM_REALPART(SCM_CADDR(component));
	Point3 origin;

	radius = radius * radius;
	component = SCM_CDDDR(component);
	V3MulPointByMatrix(point, transform_mat, &origin);

	switch (tag) {
	case BLOB_SPHERE: {
	    double rsq = 1 - V3SquaredLength(&origin) / radius;
	    if (rsq > 0)
		V3Combine(&origin, g, g, - 4.0 * rsq, 1.0);
	    break;
	}
	}
    }
}

typedef struct {
    double min, max, curvature;
} MinMax;

/****** blob_minmax
 * NAME
 *   blob_minmax
 * SYNOPSIS
 *   void blob_minmax (Point3 *b1, Point3 *b2, SCM blob, MinMax *ret)
 * FUNCTION
 *   find the minimal and maximal value for the blob function, as well
 *   as other approximate values needed to decide whether to terminate
 *   the zero-crossing search.
 ******/

void blob_minmax (Point3 *b1, Point3 *b2, SCM blob, MinMax *ret)
{
    SCM i;
    double min = 0, max = 0;
    static Point3 zeropnt = {0, 0, 0};
    Point3 gradcenter, gradx, grady, gradz;
    double maxrad = 0;

    for (i = blob; SCM_NNULLP(i); i = SCM_CDR(i)) {
	SCM component = SCM_CAR(i);
	Matrix4 *transform_mat
	    = (Matrix4*)SCM_VELTS(SCM_ARRAY_V(SCM_CADR(component)));
	int tag = SCM_INUM(SCM_CAR(component));
	double r0, radius = SCM_REALPART(SCM_CADDR(component));
	Point3 p1, p2;

	radius = (r0 = radius ) * radius;
	component = SCM_CDDDR(component);
	TransformBox(b1, b2, &p1, &p2, transform_mat);

	switch (tag) {
	case BLOB_SPHERE: {
	    double mindist, maxdist, minval, maxval;
	    double radfactor;
	    Point3 p;

	    mindist = PointBoxSqDistance (&zeropnt, &p1, &p2);
	    p.x = fabs(p1.x) > fabs(p2.x) ?  fabs(p1.x) : fabs(p2.x);
	    p.y = fabs(p1.y) > fabs(p2.y) ?  fabs(p1.y) : fabs(p2.y);
	    p.z = fabs(p1.z) > fabs(p2.z) ?  fabs(p1.z) : fabs(p2.z);
	    maxdist = V3SquaredLength(&p);
	    
	    /* estimate the size of the box compared to the radius of the
	       component */
	    radfactor = (sqrt(maxdist) - sqrt(mindist)) / r0;
	    minval = 1 - maxdist / radius;
	    maxval = 1 - mindist / radius;
	    if (minval > 0 || maxval > 0)
		if (radfactor > maxrad)
		    /* looking for the biggest component */
		    maxrad = radfactor;
	    min += minval > 0 ? minval * minval : 0;
	    max += maxval > 0 ? maxval * maxval : 0;
	    break;
	}
	}
    }
    ret->min = min;
    ret->max = max;

    /* now calculate the gradient direction change */
    if (maxrad > 0.5)
	ret->curvature = BIG;
    else {
	/* we use the roughest estimate for the time being */
	ret->curvature = maxrad;
    }
}

/****** blob_find_bounding_box
 * NAME
 *   blob_find_bounding_box
 * SYNOPSIS
 *   void blob_find_bounding_box (SCM blob, Point3 *min, Point3 *max)
 * FUNCTION
 *   Find the absolute bounding box for all the blob components
 ******/

void blob_find_bounding_box (SCM blob, Point3 *min, Point3 *max)
{
    SCM i;
    boolean first = TRUE;

    for (i = blob; SCM_NNULLP(i); i = SCM_CDR(i)) {
	SCM component = SCM_CAR(i);
	Matrix4 *transform_mat
	    = (Matrix4*)SCM_VELTS(SCM_ARRAY_V(SCM_CADR(component)));
	int tag = SCM_INUM(SCM_CAR(component));
	double radius = SCM_REALPART(SCM_CADDR(component));
	Point3 origin, min1, max1, min2, max2;
	Matrix4 boxmat;

	component = SCM_CDDDR(component);
	V3InvertMatrix(transform_mat, &boxmat);

	switch (tag) {
	case BLOB_SPHERE: {
	    min1.x = min1.y = min1.z = - radius;
	    max1.x = max1.y = max1.z = radius;
	    break;
	}
	}
	TransformBox(&min1, &max1, &min2, &max2, &boxmat);
	if (first) {
	    *min = min2;
	    *max = max2;
	    first = FALSE;
	} else {
	    if (min2.x < min->x)
		min->x = min2.x;
	    if (min2.y < min->y)
		min->y = min2.y;
	    if (min2.z < min->z)
		min->z = min2.z;
	    if (max2.x > max->x)
		max->x = max2.x;
	    if (max2.y > max->y)
		max->y = max2.y;
	    if (max2.z > max->z)
		max->z = max2.z;
	}
    }
}

typedef struct {
    double main_size;
    Point3 bbox_min, bbox_scale;
    SCM material;
    SCM ret;
    SCM blob;
    int maxdepth;
    double limit;
    int nlist, listmax;
    int *ilist;
} BlobData;

/* Here come auxiliary functions required by poly_mesh */

boolean blob_subdivide_crit (int depth, int x[], void *data)
{
    BlobData *d = data;
    Point3 min, max;
    double fact = 1.0 / (1 << depth);
    MinMax result;

    if (depth > d->maxdepth)
	return FALSE;
    min.x = x[0] * fact; min.y = x[1] * fact; min.z = x[2] * fact;
    max.x = min.x + fact; max.y = min.y + fact; max.z = min.z + fact;
    V3Scale(&min, d->main_size);
    V3Add(&min, &d->bbox_min, &min);
    V3Scale(&max, d->main_size);
    V3Add(&max, &d->bbox_min, &max);
    blob_minmax (&min, &max, d->blob, &result);
    return
	result.max > d->limit
	&& result.min < d->limit
	&& result.curvature > 0.2;
}

void blob_collect_vertices (void *data, SubdInfo *p)
{
    BlobData *d = (BlobData*)data;
    Point3 *verts_copy = MNEWARRAY(Point3, p->nverts);
    Point3 *norms_copy = MNEWARRAY(Point3, p->nverts);
    SCM c;
    SCM shape = SCM_LIST2(SCM_MAKINUM(d->nlist), SCM_MAKINUM(3));
    SCM ra;
    scm_array_dim *s;
    int i;

    for (i = 0; i < p->nverts; i++) {
	Point3 r, t, u, *v = &verts_copy[i];
	double a, delta = 0.0001;

	r = p->verts[i];

	t = r;
	V3Scale (&t, d->main_size);
	V3Add (&t, &d->bbox_min, v);

	a = blob_eval_boxed(&r, d);
	t = r; t.x += delta;
	u.x = blob_eval_boxed(&t, d) - a;
	t = r; t.y += delta;
	u.y = blob_eval_boxed(&t, d) - a;
	t = r; t.z += delta;
	u.z = blob_eval_boxed(&t, d) - a;
	V3Normalize(&u);
	norms_copy[i] = u;
    }

    SCM_NEWCELL(c);
    SCM_DEFER_INTS;
    SCM_SETCHARS(c, d->ilist);
    SCM_SETLENGTH(c, d->nlist, scm_tc7_ivect);
    SCM_ALLOW_INTS;
    ra = scm_shap2ra(shape, "Blob subdivision");
    s = SCM_ARRAY_DIMS(ra);
    SCM_ARRAY_V(ra) = c;
    s[0].inc=3;
    s[1].inc=1;
    
    d->ret = make_patch (make_dmatrix(p->nverts, 3, (double*)verts_copy),
			 make_dmatrix(p->nverts, 3, (double*)norms_copy),
			 ra, d->material);
}

void blob_collect_triangles (void *data, Point3 *verts,
			     int p1, int p2, int p3)
{
    BlobData *d = (BlobData *) data;
    int index = d->nlist * 3;

    d->nlist ++;
    if (d->nlist == d->listmax) {
	d->listmax *= 2;
	d->ilist = (int*)realloc(d->ilist, d->listmax * 3 * sizeof(int));
    }

    d->ilist[index++] = p1;
    d->ilist[index++] = p2;
    d->ilist[index] = p3;
}

double blob_eval_boxed (Point3 *p, void *data)
{
    Point3 t1 = *p, t2;
    BlobData *b = (BlobData *) data;

    V3Scale(&t1, b->main_size);
    V3Add(&t1, &b->bbox_min, &t2);
    return blob_value (&t2, b->blob) - b->limit;
}

SCM BlobSubdivide (SCM blob, double limit, int max_depth, SCM material)
{
    BlobData b;
    double main_size;

    blob_find_bounding_box (blob, &b.bbox_min, &b.bbox_scale);
    V3Sub (&b.bbox_scale, &b.bbox_min, &b.bbox_scale);
    main_size = b.bbox_scale.x;
    if (main_size < b.bbox_scale.y) main_size = b.bbox_scale.y;
    if (main_size < b.bbox_scale.z) main_size = b.bbox_scale.z;
    b.main_size = main_size;
    V3Scale (&b.bbox_scale, 1 / main_size);
    
    b.nlist = 0;
    b.listmax = 256;
    /* This will be freed after the inclusion into the uniform array */
    b.ilist = NEWARRAY(int, b.listmax * 3);
    b.material = material;
    b.blob = blob;
    b.maxdepth = max_depth;
    b.limit = limit;
    poly_mesh(blob_collect_triangles, &b,
	      blob_collect_vertices, &b,
	      blob_eval_boxed, &b,
	      blob_subdivide_crit, &b);
    return b.ret;
}

/****** subdivide_blob
 * NAME
 *   subdivide_blob
 * SYNOPSIS
 *   SCM subdivide_blob (SCM blob, SCM max_depth, SCM limit, SCM material)
 *   make-blob (gsubr)
 * FUNCTION
 *   Return Patch primitive created by subdividing a blob.
 ******/

/* SUBR (subr3) */
char s_subdivide_blob[] = "make-blob";
SCM subdivide_blob (SCM blob, SCM limit, SCM max_depth, SCM material)
{
    SCM i, p;
    extern SCM minvert_subr1 (SCM m);


    SCM_ASSERT (scm_ilength(blob) > 0, blob, SCM_ARG1, s_subdivide_blob);
    SCM_ASSERT (SCM_NIMP(limit) && SCM_REALP(limit), limit, SCM_ARG2,
	    s_subdivide_blob);
    SCM_ASSERT (SCM_INUMP(max_depth), max_depth, SCM_ARG3,
		s_subdivide_blob);
    /* invert the transformation matrices */
    for (p = SCM_LIST0, i = blob; SCM_NNULLP(i); i = SCM_CDR(i)) {
	SCM l;
	SCM head = SCM_CAR(i);

	p = scm_cons(scm_cons(SCM_CAR(head),
			      scm_cons(minvert_subr1(SCM_CADR(head)),
				       SCM_CDDR(head))),
		     p);
    }
    blob = p;
    return BlobSubdivide (blob, SCM_REALPART(limit),
			  SCM_INUM(max_depth), material);
}

void init_blob_c (void)
{
    scm_make_gsubr(s_subdivide_blob,4,0,0,subdivide_blob);
    scm_sysintern("sphere-component", SCM_MAKINUM(BLOB_SPHERE));
}
