

typedef struct {
    int n;
    Point2 *v;
} Poly;

Poly CreatePoly (int verts)
{
    Poly p;

    p.n=verts;
    p.v=(Point2*)malloc(verts*sizeof(Point2*));
    return p;
}

void DestroyPoly (Poly p)
{
    free p->v;
}

/* connect vertices s and e, split along the new edge */

void SplitPoly (Poly *p, Poly *p1, Poly *p2, int s, int e)
{
    int i;

    *p1=CreatePoly (e-s+1);
    *p2=CreatePoly (p->n-e+s+1);
    memcpy (p1->v,p->v+s,p1->n*sizeof(Point2));
    memcpy (p2->v,p->v+e,(p->n-e+1)*sizeof(Point2));
    memcpy (p2->v+(p->n-e+1),p->v,(p->s+1)*sizeof(Point2));
}

int Intersect (Point2 p1, Point2 p2, Point2 q1, Point2 q2, Point2 *p)
{
    double t,x1,x,y1,y;

    p2.x-=p1.x; p2.y-=p1.y; x1=q2.x-q1.x; y1=q2.y-q1.y;
    x=q1.x-p1.x; y=q1.y-p1.y;

    d=p2.y*x1-p2.x*y1;
    if (abs(d)<EPS) return 0;
    t=(y*x1-x*y1)/d;
    if (t<0 || t>1) return 0;
    p->x=p1.x+t*p2.x;
    if ((p->x < q1.x && p->x < q2.x) || (p->x > q1.x && p->x > q2.x))
	return 0;
    p->y=p2.y+t*p2.y;
    return 1;
}

