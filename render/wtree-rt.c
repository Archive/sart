
#include "wtree-rt.h"
#include "raytrace.h"

static
boolean ray_tbox_intersect (TraverseData *d, double *min, double *max)
{
    int i;
    double a,b;

    *max = BIG;
    *min = -BIG;
    for (i = 0; i < d->t->dim; i++) 
	if (d->dir[i] != 0.0) {
	    a = (d->pmin[i] - d->start[i]) / d->dir[i];
	    b = (d->pmax[i] - d->start[i]) / d->dir[i];
	    if (d->dir[i] > 0.0) {
		if (*min < a) *min = a;
		if (*max > b) *max = b;
	    } else {
		if (*min < b) *min = b;
		if (*max > a) *max = a;
	    }
	    if (*min > *max) return FALSE;
	}
    return TRUE;
}

/****** ray_wtree_traverse
 * NAME
 *   ray_wtree_traverse
 * SYNOPSIS
 *   void ray_wtree_traverse (double maxlength, TraverseData *d)
 * FUNCTION
 *   Traverse voxel tree using TraverseData pointed by d. Maxlength limits
 *   the traversal distance (to stay within the volume bounds). Output
 *   should be handled via TraverseFun stored in d.
 ******/

void ray_wtree_traverse (double maxlength, TraverseData *d)
{
    int axis;
    double min, max;
    Stack *stack;
    int maxaxis = d->t->dim;
    Wavenode **node;

    /* All the globals should be initialized at this point */

    axis = 0;
    node = &d->t->root;

    /* This one uses start, dir, pmin and pmax */
    if (!ray_tbox_intersect (d, &min, &max)) {
	return;
    }

    if (min < EPS) min = EPS;
    if (max > maxlength) max = maxlength;

    stack=NEWTYPE(Stack);
    InitStack(stack);

#define NEARNODE(node) (node)
#define FARNODE(node) (node + (1 << axis))

    while (!emptyStack(stack)) {
	while (axis || (*node)->branches) {
	    double position, dist;

	    position = 0.5 * (d->pmin[axis] + d->pmax[axis]);
	    
	    if (fabs(d->dir[axis] > EPS))
		dist = (position - d->start[axis]) / d->dir[axis];
	    else
		dist = -1;

	    if (!axis)
		node = (*node)->branches;

	    if (d->start[axis] <= position) {
		/* We're in the closer half-node. */

		if (dist < 0 || dist > max) {
		    /* no intersection within the node - examine only
		       the near node */

		    node = NEARNODE(node);
		    push2(stack, &d->pmax[axis]);
		    d->pmax[axis] = position;
		}
		else if (dist < min) {
		    /* plane is intersected before the node is entered
		       at all, only the far child need be processed */

		    node = FARNODE(node);
		    push2(stack, &d->pmin[axis]);
		    d->pmin[axis] = position;
		}
		else {
		    /* both nodes need a check - there will be a branching
		       point on the stack */

		    push2(stack, &d->pmin[axis]);
		    push(stack, (long)FARNODE(node), dist, max,
			 &d->pmin[axis], position, axis);
		    node = NEARNODE(node);
		    push2(stack, &d->pmax[axis]);
		    d->pmax[axis] = position;
		    max = dist;
		}
	    }
	    else {
		/* same thing, with children replaced */

		if (dist < 0 || dist > max) {
		    node = FARNODE(node);
		    push2(stack, &d->pmin[axis]);
		    d->pmin[axis] = position;
		}
		else if (dist < min) {
		    node = NEARNODE(node);
		    push2(stack, &d->pmax[axis]);
		    d->pmax[axis] = position;
		}
		else {
		    push2(stack, &d->pmax[axis]);
		    push(stack, (long)NEARNODE(node), dist, max,
			 &d->pmax[axis], position, axis);
		    node = FARNODE(node);
		    push2(stack, &d->pmin[axis]);
		    d->pmin[axis] = position;
		    max = dist;
		}
	    }
	    axis++;
	    if (axis == maxaxis) axis = 0;
	}

	/* at this point, we ran into a non-split child. */

	if (d->f((void*)(*node)->value, d->fdata)) {
	    free (stack);
	    return;
	}

	/* Now let's backtrack */

	for (;;) {
	    StackElem *p = &(stack->stack[--stack->stackPtr]);

	    if (emptyStack(stack)) break;
	    *p->address = p->value;
	    node = (Wavenode**)p->node;
	    /* -1 marks a non-branching node on stack. It is
	       only there to save the old cluster bounds */
	    if ((int)node != -1) {
		axis = p->axis;
		axis++;
		if (axis == maxaxis) axis = 0;
	        min = p->min;
	        max = p->max;
	        break;
	    }
	}
    }
    free(stack);
}
