
#ifndef _tmalloc_h_
#define _tmalloc_h_

#include <stdlib.h>

void *tmalloc (size_t size);
void tfree (void *p, size_t size);

#endif


