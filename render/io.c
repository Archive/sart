
#include "bsp.h"


/****** write_32
 * NAME
 *   write_32
 * SYNOPSIS
 *   void write_32 (unsigned l, int bytes, SCM port)
 * FUNCTION
 *   Write unsigned integer to the Scheme port. bytes is the precision.
 ******/

void write_32 (unsigned l, int bytes, SCM port)
{
    while (bytes--) {
	scm_putc(l&255, port);
	l>>=8;
    }
}

/****** read_32
 * NAME
 *   read_32
 * SYNOPSIS
 *   unsigned read_32 (int bytes, SCM port)
 * FUNCTION
 *   Read and return integer from the port.
 * SEE ALSO
 *   write_32
 ******/

unsigned read_32 (int bytes, SCM port)
{
    unsigned l, k;

    k=l=0;
    while (bytes--) {
	k+=8;
	l+=((unsigned)scm_getc(port))<<k;
    }
    return l;
}



/****** write_bit, read_bit, flush_bits
 * NAME
 *   write_bit, read_bit
 * SYNOPSIS
 *   void write_bit (int b, SCM port)
 *   int read_bit (SCM port)
 * FUNCTION
 *   Bitwise file ops. For writing, initialize the bit_count to 0 and
 *   bit_buffer to 0. For reading, initialize bit_count to 1. Finally,
 *   make sure to do scm_putc (bit_buffer&255, port) after all writes are
 *   done.
 ******/

static unsigned bit_count;
static unsigned bit_buffer;

void write_bit (int b, SCM port)
{
    bit_buffer = (bit_buffer << 1) | b;
    bit_count++;
    if (bit_count == 8) {
	bit_count = 0;
	scm_putc (bit_buffer&255, port);
    }
}

int read_bit (SCM port)
{
    int b;

    bit_count--;
    if (!bit_count) {
	bit_count = 8;
	bit_buffer = scm_getc(port);
    }
    b = (bit_buffer & 128) >> 7;
    bit_buffer <<= 1;
    return b;
}
	
/****** dbl_exponent
 * NAME
 *   dbl_exponent
 * SYNOPSIS
 *   int dbl_exponent (double val)
 * FUNCTION
 *   Take value, and normalize it to 0.5 - 1.0-EPSILON range. Return the
 *   exponent needed to do so.
 ******/

int dbl_exponent (double val)
{
    int i;

    i=0;
    if (val == 0)
	val = 1e-300;
    while (abs(val) < 0.5) {
	val *= 2;
	i++;
    }
    while (abs(val) >= 1.0) {
	val /= 2;
	i--;
    }
    return i;
}

/****** shift_dbl_exponent
 * NAME
 *   shift_dbl_exponent
 * SYNOPSIS
 *   void shift_dbl_exponent (double *val_ret, int exp)
 * FUNCTION
 *   Add exp to the exponent of *val_ret.
 ******/

void shift_dbl_exponent (double *val_ret, int exp)
{
    if (exp < 0)
	while (exp++)
	    *val_ret = *val_ret / 2;
    else
	while (exp--)
	    *val_ret = *val_ret * 2;
}

/****** write_common_exp_vector
 * NAME
 *   write_common_exp_vector
 * SYNOPSIS
 *   void write_common_exp_vector (double *val, int n, double sc, double sh
 *                                 int exp_bytes, int mant_bytes, SCM port)
 * FUNCTION
 *   Write n values to the port. The exponent will be written only once,
 *   and it will be the greatest common exponent.
 ******/

void write_common_exp_vector (double *val, int n, double sc, double sh,
			      int exp_bytes, int mant_bytes, SCM port)
{
    int max, i, e;
    double d;

    if (exp_bytes > 0) {
	max = -1000000;
	for (i=0; i<n; i++) {
	    e = dbl_exponent(val[i]);
	    if (max < e) max=e;
	}
	write_32 (max+(1 << (exp_bytes*8 - 1)), exp_bytes, port);
    }
    else
	max = 0;
    if (exp_bytes < 0)
	for (i=0; i<n; i++) {
	    int b, j, n, first_batch;

	    b = (val[i]-sh)*sc;
	    if (!b)
		write_bit(0, port);
	    else {
		write_bit(1, port);
		if (b < 0) {
		    b = -b;
		    write_bit (1, port);
		}
		else
		    write_bit (0, port);

		n = 2;
		first_batch = 1;
		while (b) {
		    if (!first_batch)
			write_bit(1, port);
		    first_batch = 0;
		    for (j=n; j; j--) {
			write_bit(b&1, port);
			b >>= 1;
		    }
		    n <<= 1;
		}
		write_bit(0, port);
	    }
	}
    else
	for (i=0; i<n; i++) {
	    d = (val[i] - sh) * sc;
	    shift_dbl_exponent (&d, max);
	    if (d<0)
		d=1-d;
	    d *= 128;
	    scm_putc ((unsigned)d, port);
	    d -= floor(d);
	    e=mant_bytes - 1;
	    while (e--) {
		d *= 256;
		scm_putc ((unsigned)d, port);
		d -= floor(d);
	    }
	}
}

/****** read_common_exp_vector
 * NAME
 *   read_common_exp_vector
 * SYNOPSIS
 *   void read_common_exp_vector (double *val, int n, double sc, double sh
 *                                int exp_bytes, int mant_bytes, SCM port)
 * FUNCTION
 *   read n values from port into vector val. exp_bytes and mant_bytes
 *   control the precision.
 * SEE ALSO
 *   write_common_exp_vector
 ******/

void read_common_exp_vector (double *val, int n, double sc, double sh,
			     int exp_bytes, int mant_bytes, SCM port)
{
    int max, i, e, s;
    unsigned c;
    double d, k;

    if (exp_bytes > 0)
	max = read_32(exp_bytes, port) - (1 << (exp_bytes*8 - 1));
    else
	max = 0;
    if (exp_bytes < 0)
	for (i=0; i<n; i++) {
	    int b, sign, n, j, k, first_batch;

	    if (!read_bit(port))
		val[i] = sh;
	    else {
		sign = read_bit(port);
		b = 0;
		k = 0;
		n = 2;
		first_batch = 1;
		while (first_batch || read_bit(port)) {
		    first_batch = 0;
		    for (j=n; j; j--) {
			b += read_bit(port) << k;
			k++;
		    }
		    n <<= 1;
		}
		if (sign)
		    b = -b;
		val[i] = b / sc + sh;
	    }
	}
    else
	for (i=0; i<n; i++) {
	    c = (unsigned)scm_getc(port);
	    k = 1/128.0;
	    if (c>128) {
		c -= 128;
		s = 1;
	    } else
		s = 0;
	    d = c * k;
	    e=mant_bytes - 1;
	    while (e--) {
		k /= 256;
		d += (unsigned)scm_getc(port) * k;
	    }
	    shift_dbl_exponent (&d, max);
	    if (s) d = -d;
	    val[i]=d / sc + sh;
	}
}

/****** spb_write
 * NAME
 *   spb_write
 * SYNOPSIS
 *   SCM spb_write (SCM ra, SCM port, SCM exp_bytes, SCM mantissa_bytes,
 *                  [SCM index_count, SCM scale, SCM shift])
 *   (spb-write ra port exp-bytes mantissa-bytes
 *              optional: index-count scale shift)
 * FUNCTION
 *   Output the ra to the port. If index-count is non-zero, it will
 *   write the array in pieces with common exponent. If shift is #f,
 *   it will look for the minimal element and use that as the shift.
 *   Element actually written is (x-shift)*scale. Finally, if exp-bytes
 *   is < 0, it will use entropic coding (the array, after scaling and
 *   shifting, must map to integers).
 ******/

struct cwdata {
    int n, ind, exp_bytes, mant_bytes;
    double sc, sh, *v;
    SCM port;
};

int do_write (SCM v, struct cwdata *d, SCM dummy)
{
    int n, inc;
    double *base;

    n = SCM_ARRAY_DIMS(v)->ubnd - SCM_ARRAY_DIMS(v)->lbnd + 1;
    inc = SCM_ARRAY_DIMS(v)->inc;
    base = (double*)SCM_VELTS(SCM_ARRAY_V(v)) + SCM_ARRAY_BASE(v);
    for (; n--; base += inc) {
	if (!(d->v))
	    d->v = NEWARRAY(double, d->n);
	d->v[d->ind] = *base;
	d->ind++;
	if (d->ind == d->n) {
	    d->ind = 0;
	    write_common_exp_vector (d->v, d->n, d->sc, d->sh,
				     d->exp_bytes, d->mant_bytes, d->port);
	}
    }
    return 1;
}

int do_read (SCM v, struct cwdata *d, SCM dummy)
{
    int n, inc;
    double *base;

    n = SCM_ARRAY_DIMS(v)->ubnd - SCM_ARRAY_DIMS(v)->lbnd + 1;
    inc = SCM_ARRAY_DIMS(v)->inc;
    base = (double*)SCM_VELTS(SCM_ARRAY_V(v)) + SCM_ARRAY_BASE(v);
    for (; n--; base += inc) {
	if (!(d->v))
	    d->v = NEWARRAY(double, d->n);
	if (d->ind == d->n) {
	    d->ind = 0;
	    read_common_exp_vector (d->v, d->n, d->sc, d->sh,
				    d->exp_bytes, d->mant_bytes, d->port);
	}
	*base = d->v[d->ind];
	d->ind++;
    }
    return 1;
}

int get_shift (SCM v, struct cwdata *d, SCM dummy)
{
    double c;
    int n, inc;
    double *base;

    n = SCM_ARRAY_DIMS(v)->ubnd - SCM_ARRAY_DIMS(v)->lbnd + 1;
    inc = SCM_ARRAY_DIMS(v)->inc;
    base = (double*)SCM_VELTS(SCM_ARRAY_V(v)) + SCM_ARRAY_BASE(v);
    for (; n--; base += inc) {
	c = *base;
	if (d->n) {
	    d->sh = c;
	    d->n=0;
	}
	else
	    if (c < d->sh)
		d->sh = c;
    }
    return 1;
}

char s_spb_write[]="spb-write";
SCM spb_write (SCM ra, SCM port, SCM a_ebyt, SCM a_mbyt,
	       SCM a_icnt, SCM a_scale, SCM a_shift)
{
    int k, mbytes, ebytes, check_shift, icnt, i, n;
    double sc, sh;
    struct cwdata d;

    sc = 1.0;
    sh = 0.0;
    check_shift = 0;
    icnt = 0;

    SCM_ASSERT(SCM_NIMP(ra) && (SCM_TYP7(ra)==scm_tc7_dvect || (SCM_ARRAYP(ra) &&
	   SCM_TYP7(SCM_ARRAY_V(ra)) == scm_tc7_dvect)), ra, SCM_ARG1, s_spb_write);
    SCM_ASSERT(SCM_NIMP(port) && SCM_OUTPORTP(port), port, SCM_ARG2, s_spb_write);

    SCM_ASSERT(SCM_INUMP(a_ebyt), a_ebyt, SCM_ARG3, s_spb_write);
    ebytes = SCM_INUM(a_ebyt);
    SCM_ASSERT(SCM_INUMP(a_mbyt), a_mbyt, SCM_ARG4, s_spb_write);
    mbytes = SCM_INUM(a_mbyt);

    if (a_icnt!=SCM_UNDEFINED) {
	SCM_ASSERT(SCM_INUMP(a_icnt), a_icnt, SCM_ARG5, s_spb_write);
	icnt = SCM_INUM(a_icnt);
	SCM_ASSERT(icnt>=0 && ((!SCM_ARRAYP(ra) && icnt<=1) ||
			   (icnt <= SCM_ARRAY_NDIM(ra))),
	       a_icnt, SCM_ARG5, s_spb_write);
    }
    if (a_scale!=SCM_UNDEFINED) {
	SCM_ASSERT(SCM_NIMP(a_scale) && SCM_REALP(a_scale),
	       a_scale, (int)"Wrong type in arg 6", s_spb_write);
	sc = SCM_REALPART(a_scale);
    }
    if (a_shift!=SCM_UNDEFINED) {
	if (a_shift == SCM_BOOL_F)
	    check_shift = 1;
	else {
	    SCM_ASSERT((SCM_NIMP(a_shift)) && SCM_REALP(a_shift),
		a_shift, (int)"Wrong type in arg 7", s_spb_write);
	    sh = SCM_REALPART(a_shift);
	}
    }

    n=1;
    if (icnt) {
	if (!SCM_ARRAYP(ra))
	    n=SCM_LENGTH(ra);
	else {
	    for (i=0; i<icnt; i++)
		n*=SCM_ARRAY_DIMS(ra)[i].ubnd-SCM_ARRAY_DIMS(ra)[i].lbnd+1;
	}
    }
    if (check_shift) {
	d.n = 1;
	scm_ramapc (get_shift, (int)&d, ra, SCM_EOL, s_spb_write);
	sh = d.sh;
    }
    d.v = NULL;
    d.n = n;
    d.ind = 0;
    d.exp_bytes = ebytes;
    d.mant_bytes = mbytes;
    d.sc = sc;
    d.sh = sh;
    d.port = port;
    if (ebytes < 0) {
	bit_count = 0;
	bit_buffer = 0;
    }
    scm_ramapc (do_write, (int)&d, ra, SCM_EOL, s_spb_write);
    if (d.v)
	free(d.v);
    if (ebytes < 0)
	scm_putc((bit_buffer << (8-bit_count))&255, port);
    return SCM_UNSPECIFIED;
}

/****** spb_read
 * NAME
 *   spb_read
 * SYNOPSIS
 *   SCM spb_read (SCM ra, SCM port, SCM exp_bytes, SCM mantissa_bytes,
 *                 [SCM intex_count, SCM scale, SCM shift])
 *   (spb-read ra port exp-bytes mantissa-bytes
 *             optional: index-count scale shift)
 * FUNCTION
 *   This is exact inverse of spb_write.
 ******/

char s_spb_read[]="spb-read";
SCM spb_read (SCM ra, SCM port, SCM a_ebyt, SCM a_mbyt,
	      SCM a_icnt, SCM a_scale, SCM a_shift)
{
    int k, mbytes, ebytes, icnt, i, n;
    double sc, sh;
    struct cwdata d;

    sc = 1.0;
    sh = 0.0;
    icnt = 0;

    SCM_ASSERT(SCM_NIMP(ra) && (SCM_TYP7(ra)==scm_tc7_dvect || (SCM_ARRAYP(ra) &&
	   SCM_TYP7(SCM_ARRAY_V(ra)) == scm_tc7_dvect)), ra, SCM_ARG1, s_spb_read);
    SCM_ASSERT(SCM_NIMP(port) && SCM_INPORTP(port), port, SCM_ARG2, s_spb_read);

    SCM_ASSERT(SCM_INUMP(a_ebyt), a_ebyt, SCM_ARG3, s_spb_read);
    ebytes = SCM_INUM(a_ebyt);
    SCM_ASSERT(SCM_INUMP(a_mbyt), a_mbyt, SCM_ARG4, s_spb_read);
    mbytes = SCM_INUM(a_mbyt);

    if (a_icnt!=SCM_UNDEFINED) {
	SCM_ASSERT(SCM_INUMP(a_icnt), a_icnt, SCM_ARG5, s_spb_read);
	icnt = SCM_INUM(a_icnt);
	SCM_ASSERT(icnt>=0 && ((!SCM_ARRAYP(ra) && icnt<=1) ||
			   (icnt <= SCM_ARRAY_NDIM(ra))),
	       a_icnt, SCM_ARG5, s_spb_read);
    }
    if (a_scale!=SCM_UNDEFINED) {
	SCM_ASSERT(SCM_NIMP(a_scale) && SCM_REALP(a_scale),
	       a_scale, (int)"Wrong type in arg 6", s_spb_read);
	sc = SCM_REALPART(a_scale);
    }
    if (a_shift!=SCM_UNDEFINED) {
	SCM_ASSERT((SCM_NIMP(a_shift)) && SCM_REALP(a_shift),
	       a_shift, (int)"Wrong type in arg 7", s_spb_read);
	sh = SCM_REALPART(a_shift);
    }

    n=1;
    if (icnt) {
	if (!SCM_ARRAYP(ra))
	    n=SCM_LENGTH(ra);
	else {
	    for (i=0; i<icnt; i++)
		n*=SCM_ARRAY_DIMS(ra)[i].ubnd-SCM_ARRAY_DIMS(ra)[i].lbnd+1;
	}
    }
    d.v = NULL;
    d.n = n;
    d.ind = n;
    d.exp_bytes = ebytes;
    d.mant_bytes = mbytes;
    d.sc = sc;
    d.sh = sh;
    d.port = port;
    if (ebytes < 0)
	bit_count = 1;
    scm_ramapc (do_read, (int)&d, ra, SCM_EOL, s_spb_read);
    if (d.v)
	free(d.v);
    return SCM_UNSPECIFIED;
}

void init_io_c (void)
{
    /* __PERL__ This code is automatically generated. Don't modify. */


    /* __END__ */
    
    scm_make_gsubr(s_spb_write,4,3,0,spb_write);
    scm_make_gsubr(s_spb_read,4,3,0,spb_read);
}
