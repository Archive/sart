/****h* (render)/bsp.c [0.1]
 * NAME
 *  bsp.c - routines for BSP tree traversal
 * COPYRIGHT
 *   Miroslav Silovic
 * MODIFICATION HISTORY
 *  
 * NOTES
 *  The procedures here are intensively called from the rest of the
 *  renderer. Some of them could use more elegance. Basic code is
 *  from Graphics Gems III, and has been extensively modified and sped
 *  up.
 * BUGS
 *  Since elements of GeomObjList are declared as pointers to GeomObjs
 *  instead of union type (to include, for example, integer references
 *  to elements of a tesselated object), this module will fail on the
 *  architectures where integers and pointers are not of the same length.
 *******
*/

#include <stdio.h>
#include <math.h>
#include "bsp.h"

/* --------------------supporting functions/macros --------------------*/

/****** PointAtDistance
 * NAME
 *  PointAtDistance
 * SYNOPSIS
 *  void PointAtDistance (Ray *ray, double distance, Point3 *pt)
 * FUNCTION
 *  Find a point located at the 'distance' from the origin of the ray.
 * INPUTS
 *  ray - contains origin and the direction.
 *  distance - as explained
 * RESULT
 *  pt will be modified.
 ******
 */

void PointAtDistance (Ray *ray, double distance, Point3 *pt)
{
    pt->x = ray->origin.x + distance * ray->direction.x;
    pt->y = ray->origin.y + distance * ray->direction.y;
    pt->z = ray->origin.z + distance * ray->direction.z;
}

/* ------------------- handle primitives ------------------- */

/****** CalcAxis
 * NAME
 *  CalcAxis
 * SYNOPSIS
 *  void CalcAxis (Point3 *p, double *x, double *y, int axis)
 * FUNCTION
 *  Project a point along a specified axis. This function is intensively
 *  called.
 * INPUTS
 *  p - the point
 *  axis - the axis along which the coordinates will be projected.
 *         Possible values are X, Y or Z.
 * RESULT
 *  Values pointed by x and y will be modified.
 * EXAMPLE
 *  Suppose p points to {2.0, 4.0, 5.0} and the axis is X. Then
 *  *x will be set to 4.0 and *y will be 5.0.
 ******
 */

void CalcAxis (Point3 *p, double *x, double *y, int axis)
{
    switch (axis) {
    case X:
	*x=p->y; *y=p->z;
	break;
    case Y:
	*x=p->z; *y=p->x;
	break;
    case Z:
	*x=p->x; *y=p->y;
	break;
    }
}

/****** Cache
 * NAME
 *  Cache (datatype)
 * FUNCTION
 *  Cache type contains the entry for the cached ray intersection.
 *  The purpose of the intersection caching is preservation of the
 *  intersection information between BSP nodes - if a ray/primitive
 *  intersection is located, but not in the current node, the information
 *  is preserved and checked in the next node.
 * SEE ALSO
 *  GeomObj, RayObjectIntersect, RayTreeIntersect
 * SOURCE
 */

static int cache_id=0; /* This is a global value, used to negate caching
			  each entry to the RayObjectIntersect. */

typedef struct {
    GeomObj *object; /* Pointer to the intersection. It can also be an
			integer reference, if the ray is checked against
			triangle container. For this reason, it is assumed
			that pointers are large enough to fit an integer
			values. Caching ensures that each primitive
			will be examined at most once, even if it is
			contained in more than one node (it may occur
			in some extreme cases that more than one
			check is performed, though). */
    GeomObj *container; /* Pointer to the toplevel container of this object */
    double distance; /* Distance to the intersection. */
    int id;          /* cache_id in the moment this object has been created. */
} Cache;

/*******/

/****** RayObjectIntersect
 * NAME
 *  RayObjectIntersect
 * SYNOPSIS
 *  typedef boolean (*rayobj_type)();
 *  boolean RayObjectIntersect (Ray *ray, GeomObjList *objList, GeomObj **obj,
 *                              double *distance, Cache *c, Raystruct *r1,
 *                              rayobj_type check_rayobj)
 * FUNCTION
 *  Intersection test between a ray and a GeomObjList. GeomObjList can
 *  contain data different from its declaration (integer references to
 *  objects, for example). Raystruct is included in the parameters because
 *  primitives that contain other objects (like GeomTree) propagate
 *  their transformation matrices through Raystruct. check_rayobj is the
 *  actual ray/primitive intersector (the function type depends on the
 *  nature of GeomObjList).
 * NOTES
 *  CSG semantics: If an object is not part of the tree, it will simply
 *  attach its CSG field to the ray's (where appropriate). If it is, the
 *  tree's CSG specification, if any, suceeds the object's. This may still
 *  cause weirdness if both are set (I haven't tested, and I can't see
 *  any specific problem(s)).
 * INPUTS
 *  ray     - ray being tested
 *  objList - the list of object in the BSP node
 *  c       - Cache structure for the current invocation of RayTreeIntersect
 *  r1      - Ray structure with additional transformation data
 *  check_rayobj - function that checks the ray vs primitive. Prototype
 *            should look like this:
 *
 *     boolean RayPrimitiveIntersection (Ray *ray, GeomObj *o, double *dist,
 *           double maxdist, Raystruct *r1,
 *           GeomObj **return_o)
 * SEE ALSO
 *  GeomObj, GeomObjList, RayPrimitiveIntersection, check_tessel,
 *  RayTreeIntersect
 * RESULT
 *  *obj - contains the valid intersection (NULL if none).
 *  *distance - contains the intersecton distance (if any was found)
 ******
 */

boolean RayObjectIntersect (Ray *ray, GeomObjList *objList, GeomObj **obj,
			    double *distance, Cache *c, Raystruct *r1,
			    rayobj_type check_rayobj)
{
    int i;
    GeomObj *o, *newo, *cached_o, *cached_cont;
    double d = c->distance, dist;
    int *rcache_id;

    *obj = NULL;
    cached_o = NULL;
    cached_cont = NULL;
    for (i = 0; i < objList->length; i++) {

 	o = objList->list[i];
	rcache_id = (int*)(*check_rayobj)(NULL,o,NULL,0.0,NULL,NULL);

	if (c->id == *rcache_id) {
 	    /* The object has been examined already */
	    
	    if (c->container == o && c->distance <= d) {
		*obj = c->object;
	    }
	}
	else {
	    *rcache_id = c->id;

	    if ((*check_rayobj)(ray, o, &dist, d, r1, &newo))
		if (dist < d && dist > EPS) {
		    d = dist;
		    cached_cont = o;
		    *obj = newo;
		    cached_o = newo;
		}
	}
    }
    *distance = d;
    if (d < c->distance || (cached_o && !c->object)) {
	c->distance = d;
	c->object = cached_o;
	c->container = cached_cont;
	/* Skip the rays that originate from virtual surfaces */
	if (check_rayobj == RayPrimitiveIntersection) {
	    if (cached_o == cached_cont) {
		r1->csg_sector = cached_o->csg;
		/* protect the eye ray */
		if (r1->csg != SCM_BOOL_T)
		    r1->csg = get_entering_csg(r1->csg_sector);
	    }
	    else
		if ((r1->csg_sector = cached_o->csg) == SCM_BOOL_F) {
		    r1->csg_sector = cached_cont->csg;
		    /* protect the eye ray */
		    if (r1->csg != SCM_BOOL_T)
			r1->csg = get_entering_csg(r1->csg_sector);
		}
	}
    }
    return *obj != NULL;
}

/****** RayTreeIntersect
 * NAME
 *  RayTreeIntersect
 * SYNOPSIS
 *  boolean RayTreeIntersect (Raystruct *ray1, BinTree *tree, GeomObj **obj,
 *            double *distance, rayobj_type check_rayobj)
 * FUNCTION
 *  Find whether a ray intersects any of the objects in the BSP tree.
 *  If so, find the actual object and the intersection distance.
 * INPUTS
 *  ray1 - Ray structure with origin, direction and (optinally) extra
 *         transformation matrices).
 *  tree - the BSP tree structure
 *  check_rayobj - function that checks whether a ray intersects a single
 *         primitive as referenced in the BinTree (see RayObjectIntersect
 *         for more detailed information).
 * RESULT
 *  the call will return TRUE or FALSE. If there is an intersection,
 *  *obj will contain the object reference, and *distance will keep
 *  the section distance
 * SEE ALSO
 *  Cache, RayObjectIntersect, GeomObj, GeomObjList, RayPrimitiveIntersection,
 *  check_tessel
 ******
 */

boolean RayTreeIntersect (Raystruct *ray1, BinTree *tree, GeomObj **obj,
			  double *distance, rayobj_type check_rayobj)
{
    int node, axis;
    double min, max;
    Point3 pmin, pmax;
    Stack *stack;
    Cache c;
    Ray ray2,*ray;

    ray=&ray2;
    ray2=*(Ray*)ray1;

#if DISABLED
    ray2.origin.x+=BIGGER_EPS*ray2.direction.x;
    ray2.origin.y+=BIGGER_EPS*ray2.direction.y;
    ray2.origin.z+=BIGGER_EPS*ray2.direction.z;
#endif

    c.id=++cache_id;
    c.object=NULL;
    c.container=NULL;
    c.distance=BIG;

    node=tree->initial;
    axis=X;
    pmin=tree->min;
    pmax=tree->max;

    if (!RayBoxIntersect (ray,&tree->min,&tree->max,&min,&max)) {
	return FALSE;
    }

    if (min < EPS) min = EPS;

    stack=NEWTYPE(Stack);
    InitStack(stack);

    while (!emptyStack(stack)) {
	while (node>0) {
	    double position, dist;

	    position=0.5*(COORD(&pmin,axis)+COORD(&pmax,axis));
	    
	    if (fabs(COORD(&ray->direction,axis))>EPS)
	      dist=(position-COORD(&ray->origin,axis))/
		COORD(&ray->direction,axis);
	    else
	      dist=-1;


#ifdef BSP_DEBUG
	    {
		int n=node>0 ? node : -node;

		printf ("node: %f %f %f / %f %f %f\n",pmin.x,pmin.y,pmin.z,pmax.x,pmax.y,pmax.z);
		printf ("orig: %f %f %f / %f %f %f\n",tree->root[n].s.pmin.x,tree->root[n].s.pmin.y,tree->root[n].s.pmin.z,tree->root[n].s.pmax.x,tree->root[n].s.pmax.y,tree->root[n].s.pmax.z);
		printf ("  myaxis: %d\n",axis);
		printf ("origaxis: %d\n",tree->root[n].s.axis);
	    }
#endif



	    if (COORD(&ray->origin,axis) <= position) {
		/* We're in the nearer half-node. */

		if (dist<0 || dist>max) {
		    /* no intersection within the node - examine only
		       the near node */

		    node=tree->root[node].s.child[0];
		    push2(stack,&COORD(&pmax,axis));
		    COORD(&pmax,axis)=position;
		}
		else if (dist<min) {
		    /* plane is intersected before the node is entered
		       at all, only the far child needs to be processed */

		    node=tree->root[node].s.child[1];
		    push2(stack,&COORD(&pmin,axis));
		    COORD(&pmin,axis)=position;
		}
		else {
		    /* both nodes need a check - there will be a branching
		       point on the stack */

		    push2(stack,&COORD(&pmin,axis));
		    push(stack,tree->root[node].s.child[1],dist,max,
			 &COORD(&pmin,axis),position,axis);
		    node=tree->root[node].s.child[0];
		    push2(stack,&COORD(&pmax,axis));
		    COORD(&pmax,axis)=position;
		    max = dist;
		}
	    }
	    else {
		/* same thing, with children swapped */

		if (dist<0 || dist>max) {
		    node=tree->root[node].s.child[1];
		    push2(stack,&COORD(&pmin,axis));
		    COORD(&pmin,axis)=position;
		}
		else if (dist<min) {
		    node=tree->root[node].s.child[0];
		    push2(stack,&COORD(&pmax,axis));
		    COORD(&pmax,axis)=position;
		}
		else {
		    push2(stack,&COORD(&pmax,axis));
		    push(stack,tree->root[node].s.child[0],dist,max,
			 &COORD(&pmax,axis),position,axis);
		    node=tree->root[node].s.child[1];
		    push2(stack,&COORD(&pmin,axis));
		    COORD(&pmin,axis)=position;
		    max = dist;
		}
	    }
	    axis=INCAXIS(axis);
	}

	/* at this point, we ran on a non-split child. Could be empty. */
	/* Here is some debugging code... */

	if (node) {


#ifdef BSP_DEBUG
	    {
		int i;
		for (i=0; i<tree->root[-node].o.length; i++)
		    printf ("#%d> ",tree->root[-node].o.list[i]->id);
		printf("\n");
	    }
#endif

	    if (RayObjectIntersect(ray, &tree->root[-node].o, obj,
				   distance, &c, ray1, check_rayobj)) {
		Point3 p;

		if (*distance >= min - EPS && *distance <= max + EPS) {
		    free(stack);
		    return TRUE;
		}
	    }
	}
	    
	/* bugger, no point found. Let's backtrack */

	for (;;) {
	    StackElem *p= &(stack->stack[--stack->stackPtr]);

	    if (emptyStack(stack)) break;
	    *p->address=p->value;
	    node=p->node;
	    if (node!=-1) { /* -1 marks a non-branching node on stack. It is
			       only there to save the old cluster bounds */
		axis=p->axis;
		axis=INCAXIS(axis);
	        min=p->min;
	        max=p->max;
	        break;
	    }
	}
    }
    free(stack);
    return FALSE;
}

boolean BoxTreeDescend (BinNode *root, int node, int axis,
			Point3 *min, Point3 *max,
			Point3 *bmin, Point3 *bmax,
			BTDCallback f, void *data, rayobj_type check_rayobj)
{
    int *rcache_id;

    if (!node) return TRUE;

    if (min->x > bmax->x || max->x <= bmin->x) return TRUE;
    if (min->y > bmax->y || max->y <= bmin->y) return TRUE;
    if (min->z > bmax->z || max->z <= bmin->z) return TRUE;

    if (node < 0) {
	int i, l;
	GeomObj *o;

	node=-node;
	l=root[node].o.length;
	for (i=0; i<l; i++) {
	    o=root[node].o.list[i];
	    rcache_id=(int*)(*check_rayobj)(NULL,o,NULL,0.0,NULL,NULL);
	    if (*rcache_id != cache_id) {
		*rcache_id = cache_id;
		if (!f(o, data))
		    return FALSE;
	    }
	}
    }
    else {
	double m, x1, x2;
	int a = INCAXIS(axis);
	boolean x;

	x1 = COORD(min, axis);
	x2 = COORD(max, axis);
	m = (x1 + x2) / 2;
	COORD(max, axis) = m;
	x=BoxTreeDescend (root, root[node].s.child[0], a, min, max,
			  bmin, bmax, f, data, check_rayobj);
	COORD(max, axis) = x2;
	if (!x) return FALSE;
	COORD(min, axis) = m;
	x=BoxTreeDescend (root, root[node].s.child[1], a, min, max,
			  bmin, bmax, f, data, check_rayobj);
	COORD(min, axis) = x1;
	if (!x) return FALSE;
    }
    return TRUE;
}

/****** BoxTreeCall
 * NAME
 *   BoxTreeCall
 * SYNOPSIS
 *   void BoxTreeCall (BinTree *tree, Point3 *bmin, Point3 *bmax,
 *                     BTDCallback f, void *data, rayobj_type check_rayobj)
 * FUNCTION
 *   Call the function f (obj, data) for every object in the tree. This
 *   will only notice the objects contained in the box delimited with bmin,
 *   bmax (it won't do the full box-box check, but will use the box
 *   to prune the tree. check_rayobj is needed for caching (so that f
 *   is only called once per object. f returning FALSE will count as
 *   bail-out status. It returns TRUE if all the calls to f return TRUE.
 ******/

boolean BoxTreeCall (BinTree *tree, Point3 *bmin, Point3 *bmax,
		     BTDCallback f, void *data, rayobj_type check_rayobj)
{
    cache_id++;
    return BoxTreeDescend (tree->root, tree->initial, X,
			   &tree->min, &tree->max, bmin, bmax, f, data,
			   check_rayobj);
}
