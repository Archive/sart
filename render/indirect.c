#include "bsp.h"
#include "hashtable.h"

#define BUCKET_SIZE_FACTOR 2

typedef struct {
    Point3 p;
    Point3 n;
    SCM irradiance;
} IndirectEntry;

typedef struct {
    int n;
    IndirectEntry *list;
} Bucket;

typedef struct {
    int cache_hits,
	cache_tries;    /* statistics */
    double bucket_size; /* This is spatial size of a bucket */
    double max;         /* maximal distance between entries */
    HashTable *table;
} ITable;

/* ------------------- */

void free_bucket (Bucket *b)
{
    if (b->list)
	free (b->list);
}

void init_bucket (Bucket *b)
{
    b->n = 0;
    b->list = NEWARRAY(IndirectEntry, 1);
}


void free_table_aux (void *loc, void *bucket, void *data)
{
    free_bucket(bucket);
}

void free_table (ITable *t)
{
    hash_foreach(t->table, free_table_aux, NULL);
    free_hashtable(t->table);
    free(t);
}

int hash_loc (void *loc)
{
    int t;
    IntPoint3 *l = loc;

    t = l->x * 128378112 + l->y * 72748913 + l->z * 66298318;
    return (t*192381099+129839991)*873897911 + l->x + l->y + l->z;
}

boolean is_same_loc (void *loc1, void *loc2)
{
    IntPoint3 *l1 = loc1, *l2 = loc2;

    return l1->x == l2->x && l1->y == l2->y && l1->z == l2->z;
}

ITable *make_table (double max)
{
    ITable *t;
    int i;

    t = NEWTYPE(ITable);
    t->max = max;
    t->bucket_size = max * BUCKET_SIZE_FACTOR;
    t->table = new_hashtable(2048, sizeof(IntPoint3), sizeof(Bucket),
			     hash_loc, is_same_loc);
    t->cache_hits = t->cache_tries = 0;
    return t;
}

/* ----------------------- */

void point_to_loc (Point3 *p, ITable *t, IntPoint3 *l)
{
    double size = t->bucket_size;

    l->x = p->x / size;
    l->y = p->y / size;
    l->z = p->z / size;
}

void add_to_bucket (IndirectEntry *e, Bucket *b)
{
    b->list = realloc(b->list, (++(b->n))*sizeof(IndirectEntry));
    b->list[b->n - 1] = *e;
}

void add_indirect_entry (IndirectEntry *e, ITable *t)
{
    IntPoint3 l;
    int ind;
    Bucket *b;

    point_to_loc(&e->p, t, &l);
    b = hash_find(t->table, &l);
    if (!b) {
	Bucket clear_bucket;

	init_bucket(&clear_bucket);
	t->table = hash_add(t->table, &l, &clear_bucket);
	b = hash_find(t->table, &l);
    }
    add_to_bucket (e, b);
}

/* ---------------------- */

/* This is reset to 0 on every startup-render from render.c, otherwise
   this module is the only one that touches this thing */

int global_indirect_depth_limit = 0;
double global_indirect_resolution = 0.2;
double global_indirect_nres = 2.0;
int global_indirect_rays = 256;

SCM *itable_set; /* this points to a global variable for debugging */

double distance_between_entries (IndirectEntry *e1, IndirectEntry *e2,
				 ITable *table)
{
    double s, max;
    Point3 t;

    max=table->max;
    s = V3DistanceBetween2Points(&e1->p, &e2->p);
    if (s > max) return BIG;
    s += V3DistanceBetween2Points(&e1->n, &e2->n) * max * global_indirect_nres;
    if (s > max) return BIG;
    /* We're assuming that e1 has already been colored */
    /* Check whether e1->p is in front of e2->p */
    V3Sub(&e1->p, &e2->p, &t);
    if (V3Dot(&e2->n, &t) > 0)
	return BIG;
    return s;
}

void process_bucket (IndirectEntry *e, Bucket *b, SCM *col,
		     double *weight, ITable *t)
{
    int i;
    double dist, max=t->max;

    for (i = 0; i < b->n; i++)
	if ((dist=distance_between_entries(e, &b->list[i], t)) < max) {
	    if (dist < EPS)
		dist = EPS;
	    dist = 1/dist-1/max;
	    *weight += dist;
	    *col = combine_colors(*col, b->list[i].irradiance, 1, dist);
	}
}

SCM get_indirect (ITable *t, SCM r)
{
    SCM col, indcell;
    IndirectEntry e;
    IntPoint3 l, l1;
    double weight, w, w1;
    int i,j,k,ind;
    Raystruct *ray, *indray;
    extern int tc16_Ray;
    Bucket *bucket;

    ray = RAYSTR(r);
    e.p = ray->section;
    e.n = ray->normal;
    point_to_loc (&e.p, t, &l);
    weight = 0;
    col = SCM_BOOL_F;
    t->cache_tries++; /* update statistics */
    for (i = -1; i < 2; i++) /* loop across the surrounding buckets */
	for (j = -1; j < 2; j++)
	    for (k = -1; k < 2; k++) {
		l1.x = l.x + i;
		l1.y = l.y + j;
		l1.z = l.z + k;
		bucket = hash_find(t->table, &l1);
		if (bucket) {
		    process_bucket(&e, bucket, &col, &weight, t);
		}
	    }
    if (weight > 3/t->max) {
	t->cache_hits++;
	return cscale_subr2(col, scm_makdbl(1/weight, 0.0));
    }


    /* Do it the hard way */
    
    indray = newray();
    indray->scene = ray->scene;
    SCM_NEWCELL(indcell);
    SCM_CDR(indcell) = (SCM)indray;
    SCM_CAR(indcell) = tc16_Ray;
    indray->weight = ray->weight*0.1;
    indray->flags = RF_REFL_DIFF;
    indray->origin = ray->section;

    srand48(arand(3, (double*)(&indray->origin)));
    col = SCM_BOOL_F;
    weight = 0;
    for (i = 0; i < global_indirect_rays; i++) {
	SCM icol;
	indray->object = SCM_BOOL_F;
	do {
	    indray->direction.x = drand48() - 0.5;
	    indray->direction.y = drand48() - 0.5;
	    indray->direction.z = drand48() - 0.5;
	} while ((w = V3Dot(&indray->direction, &ray->normal)) < 0.0
		 || (w1 = V3Dot(&indray->direction, &indray->direction)) > 0.25);
	icol = c_get_ray_rad(indcell, r);
	if (indray->object != SCM_BOOL_F && 
	    SCM_NIMP(OBJSTR(indray->object)->material) &&
	    (scm_procedure_property(OBJSTR(indray->object)->material, mtag_light) != SCM_BOOL_F))
	    continue; /* This is here because lightsources are already
			 accounted for */
	w = w / sqrt(w1);
	weight++;
	col = combine_colors(col, icol, 1, w);
    }
    col = cscale_subr2(col, scm_makdbl(1 / weight, 0.0));
    e.irradiance = col;
    add_indirect_entry(&e, t);
    deleteray(indcell);
    return col;
}

long tc16_ITable;

#define ITABSTR(x) ((ITable*)SCM_CDR(x))

scm_sizet ITable_free (SCM_CELLPTR itable)
{
    free_table(ITABSTR(itable));
    return 0;
}

void ITable_mark_aux (void *loc, void *bucket, void *data)
{
    Bucket *b = bucket;
    int j;

    for (j = 0; j < b->n; j++)
	scm_gc_mark (b->list[j].irradiance);
}

SCM ITable_mark (SCM table)
{
    ITable *t = ITABSTR(table);
    IndirectEntry *l;
    int i,j;

    scm_markcdr (table);
    hash_foreach (t->table, ITable_mark_aux, NULL);
    return SCM_BOOL_F;
}

int ITable_print (SCM scene, SCM port, int writing)
{
    Scene *s=SCENESTR(scene);
    char buf[256];

    sprintf(buf,"#<Indirect Table %x>",(int)s);
    scm_puts(buf, port);
    return 1;
}

static scm_smobfuns itab_smob = {ITable_mark, ITable_free, ITable_print, 0};

SCM create_itable (double max)
{
    ITable *tab;
    SCM s;

    tab = make_table (max);
    SCM_NEWCELL(s);
    SCM_CDR(s) = (SCM)tab;
    SCM_CAR(s) = tc16_ITable;
    return s;
}

void startup_indirect (void)
{
    int i;
    double res = global_indirect_resolution;

    *itable_set = SCM_BOOL_F;
    for (i = 0; i < global_indirect_depth_limit; i++) {
	*itable_set = scm_cons(create_itable(res), *itable_set);
	res *= 2;
    }
}

SCM get_indirect_lighting (SCM r)
{
    ITable *t;
    SCM curr_itable;
    SCM col;
    extern double weight_limit;

    if (SCM_IMP(*itable_set) || RAYSTR(r)->weight < weight_limit)
	return check_call(ambient_light, r);
    t = ITABSTR(SCM_CAR(*itable_set));
    curr_itable = *itable_set;
    *itable_set = SCM_CDR(*itable_set);
    col = get_indirect (t, r);
    *itable_set = curr_itable;
    return col;
}

/* SUBR (subr1) */
char s_set_rad_depth[] = "set-radiosity-depth";
SCM set_rad_depth (SCM depth)
{
    int n;

    SCM_ASSERT (SCM_INUMP(depth) && (n = SCM_INUM(depth), n >= 0 && n < 32),
		depth, SCM_ARG1, s_set_rad_depth);
    global_indirect_depth_limit = n;
    return SCM_UNSPECIFIED;
}

/* SUBR (subr1) */
char s_set_rad_res[] = "set-radiosity-resolution";
SCM set_rad_res (SCM v)
{
    SCM_ASSERT (SCM_NIMP(v) && SCM_REALP(v), v, SCM_ARG1, s_set_rad_res);
    global_indirect_resolution = SCM_REALPART(v);
    return SCM_UNSPECIFIED;
}

/* SUBR (subr1) */
char s_set_rad_nres[] = "set-radiosity-normal-resolution";
SCM set_rad_nres (SCM v)
{
    SCM_ASSERT (SCM_NIMP(v) && SCM_REALP(v), v, SCM_ARG1, s_set_rad_nres);
    global_indirect_nres = SCM_REALPART(v);
    return SCM_UNSPECIFIED;
}

/* SUBR (subr1) */
char s_set_rad_rays[] = "set-radiosity-rays";
SCM set_rad_rays (SCM v)
{
    int n;

    SCM_ASSERT (SCM_INUMP(v) && (n = SCM_INUM(v), n > 0),
		v, SCM_ARG1, s_set_rad_rays);
    global_indirect_rays = n;
    return SCM_UNSPECIFIED;
}

/* SUBR (subr0) */
char s_get_rad_stats[] = "get-radiosity-statistics";
SCM get_rad_stats (void)
{
    SCM stats = SCM_EOL;
    SCM i;

    for (i = *itable_set; SCM_NIMP(i); i = SCM_CDR(i)) {
	ITable *t =  ITABSTR(SCM_CAR(i));

	stats = scm_cons(scm_cons(SCM_MAKINUM(t->cache_hits),
				  SCM_MAKINUM(t->cache_tries)),
			 stats);
    }
    return stats;
}

void init_indirect_c (void)
{
    /* __PERL__ This code is automatically generated. Don't modify. */

    static scm_iproc subr0s[] = {
        {s_get_rad_stats, get_rad_stats},
        {0,0}
    };

    static scm_iproc subr1s[] = {
        {s_set_rad_depth, set_rad_depth},
        {s_set_rad_res, set_rad_res},
        {s_set_rad_nres, set_rad_nres},
        {s_set_rad_rays, set_rad_rays},
        {0,0}
    };

    scm_init_iprocs (subr0s, scm_tc7_subr_0);
    scm_init_iprocs (subr1s, scm_tc7_subr_1);

    /* __END__ */

    itable_set = &SCM_CDR(scm_sysintern("$indirect-render-tables",SCM_BOOL_F));
    tc16_ITable = scm_newsmob(&itab_smob);
}
