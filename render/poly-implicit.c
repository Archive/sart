
#include "poly-implicit.h"

typedef enum {
    L = 0,	/* left direction:	-x, -i */
    R = 1,	/* right direction:	+x, +i */
    B = 2,	/* bottom direction:    -y, -j */
    T = 3,	/* top direction:	+y, +j */
    N = 4,	/* near direction:	-z, -k */
    F = 5	/* far direction:	+z, +k */
} CubeFace;

typedef enum {
    LBN = 0,  /* left bottom near corner  */
    LBF = 1,  /* left bottom far corner   */
    LTN = 2,  /* left top near corner     */
    LTF = 3,  /* left top far corner      */
    RBN = 4,  /* right bottom near corner */
    RBF = 5,  /* right bottom far corner  */
    RTN = 6,  /* right top near corner    */
    RTF = 7   /* right top far corner     */
} CubeVertex;

static
int surf_xyz[6][3] = {{-1, 0, 0}, {1, 0, 0},
		      {0, -1, 0}, {0, 1, 0},
		      {0, 0, -1}, {0, 0, 1}};

static
int vertex_surfaces[8][3] = {{L, B, N},
			     {L, B, F},
			     {L, T, N},
			     {L, T, F},
			     {R, B, N},
			     {R, B, F},
			     {R, T, N},
			     {R, T, F}};

static
int cube_tetras[6][4] = {{LBN, LTN, RBN, LBF},
			 {RTN, LTN, LBF, RBN},
			 {RTN, LTN, LTF, LBF},
			 {RTN, RBN, LBF, RBF},
			 {RTN, LBF, LTF, RBF},
			 {RTN, LTF, RTF, RBF}};

static
Point3 tetra_surface[4][6];

static
double tetra_dist[4][6];

static
void edge_interpolate (SubdInfo *p, Corner *a, Corner *b, Point3 *out)
{
    Point3 p1, p2;
    double v1, v2;

    p1.x = (double)a->i.x / (1 << 30);
    p1.y = (double)a->i.y / (1 << 30);
    p1.z = (double)a->i.z / (1 << 30);
    p2.x = (double)b->i.x / (1 << 30);
    p2.y = (double)b->i.y / (1 << 30);
    p2.z = (double)b->i.z / (1 << 30);

    v1 = fabs(a->value);
    v2 = fabs(b->value);
    V3Combine(&p1, &p2, out, v2 / (v1 + v2), v1 / (v1 + v2));
}

static
int make_vertex(SubdInfo *p, Corner *a, Corner *b)
{
    int x[6];
    int *val;

    x[0] = a->i.x;
    x[1] = a->i.y;
    x[2] = a->i.z;
    x[3] = b->i.x;
    x[4] = b->i.y;
    x[5] = b->i.z;

    if ((val = hash_find (p->edge_table, x)))
	return *val;
    p->verts = realloc(p->verts, (p->nverts + 1) * sizeof(Point3));
    edge_interpolate(p, a, b, &p->verts[p->nverts]);
    p->edge_table = hash_add(p->edge_table, x, &p->nverts);
    return p->nverts++;
}

static
void poly_tetra (SubdInfo *p, int corners[8],
		 CubeVertex v1, CubeVertex v2, CubeVertex v3, CubeVertex v4)
{
    int index = 0, apos, bpos, cpos, dpos;
    int e_ab, e_ac, e_ad, e_bc, e_bd, e_cd;
    Corner *a = &p->corners[corners[v1]];
    Corner *b = &p->corners[corners[v2]];
    Corner *c = &p->corners[corners[v3]];
    Corner *d = &p->corners[corners[v4]];

    /* index is now 4-bit number representing one of the 16 possible cases */
    if ((apos = (a->value > 0))) index += 8;
    if ((bpos = (b->value > 0))) index += 4;
    if ((cpos = (c->value > 0))) index += 2;
    if ((dpos = (d->value > 0))) index += 1;

    /* 14 productive tetrahedral cases (0000 and 1111 do not yield polygons */
    if (apos != bpos) e_ab = make_vertex(p, a, b);
    if (apos != cpos) e_ac = make_vertex(p, a, c);
    if (apos != dpos) e_ad = make_vertex(p, a, d);
    if (bpos != cpos) e_bc = make_vertex(p, b, c);
    if (bpos != dpos) e_bd = make_vertex(p, b, d);
    if (cpos != dpos) e_cd = make_vertex(p, c, d);

    switch (index) {
	case 1:	 p->trian_out(p->tdata, p->verts, e_bd, e_cd, e_ad); return;
	case 2:	 p->trian_out(p->tdata, p->verts, e_ac, e_cd, e_bc); return;
	case 3:	 p->trian_out(p->tdata, p->verts, e_ad, e_bd, e_bc);
		 p->trian_out(p->tdata, p->verts, e_ad, e_bc, e_ac); return;
	case 4:	 p->trian_out(p->tdata, p->verts, e_ab, e_bc, e_bd); return;
	case 5:	 p->trian_out(p->tdata, p->verts, e_ad, e_ab, e_bc);
		 p->trian_out(p->tdata, p->verts, e_ad, e_bc, e_cd); return;
	case 6:	 p->trian_out(p->tdata, p->verts, e_ab, e_ac, e_cd);
		 p->trian_out(p->tdata, p->verts, e_ab, e_cd, e_bd); return;
	case 7:	 p->trian_out(p->tdata, p->verts, e_ab, e_ac, e_ad); return;
	case 8:	 p->trian_out(p->tdata, p->verts, e_ab, e_ad, e_ac); return;
	case 9:	 p->trian_out(p->tdata, p->verts, e_ab, e_bd, e_cd);
		 p->trian_out(p->tdata, p->verts, e_ab, e_cd, e_ac); return;
	case 10: p->trian_out(p->tdata, p->verts, e_ab, e_ad, e_cd);
		 p->trian_out(p->tdata, p->verts, e_ab, e_cd, e_bc); return;
	case 11: p->trian_out(p->tdata, p->verts, e_ab, e_bd, e_bc); return;
	case 12: p->trian_out(p->tdata, p->verts, e_ad, e_ac, e_bc);
		 p->trian_out(p->tdata, p->verts, e_ad, e_bc, e_bd); return;
	case 13: p->trian_out(p->tdata, p->verts, e_cd, e_ac, e_bc); return;
	case 14: p->trian_out(p->tdata, p->verts, e_bd, e_ad, e_cd); return;
    }
}

static
void poly_cube (SubdInfo *p, int corners[8])
{
    poly_tetra(p, corners, LBN, LTN, RBN, LBF);
    poly_tetra(p, corners, RTN, LTN, LBF, RBN);
    poly_tetra(p, corners, RTN, LTN, LTF, LBF);
    poly_tetra(p, corners, RTN, RBN, LBF, RBF);
    poly_tetra(p, corners, RTN, LBF, LTF, RBF);
    poly_tetra(p, corners, RTN, LTF, RTF, RBF);
}


static
int locate_corner (SubdInfo *p, int depth, int x[3])
{
    int y[3];
    int *i;

    y[0] = x[0] << (30 - depth);
    y[1] = x[1] << (30 - depth);
    y[2] = x[2] << (30 - depth);
    i = (int*)hash_find(p->corner_table, y);
    return i ? *i : -1;
}

static
int make_corner (SubdInfo *p, int depth, int x[3])
{
    int y[3];
    int i;

    y[0] = x[0] << (30 - depth);
    y[1] = x[1] << (30 - depth);
    y[2] = x[2] << (30 - depth);
    i = p->ncorners++;
    p->corner_table = hash_add(p->corner_table, y, &i);
    p->corners = realloc(p->corners, p->ncorners * sizeof(Corner));
    return i;
}

/* separating planes */
static
int sep_planes[5][3] = {{RTN, RBN, LBF}, /* 0, 1, 2 from 3, 4, 5 */
			{LTN, LBF, RBN}, /* 0 from 1, 2 */
			{RTN, LTN, LBF}, /* 1 from 2 */
			{RTN, LBF, RBF}, /* 3 from 4, 5 */
			{RTN, LTF, RBF}}; /* 4 from 5 */

static
int positive_at[5] = {RBF, RTN, LTF, LTF, RTF};

static
Point3 sep_plane_normals[5];

static
double sep_plane_dists[5];

static
void init_sep_planes (void)
{
    int i;

    for (i = 0; i < 5; i++) {
	Point3 p[3], pos, t1, t2, n;
	double dist;
	int j;

	for (j = 0; j < 3; j++) {
	    p[j].x = (sep_planes[i][j] & 1) ? 1 : 0;
	    p[j].y = (sep_planes[i][j] & 2) ? 1 : 0;
	    p[j].z = (sep_planes[i][j] & 4) ? 1 : 0;
	}

	pos.x = (positive_at[i] & 1) ? 1 : 0;
	pos.y = (positive_at[i] & 2) ? 1 : 0;
	pos.z = (positive_at[i] & 4) ? 1 : 0;

	V3Cross(V3Sub(&p[1], &p[0], &t1), V3Sub(&p[2], &p[0], &t2), &n);
	dist = -V3Dot(&n, &p[0]);
	if (V3Dot(&pos, &n) + dist < 0) {
	    V3Scale(&n, -1.0);
	    dist = -dist;
	}
	sep_plane_normals[i] = n;
	sep_plane_dists[i] = dist;
    }
}

static
void init_tetra_surfaces (void)
{
    int i, k;

    for (i = 0; i < 6; i++)
	for (k = 0; k < 4; k++) {
	    Point3 p[3], pos, t1, t2, n;
	    double dist;
	    double alpha, tmp;
	    int j, j2, vert;

	    vert = cube_tetras[i][k];

	    for (j = j2 = 0; j2 < 4; j2++)
		if (cube_tetras[i][j2] != vert) {
		    p[j].x = (cube_tetras[i][j2] & 1) ? 1 : 0;
		    p[j].y = (cube_tetras[i][j2] & 2) ? 1 : 0;
		    p[j].z = (cube_tetras[i][j2] & 4) ? 1 : 0;
		    j++;
		}
	    
	    pos.x = (vert & 1) ? 1 : 0;
	    pos.y = (vert & 2) ? 1 : 0;
	    pos.z = (vert & 4) ? 1 : 0;
	    
	    V3Cross(V3Sub(&p[1], &p[0], &t1), V3Sub(&p[2], &p[0], &t2), &n);
	    tmp = V3Dot(&n, &p[0]);
	    alpha = 1 / (V3Dot(&n, &pos) - tmp);
	    dist = - tmp * alpha;
	    V3Scale(&n, alpha);
	    tetra_surface[k][i] = n;
	    tetra_dist[k][i] = dist;
	}
}

static
int locate_tetra (Point3 *p)
{
    if (V3Dot(&sep_plane_normals[0], p) + sep_plane_dists[0] < 0) {
	if (V3Dot(&sep_plane_normals[1], p) + sep_plane_dists[1] < 0)
	    return 0;
	else {
	    if (V3Dot(&sep_plane_normals[2], p) + sep_plane_dists[2] < 0)
		return 1;
	    else
		return 2;
	}
    }
    else {
	if (V3Dot(&sep_plane_normals[3], p) + sep_plane_dists[3] < 0)
	    return 3;
	else {
	    if (V3Dot(&sep_plane_normals[4], p) + sep_plane_dists[4] < 0)
		return 4;
	    else
		return 5;
	}
    }
}

static
double tetra_interpolate (SubdInfo *p, int *x, Point3 *pos)
{
    int tetra_id = locate_tetra(pos);
    int i;
    double s;

    for (s = 0.0, i = 0; i < 4; i++) {
	double strength = V3Dot(&tetra_surface[i][tetra_id], pos) +
	    tetra_dist[i][tetra_id];
	s += strength * p->corners[x[cube_tetras[tetra_id][i]]].value;
    }
    return s;
}

static
boolean cube_in_bounds (int depth, int y[3])
{
    int n = (1 << depth);

    return y[0] >= 0
	&& y[1] >= 0
	&& y[2] >= 0
	&& y[0] < n
	&& y[1] < n
	&& y[2] < n;
}

static
void find_corners (Wavetree *t, SubdInfo *p, int depth, int x[3])
{
    int y[3];
    int surface_depth[6];
    Wavenode *n[6];
    int *corners = wavetree_ref(t, depth, x);
    int i;

    /* first check whether we already know the corners */
    if (corners[0] >= 0) return;

    for (i = 0; i < 6; i++) {
	int depth1;

	y[0] = x[0] + surf_xyz[i][0];
	y[1] = x[1] + surf_xyz[i][1];
	y[2] = x[2] + surf_xyz[i][2];
	if (cube_in_bounds(depth, y)) {
	    n[i] = biggest_containing(t, depth, y, &depth1);
	    surface_depth[i] = depth1;
	} else
	    surface_depth[i] = 0;
    }

    for (i = 0; i < 8; i++) {
	int r;
	int s;
	int y[3], j;
	Corner *current;
	int curr_id;
	Point3 pos;

	y[0] = x[0] + ((i & 1) ? 1 : 0);
	y[1] = x[1] + ((i & 2) ? 1 : 0);
	y[2] = x[2] + ((i & 4) ? 1 : 0);

	if ((curr_id = locate_corner(p, depth, y)) != -1) {
	    /* This was already handled somewhere else. */
	    corners[i] = curr_id;
	}
	else {
	    current = &p->corners[curr_id = make_corner(p, depth, y)];
	    current->i.x = y[0] << (30 - depth);
	    current->i.y = y[1] << (30 - depth);
	    current->i.z = y[2] << (30 - depth);
	    pos.x = (double)y[0] / (1 << depth);
	    pos.y = (double)y[1] / (1 << depth);
	    pos.z = (double)y[2] / (1 << depth);

	    r = 0;
	    for (j = 0; j < 3; j++) {
		if (r < surface_depth[vertex_surfaces[i][j]]) {
		    s = vertex_surfaces[i][j];
		    r = surface_depth[s];
		}
	    }
	    if (r) {
		Corner *oc;
		Point3 pos;
		int z[3];

		/* recurse here */
		z[0] = (x[0] + surf_xyz[s][0]) >> r;
		z[1] = (x[1] + surf_xyz[s][1]) >> r;
		z[2] = (x[2] + surf_xyz[s][2]) >> r;
		find_corners (t, p, depth - r, z);

		/* corners for n[s] are now defined */
		oc = p->corners + ((int*)n[s]->value)[0];
		pos.x = (double)(y[0] - (z[0] << r)) / (1 << r);
		pos.y = (double)(y[1] - (z[1] << r)) / (1 << r);
		pos.z = (double)(y[2] - (z[2] << r)) / (1 << r);
		current->value = tetra_interpolate(p, (int*)n[s]->value, &pos);
	    } else {
		current->value = p->f(&pos, p->fdata);
	    }
	    corners[i] = curr_id;
	}
    }
}

static
void subdivide_tree_rec (SubdInfo *p, Wavetree *t, Wavenode *n,
			 int depth, int x[3])
{
    int i, y[3];

    if (n->branches) {
        for (i = 0; i < 8; i++)
	    if (n->branches[i]) {
		y[0] = (x[0] << 1) + ((i & 1) ? 1 : 0);
		y[1] = (x[1] << 1) + ((i & 2) ? 1 : 0);
		y[2] = (x[2] << 1) + ((i & 4) ? 1 : 0);
		subdivide_tree_rec (p, t, n->branches[i], depth + 1, y);
	    }
    }
    else {
	find_corners(t, p, depth, x);
	poly_cube(p, (int*)n->value);
    }
}

static
boolean tree_constructor (Wavenode *n, int depth, int x[], boolean is_leaf,
			  void *data)
{
    if (is_leaf) {
	int i;
	for (i = 0; i < 8; i++)
	    ((int*)n->value)[i] = -1;
    }
    return TRUE;
}

static
void tree_destructor (Wavenode *n)
{
}

static
int corner_hash (void *data)
{
    int *y = (int*)data;

    return y[0]*18238911 + y[1] * 59182001 + y[2] * 99901043;
}

static
int edge_hash (void *data)
{
    int *y = (int*) data;
    int *z = y + 3;

    return corner_hash(y) + 19280023 * corner_hash(z);
}

static
int corner_key_eq (void *d1, void *d2)
{
    int *y = (int*)d1;
    int *z = (int*)d2;

    return y[0] == z[0] && y[1] == z[1] && y[2] == z[2];
}

static
int edge_key_eq (void *d1, void *d2)
{
    int *y = (int*)d1;
    int *z = (int*)d2;

    return y[0] == z[0] && y[1] == z[1] && y[2] == z[2]
	&& y[3] == z[3] && y[4] == z[4] && y[5] == z[5];
}

static
Wavetree *make_subd_wtree (SubdCriterium crit, void *sdata)
{
    return create_wtree (3, tree_destructor, tree_constructor, NULL,
			 crit, sdata, 0, 8 * sizeof(int));
}

static
SubdInfo *make_subd_info (TriangleOutput trian_out, void *tdata,
			  VertexOutput vert_out, void *vdata,
			  SubdFunc f, void *fdata)
{
    SubdInfo *p = NEWTYPE(SubdInfo);

    p->trian_out = trian_out;
    p->tdata = tdata;
    p->vert_out = vert_out;
    p->vdata = vdata;
    p->f = f;
    p->fdata = fdata;
    p->nverts = 0;
    p->verts = NEWARRAY(Point3, 1023);
    p->ncorners = 0;
    p->corners = NEWARRAY(Corner, 1023);
    p->edge_table = new_hashtable (1023, 6 * sizeof(int), sizeof(int),
				   edge_hash, edge_key_eq);
    p->corner_table = new_hashtable (1023, 3 * sizeof(int), sizeof(int),
				     corner_hash, corner_key_eq);
    return p;
}

static
void free_subd_info (SubdInfo *p)
{
    free (p->verts);
    free (p->corners);
    free_hashtable (p->edge_table);
    free_hashtable (p->corner_table);
    free(p);
}

/* And now... the public interface! */


/****** poly_mesh
 * NAME
 *   poly_mesh
 * SYNOPSIS
 *   void poly_mesh (TriangleOutput trian_out, void *tdata,
 *	             VertexOutput vert_out, void *vdata,
 *	             SubdFunc f, void *fdata,
 *	             SubdCriterium crit, void *cdata)
 * FUNCTION
 *   Create a smoothed triangle mesh. crit is used in the initial Wavetree
 *   creation and will determine the local mesh resolution. Subdfunc is
 *   the implicit function. TriangleOutput is called for each new triangle
 *   created. Finally, VertexOutput is called with the SubdInfo to
 *   dump the vertices and normals.
 ******/

void poly_mesh (TriangleOutput trian_out, void *tdata,
		VertexOutput vert_out, void *vdata,
		SubdFunc f, void *fdata,
		SubdCriterium crit, void *cdata)
{
    static int zero_vec[3] = {0, 0, 0};
    SubdInfo *p = make_subd_info (trian_out, tdata, vert_out, vdata, f, fdata);
    Wavetree *t = make_subd_wtree (crit, cdata);

    subdivide_tree_rec (p, t, t->root, 0, zero_vec);
    vert_out(vdata, p);
    free_subd_info (p);
    free_wavetree (t);
}

void init_poly_subdivision (void)
{
    init_sep_planes();
    init_tetra_surfaces();
}

    
