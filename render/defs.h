
#ifndef _sart_defs_h_
#define _sart_defs_h_

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "libguile.h"

#define EPS 1e-12
#define BIGGER_EPS 1e-11
#define BIG 1e8

#define SCMERROR(arg, pos, val) scm_wta(arg,(char*)pos,val)

#define NEWARRAY(type,size) ((type*)malloc((size)*sizeof(type)))
#define MNEWARRAY(type,len) ((type*)scm_must_malloc((len)*sizeof(type), "sart"))
#define MNEWTYPE(type) ((type*)(scm_must_malloc(sizeof(type), "sart")))
#define MNEWWHAT(type, what) ((type*)(scm_must_malloc(sizeof(type), what)))
#define NEWGEOM(type) ((GeomObj*)(scm_must_malloc(sizeof(GeomObj)-sizeof(GeomPrimitive)+sizeof(type), "GeomPrim")));
#define NEWSTRUCT(x)	(struct x *)(malloc((unsigned)sizeof(struct x)))
#define NEWTYPE(x)	(x *)(malloc((unsigned)sizeof(x)))



#define TRUE		1
#define FALSE		0
#define ON		1
#define OFF		0
typedef int boolean;			/* boolean data type */
typedef boolean flag;			/* flag data type */

#endif
