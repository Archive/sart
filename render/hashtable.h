
#ifndef _hashtab_h_
#define _hashtab_h_

#include <stdlib.h>
#include "defs.h"

typedef int (*HashFunc)(void *);
typedef boolean (*PtrEq)(void *, void*);
typedef void (*HashMapFunc)(void*, void*, void*); /* used in hash_foreach */

typedef struct {
    int size;
    void *key_array;
    void *value_array;
    int *entry;
    int *next;
    int free;
    HashFunc hash;
    PtrEq key_eq;
    int keysize, valuesize;
} HashTable;

HashTable *new_hashtable (int starting_alloc, int keysize, int valuesize,
			  HashFunc hash, PtrEq key_eq);
void free_hashtable (HashTable *p);
void *hash_find (HashTable *p, void *key);
HashTable *hash_add (HashTable *p, void *key, void *value);
void hash_foreach (HashTable *p, HashMapFunc f, void *data);

#endif

