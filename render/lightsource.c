#include "bsp.h"
#include <math.h>
#include "lightsource.h"

extern double sq(double);

/****** PBSDistance2
 * NAME
 *  PBSDistance2
 * SYNOPSIS
 *  double PBSDistance2 (Point3 *o, LightSourceBox *box)
 * FUNCTION
 *  Find the distance between the point e, and the orthogonal (not necessarily
 *  axis-aligned) box with one vertex at o, and sides pointing to v1, v2, v3.
 *  l1, l2, l3 are lengths of these vectors. They are not computed here
 *  for speed.
 *******
 */

double PBSDistance2 (Point3 *e, LightSourceBox *box)
{
    Point3 p, q;

    V3Sub (e, box->p0, &p);
    q.x = V3Dot(box->v1, &p) / box->l1;
    q.y = V3Dot(box->v2, &p) / box->l2;
    q.z = V3Dot(box->v3, &p) / box->l3;
    if (q.x > 0) q.x = q.x > box->l1 ? q.x - box->l1 : 0;
    if (q.y > 0) q.y = q.y > box->l2 ? q.y - box->l2 : 0;
    if (q.z > 0) q.z = q.z > box->l3 ? q.z - box->l3 : 0;
    return V3SquaredLength(&q);
}

/* --- lightsource sampling. Will handle only some primitives ----- */

extern SCM global_ray;
Raystruct *parent;

boolean LightCallback (Point3 *org, Point3 *p, GeomObj *o, double weight,
		       double *ret_weight, SCM *lightlist)
{
    double dist;
    Ray r0;
    Raystruct *ray;
    SCM raycell;
    extern long tc16_Ray;

    ray=newray();
    ray->origin=*org;
    V3Sub(p,org,&ray->direction);
    V3Normalize(&ray->direction);
    ray->weight=parent->weight;
    ray->scene=parent->scene;
    ray->flags=RF_SHADOW;
    ray->ignore_id = parent->ignore_id_out;
    ray->ignore_sphere = parent->ignore_sphere_out;
    SCM_NEWCELL(raycell);
    SCM_CDR(raycell)=(SCM)ray;
    SCM_CAR(raycell)=tc16_Ray;
    global_ray=raycell; /* is this okay?! */
    if (!RayPrimitiveIntersection((Ray*)ray,o,&dist,0,ray,&o)) {
	deleteray(raycell);
	return FALSE; /* We blew it, subdivide */
    }
    *ret_weight += weight;
    ray->shadow_length=dist;
    *lightlist = scm_cons(scm_cons(scm_makdbl(weight,0.0),raycell),
			  *lightlist);
    return TRUE;
}

/* call with flag=0 */
void SampleBox (LightSourceBox *box, Point3 *eye, GeomObj *p, int flag,
		double weight, double *ret_weight, SCM *lightlist)
{
    double a;
    Point3 mid, v;
    Point3 *p0 = box->p0;
    double l1 = box->l1;
    double l2 = box->l2;
    double l3 = box->l3;
    Point3 *v1 = box->v1;
    Point3 *v2 = box->v2;
    Point3 *v3 = box->v3;

    mid.x = p0->x+(v1->x+v2->x+v3->x)/2;
    mid.y = p0->y+(v1->y+v2->y+v3->y)/2;
    mid.z = p0->z+(v1->z+v2->z+v3->z)/2;

    a=sqrt(PBSDistance2(eye, box));
    if ((l1 + l2 + l3) / (1.5 * a) < box->angle) {
	/* the weight calculation here is naive. But it might work
	   well enough */
	if (LightCallback(eye, &mid, p, weight, ret_weight, lightlist)
	    || flag>=4)
	    return;
	flag++;
    }
    weight /= 2;
    if (l1 > l2 && l1 > l3) {
	v.x = v1->x / 2;
	v.y = v1->y / 2;
	v.z = v1->z / 2;
	box->v1 = &v;
	box->l1 = l1 / 2;
	SampleBox(box,eye,p,flag,weight,ret_weight,lightlist);
	V3Add(p0, &v, &mid);
	box->p0 = &mid;
	SampleBox(box,eye,p,flag,weight,ret_weight,lightlist);
	box->p0 = p0;
	box->v1 = v1;
	box->l1 = l1;
    }
    else if (l2 > l3) {
	v.x = v2->x / 2;
	v.y = v2->y / 2;
	v.z = v2->z / 2;
	box->v2 = &v;
	box->l2 = l2 / 2;
	SampleBox(box,eye,p,flag,weight,ret_weight,lightlist);
	V3Add(p0, &v, &mid);
	box->p0 = &mid;
	SampleBox(box,eye,p,flag,weight,ret_weight,lightlist);
	box->p0 = p0;
	box->v2 = v2;
	box->l2 = l2;
    }
    else {
	v.x = v3->x / 2;
	v.y = v3->y / 2;
	v.z = v3->z / 2;
	box->v3 = &v;
	box->l3 = l3 / 2;
	SampleBox(box,eye,p,flag,weight,ret_weight,lightlist);
	V3Add(p0, &v, &mid);
	box->p0 = &mid;
	SampleBox(box,eye,p,flag,weight,ret_weight,lightlist);
	box->p0 = p0;
	box->v3 = v3;
	box->l3 = l3;
    }
}

void CreateLightList (GeomObj *o, double angle, Raystruct *par, double w,
		      SCM *lightlist)
{
    double final_weight;

    /* init the three globals */
    *lightlist = SCM_EOL;
    parent=par;

    switch (o->var.type) {
    case TYPE_POINT:
	/* weight of a point is just 1/squared distance */
	LightCallback(&par->section, &o->var.point.p, o,
		      1.0 / V3SquaredDistance(&o->var.point.p, &par->section),
		      &final_weight, lightlist);
	break;
    case TYPE_POLY: {
	Point3 v1, v2, v3, origin;
	LightSourceBox b;
	static Point3 e1 = {1,0,0};
	static Point3 e2 = {0,1,0};
	static Point3 e3 = {0,0,1};
	GeomPoly *p = &o->var.polygon;
	int axis;
	SCM l;

	v1 = p->normal;
	axis = find_axis (&v1);
	switch (axis) {
	case X:
	    origin.y = p->min.x;
	    origin.z = p->min.y;
	    origin.x = -(v1.y*origin.y + v1.z*origin.z + p->ndist)/v1.x;
	    v2 = e2;
	    v3 = e3;
	    v2.y = fabs((p->max.x - p->min.x)*sqrt(v1.y*v1.y + v1.x*v1.x)/v1.x);
	    v2.x = -v1.y*v2.y/v1.x;
	    v3.z = fabs((p->max.y - p->min.y)*sqrt(v1.z*v1.z + v1.x*v1.x)/v1.x);
	    v3.x = -v1.z*v2.z/v1.x;
	    break;
	case Y:
	    origin.z = p->min.x;
	    origin.x = p->min.y;
	    origin.y = -(v1.z*origin.z + v1.x*origin.x + p->ndist)/v1.y;
	    v2 = e3;
	    v3 = e1;
	    v2.z = fabs((p->max.x - p->min.x)*sqrt(v1.z*v1.z + v1.y*v1.y)/v1.y);
	    v2.y = -v1.z*v2.z/v1.y;
	    v3.x = fabs((p->max.y - p->min.y)*sqrt(v1.x*v1.x + v1.y*v1.y)/v1.y);
	    v3.y = -v1.x*v2.x/v1.y;
	    break;
	case Z:
	    origin.x = p->min.x;
	    origin.y = p->min.y;
	    origin.z = -(v1.x*origin.x + v1.y*origin.y + p->ndist)/v1.z;
	    v2 = e1;
	    v3 = e2;
	    v2.x = fabs((p->max.x - p->min.x)*sqrt(v1.x*v1.x + v1.z*v1.z)/v1.z);
	    v2.z = -v1.x*v2.x/v1.z;
	    v3.y = fabs((p->max.y - p->min.y)*sqrt(v1.y*v1.y + v1.z*v1.z)/v1.z);
	    v3.z = -v1.y*v2.y/v1.z;
	    break;
	}
	V3Scale (&v1, EPS);
	/* polygon weight is viewing angle divided by 2pi */
	final_weight = 0;
	b.p0 = &origin;
	b.v1 = &v1; b.v2 = &v2; b.v3 = &v3;
	b.l1 = V3Length(&v1); b.l2 = V3Length(&v2); b.l3 = V3Length(&v3);
	b.angle = 0.5;
	SampleBox (&b, &par->section, o, 0, 1.0, &final_weight, lightlist);
	for (l = *lightlist; SCM_NNULLP(l); l = SCM_CDR(l)) {
	    SCM_SETCAR(SCM_CAR(l), scm_makdbl(SCM_REALPART(SCM_CAAR(l))
		* PolygonViewingAngle(&par->section, p)
		/ (2 * PI) / final_weight, 0.0));
	}
	break;
    }
    default:
	printf ("Unsupported lightsource type\n");
	break;
    }
}

/****** get_lightsources_contrib
 * NAME
 *  get_lightsources_contrib
 * SYNOPSIS
 *  void get_lightsources_contrib (SCM rcell, Raystruct *ray, double
 *  tdiffuse, double tspec, double transmittance, double specularity,
 *  double roughness, double cos1, SCM light, SCM *dcol, SCM *scol, SCM
 *  *tcol)
 * FUNCTION
 *  Find the contribution from the lightsources, for isotropic plastic/metal
 *  like materials. rcell and ray are the incoming ray, tdiffuse, tspec, 
 *  transmittance, specularity and roughness are the material parameters,
 *  cos1 is the scalar product between normal and the ray, and light is the
 *  light ray list. dcol returns the diffuse component, scos is specular,
 *  and tcol is total transparent light.
 * NOTES
 *  tcol should be separated into diffuse and specular component.
 ******
 */

void get_lightsources_contrib (SCM rcell, Raystruct *ray, double tdiffuse, double tspec, double transmittance, double specularity, double roughness, double cos1, SCM light, SCM *dcol, SCM *scol, SCM *tcol)
{
    double c1, weight;
    Raystruct *r;
    double df,sp=0,e;
    int flag=1;

    *dcol=*scol=*tcol=SCM_BOOL_F;

    while (SCM_NNULLP(light)) {
	SCM l = SCM_CAR(light), temp, node, c;
	
	light=SCM_CDR(light);
	if (is_light(OBJSTR(l)->material,rcell)==SCM_BOOL_T) {
            /* The second is_light checked for the directional lightsource */
	    SCM llist;

	    CreateLightList (OBJSTR(l), 0.1, ray, 0.5, &llist);
	    while (SCM_NNULLP(llist)) {
		node=SCM_CAR(llist);
		r=RAYSTR(temp=SCM_CDR(node));
		weight=SCM_REALPART(SCM_CAR(node));
		r->object=l;
		V3Normalize(&r->direction);
		c1=df=V3Dot(&r->direction, &ray->pat_n);
		if (df<0) {
		    df=-tdiffuse*df*(1-transmittance);
		    flag=0;
		}
		else df=df*(1-specularity)*(1-transmittance);
		
		if (df!=0 && (c=c_get_ray_rad(temp, rcell))
		    != SCM_BOOL_F) {
		    if (flag) {
			double rp;

			if (specularity==0) sp=0;
			else {
			    Point3 bisdir;
			    V3Normalize(V3Sub(&ray->direction,&r->direction,
					      &bisdir));
			    rp=sq(roughness);
			    e=sq(V3Dot(&bisdir,&ray->pat_n));
			    e=exp((e-1)/e/rp)/(rp*4);
			    sp=specularity*e*sqrt(-c1/cos1);
			    if (fabs(sp)>EPS) *scol=cplus_subr2(*scol,cscale_subr2(c,scm_makdbl(weight*sp,0.0)));
			}
		    } else if (tspec != 0) {
			double rp;
			Point3 t;

			V3Normalize(V3Sub(&ray->direction, &ray->pat_n, &t));
			rp=sq(roughness);
			e=exp((2*V3Dot(&r->direction,&t)-2)/rp)/rp;
			sp=tspec*e*sqrt(c1/cos1);
			if (fabs(sp)>EPS) *tcol=cplus_subr2(*tcol,cscale_subr2(c,scm_makdbl(weight*sp,0.0)));

		    } else sp=0;
		    if (df) *dcol=cplus_subr2(*dcol,cscale_subr2(c,scm_makdbl(weight*df,0.0)));
		}
		llist=SCM_CDR(llist);
		deleteray(temp);
	    }
	}
    }
}
