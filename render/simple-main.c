#include <stdio.h>

void init_user_scm (void)
{
}

char *getenv();
int main( argc, argv )
     int argc;
     char **argv;
{
#ifdef SCM_IMPLINIT
  char *initpath = SCM_IMPLINIT;
#else
  char *initpath = "Init.scm";
#endif
#ifndef nosve
  if (getenv("SCM_INIT_PATH")) initpath = getenv("SCM_INIT_PATH");
#endif
#ifndef NOSETBUF
# ifndef GO32
#  ifndef _DCC
#   ifndef ultrix
#    ifndef __WATCOMC__
#     ifndef THINK_C
#      if (__TURBOC__ != 1)
  if (isatty(fileno(stdin))) setbuf(stdin,0); /* turn off stdin buffering */
#      endif
#     endif
#    endif
#   endif
#  endif
# endif
#endif
  return run_scm(argc, argv,
                 (isatty(fileno(stdin)) && isatty(fileno(stdout)))
                 ? (argc <= 1) ? 2 : 1 : 0,
                 initpath);
}

