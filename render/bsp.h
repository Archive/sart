
/****h* (render)/bsp.h [0.1]
 * NAME
 *  bsp.h
 * COPYRIGHT
 *   Miroslav Silovic
 * MODIFICATION HISTORY
 *  
 * NOTES
 *  This file contains various prototypes, macros and declarations. This
 *  document will only outline the most important type declarations.
 ******
 */

#ifndef _bsp_h_
#define _bsp_h_

#include "defs.h"
#include "linear.h"
#include "dalloc.h"
#include "geom-types.h"
#include "raytrace.h"
#include "render.h"

boolean PointInBox (Point3 *, Point3 *, Point3 *);
void PointAtDistance (Ray *ray, double distance, Point3 *pt);
boolean RayBoxIntersect (Ray *ray, Point3 *min, Point3 *max, double *returnMin, double *returnMax);
void CalcAxis (Point3 *p, double *x, double *y, int axis);
boolean RayPolygonIntersection (Ray *ray, GeomPoly *poly, double *dist);
boolean RayTriangleIntersection (Ray *ray, Point3 *p1, Point3 *p2, Point3 *p3, Point3 *nPtr, double d, double *dist);
void smooth_triangle (Point3 *v[3], Point3 *h[3], Point3 *section, Raystruct *r);
double PolygonViewingAngle (Point3 *t, GeomPoly *p);
double DistanceToLine (Point3 *t1, Point3 *t2, Point3 *t);
double DistanceToPolygon (Point3 *t, GeomPoly *p);
typedef boolean (*rayobj_type)();
boolean RayTreeIntersect (Raystruct *, BinTree *BSPTree, GeomObj **obj,
			  double *distance, rayobj_type);
#define CONTAINS_VOXEL 111
void InitBinTree (BinTree *tree, GeomObjList *l, geominbox_type);
void InitBinTree2 (BinTree *tree, GeomObjList *l, geominbox_type);
int FreeBinTree (BinTree *tree);
int DumpNode (BinNode *root, int node, int depth);

typedef void (*VectorOutputHook)(Point3 *, Point3 *);
void VectorPrimitive (GeomObj *, VectorOutputHook f);
void xview_list (GeomObjList *);
void rtrace_test (BinTree *);

int find_axis (Point3 *normal);
void SplitByPlane (GeomPoly *, GeomPoly *, GeomPoly *, Point3 *, double, boolean);
void PolyFree (GeomPoly *);


void zb_persp_poly (GeomPoly*, int id);
void zb_init (int height, int width);
void set_zbstruct (ZBuffer *);
void ZBufferPersp (GeomObj*);
int retrieve_id (Scene *s, int id);

SCM make_dvect (int n, double *e);
SCM make_v3(long, double, double, double);
SCM combine_colors (SCM c1, SCM c2, double t1, double t2);
SCM colorp(SCM);
SCM cscale_subr2(SCM, SCM);
SCM cplus_subr2(SCM, SCM);
SCM cfilter_subr2(SCM, SCM);

int NewId (void);
int SetSceneIDs (Scene *);
boolean PrimitiveBoxTest (Point3 *, Point3 *, GeomObj *);
boolean RayPrimitiveIntersection (Ray *, GeomObj *, double *, double,
				  Raystruct *, GeomObj**);
GeomObj *CopyPrimitive (GeomObj *);
int FreePrimitive (GeomObj *);
void BindPrimitive (GeomObj *);
void TransformPrimitive (GeomObj *, Matrix4 *);

void EvalWithPrimitive (Raystruct *r);

SCM primitivep(SCM);
void TransformBox (Point3 *imin, Point3 *imax, Point3 *omin, Point3 *omax,
		   Matrix4 *mat);

typedef boolean (*BTDCallback)(GeomObj*, void*);
boolean BoxTreeCall (BinTree *tree, Point3 *bmin, Point3 *bmax,
		     BTDCallback f, void *data, rayobj_type check_rayobj);



void startup_indirect(void);
SCM get_indirect_lighting (SCM r);

double PointBoxSqDistance (Point3 *o, Point3 *min, Point3 *max);

#endif
