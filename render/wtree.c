
#include "tmalloc.h"
#include "wtree.h"

static
void free_wavetree_rec (Wavenode *root, Wavetree *t)
{
    if (root) {
	if (root->branches) {
	    int i;
	    
	    for (i = 0; i < t->branch_count; i++)
		free_wavetree_rec (root->branches[i], t);
	    tfree(root->branches, t->branch_count * sizeof(Wavenode*));
	}
	if (t->d)
	    t->d(root);
	tfree(root, sizeof(Wavenode**)
	      + root->branches ? t->extra : t->leaf_extra);
    }
}

/****** free_wavetree
 * NAME
 *   free_wavetree
 * SYNOPSIS
 *   void free_wavetree (Wavetree *t)
 * FUNCTION
 *   Free the wavetree. It uses ValueDestructor to clean up the values.
 ******/

void free_wavetree (Wavetree *t)
{
    free_wavetree_rec (t->root, t);
    tfree (t, sizeof(Wavetree));
}


/****** wavetree_ref
 * NAME
 *   wavetree_ref
 * SYNOPSIS
 *   void *wavetree_ref (Wavetree *t, int depth, int x[])
 * FUNCTION
 *   Return the value pointer for the node at given depth and coordinates.
 *   Legal coordinates range from 0 to 2^depth - 1. If there is no such
 *   node, return NULL.
 ******/

void *wavetree_ref (Wavetree *t, int depth, int x[])
{
    int nhalf, i, k;
    Wavenode *n = t->root;;

    while (1) {
	if (!depth)
	    return (void*)n->value;
	if (!n->branches)
	    return NULL;
	nhalf = (1 << (depth - 1));
	for (i = k = 0; i < t->dim; i++) {
	    if (x[i] & nhalf) {
		k += (1 << i);
	    }
	}
	n = n->branches[k];
	depth --;
    }
}

/****** biggest_containing
 * NAME
 *   biggest_containing
 * SYNOPSIS
 *   Wavenode *biggest_containing (Wavetree *t, int depth, int x[], 
 *                                 int *depth_out)
 * FUNCTION
 *   Return the wavenode pointer for the biggest node containing the one
 *   specified by coordinates and depth. depth_out will contain the
 *   difference between the depth given and the one reached (i.e. 0
 *   if that's exactly the node specified with depth and x, and depth
 *   if the root node was returned.
 ******/

Wavenode *biggest_containing (Wavetree *t, int depth, int x[], int *depth_out)
{
    int nhalf, i, k;
    Wavenode *n1, *n = t->root;

    while (1) {
	if (!depth || !n->branches) {
	    *depth_out = depth;
	    return n;
	}
	nhalf = (1 << (depth - 1));
	for (i = k = 0; i < t->dim; i++) {
	    if (x[i] & nhalf) {
		k += (1 << i);
	    }
	}
	n1 = n->branches[k];
	if (!n1) {
	    *depth_out = depth;
	    return n;
	}
	n = n1;
	depth --;
    }
}

static
Wavenode *create_wtree_rec (Wavetree *t, int depth, int x[])
{
    boolean is_leaf;
    Wavenode *n;

    if (t->crit(depth, x, t->critdata)) {
	Wavenode **branches = tmalloc(t->branch_count * sizeof(Wavenode*));
	int i, j;
	int y[16], z[16];
	boolean flag;

	for (j = 0; j < t->dim; j++)
	    z[j] = x[j] << 1;
	flag = FALSE;
	for (i = 0; i < t->branch_count; i++) {
	    for (j = 0; j < t->dim; j++)
		y[j] = z[j] + ((i & (1 << j)) ? 1 : 0);
	    branches[i] = create_wtree_rec(t, depth + 1, y);
	    flag = flag || branches[i];
	}
	is_leaf = !flag;
	if (flag) {
	    n = (Wavenode*)tmalloc(sizeof(Wavenode**) + t->extra);
	    n->branches = branches;
	}
	else {
	    n = (Wavenode*)tmalloc(sizeof(Wavenode**) + t->leaf_extra);
	    n->branches = NULL;
	    tfree(branches, t->branch_count * sizeof(Wavenode*));
	}
    } else {
	n = (Wavenode*)tmalloc(sizeof(Wavenode**) + t->leaf_extra);
	is_leaf = TRUE;
	n->branches = NULL;
    }
    if (!t->constr(n, depth, x, is_leaf, t->consdata) && depth) {
	/* this subtree is reduntant and non-root. Kill it */
	free_wavetree_rec(n, t);
	return NULL;
    }
    return n;
}


/****** create_wtree
 * NAME
 *   create_wtree
 * SYNOPSIS
 *   Wavetree *create_wtree (int dim,
 *		             ValueDestructor d,
 *		             ValueConstructor constr, void *consdata,
 *		             SubdCriterium crit, void *critdata,
 *		             int extra, int leaf_extra)
 * FUNCTION
 *   Create and initialize a wavetree. This function will perform the entire
 *   creation foo. You need to supply constructor and destructor for the
 *   values, as well as subdivision criterium. Constructor will then be
 *   called with the node pointer, depth, coordinates, flag specifying
 *   whether the node is a leaf, and consdata. Criterium should accept
 *   depth, coordinates, and critdata. Note that if a constructor returns
 *   false, the corresponding node will be freed (possibly causing 'chain
 *   leafing' of the nodes).
 ******/

Wavetree *create_wtree (int dim,
			ValueDestructor d,
			ValueConstructor constr, void *consdata,
			SubdCriterium crit, void *critdata,
			int extra, int leaf_extra)
{
    int x[16], i;
    Wavetree *t = (Wavetree*)tmalloc(sizeof(Wavetree));

    if (dim > 16) return NULL;
    for (i = 0; i < dim; i++)
	x[i] = 0;
    t->dim = dim;
    t->branch_count = (1 << dim);
    t->d = d;
    t->constr = constr;
    t->consdata = consdata;
    t->crit = crit;
    t->critdata = critdata;
    t->extra = extra;
    t->leaf_extra = leaf_extra;
    t->root = create_wtree_rec (t, 0, x);
    return t;
}
