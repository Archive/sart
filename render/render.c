/****h* (render)/render.c [1.0]
 * NAME
 *  render.c
 * COPYRIGHT
 *  Miroslav Silovic
 * MODIFICATION HISTORY
 *
 * NOTES
 *  trace protocol: spawn_eye_ray to get the csgs operational. Then
 *  parent the new rays to the result of the function.  Shadow rays
 *  must have the shadow_length given in advance - materials MUST take
 *  care to propagate it. A call to process_shadow_ray should be able
 *  to fix it.
 *    
 *     csg material protocol: called with a ray, forward the ray to
 *  the surface of the csg. You may need to construct a new ray to
 *  calculate the bending. Return the color. FINISH: add Fresnel's correction
 *    
 *     material formats:
 *  lambda
 *  scm_cons(lambda, options)
 *  each option is a symbol, or scm_cons(symbol, value)
 *    
 *     surface material protocol:
 *  called with a ray. Return the color value. If the ray is a shadow ray,
 *  only try to forward it toward the lightsource.
 *    
 *  TODO: Fix the shadow rays so that they're transmitted by default
 * BUGS
 *  CSG function were not tested for the case when surface delimits two
 *  csg containers.
 ******
 */

#include "bsp.h"
#include <math.h>

/****** global_lock,global_ray(2)
 * FUNCTION
 *  These two global variables are used to store the ray for
 *  evaluation with tree primitives (called with Scheme
 *  selector). Since the primitive must be able to eval the original
 *  ray, no matter how deeply the trees were nested, global ray is set
 *  only once, and held by incrementing global_lock.
 * SOURCE
 */

extern int global_lock;
extern SCM global_ray;

/*******/

/****** material_tags
 * NAME
 *  mtag_light, mtag_transmit_shadows, mtag_texture_mat, mtag_ior
 * SYNOPSIS
 *  SCM mtag_light; 
 *  SCM mtag_transmit_shadows; 
 *  SCM mtag_texture_mat; 
 *  SCM mtag_ior
 *  lightsource, transmit-shadows, texture-matrix, ior (symbols)
 * FUNCTION
 *  'lightsource marks the material as a source of light (and a target
 *   for the shadow rays)
 *
 *  'transmit-shadows specifies that the transparent materials should
 *   transmit shadow rays (this is *NOT* the default, because of the
 *   fact that some user materials might have problems with shadow
 *
 *   ray propagation (see: render.c, notes).
 *  'texture-matrix transforms the ray texture section and texture
 *   normal before evaluating the texture.
 * NOTES
 *  These are implemented as immediate integers right now, this shouls
 *  be changed to real symbols.
 ******
 */

SCM mtag_light;
SCM mtag_transmit_shadows;
SCM mtag_texture_mat;
SCM mtag_ior;

extern long scm_tc16_array;

/****** ambient_light
 * NAME
 *  ambient_light, ambient_infinity, weight_limit
 * FUNCTION
 *  These globals are checked from the raytracer recursion. If the
 *  ray weight is below weight_limit, ambient light is returned
 *  without further evaluation. Both colors may be procedures.
 *  Ambient_infinity, on the other hand, is used for infinity hits.
 * SOURCE
 */

SCM ambient_light = SCM_BOOL_F;
SCM ambient_infinity = SCM_BOOL_F; 
double weight_limit = 0.01;

/******/

/****** sphere_geom
 * NAME
 *  sphere_geom
 *  global_zbuffer_mode
 * FUNCTION
 *  This global is initialized from the scheme-core, and contains
 *  parametric version of the sphere. global_zbuffer_mode controls
 *  whether parametric or standard version of the sphere will be
 *  used. sphere_geom is *required* to be tessel inscribed in
 *  the sphere of radius 1.
 * SOURCE
 */

SCM *loc_sphere_geom;
SCM global_zbuffer_mode = SCM_BOOL_F;

/******/

/****** ray_vector
 * NAME
 *  loc_ray_vector
 * FUNCTION
 *  This is the global containing the vector of reusable ray structures.
 *  It's bound to an internal symbol to protect it from gc, and this variable
 *  points to the SCM that can be overwriten to get the actual location.
 *  You don't want to modify that variable.
 * SOURCE
 */

SCM *loc_ray_vector;

/******/

/****** global_tree_depth
 * NAME
 *  global_tree_depth, global_tree_node_size
 * FUNCTION
 *  These two settings affect parameters of each BinTree created. Larger
 *  nodes will make enclosing faster, but they'll slow down the rendering
 * SOURCE
 */

int global_tree_depth;
int global_tree_node_size;

/******/

/****** set_bintree_defaults
 * NAME
 *   set_bintree_defaults
 * SYNOPSIS
 *   SCM set_bintree_defaults (SCM depth, SCM node)
 *   set-tree-defaults (subr2)
 * FUNCTION
 *   Sets global_tree_depth and global_tree_node_size. Defaults are
 *   30 and 8, respectively.
 ******/

/* SUBR (subr2) */
char s_set_bintree_defaults[] = "set-tree-defaults";
SCM set_bintree_defaults (SCM depth, SCM node)
{
    SCM_ASSERT (SCM_INUMP(depth), depth, SCM_ARG1, s_set_bintree_defaults);
    SCM_ASSERT (SCM_INUMP(node), node, SCM_ARG2, s_set_bintree_defaults);
    global_tree_depth = SCM_INUM(depth);
    global_tree_node_size = SCM_INUM(node);
    return SCM_UNSPECIFIED;
}

/****** is_light
 * NAME
 *  is_light
 * SYNOPSIS
 *  int is_light (SCM material, SCM r) 
 *  is-lightsource (subr2)
 * FUNCTION
 *  Checks for the presence of lightsource tag. If its value is a
 *  predicate, it will be called with the second argument (ray
 *  expected) - useful for directional lights.
 ******
 */

/* SUBR (subr2) */
char s_is_light[] = "is-lightsource";
SCM is_light (SCM material, SCM r)
{
    SCM f;
    
    /* check whether the material is a lightsource. Simple: Just look for
       'light symbol, or ('light predicate) in the proplist, predicate will
       not be tested if r is #f */

    if (SCM_IMP(material)) return SCM_BOOL_F; /* for tree primitives */
    if ((f=scm_procedure_property(material,mtag_light))!=SCM_BOOL_F) {
	if (scm_procedure_p(f)==SCM_BOOL_F || r==SCM_BOOL_F) return SCM_BOOL_T;
	return scm_apply(f,r,scm_listofnull);
    }
    return SCM_BOOL_F;
}

/* ----------------- scene utilities ------------------ */


/****** SceneSMOB
 * NAME
 *  SceneSMOB
 * FUNCTION
 *  SceneSMOB keeps an enclosed tree with the objects. It will
 *  also hold a lightsource and a CSG list.
 * NOTES
 *  Putting the same object (that is, multiple references to a single
 *  object) into more than one scene structure is not checked for, and
 *  is *very* bad idea. Copy (or construct again) the object you
 *  want to have multiplied. Use trees to save memory, they are safe.
 * SEE ALSO
 *  make_scene
 * SOURCE
 */

long tc16_Scene;

SCM Scene_mark (SCM scene)
{
    int i;
    Scene *s=(Scene*)scm_markcdr(scene);
    
    for (i=0; i<s->n; i++)
	scm_gc_mark(s->object_list[i]);
    scm_gc_mark(s->light_list);
    scm_gc_mark(s->csg_list);
    return SCM_BOOL_F;
}

scm_sizet Scene_free (SCM_CELLPTR scene)
{
    Scene *s=SCENESTR(scene);
    int size;

    size=sizeof(SCM)*(s->n);
    free(s->object_list);
    size+=FreeBinTree (s->object_tree);
    size+=sizeof(Scene);
    free(s);
    return size;
}

int Scene_print (SCM scene, SCM port, int writing)
{
    Scene *s=SCENESTR(scene);
    char buf[256];

    sprintf(buf,"#<Scene/%x o=%d>",(int)s, s->n);
    scm_puts(buf, port);
    return 1;
}

static scm_smobfuns Scenesmob = {Scene_mark, Scene_free, Scene_print, 0};

/******/

/****** scenep
 * NAME
 *  scenep
 * SYNOPSIS
 *  SCM scenep (SCM scene) 
 *  scene? (subr1)
 * FUNCTION
 *  This is the standard predicate for the Scene type.
 * SEE ALSO
 *  SceneSMOB
 ******
 */

/* SUBR (subr1) */
char s_scenep[] = "scene?";
SCM scenep (SCM scene)
{
    if (SCM_NIMP(scene) && SCM_TYP16(scene)==tc16_Scene)
	return SCM_BOOL_T;
    return SCM_BOOL_F;
}

void enclose_scene (Scene *scene)
{
    int i;
    GeomObjList l;
    SCMPTR o=scene->object_list;
    SCM c,t,u;
    extern boolean GeomInBox();
    
    l.length=scene->n;
    l.list=NEWARRAY(GeomObj*,l.length);
    for (i=0; i<l.length; i++) {
	l.list[i]=OBJSTR(o[i]);
	l.list[i]->cell=o[i];
    }
    SetSceneIDs(scene);
    scene->object_tree=NEWTYPE(BinTree);
    InitBinTree(scene->object_tree,&l,GeomInBox);
    
    /* construct the lightsource list */
    u=SCM_EOL;
    for (i=0; i<l.length; i++)
	if (is_light(OBJSTR(o[i])->material, SCM_BOOL_F)==SCM_BOOL_T)
	    u=scm_cons(o[i],u);
    scene->light_list=u;

    /* now the csg list */
    for (i=0; i<l.length; i++) {
	c=CSGCONT(o[i]);
	while (c!=SCM_BOOL_F) {
	    Csg *cc = CSGFIELD(c);
	    cc->nactive = 1;
	    c = cc->parent;
	}
    }
    t=SCM_EOL; /* different var, for gc */
    for (i=0; i<l.length; i++) {
	c=CSGCONT(o[i]);
	while (c!=SCM_BOOL_F) {
	    Csg *cc = CSGFIELD(c);
	    if (cc->nactive) {
		t = scm_cons(c,t);
		cc->nactive = 0;
	    }
	    c = cc->parent;
	}
    }
    scene->csg_list = t;
    scene->zbuffer = SCM_BOOL_F;
}

/****** make_scene
 * NAME
 *  make_scene
 * SYNOPSIS
 *  SCM make_scene (SCM objects) 
 *  make-scene (subr1)
 * FUNCTION
 *  The scene is constructed from a list of objects. The real work is done
 *  in the enclose_scene function, and involves creation of lightsource
 *  and csg lists.
 ******
 */

/* SUBR (subr1) */
char s_make_scene[]="make-scene";
SCM make_scene (SCM objects)
{
    int len,i;
    SCM s;
    Scene *scene;
    SCM o=objects;
    extern long tc16_Geom;

    SCM_ASSERT((len=scm_ilength(objects))>0,objects,SCM_ARG1,s_make_scene);
    scene=MNEWTYPE(Scene);
    scene->eye_csg=SCM_BOOL_F;
    scene->n=len;
    scene->object_list=MNEWARRAY(SCM,len);
    scene->outer_volume_mat = SCM_BOOL_F;
    for (i=0; i<len; i++) {
	SCM_ASRTGO (SCM_NIMP(SCM_CAR(o)) && SCM_TYP16(SCM_CAR(o))==tc16_Geom,badobj);
	scene->object_list[i]=SCM_CAR(o);
	o=SCM_CDR(o);
    }
    enclose_scene(scene);
    SCM_NEWCELL(s);
    SCM_CDR(s)=(SCM)scene;
    SCM_CAR(s)=tc16_Scene;
    return s;
    
  badobj:
    free(scene->object_list);
    free(scene);
    SCMERROR(o,SCM_ARG1,s_make_scene);
    return SCM_BOOL_F;
}


/****** set_outer_mat
 * NAME
 *   set_outer_mat
 * SYNOPSIS
 *   SCM set_outer_mat (SCM scene, SCM mat)
 * FUNCTION
 *   Set the outer volume material for the scene. This material will transmit
 *   the rays outside all the other CSGs. Currently CSGs whose material
 *   is #f will NOT default to this.
 ******/

/* SUBR (subr2) */
char s_set_outer_mat[]="set-scene-outer-volume!";
SCM set_outer_mat (SCM scene, SCM mat)
{
    SCM_ASSERT(scenep(scene) == SCM_BOOL_T, scene, SCM_ARG1, s_set_outer_mat);
    SCENESTR(scene)->outer_volume_mat = mat;
    return SCM_UNSPECIFIED;
}

/* --------------------- csg utilities ----------------- */

/****** true_section
 * NAME
 *  true_section
 * SYNOPSIS
 *  boolean true_section (SCM s)
 * FUNCTION
 *  s is a csg container field of an object that has just been hit by
 *  the ray (can be #f, in which case the section is always true, or a
 *  CSG container, or a scm_cons of two containers). This function goes up
 *  the CSG object hierarchy and checks whether the intersection
 *  trully occured, that is, whether the surface is really a boundary
 *  of a volume.
 *  The idea is that CSG containers are tracked for the number of the
 *  active subcontainers (active = point on the ray's side are inside
 *  the container) and for their own activity. The section is true
 *  if the toplevel container's activity is about to change.
 *  Rules:
 *     CONTAINER    -  all the intersections are true.
 *     INTERSECTION -  current container is inactive, there is only one
 * 		       inactive container, and that one is the parent
 * 	     OR:
 * 		       current container is active and so are all the others
 *     UNION        -  current container is inactive amd none other is
 * 	     OR:
 * 		       current container is active, one member is active, and
 * 		       that one is the parent
 *     MINUS        -  when entered from aux, intersection is real if no other
 * 		       objects are active
 * 		       when entered from anything else, aux must be active
 * 		       and similar rule to UNION applies
 * 		       note that nactive in this case applies only to the
 * 		       substracted objects.
 ******
 */

boolean true_section (SCM s)
{
    Csg *current;
    SCM s_current, prev_current;
    int active, lastactive;
    
    if (s==SCM_BOOL_F) return TRUE; /* No csgs affect this object */
    if (PAIR_P(s)) return true_section ((CSGFIELD(SCM_CAR(s))->flags & ORIGIN_MARK) ? SCM_CAR(s) : SCM_CDR(s));
    prev_current=s_current=s;
    while (s_current != SCM_BOOL_F) {
	current=CSGFIELD(s_current);
	active=current->flags & ORIGIN_MARK;
	switch (current->type) {
	case CSG_CONTAINER:
	    break;
	case CSG_INTERSECTION:
	    if ((active && (current->ntotal == current->nactive)) ||
		(!active && (current->ntotal == current->nactive+1) &&
		 !lastactive))
		break;
	    return FALSE;
	case CSG_UNION:
	    if ((!active && (current->nactive == 0)) ||
		(active && (current->nactive == 1) && lastactive))
		break;
	    return FALSE;
	case CSG_MINUS:
	    if (prev_current==current->aux)
		if (current->nactive==0)
		    break;
		else
		    return FALSE;
	    if (!((CSGFIELD(current->aux))->flags & ORIGIN_MARK))
		return FALSE;
	    if ((active && (current->nactive == 0)) ||
		(!active && (current->nactive == 1) && lastactive))
		break;
	    return FALSE;
	default:
	    printf ("FATAL: ILLEGAL CSG TYPE");
	    exit(0);
	}
	lastactive=active;
	prev_current=s_current;
	s_current = current->parent;
    }
    return TRUE;
}

/****** cross_intersection
 * NAME
 *  cross_intersection
 * SYNOPSIS
 *  SCM last_container_found;
 *  void cross_intersection (SCM s)
 * FUNCTION
 *  Once the true intersection is detected, this call will update the
 *  section counts and activity flags for all the CSGs that supersede
 *  s in the hierarchy.
 * RESULT
 *  last_container_found is a global return of the call, and it will
 *  contain the toplevel container for the ray. It will be used to
 *  determine eye_csg in the init_intersections.
 ******
 */

SCM last_container_found;
void cross_intersection (SCM s)
{
    Csg *current;
    SCM s_current, prev_current;
    int active, nactive;
    int lastactive=0; /* The first csg is a container, for which the
			 lastactive variable isn't important */
    
    if (s==SCM_BOOL_F) return;
    if (PAIR_P(s)) {
	if ((CSGFIELD(SCM_CAR(s))->flags & ORIGIN_MARK)) {
	    cross_intersection(SCM_CAR(s));
	    cross_intersection(SCM_CDR(s));
	}
	else {
	    cross_intersection(SCM_CDR(s));
	    cross_intersection(SCM_CAR(s));
	}
	return;
    }
    prev_current=s_current=s;
    while (s_current != SCM_BOOL_F) {
	last_container_found=s_current; /* for init_intersections */
	current=CSGFIELD(s_current);
	nactive=current->nactive;
	current->nactive+=lastactive;
	active=current->flags & ORIGIN_MARK;
	switch (current->type) {
	case CSG_CONTAINER:
	    break;
	case CSG_INTERSECTION:
	    if ((active && (current->ntotal == nactive)) ||
		(!active && (current->ntotal == nactive+1) &&
		 (lastactive == 1)))
		break;
	    return;
	case CSG_UNION:
	    if ((!active && (nactive == 0)) ||
		(active && (nactive == 1) && (lastactive == -1)))
		break;
	    return;
	case CSG_MINUS:
	    if (prev_current==current->aux) {
		current->nactive=nactive;
		if (nactive==0)
		    break;
		else
		    return;
	    }
	    if (!((CSGFIELD(current->aux))->flags & ORIGIN_MARK))
		return;
	    if ((active && (nactive == 0)) ||
		(!active && (nactive == 1) && (lastactive == -1)))
		break;
	    return;
	  default:
	    printf ("FATAL: ILLEGAL CSG TYPE");
	    exit(0);
	}
	current->flags ^= ORIGIN_MARK;
	lastactive=active ? -1 : 1; /* active is now negated */
	prev_current=s_current;
	s_current = current->parent;
    }
}

/****** cast_ray
 * NAME
 *  cast_ray
 * SYNOPSIS
 *  boolean cast_ray (Raystruct *ray)
 * INPUTS
 *  This function reads scene, origin and the direction from the
 *  raystruct.
 *  global_ray must be set prior to the call to this function,
 *  as it will be used by tree primitives.
 * RESULT
 *  TRUE or FALSE, depending on whether the ray has penetrated the
 *  scene's bsp tree.
 *  ray->section and length will be overwritten.
 * NOTES
 *  It isn't checked whether the intersection obtained is true
 *  (true_section needs to be called after this).
 ******
 */

boolean cast_ray (Raystruct *ray)
{
    double d;
    GeomObj *o;
    
    ray->ignore_id_out = RI_DO_NOT_IGNORE; /* reset the output to default */
    if (!RayTreeIntersect(ray,SCENESTR(ray->scene)->object_tree,&o,&d,
			  RayPrimitiveIntersection))
	return FALSE;
    ray->object=o->cell;
    ray->length=d;
    PointAtDistance((Ray*)ray,d,&ray->section);
    return TRUE;
}

/****** init_intersections
 * NAME
 *  init_intersections
 * SYNOPSIS
 *  void init_intersections (Raystruct *ray_in)
 * FUNCTION
 *  ray_in is called with the origin and scene information. The
 *  origin is *expected* to be used as an eye afterwards (since the
 *  scene will carry the csg startup information in the eye_csg.
 *  This function is very cheap if there are no CSGs in the scene
 *  (because the csg_list is empty in that case).
 ******
 */

void init_intersections (Raystruct *ray_in)
{
    Raystruct ray=*ray_in;
    static Point3 trv = {-1, -1, -1};
    Scene *scene;
    SCM t;
    
    scene=SCENESTR(ray.scene);
    scene->eye_csg=SCM_BOOL_F;
    for (t=scene->csg_list; SCM_NNULLP(t); t=SCM_CDR(t)) {
	SCM o=SCM_CAR(t);
	Csg *c=CSGFIELD(o);
	    
	c->flags &= ~ORIGIN_MARK;
	c->nactive=0;
    }

    if (!PointInBox(&scene->object_tree->min, &scene->object_tree->max,
		    &ray.origin)) {
	ray_in->ignore_id_out = RI_DO_NOT_IGNORE;
	return;
    }

    if (SCM_NNULLP(scene->csg_list)) {
	last_container_found=SCM_BOOL_F;
	V3Add(&scene->object_tree->min, &trv, &ray.origin);
	V3Sub(&ray_in->origin, &ray.origin, &ray.direction);
	ray.ignore_id = RI_DO_NOT_IGNORE;
	while (cast_ray(&ray) &&
	       ray.section.x+EPS<ray_in->origin.x &&
	       ray.section.y+EPS<ray_in->origin.y &&
	       ray.section.z+EPS<ray_in->origin.z) {
	    cross_intersection(ray.csg_sector);
	    ray.origin=ray.section;
	    ray.ignore_id = ray.ignore_id_out;
	    ray.ignore_sphere = ray.ignore_sphere_out;
	}
	/* We use global which cross_intersection defines specifically for
	   this purpose */
	scene->eye_csg=last_container_found;
	ray_in->ignore_id_out = RI_DO_NOT_IGNORE;
	return;
    }
    ray_in->ignore_id_out = RI_DO_NOT_IGNORE;
}

/* --------------------- ray utilities ----------------- */

/****** RaySMOB
 * NAME
 *  RaySMOB
 * FUNCTION
 *  The carrier for the ray structure (defined in render.h).
 * SOURCE
 */

long tc16_Ray;

SCM mark_Ray (SCM o)
{
    Raystruct *r=(Raystruct*)scm_markcdr(o);

    scm_gc_mark(r->parent);
    return r->scene;
}

scm_sizet free_Ray (SCM_CELLPTR o)
{
    Raystruct *r=RAYSTR(o);
    int size;

    size = sizeof(Raystruct);
    if (r->ndata)
	free(r->ndata);
    free(r);
    return size;
}

int print_Ray (SCM r, SCM port, int writing)
{
    Raystruct *ray=RAYSTR(r);
    Point3 *o=&ray->origin, *d=&ray->direction;
    char buf[256];

    sprintf (buf, "#<Ray/%x org=(%f %f %f) dir=(%f %f %f) w=%f flags=%d>",
	     (int)ray, o->x, o->y, o->z, d->x, d->y, d->z,
	     ray->weight, ray->flags);
    scm_puts(buf, port);
    return 1;
}

static scm_smobfuns Raysmob = {mark_Ray, free_Ray, print_Ray, 0};

SCM init_ray_vector (void)
{
#define STARTING_SIZE 32
    int i;
    SCM *v, vect;

    vect = scm_make_vector(SCM_MAKINUM(STARTING_SIZE + 1), SCM_UNSPECIFIED);
    v = SCM_VELTS(vect);
    v[0] = SCM_MAKINUM(1); /* The first element is stack pointer */
    return vect;
}

SCM store_ray (SCM vect, SCM ray)
{
    int len, ind;
    SCM *v;

    len = SCM_LENGTH(vect);
    v = SCM_VELTS(vect);
    v[0] = SCM_MAKINUM((ind = SCM_INUM(v[0])) + 1);
    if (ind >= len) {
	int i;
	SCM *v1, tmp;

	tmp = scm_make_vector (SCM_MAKINUM(len*2-1), SCM_UNSPECIFIED);
	v1 = SCM_VELTS(tmp);
	for (i=0; i<len; i++)
	    v1[i] = v[i];
	vect = tmp;
	v = v1;
    }
    v[ind] = ray;
    return vect;
}

SCM retrieve_ray (SCM vect)
{
    int ind;
    SCM *v, ray;

    v = SCM_VELTS(vect);
    ind = SCM_INUM(v[0]) - 1;
    if (ind > 0) {
	v[0] = SCM_MAKINUM(ind);
	ray = v[ind];
	v[ind] = SCM_UNSPECIFIED;
	return ray;
    }
    else
	return SCM_UNSPECIFIED;
}

/********/

/****** rayp
 * NAME
 *  rayp
 * SYNOPSIS
 *  SCM rayp (SCM ray)
 *  ray? (subr1)
 * FUNCTION
 *  Standard predicate for RaySMOB.
 ******
 */

/* SUBR (subr1) */
char s_rayp[] = "ray?";
SCM rayp (SCM ray)
{
    if (SCM_NIMP(ray) && SCM_TYP16(ray)==tc16_Ray)
	return SCM_BOOL_T;
    return SCM_BOOL_F;
}

/****** newray
 * NAME
 *  newray
 * SYNOPSIS
 *  Raystruct *newray(void)
 * FUNCTION
 *  Allocates a new Raystruct and returns a pointer to is. This is the
 *  recommended way of creating new rays, because it will also set its
 *  ndata pointer to zero (absolutely essential, since noninitialized
 *  ndata field can crash the renderer).
 *  Thus, NEWTYPE(Raystruct) anywhere except in this function is
 *  probably an error.
 ******
 */

Raystruct *newray(void)
{
    SCM ray;
    Raystruct *r;

    if ((ray = retrieve_ray(*loc_ray_vector)) == SCM_UNSPECIFIED) {
	r = MNEWWHAT(Raystruct, "ray");
    }
    else {
	SCM_DEFER_INTS;
	r = RAYSTR(ray);
	SCM_CDR(ray) = 1.0;
	SCM_CAR(ray) = scm_tc_flo;
	SCM_ALLOW_INTS;
    }
    r->parent=SCM_BOOL_F;
    r->ndata=NULL;
    r->object=r->mat_override=SCM_BOOL_F;
    r->csg=SCM_BOOL_F;
    return r;
}

/****** deleteray
 * NAME
 *   deleteray
 * SYNOPSIS
 *   void deleteray (SCM ray)
 * FUNCTION
 *   Store the ray to a list of unallocated ray structures, for salvage
 *   by newray().
 ******/

void deleteray (SCM ray)
{
    Raystruct *r = RAYSTR(ray);

    if (r->ndata)
	free(r->ndata);
    r->ndata = NULL;
    *loc_ray_vector = store_ray(*loc_ray_vector, ray);
}

/****** spawn_eye_ray2
 * NAME
 *  spawn_eye_ray2
 * SYNOPSIS
 *  SCM spawn_eye_ray2 (Point3 *origin, SCM scene)
 * FUNCTION
 *  origin is the origin of the new child ray. Scene is the initial
 *  scene. The new ray is allocated and initialized
 *  (defaults: flags=0, weight=1, parent=#t, csg=#t <-- this is the
 *   eye-ray's mark)
 * NOTES
 *  If global lock is 0, global ray will also be set.
 * SEE ALSO
 *  find_intersecting_object, spawn_eye_ray
 ******
 */

SCM spawn_eye_ray2 (Point3 *origin, SCM scene)
{
    Raystruct *r;
    static Point3 csg_direction={1.231,2.18233,3.12873}; /* arbitrary */
    SCM c;
    
    r=newray();
    r->origin=*origin;
    r->direction=csg_direction;
    r->weight=1;
    r->flags=0;
    r->scene=scene;
    r->parent=SCM_BOOL_T;
    r->csg=SCM_BOOL_T; /* csg=SCM_BOOL_T is the mark of the eye ray */
    SCM_NEWCELL(c);
    SCM_CDR(c)=(SCM)r;
    SCM_CAR(c)=tc16_Ray;
    if (global_lock==0) global_ray=c;
    init_intersections(r);
    r->object=SCM_BOOL_F;
    r->section=r->origin;

    return c;
}

/****** spawn_eye_ray
 * NAME
 *  spawn_eye_ray
 * SYNOPSIS
 *  SCM spawn_eye_ray (SCM origin, SCM scene)
 *  spawn-eye-ray (subr2)
 * INPUTS
 *  origin is a 3-dim dvector.
 *  scene is a Scene object.
 * FUNCTION
 *  This is a Scheme interface to spawn_eye_ray2.
 ******
 */

/* SUBR (subr2) */
char s_spawn_eye_ray[] = "spawn-eye-ray";
SCM spawn_eye_ray (SCM origin, SCM scene)
{
    SCM_ASSERT (SCM_NIMP(origin) && SCM_TYP7(origin)==scm_tc7_dvect && SCM_LENGTH(origin)==3, origin, SCM_ARG1, s_spawn_eye_ray);
    SCM_ASSERT (SCM_NIMP(scene) && SCM_TYP16(scene)==tc16_Scene,scene,SCM_ARG5, s_spawn_eye_ray);
    return spawn_eye_ray2 ((Point3*)SCM_VELTS(origin), scene);
}

char s_ray_access[] = "(ray access)";
SCM get_p3 (SCM r, int offset)
{
    Point3 *e;
    SCM c;

    SCM_ASSERT(SCM_NIMP(r) && SCM_TYP16(r)==tc16_Ray,r,SCM_ARG1,s_ray_access);
    e=MNEWTYPE(Point3);
    *e=*(Point3*)((int)(SCM_CDR(r))+offset);
    SCM_NEWCELL(c);
    SCM_DEFER_INTS;
    SCM_SETCHARS(c,e);
    SCM_SETLENGTH(c,3,scm_tc7_dvect);
    SCM_ALLOW_INTS;
    return c;
}

Raystruct rs;

/****** ray_accessors
 * NAME
 *  Ray access subroutines
 * SYNOPSIS
 *  SCM ray_orig (SCM r) 
 *  SCM ray_dir (SCM r) 
 *  SCM ray_nrm (SCM r) 
 *  SCM ray_length (SCM r)  
 *  SCM ray_sect (SCM r) 
 *  SCM ray_pos (SCM r) 
 *  SCM ray_tex (SCM r) 
 *  SCM ray_object (SCM r)  
 *  SCM ray_scene (SCM r)  
 *  SCM ray_flags (SCM r)  
 *  SCM ray_weight (SCM r)  
 *  SCM ray_csg (SCM r)  
 *  SCM ray_parent (SCM r)  
 *  ray-origin (subr1)
 *  ray-direction (subr1)
 *  ray-normal (subr1)
 *  ray-length (subr1)
 *  ray-section (subr1)
 *  ray-position (subr1)
 *  ray-texture (subr1)
 *  ray-object (subr1)
 *  ray-scene (subr1)
 *  ray-flags (subr1)
 *  ray-weight (subr1)
 *  ray-csg (subr1)
 *  ray-parent (subr1)
 *
 *
 *  SCM ray_disp (SCM r, SCM p)
 *  SCM ray_retext (SCM r, SCM p)
 *  ray-displace! (subr2)
 *  ray-settext! (subr2)
 * FUNCTION
 *  These are accessors to the Raystruct type. ray-displace! sets the
 *  ray-position, ray-settext! sets the ray texture normal.
 ******
 */

/* SUBR (subr1) */
char s_ray_orig[] = "ray-origin";
SCM ray_orig (SCM r) {return get_p3 (r,0);}
/* SUBR (subr1) */
char s_ray_dir[] = "ray-direction";
SCM ray_dir (SCM r) {return get_p3 (r,(int)(&rs.direction)-(int)(&rs));}
/* SUBR (subr1) */
char s_ray_nrm[] = "ray-normal";
SCM ray_nrm (SCM r) {return get_p3 (r,(int)(&rs.normal)-(int)(&rs));}
/* SUBR (subr1) */
char s_ray_length[] = "ray-length";
SCM ray_length (SCM r)  {SCM_ASSERT(SCM_NIMP(r) && SCM_TYP16(r)==tc16_Ray,r,SCM_ARG1,s_ray_access); return scm_makdbl(RAYSTR(r)->length,0.0);}
/* SUBR (subr1) */
char s_ray_sect[] = "ray-section";
SCM ray_sect (SCM r) {return get_p3 (r,(int)(&rs.section)-(int)(&rs));}
/* SUBR (subr1) */
char s_ray_pos[] = "ray-position";
SCM ray_pos (SCM r) {return get_p3 (r,(int)(&rs.pat_pos)-(int)(&rs));}
/* SUBR (subr1) */
char s_ray_text[] = "ray-texture";
SCM ray_tex (SCM r) {return get_p3 (r,(int)(&rs.pat_n)-(int)(&rs));}
/* SUBR (subr1) */
char s_ray_obj[] = "ray-object";
SCM ray_object (SCM r)  {SCM_ASSERT(SCM_NIMP(r) && SCM_TYP16(r)==tc16_Ray,r,SCM_ARG1,s_ray_access); return RAYSTR(r)->object;}
/* SUBR (subr1) */
char s_ray_scn[] = "ray-scene";
SCM ray_scene (SCM r)  {SCM_ASSERT(SCM_NIMP(r) && SCM_TYP16(r)==tc16_Ray,r,SCM_ARG1,s_ray_access); return RAYSTR(r)->scene;}
/* SUBR (subr1) */
char s_ray_flags[] = "ray-flags";
SCM ray_flags (SCM r)  {SCM_ASSERT(SCM_NIMP(r) && SCM_TYP16(r)==tc16_Ray,r,SCM_ARG1,s_ray_access); return SCM_MAKINUM(RAYSTR(r)->flags);}
/* SUBR (subr1) */
char s_ray_weight[] = "ray-weight";
SCM ray_weight (SCM r)  {SCM_ASSERT(SCM_NIMP(r) && SCM_TYP16(r)==tc16_Ray,r,SCM_ARG1,s_ray_access); return scm_makdbl(RAYSTR(r)->weight,0.0);}
/* SUBR (subr1) */
char s_ray_csg[] = "ray-csg";
SCM ray_csg (SCM r)  {SCM_ASSERT(SCM_NIMP(r) && SCM_TYP16(r)==tc16_Ray,r,SCM_ARG1,s_ray_access); return RAYSTR(r)->csg;}
/* SUBR (subr1) */
char s_ray_parent[] = "ray-parent";
SCM ray_parent (SCM r)  {SCM_ASSERT(SCM_NIMP(r) && SCM_TYP16(r)==tc16_Ray,r,SCM_ARG1,s_ray_access); return RAYSTR(r)->parent;}

/* SUBR (subr2) */     
char s_ray_disp[] = "ray-displace!";
SCM ray_disp (SCM r, SCM p)
{
    SCM_ASSERT(SCM_NIMP(r) && SCM_TYP16(r)==tc16_Ray,r,SCM_ARG1,s_ray_access);
    SCM_ASSERT(SCM_NIMP(p) && SCM_TYP7(p)==scm_tc7_dvect,p,SCM_ARG2,s_ray_access);
    RAYSTR(r)->pat_pos=*(Point3*)SCM_VELTS(p);
    return r;
}
/* SUBR (subr2) */     
char s_ray_retext[] = "ray-settext!";
SCM ray_retext (SCM r, SCM p)
{
    Raystruct *ray=RAYSTR(r);

    SCM_ASSERT(SCM_NIMP(r) && SCM_TYP16(r)==tc16_Ray,r,SCM_ARG1,s_ray_access);
    SCM_ASSERT(SCM_NIMP(p) && SCM_TYP7(p)==scm_tc7_dvect,p,SCM_ARG2,s_ray_access);
    ray->pat_n=*(Point3*)SCM_VELTS(p);
    V3Normalize(&ray->pat_n);
    return r;
}

/****** ray_eval_rec
 * NAME
 *  ray_eval_rec
 * SYNOPSIS
 *  SCM ray_eval_rec (SCM raycell, SCM *backtrace)
 * FUNCTION
 *  This call will propagate the ray until it either misses all the
 *  surfaces or it hits a true intersection (see: true_section).
 * RESULT
 *  The function returns #f if nothing was found, or the object if
 *  the section has occured. backtrace list will contain the list
 *  of all the surfaces penetrated (this list *MUST* be ran back
 *  through cross_intersection, otherwise the subsequent rays will
 *  be bugged. This call will check for the presence of zbuffer on
 *  the parent ray and use it if possible.
 ******
 */

SCM ray_eval_rec (SCM raycell, SCM *backtrace)
{
    double len=0;
    Raystruct *ray=RAYSTR(raycell), *parent;
    int z_flag;

    *backtrace=SCM_EOL;

    /* We need this to make the tree primitive working */
    if (global_lock==0) global_ray=raycell;

    z_flag=FALSE;
    if (rayp(ray->parent)) {
	parent=RAYSTR(ray->parent);
	if (PAIR_P(parent->parent)) {
	    /* zbuffer is there. Now see if we're hitting the exact pixel */
	    z_flag = get_zbuffer_point(parent->parent, ray);
	    if (z_flag == -1)
		return SCM_BOOL_F; /* no intersection (authoritative) */
	}
    }

    for (;;) {
        if (!z_flag && !cast_ray(ray))
            return SCM_BOOL_F;
	z_flag = FALSE;
        if (true_section(ray->csg_sector)) {
	    ray->length+=len;
            return ray->object;
        }
    
	*backtrace=scm_cons(ray->csg_sector, *backtrace);
	cross_intersection(ray->csg_sector);
	len+=ray->length;
	ray->origin=ray->section;
	ray->ignore_id = ray->ignore_id_out;
	ray->ignore_sphere = ray->ignore_sphere_out;
    }
}

/****** find_intersecting_object
 * NAME
 *  find_intersecting_object
 * SYNOPSIS
 *  boolean find_intersecting_object (Point3 *origin, Point3 *direction, 
 *              SCM scene, GeomObj **obj, SCM *return_ray)
 * INPUTS
 *  origin, direction   - origin and the direction of the new ray
 *  scene - the scene ray is cast into
 * FUNCTION
 *  Casts a new ray into the scene. The scene's CSGs are reinitialized,
 *  so that it may be called with any origin, not in the standard
 *  reytree-like fashion.
 * RESULT
 *  returns TRUE if an object is intersected
 *  In that case, *obj is set the the object, and the return ray
 *  contains the ray that was used to find the intersection, with
 *  section data for the caller.
 * NOTES
 *  This function is extensively used by Tree and similar primitives.
 *******
 */

boolean find_intersecting_object (int ig_id, int ig_s, Point3 *origin, Point3 *direction, SCM scene, GeomObj **obj, SCM *return_ray)
{
    double dummy1, dummy2;
    Raystruct *r;
    SCM o,rc, dummy_backtr,eye;
    Point3 or2;

    r = newray();
    r->scene = scene;
    r->origin = *origin;
    r->direction = *direction;
    r->ignore_id = ig_id;
    r->ignore_sphere = ig_s;
    SCM_NEWCELL (rc);
    SCM_CDR(rc) = (SCM)r;
    SCM_CAR(rc) = tc16_Ray;
    if (!RayBoxIntersect((Ray*)r, &SCENESTR(scene)->object_tree->min, 
			 &SCENESTR(scene)->object_tree->max, 
			 &dummy1, &dummy2)) {
	deleteray(rc);
	return FALSE;
    }
    V3Combine(origin, direction, &or2, 1.0, 25*BIGGER_EPS);
    eye = spawn_eye_ray2(&or2, scene);
    r->parent = eye;
    r->flags = RAYSTR(global_ray)->flags;
    r->weight = RAYSTR(global_ray)->weight;
    o = ray_eval_rec(rc,&dummy_backtr);
    if (o == SCM_BOOL_F) { 
	deleteray (eye);
	deleteray (rc);
	return FALSE;
    }
    *obj = OBJSTR(o);
    *return_ray = rc;
    return TRUE;
}

/****** get_entering_csg
 * NAME
 *  get_entering_csg
 * SYNOPSIS
 *  SCM get_entering_csg (SCM s)
 * FUNCTION
 *  For an object that has just been intersected by a ray, returns the
 *  toplevel container that the ray has just entered. s is the CSG
 *  field of the object.
 *  Note that if the ray is /leaving/ a volume, the value returned is SCM_BOOL_F.
 ******
 */

SCM get_entering_csg (SCM s)
{
    /* get a toplevel CSG the ray has just entered. SCM_BOOL_F if none */

    SCM par, s1;

    if (s==SCM_BOOL_F) return SCM_BOOL_F;
    if (PAIR_P(s)) {
	/* two containers. Let's see which one is real */
	s1=SCM_CAR(s);
	if (!(CSGFIELD(s1)->flags & ORIGIN_MARK))
	    s1=SCM_CDR(s);
	s=s1;
    }
    else if ((CSGFIELD(s)->flags & ORIGIN_MARK))
        return SCM_BOOL_F;
    while ((par=(CSGFIELD(s))->parent)!=SCM_BOOL_F) s=par;
    return s;
}

/****** get_iors
 * NAME
 *  get_iors
 * SYNOPSIS
 *  void get_iors (SCM s, SCM raycell, double *ior1, double *ior2)
 * FUNCTION
 *  For the volumes delimited by a surface whose CSG field is s,
 *  return the CSGs on each side. Results stored in *ior1 and *ior2
 *  Ior of vacuum (SCM_BOOL_F) defaults to 1.0.
 *  This function finds toplevel containers for the volumes, and calls
 *  their volume functions with raycell and #t parameter.
 * BUGS
 *  This is unintegrated and completely untested.
 ******
 */

void get_iors (SCM s, SCM raycell, double *ior1, double *ior2)
{
    SCM c1, c2;
    SCM par;
    double t;

    if (s==SCM_BOOL_F) {
	*ior1=*ior2=1.0;
	return;
    }
    if (PAIR_P(s)) {
	c1=SCM_CAR(s);
	if ((CSGFIELD(c1)->flags & ORIGIN_MARK)) {
	    c2=SCM_CDR(s);
	} else {
	    c1=SCM_CDR(s);
	    c2=SCM_CAR(s);
	}
   	while ((par=(CSGFIELD(c1))->parent)!=SCM_BOOL_F) c1=par;
	c1=CSGFIELD(c1)->material;
	if (PAIR_P(c1))
	    c1=SCM_CAR(c1);
	if (scm_procedure_p(c1)==SCM_BOOL_F) *ior1=1.0;
	else
	    c1=check_call(scm_procedure_property(c1, mtag_ior), raycell);
 	while ((par=(CSGFIELD(c2))->parent)!=SCM_BOOL_F) c2=par;
	c2=CSGFIELD(c2)->material;
	if (PAIR_P(c2))
	    c2=SCM_CAR(c2);
	if (scm_procedure_p(c2)==SCM_BOOL_F) *ior2=1.0;
	else
	    c2=check_call(scm_procedure_property(c2, mtag_ior), raycell);
 	if (SCM_NIMP(c1) && SCM_REALP(c1))
	    *ior1=SCM_REALPART(c1);
	else
	    *ior1=1.0;
 	if (SCM_NIMP(c2) && SCM_REALP(c2))
	    *ior2=SCM_REALPART(c2);
	else
	    *ior2=1.0;
	return;
    }
    while ((par=(CSGFIELD(s))->parent)!=SCM_BOOL_F) s=par;
    c1=CSGFIELD(s)->material;
    if (PAIR_P(c1))
        c1=SCM_CAR(c1);
    if (scm_procedure_p(c1)==SCM_BOOL_F) {
	*ior1=*ior2=1.0;
	return;
    }
    c1=check_call(scm_procedure_property(c1, mtag_ior), raycell);
    if (SCM_NIMP(c1) && SCM_REALP(c1)) {
        t=SCM_REALPART(c1);
	if ((CSGFIELD(s)->flags & ORIGIN_MARK)) {
	    *ior1=t;
	    *ior2=1.0;
	} else {
	    *ior1=1.0;
	    *ior2=t;
	}
	return;
    }
    *ior1=*ior2=1.0;
    return;
}

/****** c_get_ray_rad
 * NAME
 *  c_get_ray_rad
 * SYNOPSIS
 *  SCM c_get_ray_rad (SCM raycell, SCM pcell)
 * FUNCTION
 *  This complicated call seeks the ray intersection, then finds the
 *  color for the object, modifies it with the medium the ray has
 *  passed through and finally returns the color.
 *  raycell is SCM cell for the ray, pcell is its parent.
 * NOTES
 *    if pcell is #f, this function will assume that the ray is being
 *  forwarded by a volume.
 *    cross_intersection is called as needed. After this call returns, 
 *  CSG counters should be in the same state as on the entry to this
 *  routine.
 *  This routine serves as an arbiter between surface protocol, volume
 *  protocol etc.
 *******
 */

#define GET_MATERIAL(ray) ((ray)->mat_override!=SCM_BOOL_F ? \
			   (ray)->mat_override : \
			   OBJSTR((ray)->object)->material)
#define LAST_SHADOW(ray) (OBJSTR((ray)->object)->last_shadow_id)

void handle_backtrack (int icrossed, SCM btrc,
		       Raystruct *ray, Raystruct *parent)
{
    for (; SCM_NNULLP(btrc); btrc=SCM_CDR(btrc)) {
	cross_intersection(SCM_CAR(btrc));
    }
    if (icrossed) {
	cross_intersection(parent->csg_sector);
    }
}

static SCM eval_surface_texture (SCM raycell, Raystruct *ray)
{
    SCM mat=GET_MATERIAL(ray);
    SCM tmat;
    Point3 temp_n;
    Matrix4 *m1;
	
    EvalWithPrimitive(ray);
    V3Normalize(&ray->normal);
    if ((tmat=scm_procedure_property(mat, mtag_texture_mat))!=SCM_BOOL_F) {
	/* We won't bother with too much typechecking here */
	/* Expected information are two matrices: One is for
	       the pattern position, the other is for the normal. */
	SCM_ASSERT(SCM_ARRAYP(tmat), tmat, "Illegal texture transformation","(material evaluator)");
	m1=(Matrix4*)SCM_VELTS(SCM_ARRAY_V(tmat));
	V3MulPointByMatrix(&ray->pat_pos, m1, &temp_n);
	ray->pat_pos=temp_n;
	V3MulVectorByMatrix(&ray->pat_n, m1+16, &temp_n);
	ray->pat_n=temp_n;
    }
	  
    V3Normalize(&ray->pat_n);
    return scm_apply(mat,raycell,scm_listofnull);
}

SCM c_get_ray_rad (SCM raycell, SCM pcell)
{
    Raystruct *ray=RAYSTR(raycell);
    Raystruct *parent;
    Scene *scene=SCENESTR(ray->scene);
    int icrossed=0;
    Point3 save_origin;
    
    SCM csg,mat,c;
    SCM btrc;
    Csg *csf;

    V3Normalize (&ray->direction);
    if (pcell==SCM_BOOL_F) { /* We're called by volume material, and are
			    only supposed to forward the ray */
	pcell=ray->parent; /* We won't be evaluating the eye ray. Ever. */
	parent=RAYSTR(pcell);
	goto forward_ray;
    }
    if (ray->weight<weight_limit) {
	ray->length=BIG;
	return check_call(ambient_light, raycell);
    }
    ray->parent=pcell;
    parent=RAYSTR(pcell);
    ray->object=parent->object; /* required by cross_intersection */
    if (SCM_NIMP(parent->object) && V3Dot(&ray->direction, &parent->normal)*
	V3Dot(&parent->direction, &parent->normal)>0) {
	icrossed=1;
	cross_intersection(parent->csg_sector);
    } else
      icrossed=0;

    if (ray->object!=SCM_BOOL_F || scene->eye_csg!=SCM_BOOL_F)
	/* ray->object is the object from the parent, if it's FALSE, then
	   the ray is root, and we have to check the csg container of the eye.
	   ray->object==TRUE implies the virtual ray generated by volume
	   rendering, we check for the parent's container. */

	/*	if ((ray->object==SCM_BOOL_T && (csg=parent->csg)) ||
		(ray->object!=SCM_BOOL_F && ((csg=get_entering_csg(CSGCONT(ray->object)))!=SCM_BOOL_F)) ||
		(ray->object==SCM_BOOL_F && (csg=scene->eye_csg)!=SCM_BOOL_F)) { */
	if ((ray->object == SCM_BOOL_F && (csg = scene->eye_csg)!=SCM_BOOL_F) ||
	    (ray->object != SCM_BOOL_F && (csg = parent->csg)!=SCM_BOOL_F)) {
	    ray->csg = csg;
	    csf=CSGFIELD(csg);
	    if ((mat=csf->material)!=SCM_BOOL_F) {	
		c=scm_apply(mat,raycell,scm_listofnull);
		if (icrossed) {
		    cross_intersection(parent->csg_sector);
	 	}
		return c;
	    }
	} else if ((mat = scene->outer_volume_mat != SCM_BOOL_F)) {
	    /* outside all CSG objects, but the outer volume is defined */
	    c = scm_apply(mat, raycell, scm_listofnull);
	    if (icrossed) {
		cross_intersection(parent->csg_sector);
	    }
	    return c;
	}
    
    /* We can't use the volume to transmit the ray, either because
       there isn't any or a volume reentered this routine. We do the
       straightforward trace */
 forward_ray:
    ray->length=0;

    save_origin = ray->origin;
    ray->ignore_id = parent->ignore_id_out;
    ray->ignore_sphere = parent->ignore_sphere_out;
    ray->object=ray_eval_rec(raycell, &btrc);
    ray->origin=save_origin;

    if (ray->weight<weight_limit) {
	handle_backtrack (icrossed, btrc, ray, parent);
	return check_call(ambient_light, raycell);
    }
    if (ray->flags==RF_SHADOW && fabs(ray->length-ray->shadow_length) > EPS &&
	(ray->object==SCM_BOOL_F || scm_procedure_property(GET_MATERIAL(ray)
				  , mtag_transmit_shadows)==SCM_BOOL_F)) {
	handle_backtrack (icrossed, btrc, ray, parent);
	return SCM_BOOL_F; /* shadow ray test failure */
    }
    if (ray->object!=SCM_BOOL_F)
	c=eval_surface_texture (raycell, ray);
    else
	c=check_call(ambient_infinity, raycell);
    handle_backtrack (icrossed, btrc, ray, parent);
    return c;
}

/****** process_shadow_ray
 * NAME
 *  process_shadow_ray
 * SYNOPSIS
 *  SCM process_shadow_ray (SCM ray, SCM transparence)
 *  process-shadow-ray (subr2)
 * FUNCTION
 *  This call implements shadow-ray protocol. All the materials should
 *  check for shadow rays, to prevent extra shadows in the image. ray
 *  is the ray on the point of intersection, while transparence is the
 *  transparence (color or real number) for that material. If the ray
 *  is not a shadow ray, nothing happens and #t is returned. Otherwise,
 *  the ray is forwarded and the color is returned.
 * BUGS
 *  For now, it is assumed that the transparence color is #f or RGB.
 *  This routine is untested.
 *******
 */

/* SUBR (subr2) */
char s_process_shadow_ray[]="process-shadow-ray";
SCM process_shadow_ray (SCM ray, SCM transparence)
{
    Raystruct *r, *shdray;
    SCM shdcell;
    SCM color;

    SCM_ASSERT (rayp(ray), ray, SCM_ARG1, s_process_shadow_ray);
    SCM_ASSERT ((SCM_NIMP(transparence) && SCM_REALP(transparence)) || colorp(transparence),
	    transparence, SCM_ARG2, s_process_shadow_ray);
    r=RAYSTR(ray);
    if ((r->flags & RF_SHADOW)) {
	int transtype;
	SCM transcol;
	double *d=NULL;
	double e;
	    
	if (colorp(transparence)) {
	    transtype=1;
	    transcol=transparence;
	    if (transcol!=SCM_BOOL_F) {
		d=(double*)SCM_VELTS(transcol);
		e=0.45*d[1]+0.40*d[1]+0.15*d[2];
	    }
	}
	else {
	    transtype=2;
	    e=SCM_REALPART(transparence);
	}
	if (e<EPS)
	    return SCM_BOOL_F;  /* the material is non-transparent */

	shdray=newray();
	SCM_NEWCELL(shdcell);
	SCM_CDR(shdcell)=(SCM)shdray;
	SCM_CAR(shdcell)=tc16_Ray;
	shdray->scene=r->scene;
	shdray->origin=r->section;
	shdray->direction=r->direction;
	shdray->flags=RF_SHADOW | RF_REFR_SPEC;
	shdray->weight=r->weight*e;
	shdray->object=r->object;
	shdray->shadow_length=r->shadow_length-r->length;
	color = transtype == 2 ?
	    cscale_subr2(c_get_ray_rad(shdcell,ray),
			 scm_makdbl(transparence,0.0))
	    : cfilter_subr2(c_get_ray_rad(shdcell, ray), transparence);
	deleteray(shdcell);
	return color;
    }
    return SCM_BOOL_T;
}
	

/****** get_ray_rad
 * NAME
 *  get_ray_rad
 * SYNOPSIS
 *  SCM get_ray_rad (SCM parent, SCM direction, SCM weight, SCM flags)
 *  get-ray-radiance (gsubr)
 * INPUTS
 *  parent - the parent of the ray to be spawned
 *  direction - direction of the new ray
 *  weight - weight of the new ray
 *  flags - flags of the new ray
 * FUNCTION
 *  This call spawns a child of a the ray, and returns the list of
 *. radiance color, object, distance, and object's material.
 *  If the intersected object is a member of a tree, return the tree's
 *  subprimitive, rather than entire tree (this may not be what you wanted).
 *  This is a Scheme interface for c_get_ray_rad.
 *  This function will not handle shadow rays correctly, use
 *  process_shadow_ray instead.
 ******
 */

char s_main_eval_ray[] = "get-ray-radiance";
SCM get_ray_rad (SCM p1, SCM p2, SCM p3, SCM p4)
{
    Raystruct *ray,*parent;
    SCM raycell;
    SCM col;
    SCM length, object, mat;
    
    SCM_ASSERT(SCM_NIMP(p1) && SCM_TYP16(p1)==tc16_Ray,p1,SCM_ARG1,s_main_eval_ray);
    SCM_ASSERT(SCM_NIMP(p2) && SCM_TYP7(p2)==scm_tc7_dvect && SCM_LENGTH(p2)==3,
	   p2,SCM_ARG2,s_main_eval_ray);
    SCM_ASSERT(SCM_NIMP(p3) && SCM_REALP(p3),p3,SCM_ARG3,s_main_eval_ray);
    SCM_ASSERT(SCM_INUMP(p4),p4,SCM_ARG4,s_main_eval_ray);
    
    ray=newray();
    parent=RAYSTR(p1);
    ray->origin=parent->section;
    ray->direction=*(Point3*)SCM_VELTS(p2);
    ray->weight=parent->weight*SCM_REALPART(p3);
    ray->flags=SCM_INUM(p4);
    ray->scene=parent->scene;

    SCM_NEWCELL(raycell);
    SCM_CDR(raycell)=(SCM)ray;
    SCM_CAR(raycell)=tc16_Ray;
    
    col=c_get_ray_rad (raycell, p1);
    if (col == SCM_BOOL_F) {
	col = check_call(ambient_infinity, raycell);
	deleteray(raycell);
	return SCM_LIST4(col,           /* color */
			 SCM_BOOL_F,    /* intersected object */
			 SCM_BOOL_F,    /* distance */
			 SCM_BOOL_F);   /* material */
    }
    if (ray->object == SCM_BOOL_F) {
	deleteray(raycell);
	return SCM_LIST4(col,
			 SCM_BOOL_F,
			 SCM_BOOL_F,
			 SCM_BOOL_F);
    }
    length = scm_makdbl(ray->length, 0.0);
    object = ray->object;
    mat = GET_MATERIAL(ray);
    deleteray(raycell);
    return SCM_LIST4(col, object, length, mat);
}

/****** forw_ray_d
 * NAME
 *   forw_ray_d
 * SYNOPSIS
 *   SCM forw_ray_d (SCM rcell, SCM dir)
 *   forward-ray-distance
 * FUNCTION
 *   Instead of returning color, it only returns the distance to the
 *   nearest intersection in a given direction from rcell's section.
 *   Use for edge effects.
 ******/

/* SUBR (subr2) */
char s_forw_ray_d[]="forward-ray-distance";
SCM forw_ray_d (SCM rcell, SCM dir)
{
    double l;
    Raystruct *r, *r1;
    SCM rc;
    SCM btrc;
    int icrossed;

    SCM_ASSERT (rayp(rcell), rcell, SCM_ARG1, s_forw_ray_d);
    SCM_ASSERT (SCM_NIMP(dir) && SCM_TYP7(dir)==scm_tc7_dvect,
		dir, SCM_ARG2, s_forw_ray_d);

    r = RAYSTR(rcell);
    r1 = newray();

    r1->origin = r->section;
    r1->direction = *(Point3*)SCM_VELTS(dir);
    r1->parent = rcell;
    r1->scene = r->scene;
    r1->ignore_id = r->ignore_id_out;
    r1->ignore_sphere = r->ignore_sphere_out;
    SCM_NEWCELL(rc);
    SCM_CDR(rc) = (SCM)r1;
    SCM_CAR(rc) = tc16_Ray;

    if (SCM_NIMP(r->object) && V3Dot(&r1->direction, &r->normal)*
	V3Dot(&r1->direction, &r->normal)>0) {
	icrossed=1;
	cross_intersection(r->csg_sector);
    } else
      icrossed=0;

    ray_eval_rec(rc, &btrc);
    l = r1->length;
    handle_backtrack (icrossed, btrc, r1, r);
    deleteray (rc);
    return scm_makdbl (l, 0.0);
}

/****** check_call
 * NAME
 *  check_call
 * SYNOPSIS
 *  SCM check_call (SCM val, SCM ray)
 *  check-call (subr2)
 * FUNCTION
 *  If val is lambda, call it with the second argument, otherwise just return
 *  val. This is trivial call, and is used mainly by C-coded materials.
 ******
 */

/* SUBR (subr2) - call value if not constant */
char s_check_call[] = "check-call";
SCM check_call (SCM val, SCM ray)
{
    if (scm_procedure_p(val)==SCM_BOOL_T)
	return scm_apply(val, ray, scm_listofnull);
    return val;
}

double sq(double x) {return x*x;}

/****** set_ambient
 * NAME
 *  set_ambient
 * SYNOPSIS
 *  SCM set_ambient(SCM color)
 *  set-ambient (subr1)
 * FUNCTION
 *  Set the ambient light color / procedure
 ******
 */

/* SUBR (subr1) */
char s_set_amb[]= "set-ambient";
SCM set_ambient(SCM color)
{
    SCM_ASSERT (colorp(color), color, SCM_ARG1, s_set_amb);
    ambient_light=color;
    return color;
}

/****** set_infinity
 * NAME
 *  set_infinity
 * SYNOPSIS
 *  SCM set_infinity(SCM color)
 *  set-infinty (subr1)
 * FUNCTION
 *  Set the infinity light color / procedure
 ******
 */

/* SUBR (subr1) */
char s_set_inf[]= "set-infinity";
SCM set_infinity(SCM color)
{
    SCM_ASSERT (colorp(color), color, SCM_ARG1, s_set_inf);
    ambient_infinity = color;
    return color;
}

/****** startup_render
 * NAME
 *   startup_render
 * SYNOPSIS
 *   SCM startup_render(void)
 * FUNCTION
 *   This should be called before you cast the first ray into the image.
 ******/

/* SUBR (subr0) */
char s_startup_render[]= "startup-render";
SCM startup_render(void)
{
    global_ray = SCM_BOOL_F;
    global_lock = 0;
    startup_indirect();
    startup_hyper();
    return SCM_UNSPECIFIED;
}

/* -------------------- zbuffer functions -------------------- */

void zbuffer_check (SCM in, char *where, ZBuffer *z)
{
    SCM d, i;

    /* in is a pair of two contiguous arrays */

    SCM_ASSERT(PAIR_P(in) && PAIR_P(SCM_CDR(in)), in, SCM_ARG1, where);
    d=SCM_CAR(in);
    i=SCM_CAR(SCM_CDR(in));
    SCM_ASSERT(SCM_NIMP(d) && SCM_ARRAYP(d) && SCM_ARRAY_CONTP(d) &&
	   SCM_ARRAY_NDIM(d)==2 && SCM_TYP7(SCM_ARRAY_V(d))==scm_tc7_dvect,
	   d, SCM_ARG1, where);
    z->z = (double*)(SCM_VELTS(SCM_ARRAY_V(d)))+SCM_ARRAY_BASE(d);
    z->height = SCM_ARRAY_DIMS(d)[0].ubnd - SCM_ARRAY_DIMS(d)[0].lbnd + 1;
    z->width = SCM_ARRAY_DIMS(d)[1].ubnd - SCM_ARRAY_DIMS(d)[1].lbnd + 1;
    SCM_ASSERT(SCM_NIMP(i) && SCM_ARRAYP(i) && SCM_ARRAY_CONTP(i) &&
	   SCM_ARRAY_NDIM(i)==2 && SCM_TYP7(SCM_ARRAY_V(i))==scm_tc7_ivect &&
	   z->height==SCM_ARRAY_DIMS(i)[0].ubnd-SCM_ARRAY_DIMS(i)[0].lbnd+1 &&
	   z->width==SCM_ARRAY_DIMS(i)[1].ubnd-SCM_ARRAY_DIMS(i)[1].lbnd+1,
	   i, SCM_ARG1, where);
    z->i = (int*)(SCM_VELTS(SCM_ARRAY_V(i)))+SCM_ARRAY_BASE(i);
}

/****** set_eye_zbuffer
 * NAME
 *   set_eye_zbuffer
 * SYNOPSIS
 *   SCM set_eye_zbuffer (SCM ray, SCM zb)
 *   set-eye-zbuffer
 * FUNCTION
 *   For a given eye ray and zbuffer structure, this call sets zbuffer
 *   for the eye ray's viewpoint.
 ******/

/* SUBR (subr2) */
char s_set_eye_zbuffer[] = "set-eye-zbuffer!";
SCM set_eye_zbuffer (SCM ray, SCM zb)
{
    ZBuffer dummy;

    SCM_ASSERT (rayp(ray) && RAYSTR(ray)->csg==SCM_BOOL_T &&
	    RAYSTR(ray)->parent==SCM_BOOL_T, ray, SCM_ARG1, s_set_eye_zbuffer);
    SCM_ASSERT (global_zbuffer_mode==SCM_BOOL_T, global_zbuffer_mode,
	    "Zbuffer mode is off", s_set_eye_zbuffer);
    zbuffer_check (zb, s_set_eye_zbuffer, &dummy);
    RAYSTR(ray)->parent = zb;
}

/****** zbuff
 * NAME
 *   zbuff
 * SYNOPSIS
 *   SCM zbuff (SCM zb, SCM scene, SCM trans, SCM eye_ray)
 *   zbuffer-scene!
 * FUNCTION
 *   This will transform the scene with trans matrix, and zbuffer it.
 *   Eye ray is needed for global_ray (i.e. as the tree selector argument)
 *   Spheres will not appear, use parametrics to render them. Polygons
 *   are clipped so that z>0, |x/z|<=1, |y/z|<=1.
 ******/

char s_zbuff[] = "zbuffer-scene!";
SCM zbuff (SCM zb, SCM scene, SCM trans, SCM eye_ray)
{
    ZBuffer z;
    Scene *s;
    int i;

    if (global_lock==0) {
	global_ray=eye_ray;
    }
    zbuffer_check(zb, s_zbuff, &z);
    SCM_ASSERT (global_zbuffer_mode==SCM_BOOL_T, global_zbuffer_mode,
	    "Zbuffer mode is off", s_set_eye_zbuffer);
    SCM_ASSERT(SCM_ARRAYP(trans) && SCM_TYP7(SCM_ARRAY_V(trans))==scm_tc7_dvect,
	   trans, SCM_ARG3, s_zbuff);
    z.trans = (Matrix4*)SCM_VELTS(SCM_ARRAY_V(trans));
    SCM_ASSERT(scenep(scene), scene, SCM_ARG2, s_zbuff);
    set_zbstruct(&z);
    s=SCENESTR(scene);
    for (i=0; i<s->n; i++)
	ZBufferPersp(OBJSTR(s->object_list[i]));
}



/****** s_gl_z
 * NAME
 *   s_gl_z
 * SYNOPSIS
 *   SCM s_gl_z (SCM mode)
 *   $set-zbuffer-mode (subr1)
 * FUNCTION
 *   Call this with #t to treat spheres as tesselated objects. This must
 *   be called before constructing any scenes. It returns the old status.
 *   There is wrapper for this in scheme-core that also calculates the
 *   sphere primitive on need.
 ******/

/* SUBR (subr1) */
char s_set_gl_z[] = "$set-zbuffer-mode";
SCM set_gl_z (SCM mode)
{
    SCM old_mode;

    SCM_ASSERT(SCM_IMP(mode), mode, SCM_ARG1, s_set_gl_z);
    old_mode = global_zbuffer_mode;
    global_zbuffer_mode = mode;
    return old_mode;
}

/* ------------------- initializer --------------------- */


void init_render_c (void)
{

    /* __PERL__ This code is automatically generated. Don't modify. */

    static scm_iproc subr0s[] = {
        {s_startup_render, startup_render},
        {0,0}
    };

    static scm_iproc subr1s[] = {
        {s_scenep, scenep},
        {s_make_scene, make_scene},
        {s_rayp, rayp},
        {s_ray_orig, ray_orig},
        {s_ray_dir, ray_dir},
        {s_ray_nrm, ray_nrm},
        {s_ray_length, ray_length},
        {s_ray_sect, ray_sect},
        {s_ray_pos, ray_pos},
        {s_ray_text, ray_tex},
        {s_ray_obj, ray_object},
        {s_ray_scn, ray_scene},
        {s_ray_flags, ray_flags},
        {s_ray_weight, ray_weight},
        {s_ray_csg, ray_csg},
        {s_ray_parent, ray_parent},
        {s_set_amb, set_ambient},
        {s_set_inf, set_infinity},
        {s_set_gl_z, set_gl_z},
        {0,0}
    };

    static scm_iproc subr2s[] = {
        {s_set_bintree_defaults, set_bintree_defaults},
        {s_is_light, is_light},
        {s_set_outer_mat, set_outer_mat},
        {s_spawn_eye_ray, spawn_eye_ray},
        {s_ray_disp, ray_disp},
        {s_ray_retext, ray_retext},
        {s_process_shadow_ray, process_shadow_ray},
        {s_forw_ray_d, forw_ray_d},
        {s_check_call, check_call},
        {s_set_eye_zbuffer, set_eye_zbuffer},
        {0,0}
    };

    scm_init_iprocs (subr0s, scm_tc7_subr_0);
    scm_init_iprocs (subr1s, scm_tc7_subr_1);
    scm_init_iprocs (subr2s, scm_tc7_subr_2);

    /* __END__ */

    /* intern symbols */
    
    scm_sysintern("lightsource", mtag_light=SCM_MAKINUM(10001));
    scm_sysintern("transmit-shadows", mtag_transmit_shadows=SCM_MAKINUM(10002));
    scm_sysintern("texture-matrix", mtag_texture_mat=SCM_MAKINUM(10003));
    scm_sysintern("ior", mtag_ior=SCM_MAKINUM(10004));
    loc_ray_vector = &SCM_CDR(scm_sysintern("$ray-vector", init_ray_vector()));
    loc_sphere_geom = &SCM_CDR(scm_sysintern("$sphere-primitive",SCM_BOOL_F));

    /* create SMOBs */

    tc16_Scene=scm_newsmob(&Scenesmob);
    tc16_Ray=scm_newsmob(&Raysmob);

    /* register gsubrs */

    scm_make_gsubr(s_main_eval_ray,4,0,0,get_ray_rad);
    scm_make_gsubr(s_zbuff,4,0,0,zbuff);

    /* init globals */

    global_tree_node_size = 8;
    global_tree_depth = 30;
}
