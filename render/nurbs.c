/*
 * NurbEval.c - Code for evaluating NURB surfaces.
 *
 * John Peterson
 */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "bsp.h"

typedef struct Point4Struct {
    double x, y, z, w;
} Point4;

typedef Point4 Vector4;

/*
 * Sampled point on a surface.	This contains the point, normal and
 * surface coordinates (u,v).  This structure is passed to the rendering
 * code for shading, etc.
 */

typedef struct SurfSample {
    Point3 point, normal;   /* Point on surface, normal at that point */
    double normLen;	    /* Used for normalizing normals */
    double u, v;	    /* Parameters, e.g., used for texture mapping. */
    /* Note the parameter's range is determined by the surface's knot vector,
     * i.e., u goes from kvU[orderU-1] to kvU[numU], and likewise for v */
} SurfSample;

#define MAXORDER 20	    /* Maximum order allowed (for local array sizes) */

typedef struct NurbSurface {
    /* Number of Points in the U and V directions, respectivly */
    long numU, numV;
    /* Order of the surface in U and V (must be >= 2, < MAXORDER) */
    long orderU, orderV;
    /* Knot vectors, indexed as [0..numU+orderU-1] and [0..numV+orderV-1] */
    double * kvU, * kvV;
    /* Control points, indexed as points[0..numV-1][0..numU-1] */
    /* Note the w values are *premultiplied* with the x, y and z values */
    Point4 * points;

    /* These fields are added to support subdivision */
    boolean strV0, strVn,   /* Edge straightness flags for subdivision */
	    strU0, strUn;
    boolean flatV, flatU;   /* Surface flatness flags for subdivision */
    SurfSample c00, c0n,
	       cn0, cnn;    /* Corner data structures for subdivision */
} NurbSurface;

extern double SubdivTolerance;	/* Screen space tolerance for subdivision */

#define DIVW( rpt, pt ) \
    { (pt)->x = (rpt)->x / (rpt)->w; \
      (pt)->y = (rpt)->y / (rpt)->w; \
      (pt)->z = (rpt)->z / (rpt)->w; }

#define POINTS(_N_,_P_,_Q_) ((_N_)->points[(_P_)*((_N_)->numU) + (_Q_)])


/* ----------------------------------------------------------- */

/*
 * Return the current knot the parameter u is less than or equal to.
 * Find this "breakpoint" allows the evaluation routines to concentrate on
 * only those control points actually effecting the curve around u.]
 *
 *	m   is the number of points on the curve (or surface direction)
 *	k   is the order of the curve (or surface direction)
 *	kv  is the knot vector ([0..m+k-1]) to find the break point in.
 */

long FindBreakPoint (double u, double *kv, long m, long k)
{
    long i;

    if (u == kv[m+1])	/* Special case for closed interval */
	return m;

    i = m + k;
    while ((u < kv[i]) && (i > 0)) i--;
    return i;
}

/*
 * Compute Bi,k(u), for i = 0..k.
 *  u		is the parameter of the spline to find the basis functions for
 *  brkPoint	is the start of the knot interval ("segment")
 *  kv		is the knot vector
 *  k		is the order of the curve
 *  bvals	is the array of returned basis values.
 *
 * (From Bartels, Beatty & Barsky, p.387)
 */

static void BasisFunctions (double u, long brkPoint, double *kv,
			    long k, double *bvals)
{
    long r, s, i;
    double omega;

    bvals[0] = 1.0;
    for (r = 2; r <= k; r++) {
	i = brkPoint - r + 1;
	bvals[r - 1] = 0.0;
	for (s = r-2; s >= 0; s--) {
	    i++;
	    if (i < 0)
		omega = 0;
	    else
		omega = (u - kv[i]) / (kv[i + r - 1] - kv[i]);
	    bvals[s + 1] = bvals[s + 1] + (1 - omega) * bvals[s];
	    bvals[s] = omega * bvals[s];
	}
    }
}

/*
 * Compute derivatives of the basis functions Bi,k(u)'
 */
static void BasisDerivatives (double u, long brkPoint, double *kv, long k,
			      double *dvals)
{
    long s, i;
    double omega, knotScale;

    BasisFunctions( u, brkPoint, kv, k - 1, dvals );

    dvals[k-1] = 0.0;	    /* BasisFunctions misses this */

    knotScale = kv[brkPoint + 1L] - kv[brkPoint];

    i = brkPoint - k + 1L;
    for (s = k - 2L; s >= 0L; s--) {
	i++;
	omega = knotScale * ((double)(k-1L)) / (kv[i+k-1L] - kv[i]);
	dvals[s + 1L] += -omega * dvals[s];
	dvals[s] *= omega;
    }
}

/*
 * Calculate a point p on NurbSurface n at a specific u, v using the
 * tensor product.
 * If utan and vtan are not nil, compute the u and v tangents as well.
 *
 * Note the valid parameter range for u and v is
 * (kvU[orderU] <= u < kvU[numU), (kvV[orderV] <= v < kvV[numV])
 */

void CalcPoint (double u, double v, NurbSurface *n,
		Point3 *p, Point3 *utan, Point3 *vtan)
{
    long i, j, ri, rj;
    Point4 * cp;
    double tmp;
    double wsqrdiv;
    long ubrkPoint, ufirst;
    double bu[MAXORDER], buprime[MAXORDER];
    long vbrkPoint, vfirst;
    double bv[MAXORDER], bvprime[MAXORDER];
    Point4 r, rutan, rvtan;

    r.x = 0.0;
    r.y = 0.0;
    r.z = 0.0;
    r.w = 0.0;

    rutan = r;
    rvtan = r;

    /* Evaluate non-uniform basis functions (and derivatives) */

    ubrkPoint = FindBreakPoint( u, n->kvU, n->numU-1, n->orderU );
    ufirst = ubrkPoint - n->orderU + 1;
    BasisFunctions( u, ubrkPoint, n->kvU, n->orderU, bu );
    if (utan)
	BasisDerivatives( u, ubrkPoint, n->kvU, n->orderU, buprime );

    vbrkPoint = FindBreakPoint( v, n->kvV, n->numV-1, n->orderV );
    vfirst = vbrkPoint - n->orderV + 1;
    BasisFunctions( v, vbrkPoint, n->kvV, n->orderV, bv );
    if (vtan)
	BasisDerivatives( v, vbrkPoint, n->kvV, n->orderV, bvprime );

    /* Weight control points against the basis functions */

    for (i = 0; i < n->orderV; i++)
	for (j = 0; j < n->orderU; j++) {
	    ri = n->orderV - 1L - i;
	    rj = n->orderU - 1L - j;

	    tmp = bu[rj] * bv[ri];
	    cp = &POINTS(n, i+vfirst, j+ufirst);
	    r.x += cp->x * tmp;
	    r.y += cp->y * tmp;
	    r.z += cp->z * tmp;
	    r.w += cp->w * tmp;

	    if (utan) {
		tmp = buprime[rj] * bv[ri];
		rutan.x += cp->x * tmp;
		rutan.y += cp->y * tmp;
		rutan.z += cp->z * tmp;
		rutan.w += cp->w * tmp;
	    }
	    if (vtan) {
		tmp = bu[rj] * bvprime[ri];
		rvtan.x += cp->x * tmp;
		rvtan.y += cp->y * tmp;
		rvtan.z += cp->z * tmp;
		rvtan.w += cp->w * tmp;
	    }
	}

    /* Project tangents, using the quotient rule for differentiation */

    wsqrdiv = 1.0 / (r.w * r.w);
    if (utan) {
	utan->x = (r.w * rutan.x - rutan.w * r.x) * wsqrdiv;
	utan->y = (r.w * rutan.y - rutan.w * r.y) * wsqrdiv;
	utan->z = (r.w * rutan.z - rutan.w * r.z) * wsqrdiv;
    }
    if (vtan) {
	vtan->x = (r.w * rvtan.x - rvtan.w * r.x) * wsqrdiv;
	vtan->y = (r.w * rvtan.y - rvtan.w * r.y) * wsqrdiv;
	vtan->z = (r.w * rvtan.z - rvtan.w * r.z) * wsqrdiv;
    }

    p->x = r.x / r.w;
    p->y = r.y / r.w;
    p->z = r.z / r.w;
}

/* ------------- SCM interface --------------- */

static void set_surf (SCM n, NurbSurface *s, char *where)
{
    SCM *v;
    SCM p, k, l;
    int u1, v1;

    SCM_ASSERT (SCM_NIMP(n) && SCM_VECTORP(n) && SCM_LENGTH(n)==3, n, SCM_ARG1, where);
    v = (SCM*)SCM_VELTS(n);
    p = v[0];
    SCM_ASSERT (SCM_NIMP(p) && SCM_ARRAYP(p) &&
		SCM_ARRAY_NDIM(p)==3 && SCM_ARRAY_CONTP(p) &&
		SCM_ARRAY_DIMS(p)[0].lbnd==0 && (u1 = SCM_ARRAY_DIMS(p)[0].ubnd)>=0 &&
		SCM_ARRAY_DIMS(p)[1].lbnd==0 && (v1 = SCM_ARRAY_DIMS(p)[1].ubnd)>=0 &&
		SCM_ARRAY_DIMS(p)[2].lbnd==0 && SCM_ARRAY_DIMS(p)[2].ubnd==3,
		p, SCM_ARG1, where);
    k = v[1];
    SCM_ASSERT (SCM_NIMP(k) && SCM_TYP7(k) == scm_tc7_dvect && SCM_LENGTH(k) > u1+1,
		k, SCM_ARG1, where);
    l = v[2];
    SCM_ASSERT (SCM_NIMP(l) && SCM_TYP7(l) == scm_tc7_dvect && SCM_LENGTH(l) > v1+1,
		l, SCM_ARG1, where);
    s->numU = u1+1; s->numV = v1+1;
    s->orderU = SCM_LENGTH(k) - u1 - 1; s->orderV = SCM_LENGTH(l) - v1 - 1;
    s->kvU = ((double*)SCM_VELTS(k)); s->kvV = ((double*)SCM_VELTS(l));
    s->points = (Point4*)(((double*)SCM_VELTS(SCM_ARRAY_V(p))) + SCM_ARRAY_BASE(p));
}

/* SUBR (subr3) */
char s_nurb_eval[] = "eval-nurbs";
SCM nurb_eval (SCM n, SCM u, SCM v)
{
    NurbSurface surf;
    SCM v1, v2, v3;
    Point3 p;

    SCM_ASSERT (SCM_NIMP(u) && SCM_REALP(u), u, SCM_ARG2, s_nurb_eval);
    SCM_ASSERT (SCM_NIMP(v) && SCM_REALP(v), v, SCM_ARG3, s_nurb_eval);
    set_surf (n, &surf, s_nurb_eval);
    CalcPoint (SCM_REALPART(u), SCM_REALPART(v), &surf, &p, NULL, NULL);
    return make_dvect (3, (double*)(&p));
}

void init_nurbs_c (void)
{

    /* __PERL__ This code is automatically generated. Don't modify. */

    static scm_iproc subr3s[] = {
        {s_nurb_eval, nurb_eval},
        {0,0}
    };

    scm_init_iprocs (subr3s, scm_tc7_subr_3);

    /* __END__ */
}
