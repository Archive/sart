

;we always subdivide to certain level, after that level we only subdivide
;if needed. Eps value controls the subdivision after the minimal level

(define (get-eps subdinfo)
  (car subdinfo))

(define (get-minlevel subdinfo)
  (cdr subdinfo))

(define (box-vertices box)
  (define vectorlist
    (let subdivide ((index 0))
      (if (= index (uniform-vector-length (car box)))
	  '(())
	  (let loop ((args (subdivide (1+ index))))
	    (if (null? args)
		'()
		(cons (cons (array-ref (car box) index) (car args))
		      (cons (cons (array-ref (cdr box) index) (car args))
			    (loop (cdr args)))))))))
  (map (lambda (l) (apply dvector l)) vectorlist))

(define (box-midpoint box)
  (vscale 0.5 (v+ (car box) (cdr box))))

(define (show x) (write x) (display " ") x)

(define (do-octree box level function subdinfo octree current-index)
  (define k (uniform-vector-length (car box)))
  (define b0 (make-uniform-array 1/3 k))
  (define b1 (make-uniform-array 1/3 k))
  (for i 0 k
       (let ((x (array-ref (car box) i))
	     (y (array-ref (cdr box) i)))
	 (if (< x y)
	     (begin
	       (array-set! b0 x i)
	       (array-set! b1 y i))
	     (begin
	       (array-set! b0 y i)
	       (array-set! b1 x i)))))
  (set-car! box b0)
  (set-cdr! box b1)
  (define v (box-vertices box))
  (define m (box-midpoint box))
  (define subdivide?
    (if (> level (get-minlevel subdinfo))
	;check whether to subdivide
	(let* ((values (map function v))
	       (diameter (apply vdistance values)))
	  (> diameter (get-eps subdinfo)))
	#t))

  (if subdivide?
      (let ((newlevel (1+ level)))
	(let loop ((ind1 (add-voxel octree current-index #f))
		   (verts v))
	  (if (null? verts)
	      #t
	      (begin
		(do-octree (cons m (car verts)) newlevel function
			   subdinfo octree ind1)
		(loop (1+ ind1) (cdr verts))))))
      (add-voxel octree current-index (function m))))

(define (make-voxel box function subdinfo matrix)
  (define tree (create-voxel matrix
			     (uniform-vector-length (car box))
			     (uniform-vector-length (function (car box)))))
  (do-octree box 0 function subdinfo tree -1)
  tree)

