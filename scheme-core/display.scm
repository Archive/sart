(define (show-progress-default count left)
  (cond
   ((not (= (remainder count 50) 0))
    (write-char #\. (current-error-port)))
   ((> count 0)
    (write count (current-error-port))
    (let ((stats (get-radiosity-statistics)))
      (while (not (null? stats))
	     (display " [RCR:"  (current-error-port))
	     (write (inexact->exact
		     (floor (* 100 (/ (caar stats)
				      (cdar stats)))))
		    (current-error-port))
	     (display "%]" (current-error-port))
	     (set! stats (cdr stats)))
      (newline (current-error-port))))))

(define (write-ppm-headers xres yres)
  (display "P6\n")
  (write xres)
  (display " ")
  (write-line yres)
  (write-line 255))

(define (make-pixrow x)
  (define a (make-uniform-array #\x (* x 3)))
  (define b (make-shared-array a (lambda (x1 x2) (list (+ (* x1 3) x2))) x 3))
  (cons a b))

(define (mktest xres yres scene origin looking-at right down xpos ypos
		. options)
  (define zbuffer #f)
  (let loop ((r options))
    (if (not (null? r))
	(let ((name (caar r))
	      (value (cdar r)))
	  (case name
	    ('eye-zbuffer (set! zbuffer value))
	    (else #f))
	  (loop (cdr r)))))
  (define amb gray40)
  (set-ambient amb)
  (define forward (v- looking-at origin))
  (set! forward (vscale (/ 1 (vnorm forward)) forward))
  (set! right (orthogonalize right forward))
  (set! down  (orthogonalize down forward right))
  (define maxdim (exact->inexact (max xres yres)))
  (set! right (vscale (/ 1 maxdim) right))
  (set! down (vscale (/ 1 maxdim) down))
  (define vect (v+ forward 
		   (vscale (+ (/ xres -2.0) xpos) right)
		   (vscale (+ (/ yres -2.0) ypos) down)))
  (startup-render)
  (define eyeray (spawn-eye-ray origin scene))
  (if zbuffer
      (set-eye-zbuffer! eyeray zbuffer))
  (get-ray-radiance eyeray vect 1.0 0))

	   
(define (mkppm xres yres scene origin looking-at right down)
  (define (round-value f)
    (let ((x (* 256.0 f)))
      (if (> x 255.0)
	  (set! x 255.0))
      (integer->char (inexact->exact x))))
  (define (rgb-pixel! array color . index)
    (let ((v (rgb->vector color)))
      (apply array-set! `(,array ,(round-value (array-ref v 0)) ,@index 0))
      (apply array-set! `(,array ,(round-value (array-ref v 1)) ,@index 1))
      (apply array-set! `(,array ,(round-value (array-ref v 2)) ,@index 2))))
  (define amb gray40)
  (set-ambient amb)
  (define forward (v- looking-at origin))
  (set! forward (vscale (/ 1 (vnorm forward)) forward))
  (set! right (orthogonalize right forward))
  (set! down  (orthogonalize down forward right))
  (write-line "Generating ppm image..." (current-error-port))
  (write-ppm-headers xres yres)
  (define maxdim (exact->inexact (max xres yres)))
  (set! right (vscale (/ 1 maxdim) right))
  (set! down (vscale (/ 1 maxdim) down))
  (startup-render)
  (do ((i 0 (1+ i))
       (xvect (v- forward
		  (vscale (/ xres 2.0) right)
		  (vscale (/ yres 2.0) down))
	      (v+ xvect down))
       (red (rgb 255.0 0.0 0.0))
       (green (rgb 0.0 255.0 0.0))
       (blue (rgb 0.0 0.0 255.0))
       (eyeray (spawn-eye-ray origin scene)))
      ((>= i yres))
    (show-progress i)
    (do ((yvect xvect (v+ yvect right))
	 (pix (make-pixrow xres))
	 (j 0 (1+ j)))
	((>= j xres) (display (car pix)))
      (let ((col (get-ray-radiance eyeray yvect 1.0 0)))
	(if col
	    (rgb-pixel! (cdr pix) col j)
	    (rgb-pixel! (cdr pix) amb j))))))

(define display-options
  `((#:epsilon . 0.05)
    (#:square . 6)
    (#:anti-alias . 1)
    (#:ambient . ,gray40)
    (#:infinity . ,gray40)
    (#:resolution . (320 . 240))
    (#:jitter . 0.0)
    (#:progress . ,show-progress-default)
    (#:eye-zbuffer . #f)
    (#:output-distances . #f)
    (#:output-objects . #f)
    (#:output-materials . #f)
    (#:aperture . #f))) ; (0.06 3.8 1 0.95))))

(define (do-map scene xsize ysize forward right down eye maxsq output)
  (let* ((h (1+ maxsq))
	 (pix2 (make-uniform-array 1/3 h xsize 3))
	 (pix (make-shared-array pix2 (lambda (x y r) (list y x r)) xsize h 3))
	 (flags (make-array #f xsize h))
	 (lastcount -100)
	 (jitter (assq-ref display-options #:jitter))
	 (aa (assq-ref display-options #:anti-alias))
	 (aperture (assq-ref display-options #:aperture))
	 (forw2 0)
	 (trace-pixel2
	  (if aperture
	      (let* ((size (car aperture))
		     (focal-length (cadr aperture))
		     (resolution (caddr aperture))
		     (jitter (cadddr aperture))
		     (plane (find-orthogonal-plane forward))
		     (v1 (car plane))
		     (v2 (cdr plane))
		     (dr (/ size resolution))
		     (origin (ray-origin eye)))
		(lambda (eye dir weight flags)
		  (do ((i 0 (1+ i))
		       (looking-at (v+ origin (vscale focal-length dir)))
		       (segs 1 (+ 2 segs))
		       (r 0.0 (+ r dr))
		       (ret 0)
		       (color-total #f))
		      ((= i resolution)
		       (set-car! ret (cscale color-total
					     (/ 1.0 resolution resolution)))
		       ret)
		    (let ((dfi (/ pi2 segs)))
		      (do ((j 0 (1+ j))
			   (fi 0 (+ fi dfi)))
			  ((= j segs))
			(let* ((r-jitt (+ r (* (random) jitter dr)))
			       (fi-jitt (+ fi (* (random) jitter dfi)))
			       (x (* r-jitt (sin fi-jitt)))
			       (y (* r-jitt (cos fi-jitt)))
			       (orig2 (v+ origin (vscale x v1) (vscale y v2)))
			       (dir2 (v- looking-at orig2))
			       ;; This can be optimized for jitter == 0
			       (eye-ray (spawn-eye-ray orig2 scene)))
			  (set! ret (get-ray-radiance eye-ray dir2 weight flags))
			  (set! color-total (c+ color-total (car ret)))))))))
	      ;; The aperture isn't set - use normal raytracing
	      get-ray-radiance))
	 (trace-pixel
	  (if (> jitter 0)
	      (lambda (x y) ; jittered version
		(trace-pixel2
		 eye
		 (v+ forw2
		     (vscale (+ (exact->inexact x)
				(* (- (random) 0.5) jitter))
			     right)
		     (vscale (+ (exact->inexact y)
				(* (- (random) 0.5) jitter))
			     down))
		 1.0 0))
	      (lambda (x y) ;; simple version
		(trace-pixel2
		 eye
		 (v+ forw2
		     (vscale (exact->inexact x) right)
		     (vscale (exact->inexact y) down))
		 1.0 0))))
	 (show-progress (assq-ref display-options #:progress))
	 (eps (assq-ref display-options #:epsilon))
	 (pixel-row
	  (lambda (y)
	    (make-shared-array pix (lambda (x k) (list x y k)) xsize 3)))
	 (flags-row
	  (lambda (y)
	    (make-shared-array flags (lambda (x) (list x y)) xsize))))
    (for-step j 0 ysize maxsq
      (define yy (min h (- ysize j)))
      (for-step i 0 xsize maxsq
	(let ((xx (min h (- xsize i))))
	  (set! forw2 (v+ forward 
			  (vscale (exact->inexact i) right)
			  (vscale (exact->inexact j) down)))
	  (square-iterate! trace-pixel
			   eps
			   (make-shared-array pix
			    (lambda (x y c) (list (+ x i) y c)) xx yy 3)
			   (make-shared-array flags
			    (lambda (x y) (list (+ x i) y)) xx yy)
			   #f      ; zdistance
			   #f      ; objects
			   #f)))  ; materials
      (for i 0 (min maxsq (- ysize j))
	   (let ((rowcount (quotient (+ i j) aa)))
	     (if (not (= rowcount lastcount))
		 (begin
		   (set! lastcount rowcount)
		   (show-progress rowcount (quotient ysize aa)))))
	   (output (pixel-row i)))
      (array-copy! (pixel-row maxsq) (pixel-row 0))
      (array-copy! (flags-row maxsq) (flags-row 0))
      (array-fill! (make-shared-array flags (lambda (x y) (list x (1+ y))) xsize maxsq) #f))

  ;; signal end of file and flush
  (output #f)))

(define (antialias-filter channels out)
  (let* ((aa (assq-ref display-options #:anti-alias))
	 (xres (* aa (car (assq-ref display-options #:resolution)))))
    (if (= aa 1)
	out
	(let* ((a2 (1- aa))
	       (xr1 (quotient (+ xres a2) aa))
	       (rmap (make-uniform-array 1/3 xres aa channels))
	       (omap (make-uniform-array 1/3 xr1 channels))
	       (rcnt 0)
	       (aa-filter (let ((f (make-uniform-array 1/3 aa aa)))
			    (array-fill! f 1.0)
			    f))
	       (resize (lambda (map1 map2)
			 ;; map1 is xres,yy,channels array
			 (define n 0)
			 (for-step i 0 xres aa
				   (convolve 
				    map1 aa-filter i 0 
				    (make-shared-array map2 (lambda (col) (list n col)) channels) #t)
				   (set! n (1+ n))))))
	  (lambda (slice . rest)
	    (if slice
		(begin
		  (array-copy! slice (make-shared-array rmap (lambda (x y) (list x rcnt y)) xres channels))
		  (set! rcnt (1+ rcnt))))
	    (if (or (= rcnt aa) (not slice))
		(begin
		  (resize rmap omap)
		  (set! rcnt 0)
		  (apply out (cons omap rest)))))))))

(define (make-color-filter filter)
  (let* ((fshape (array-shape filter)))
    (apply make-shared-array
	   (list filter
		 (lambda (x y k) (list x y))
		 (car fshape) (cadr fshape) 1))))

(define (filter-rendering filter out . filter-proc)
  (let* ((xres (car (assq-ref display-options #:resolution)))
	 (row (make-uniform-array 1/3 xres 1 3)))
    (if (and (array? filter) (= (array-rank filter) 2))
	(set! filter (make-color-filter filter)))
    (apply pipe-convolve-forward `(,row 1 ,filter ,out ,@filter-proc))))

(define (standard-filter spread level . rest)
  (let* ((res (assq-ref display-options #:resolution))
	 (maxdim (max (car res) (cdr res)))
	 (s (inexact->exact (* maxdim spread 6))))
    (if (< s 1) (set! s 1))
    (define filter
      (gaussian-filter s
		       (* maxdim spread)
		       level))
    ;; 1.0 is not exactly right but the error isn't too big
    (array-set! filter 1.0 0 0)
    (let* ((filters
	    ;; process the rest argument list
	    (map (lambda (threshold)
		   (let loop ((index 1))
		     (if (< (array-ref filter 0 index) (/ 0.001 threshold))
			 (cons threshold
			       (gaussian-filter (- index 1)
						(* maxdim spread)
						level))
			 (loop (1+ index)))))
		 rest))
	   (selector (if (null? filters))
		     (lambda (val) filter)
		     (lambda (val)
		       (let loop ((f filters))
			 (if (null? f)
			     filter
			     (if (< val (caar f))
				 (cdar f)
				 (loop cdr f)))))))
      (cons filter . selector))))

(define (output-as-ppm)
  (let* ((resolution (assq-ref display-options #:resolution))
	 (xres (car resolution))
	 (yres (cdr resolution)))
    (write-ppm-headers xres yres)
    (if (> (verbose) 1)
	(let ((ce (current-error-port)))
	  (display "Generating " ce)
	  (write xres ce)
	  (display "x" ce)
	  (write yres ce)
	  (display " ppm image...\n" ce)))
    (lambda (slice . rest)
      (if slice
	  (begin
	    (clamp-color-vector! (transpose-array slice 1 0))
	    (spb-write slice
		       (current-output-port)
		       0 1 0 1.999 0.0))))))

(define (render-image scene origin looking-at right down output)
  (define zbuffer (assq-ref display-options #:eye-zbuffer))
  (define resolution (assq-ref display-options #:resolution))
  (define xres (car resolution))
  (define yres (cdr resolution))
  (set-ambient (assq-ref display-options #:ambient))
  (set-infinity (assq-ref display-options #:infinity))
  (define forward (v- looking-at origin))
  (set! forward (vscale (/ 1 (vnorm forward)) forward))
  (set! right (orthogonalize right forward))
  (set! down  (orthogonalize down forward right))
  (define aa (assq-ref display-options #:anti-alias))
  (set! xres (* aa xres))
  (set! yres (* aa yres))
  (define maxdim (exact->inexact (max xres yres)))
  (set! right (vscale (/ 1 maxdim) right))
  (set! down (vscale (/ 1 maxdim) down))
  (startup-render)
  (define eye (spawn-eye-ray origin scene))
  (if zbuffer
      (set-eye-zbuffer! eye zbuffer))
  (set! forward (v- forward
		    (vscale (/ xres 2.0) right)
		    (vscale (/ yres 2.0) down)))
  (do-map scene xres yres forward right down eye
	  (assq-ref display-options #:square)
	  (antialias-filter 3 output)))

(define (make-eye-zbuffer res-factor scene origin looking-at right down)
  (if (> (assq-ref display-options #:jitter) 0)
      (display "WARNING: using zbuffering with non-zero jitter may be counterproductive.\n" (current-error-port)))
  (let* ((resolution (assq-ref display-options #:resolution))
	 (xres (* res-factor (car resolution)))
	 (yres (* res-factor (cdr resolution)))
	 (forward (v- looking-at origin))
	 (t ident-mat)
	 (t1 (scale (dvector (/ 2.0 xres) (/ 2.0 yres) 1.0)))
	 (zbuf (make-uniform-array 1/3 yres xres))
	 (ibuf (make-uniform-array -1 yres xres))
	 (ray (spawn-eye-ray origin scene)))
    (set! forward (vscale (/ 1 (vnorm forward)) forward))
    (set! right (orthogonalize right forward))
    (set! down  (orthogonalize down forward right))
    (define maxdim (* 2.0 (max xres yres)))
    (for i 0 3
	 (array-set! t (array-ref forward i) i 2)
	 (array-set! t (array-ref right i) i 0)
	 (array-set! t (array-ref down i) i 1))
    (array-fill! zbuf 1e38)
    (array-fill! ibuf -1)
    (ensure-zbuffered-sphere)
    (let* ((mat (m* (scale (dvector (/ maxdim xres) (/ maxdim yres)
				    1.0))
		    (minvert t)
		    (translate (vscale -1.0 origin))))
	   (c (list zbuf ibuf mat (minvert mat))))
      (zbuffer-scene! c scene (caddr c) ray)
      c)))

(define (set-zbuffer-mode mode)
  (if mode
      (ensure-zbuffered-sphere))
  ($set-zbuffer-mode mode))
