;;; List normalization will convert the list into a list of triangles

(define (normalize-convex-polylist vlist)
  (let loop ((l vlist)
	     (res '()))
    (if (null? l)
	res
	(loop (cdr l)
	      (let ((starting (caar l)))
		(let loop2 ((v (cdar l))
			    (r1 res))
		  (if (null? (cdr v))
		      r1
		      (loop2 (cdr v)
			     (cons (list starting (car v) (cadr v)) r1)))))))))

(define (hash-for-each func table)
  (array-for-each (lambda (x)
		    (for-each (lambda (pair)
				(func (car pair) (cdr pair)))
			      x))
		  table))

(define polystruct-rtd (make-record-type "poly" 
			 '(vertices edges surfaces)))

;;; Record format:
;;;   vertices: a vector of dvectors
;;;   edges:    a hashtable of elements like (v1 v2) -> (poly poly ...)
;;;   surfaces: a vector of vertex lists
;;;(to add?)   v-edges:  a vector of lists of vertices reachable from a vertex
;;;(to add?)   v-surfs:  a vector of surfaces from each vertes

(define make-polystruct (record-constructor polystruct-rtd))
(define vertices (record-accessor polystruct-rtd 'vertices))
(define edges (record-accessor polystruct-rtd 'edges))
(define surfaces (record-accessor polystruct-rtd 'surfaces))
(define set-vertices! (record-modifier polystruct-rtd 'vertices))
(define set-edges! (record-modifier polystruct-rtd 'edges))
(define set-surfaces! (record-modifier polystruct-rtd 'surfaces))

(define (count-hashtable table)
  ;;; count number of elements in the hashtable
  (let ((c 0))
    (hash-for-each (lambda (key val) (set! c (1+ c)))
		   table)
    c))

(define (hashtable->vector table)
  ;;; convert a hashtable into a vector in 'edges' format
  (let ((v (make-vector (count-hashtable table)))
	(i 0))
    (hash-for-each
     (lambda (key val)
       (set! v i (cons key val))
       (set! i (1+ i))))
    v))

(define (polylist->polystruct plist verts)
  (let* ((add-new-key!
	  ;;; add a new key to the hashtable of lists
	  (lambda (table key elem)
	    (let ((c (hash-ref table key)))
	      (if c
		  (hash-set! table key (cons elem c))
		  (hash-set! table key (list elem))))))
	 (add-new-edge!
	  ;;; add a new edge to the hashtable
	  (lambda (table poly p q)
	    (let ((key (if (< p q)
			   (cons p q)
			   (cons q p))))
	      (add-new-key! table key poly))))
	 (explode-poly!
	  ;;; explode poly to edges and add them all to the table with
	  ;;; a ref to the poly
	  (lambda (table poly vlist)
	    (let ((first (car vlist)))
	      (let loop ((l vlist))
		(if (null? (cdr l))
		    (add-new-edge! table poly (car l) first)
		    (begin
		      (add-new-edge! table poly (car l) (cadr l))
		      (loop (cdr l))))))))

	 (surf-vector (list->vector plist))
	 (len (vector-length surf-vector))
	 (table (make-hash-table (* 2 (length plist)))))
    (do ((i 0 (1+ i)))
	((>= i len) table)
      (explode-poly! table i (vector-ref surf-vector i)))
    (make-polystruct (list->vector verts)
		     table
		     surf-vector)))

(define (baricentric-subdivision p midpoint)
  ;;; We assume we're dealing with triangles, at this point
  (let* ((v (vertices p))
	 (e (edges p))
	 (s (surfaces p))
	 (len-v (vector-length v))
	 (len-e (count-hashtable e))
	 (len-s (vector-length s))
	 (p-ind 0)
	 (mids (make-hash-table len-e))
	 (mid
	  (lambda (v1 v2)
	    (let ((key (if (< v1 v2)
			   (cons v1 v2)
			   (cons v2 v1))))
	      (hash-ref mids key))))
	 (pts (make-vector (+ len-e len-v)))
	 (slist '()))
    (for i 0 len-v
	 (vector-set! pts p-ind (vector-ref v i))
	 (set! p-ind (1+ p-ind)))
    (hash-for-each
     (lambda (key val)
       (vector-set! pts p-ind
		    (midpoint (vector-ref v (car key))
			      (vector-ref v (cdr key))))
       (hash-set! mids key p-ind)
       (set! p-ind (1+ p-ind)))
     e)
    (for i 0 len-s
	 (let* ((v (vector-ref s i))
		(v1 (car v))
		(v2 (cadr v))
		(v3 (caddr v)))
	   (set! slist (cons (list v1 (mid v1 v2) (mid v1 v3)) slist))
	   (set! slist (cons (list v2 (mid v2 v3) (mid v2 v1)) slist))
	   (set! slist (cons (list v3 (mid v3 v1) (mid v3 v2)) slist))
	   (set! slist (cons (list (mid v1 v2) (mid v2 v3) (mid v3 v1)) slist))))
    (polylist->polystruct slist (vector->list pts))))

(define (multiple-baricentric-subs p midpoint count)
  (for i 0 count
       (set! p (baricentric-subdivision p midpoint)))
  p)

(define (show-polystruct p material)
  (let ((v (vertices p))
	(s (surfaces p))
	(l '()))
    (for i 0 (vector-length s)
	 (set! l (cons (make-polygon
			(map (lambda (x) (vector-ref v x))
			     (vector-ref s i))
			material) l)))
    l))

(define (half-midpoint p1 p2)
  (vscale 0.5 (v+ p1 p2)))

(define (spherical-midpoint radius)
  (lambda (p1 p2)
    (let* ((x (v+ p1 p2)))
      (vscale (/ radius (vnorm x)) x))))

(define (self-subdivide p midpoint)
  (let ((v (vertices p)))
    (for i 0 (vector-length v)
	 (vector-set! v i (midpoint (vector-ref v i)
				    (vector-ref v i)))))
  p)

(define tetrahedron
  '(((0 1 2)
     (0 2 3)
     (0 1 3)
     (1 2 3))
    (#i(0. 1. 0.)
     #i(0. -0.333313247568237 -0.942816142731718)
     #i(-0.816502730703724 -0.333313247568237 0.471408071365857)
     #i(0.816502730703724 -0.333313247568237 0.471408071365857))))

(define octahedron
  '(((0 1 2)
     (0 2 3)
     (0 3 4)
     (0 4 1)
     (5 1 2)
     (5 2 3)
     (5 3 4)
     (5 4 1))
    (#i(0. 1. 0.)
     #i(0.707106781186547 0. 0.707106781186547)
     #i(-0.707106781186547 0. 0.707106781186547)
     #i(-0.707106781186547 0. -0.707106781186547)
     #i(0.707106781186547 0. -0.707106781186547)
     #i(0. -1. 0.))))

(define dodecahedron
  (list (normalize-convex-polylist
	 '((0 2 4 6 8)
	   (1 3 5 7 9)
	   (0 2 12 11 10)
	   (2 4 14 13 12)
	   (4 6 16 15 14)
	   (6 8 18 17 16)
	   (8 0 10 19 18)
	   (1 3 13 12 11)
	   (3 5 15 14 13)
	   (5 7 17 16 15)
	   (7 9 19 18 17)
	   (9 1 11 10 19)))

	'(#i(0.0                0.79507669202492 0.606508906611197)
          #i(0.356496990690094  -0.79507669202492 0.490676012688227)
	  #i(0.576824247823628  0.79507669202492 0.187421559382629)
	  #i(0.576824247823628  -0.79507669202492 -0.187421559382627)
	  #i(0.356496990690096  0.79507669202492 -0.490676012688225)
	  #i(0.0                -0.79507669202492 -0.606508906611197)
	  #i(-0.356496990690093 0.79507669202492 -0.490676012688228)
	  #i(-0.576824247823627 -0.79507669202492 -0.18742155938263)
	  #i(-0.576824247823629 0.79507669202492 0.187421559382625)
	  #i(-0.356496990690097 -0.79507669202492 0.490676012688224)
     
	  #i(0.0                0.187595611958749 0.982246347091106)
	  #i(0.577349916938305  -0.187595611958749 0.794653987459418)
	  #i(0.934171789008107  0.187595611958749 0.303530813913866)
	  #i(0.934171789008108  -0.187595611958749 -0.303530813913863)
	  #i(0.577349916938308  0.187595611958749 -0.794653987459416)
	  #i(0.0                -0.187595611958749 -0.982246347091106)
	  #i(-0.577349916938303 0.187595611958749 -0.79465398745942)
	  #i(-0.934171789008106 -0.187595611958749 -0.303530813913869)
	  #i(-0.934171789008109 0.187595611958749 0.30353081391386)
	  #i(-0.57734991693831  -0.187595611958749 0.794653987459415))))

(define icosahedron
  '(((0 3 5)
     (0 5 7)
     (0 7 9)
     (0 9 11)
     (0 11 3)
     (1 2 4)
     (1 4 6)
     (1 6 8)
     (1 8 10)
     (1 10 2)
     (2 3 4)
     (3 4 5)
     (4 5 6)
     (5 6 7)
     (6 7 8)
     (7 8 9)
     (8 9 10)
     (9 10 11)
     (10 11 2)
     (11 2 3))

    (#i(0. 1. 0.)
     #i(0. -1. 0.)
     #i(0.0                -0.447213595499989 0.8944271909999)
     #i(0.525731112119124  0.447213595499989 0.723606797749967)
     #i(0.850650808352025  -0.447213595499989 0.276393202250017)
     #i(0.850650808352026  0.447213595499989 -0.276393202250015)
     #i(0.525731112119126  -0.447213595499989 -0.723606797749965)
     #i(0.0                0.447213595499989 -0.8944271909999)
     #i(-0.525731112119122 -0.447213595499989 -0.723606797749968)
     #i(-0.850650808352024 0.447213595499989 -0.27639320225002)
     #i(-0.850650808352027 -0.447213595499989 0.276393202250012)
     #i(-0.525731112119129 0.447213595499989 0.723606797749963))))

