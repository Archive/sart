;;; basic subs


(define rmd remainder)
(define toint inexact->exact)
(define (frac x) (- x (floor x)))
(define (mod x y) (* y (frac (/ x y))))
(define (clip-to-box v size)
  "Clip the vector v to the box (0,0,0) .. size (where size is a vector."
  (dvector (mod (array-ref v 0) (array-ref size 0))
	   (mod (array-ref v 1) (array-ref size 1))
	   (mod (array-ref v 2) (array-ref size 2))))
(define (alternate x)
  "Reduce x to 0..1 interval alternating between linear rise and linear fall (saw function)."
  (define p (mod x 2))
  (if (> p 1) (- p 1) (- 2 p)))

;;; basic textures

(define (bumps x scale material)
  "Modify a material by adding bumpy normal map, where x is bump magnitude, while scale is spatial scale of the bumps."
  (lambda (ray)
    (ray-settext! ray
      (v+ (ray-texture ray)
	  (let ((p (vscale scale (ray-position ray))))
	    (vscale x (snoise3 p)))))
    (material ray)))

(define (lightsource-material color)
  "Create lightsource material with given color."
  (let ((mat (lambda (ray) color)))
    (set-procedure-property! mat lightsource #t)
    mat))

(define (make-lightsource proc)
  "Add lightsource flag to the proc."
  (set-procedure-property! proc lightsource #t)
  proc)

(define (make-shadow-transmitter proc)
  "Add transmit-shadows flag to the proc."
  (set-procedure-property! proc transmit-shadows #t)
  proc)

(define (load-ppm filename)
  "Load color PPM, return array of characters."
  (letrec ((read-line-skip-comments
	    ;; read a line but skip the '#'
	    (lambda ()
	      (let ((line (read-line)))
		(if (equal? (substring line 0 1) "#")
		    (read-line-skip-comments) ;retry
		    line)))))
    (with-input-from-file filename
      (lambda ()
	(if (not (equal? (read-line) "P6"))
	    (throw 'filetype "Raw PPM file expected."))
	(let ((value 0)
	      (x 0)
	      (y 0))
	  (with-input-from-string (read-line-skip-comments)
	    (lambda ()
	      (set! x (read))
	      (set! y (read))))
	  (with-input-from-string (read-line-skip-comments)
	    (lambda ()
	      (set! value (read))))
	  (let ((array (make-uniform-array #\x x y 3)))
	    (array-map! array read-char)
	    array))))))

(define (imagemap-texture array transformer)
  "Given array of characters or doubles, and a transformer function, return the texture with imagemap."
  (if (array? transformer)
      (lambda (ray)
	(let ((v (m* transformer (ray-position ray))))
	  (get-pixel-interpolate array (array-ref v 0) (array-ref v 1))))
      (lambda (ray)
	(let ((v (transformer (ray-position ray))))
	  (get-pixel-interpolate array (array-ref v 0) (array-ref v 1))))))

(define (create-mipmap image)
  "Create a MIP map from an image (3 dimensional array of doubles or chars). The image needs not be a square but its dimensions must be powers of two."
  ;; temporary local function
  (define (halfsize-image array)
    (let* ((shape (array-shape array))
	   (x (1+ (cadar shape)))
	   (y (1+ (cadadr shape)))
	   (filter #2i((1.0 1.0) (1.0 1.0)))
	   (output
	    (if (char? (array-ref array 0 0 0))
		(make-uniform-array #\x (/ x 2) (/ y 2) 3)
		(make-uniform-array 1/3 (/ x 2) (/ y 2) 3)))
	   (input-row (lambda (y)
			(make-shared-array array
					   (lambda (x k col) (list x (+ y k) col))
					   x 2 3)))
	   (output-row (lambda (y)
			 (make-shared-array output
					    (lambda (x col) (list x y col))
					    (/ x 2) 3)))
	   (resize (lambda (map1 map2)
		     (define n 0)
		     (for-step i 0 x 2
			       (convolve
				map1 filter i 0
				(make-shared-array map2 (lambda (col) (list n col)) 3)
				#t)
			       (set! n (1+ n))))))
      (define n 0)
      (for-step j 0 y 2
		(resize (input-row j) (output-row n))
		(set! n (1+ n)))
      output))
  (let* ((shape (array-shape image))
	 (x (1+ (cadar shape)))
	 (y (1+ (cadadr shape))))
    (let loop ((l ())
	       (current-image image)
	       (current-x x)
	       (current-y y))
      (if (= current-x 1)
	  (list->vector (reverse l))
	  (if (= current-y 1)
	      (list->vector (reverse l))
	      ;; at this point, neither of the dimensions is 1
	      (if (or (odd? current-x) (odd? current-y))
		  (throw 'range "Dimensions are not powers of two.")
		  (let ((new-image (halfsize-image current-image)))
		    (loop (cons new-image l)
			  new-image
			  (/ current-x 2) (/ current-y 2)))))))))

(define (mip-from-origin origin scale)
  "Choose the MIP map pixel by its distance from the origin."
  (let ((log2 (log 2)))
    (lambda (ray)
      (* (log (* (vdistance (ray-section ray) origin) scale)) log2))))

(define (mipmap-texture mip transformer selector)
  "Given array of characters or doubles, a transformer function, and a selector for the proper image in the mipmap, return the texture."
  (let ((t (if (array? transformer)
	       (lambda (v) (m* transformer v))
	       (transformer))))
    (lambda (ray)
      (let ((s (selector ray))
	    (l (- (vector-length mip) 1)))
	(if (< s 0.0)
	    (set! s 0.0)
	    (if (> s l)
		(set! s l)))
	(let ((v (t (ray-position ray)))
	      (array (vector-ref mip (inexact->exact (floor s)))))
	  (get-pixel-interpolate array (array-ref v 0) (array-ref v 1)))))))

(define (turbulence x scale material)
  "Modify the material by adding turbulence to it. This overwrites ray structure (so it may give unexpected results if used in chain with several materials."
  (lambda (ray)
    (ray-displace! ray
      (v+ (ray-position ray)
	  (vscale x (fnoise3 (vscale scale (ray-position ray))))))
    (material ray)))

(define (save-ray-position func)
  "Call create material that calls func safely, i.e. while saving the ray position. It will not save the ray normal."
  (lambda (ray)
    (define t1 (ray-position ray))
    (define t2 (func ray))
    (ray-displace! ray t1)
    t2))

(define (marble map dir)
  "Create a marble texture, in direction dir, with given colormap."
  (lambda (ray)
    (map (frac (vdot dir (ray-position ray))))))

(define (blobs map scale)
  "Create blobs tecture, with spatial size scale, and colormap map."
  (lambda (ray)
    (map (snoise (vscale scale (ray-position ray))))))

(define (wood map dir)
  "Create wood tecture, with spatial size scale, and colormap map."
  (define l (vnorm dir))
  (define dir1 (vscale (/ 1.0 l) dir))
  (lambda (ray)
    (map (frac
	  (let ((pos (ray-position ray)))
	    (* l (vnorm (v- pos (vscale (/ (vdot pos dir1) (vnorm pos)) pos)))))))))

(define (radial map)
  "Create radial texture."
  (lambda (ray)
    (map (vnorm (ray-position ray)))))

(define hex-project
  (let* ((sq3 (sqrt 3))
	 (sq12 (* 2 sq3))
	 (sq15 (* 1.5 sq3))
	 (sq32 (* 0.5 sq3))
	 (diffv1 (dvector 1.0 sq32))
	 (reflect (lambda (v)
		    (dvector (array-ref v 0) (- sq3 (array-ref v 1)))))
	 (hex (lambda (x y)
		(if (> y (* sq3 x))
		    (dvector (+ 1.5 x) y)
		    (if (> y (- sq12 (* sq3 x)))
			(dvector (- x 1.5) y)
			(dvector x (+ sq32 y)))))))
    (lambda (x y)
      (let ((recx (mod x 3))
	    (recy (mod y sq12)))
	(v-
	 (cond
	  ((> recy sq15) (reflect (hex recx (- sq12 recy))))
	  ((> recy sq3) (hex recx (- recy sq3)))
	  ((> recy sq32) (reflect (hex recx (- sq3 recy))))
	  (else (hex recx recy)))
	 diffv1)))))

(define hex-norm
  (let* ((sq3 (sqrt 3))
	 (sq32 (* 0.5 sq3)))
    (lambda (x y)
      (let ((ax (abs x))
	    (ay (abs y)))
	(if (> ay (* sq3 ax))
	    ay
	    (+ (* sq32 ax) (* 0.5 ay)))))))

(define (hex hex-material)
  "Create a hexagonal texture, where hex material gets called as (hex-material ray tile-xy), and tile-xy is a 2d vector clipped to the origin of the hex tile."
  (lambda (ray)
    (let* ((p (ray-position ray))
	   (x (array-ref p 0))
	   (y (array-ref p 1)))
      (hex-material ray (hex-project x y)))))

(define (chequer color1 color2 scale)
  "Basic chequer texture with two colors."
  (lambda (ray)
    (let* ((p (ray-section ray))
	   (x (frac (* (array-ref p 0) scale)))
	   (y (frac (* (array-ref p 1) scale))))
      (if (or (and (< x 0.5) (< y 0.5))
	      (and (>= x 0.5) (>= y 0.5)))
	  (if (color? color1)
	      color1
	      (color1 ray))
	  (if (color? color2)
	      color2
	      (color2 ray))))))

(define (range-limit x)
  (cond
   ((< x 0.0) 0.0)
   ((> x 1.0) 1.0)
   (else x)))

(define (mixture cfunc c1 c2)
  "Mixture of two textures, where mixfunc is used to determine the ratio (0 => first texture only, 1 => Second texture only)."
  (lambda (ray)
    (let ((x (cfunc ray)))
      (cond
       ((<= x 0.0) (check-call c1 ray))
       ((>= x 1.0) (check-call c2 ray))
       (else (linear-spline x (list 0.0 (check-call c1 ray)
				    1.0 (check-call c2 ray))))))))

(define (layered-texture . layers)
  "Layered texture requires several layers, each a material that returns a pair (color alpha) rather than plain color. It will stack the laters until cummulative alpha reaches 0. Note that the transparency can't be achieved with this texture, as it's handled by materials themselves (being physical, rather than color property)."
  (lambda (ray)
    (let loop ((l layers))
      (define val ((car l) ray))
      (if (pair? val)
	  (if (> (cdr val) 0.0)
	      (if (null? l)
		  (car val)
		  (linear-spline (cdr val) (list 0.0 (car val)
						 1.0 (loop (cdr l)))))
	      (car val))
	  val))))
	
(define (color-map . map)
  "Color map returns a function that maps real number into color (or any other interpolated thing). map is a list of arguments, of the form  threshold value, where value will be hooked to that particular threshold. Interpolator will handle cons cells, vectors, colors and procedures."
  (lambda (x)
    (linear-spline x map)))

(define (alternating-map . map)
  "Create colormap that uses saw function."
  (lambda (x)
    (linear-spline (alternate x) map)))

(define (sinus-map . map)
  "Create a map that uses sinus."
  (lambda (x)
    (linear-spline (sin x) map)))

(define cherry-wood (color-map 0.00 (rgb 0.666 0.312 0.2)
			       0.80 (rgb 0.666 0.312 0.2)
			       0.81 (rgb 0.4 0.133 0.066)
			       1.00 (rgb 0.2 0.065 0.033)))

(define pine-wood (color-map 0.00 (rgb 1.0 0.719 0.25)
			     0.80 (rgb 1.0 0.719 0.25)
			     0.81 (rgb 0.5 0.5 0.066)
			     1.00 (rgb 0.4 0.4 0.033)))

(define dark-wood (color-map 0.00 (rgb 0.429 0.238 0.047)
			     0.80 (rgb 0.429 0.238 0.047)
			     0.81 (rgb 0.400 0.333 0.066)
			     1.00 (rgb 0.200 0.333 0.033)))

(define tan-wood (color-map 0.00 (rgb 0.888 0.600 0.300)
			    0.80 (rgb 0.888 0.600 0.300)
			    0.81 (rgb 0.600 0.400 0.200)
			    1.00 (rgb 0.400 0.300 0.200)))

(define white-wood (color-map 0.00 (rgb 0.930 0.710 0.532)
			      0.80 (rgb 0.980 0.810 0.600)
			      0.81 (rgb 0.600 0.333 0.266)
			      1.00 (rgb 0.700 0.600 0.230)))

(define dark-oak-wood (color-map 0.00 (rgb 0.60 0.30 0.18)
				 0.10 (rgb 0.60 0.30 0.18)
				 0.90 (rgb 0.30 0.15 0.09)
				 1.00 (rgb 0.30 0.15 0.09)))

(define light-oak-wood (color-map 0.00 (rgb 0.42 0.26 0.15)
				  0.10 (rgb 0.42 0.26 0.15)
				  0.90 (rgb 0.52 0.37 0.26)
				  1.00 (rgb 0.52 0.37 0.26)))

(define yellow-pine (color-map 0.000 (rgb 0.808 0.671 0.251) 
			       0.222 (rgb 0.808 0.671 0.251) 
			       0.342 (rgb 0.600 0.349 0.043) 
			       0.393 (rgb 0.808 0.671 0.251) 
			       0.709 (rgb 0.808 0.671 0.251) 
			       0.821 (rgb 0.533 0.298 0.027) 
			       1.000 (rgb 0.808 0.671 0.251)))
(define yellow-pine-2 (color-map 0.000 (cons (rgb 1.000 1.000 1.000) 1.000)
				 0.120 (cons (rgb 0.702 0.412 0.118) 0.608)
				 0.231 (cons (rgb 0.702 0.467 0.118) 0.608)
				 0.496 (cons (rgb 1.000 1.000 1.000) 1.000)
				 0.701 (cons (rgb 1.000 1.000 1.000) 1.000)
				 0.829 (cons (rgb 0.702 0.467 0.118) 0.608)
				 1.000 (cons (rgb 1.000 1.000 1.000) 1.000)))
(define yellow-pine-texture
  (plastic
   (layered-texture (save-ray-position (turbulence 0.015 10.0 (wood yellow-pine-2 #i(0. 2. 0.))))
		    (turbulence 0.08 10.0 (wood yellow-pine #i(0. 10. 0.))))
   0.02 0.1))

(define embwood (color-map 0.00 (rgb 0.58 0.45 0.23) 
			   0.34 (rgb 0.65 0.45 0.25) 
			   0.40 (rgb 0.33 0.23 0.13) 
			   0.47 (rgb 0.60 0.40 0.20) 
			   1.00 (rgb 0.25 0.15 0.05)))
(define embwood-2 (color-map 0.00 (cons (rgb 1.00 1.00 1.00) 1.00)
			     0.80 (cons (rgb 1.00 0.90 0.80) 0.80)
			     1.00 (cons (rgb 0.30 0.20 0.10) 0.40)))
(define embwood-texture
  (plastic
   (layered-texture (blobs embwood-2 4.)
		    (turbulence 0.08 10.0 (wood embwood #i(0. 1. 0.))))
   0.02 0.1))

(define rosewood (color-map 0.000 (rgb 0.204 0.110 0.078)
			    0.256 (rgb 0.231 0.125 0.090)
			    0.393 (rgb 0.247 0.133 0.090)
			    0.581 (rgb 0.204 0.110 0.075)
			    0.726 (rgb 0.259 0.122 0.102)
			    0.983 (rgb 0.231 0.125 0.086)
			    1.000 (rgb 0.204 0.110 0.078)))
(define rosewood-2 (color-map 0.000 (cons (rgb 0.545 0.349 0.247) 1.000)
			      0.139 (cons (rgb 0.000 0.000 0.000) 0.004)
			      0.148 (cons (rgb 0.000 0.000 0.000) 0.004)
			      0.287 (cons (rgb 0.545 0.349 0.247) 1.000)
			      0.443 (cons (rgb 0.545 0.349 0.247) 1.000)
			      0.626 (cons (rgb 0.000 0.000 0.000) 0.004)
			      0.635 (cons (rgb 0.000 0.000 0.000) 0.004)
			      0.843 (cons (rgb 0.545 0.349 0.247) 1.000)
			      1.000 (cons (rgb 0.545 0.349 0.247) 1.000)))
(define rosewood-texture
  (plastic
   (layered-texture (turbulence 0.04 8.0 (wood rosewood-2 #i(0. 2. 0.)))
		    (blobs rosewood 8.0))
   0.02 0.1))


