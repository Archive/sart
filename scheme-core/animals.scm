

(defmacro make-skeleton (. body)
  "This creates skeleton object. The syntax of the body is:
(make-skeleton
 (tag distance transformation
      (tag ...)
      (tag ...)
      ...)
 (tag ...)
 (tag ...)
 ...)

Where tag is the unique symbol denoting the body part, distance is the
length of the further end of the bodypart from its anchor and transformation
is the matrix associated with the part. Distance and transform are evaluated
after macroexpansion, tags are not."

  (let ((tags '()))
    (let process-body ((b body))
      `(list
	,@(map (lambda (s)
		 (let ((tag (car s))
		       (length (cadr s))
		       (transform (caddr s))
		       (subbody (cdddr s)))
		   (if (member tag tags)
		       (error "Duplicate skeleton tag")
		       (set! tags (cons tag tags)))
		   `(list (quote ,tag) ,length ,transform
			  ,@(cdr (process-body subbody))))) ; skip list
	       b)))))

(define human
  (make-skeleton
   (l-shoulder 15.0 (rotate-x-deg 102.)
	       (l-arm 35. (m* (rotate-y-deg 15.) (rotate-x-deg 60.))
		      (l-forearm 35. (rotate-y-deg 50.))))
   (r-shoulder 15.0 (rotate-x-deg -102.)
	       (r-arm 35. (m* (rotate-y-deg 15.) (rotate-x-deg -60.))
		      (r-forearm 35. (rotate-y-deg 50.))))
   (spine 60. (rotate-x-deg -180.))))

(define (skeleton-csystems-alist skeleton)
  "Return alist with transformation matrices, one for each local coordinate
system."
  (let ((out '()))
    (let recurse ((skel skeleton)
		  (current ident-mat))
      (if (not (null? skel))
	  (let* ((start (m* current (caddar skel)))
		 (end (m* start (translate (dvector 0. 0. (cadar skel))))))
	    (set! out (assq-set! out (caar skel) start))
	    (recurse (cdddar skel) end)
	    (recurse (cdr skel) current))))
    out))

(define (skeleton-tag-alist skeleton)
  "Make alist of tags with their expansions."
  (let ((out '()))
    (let recurse ((skel skeleton))
      (if (not (null? skel))
	  (begin
	    (set! out (assq-set! out (caar skel) (cdar skel)))
	    (recurse (cdddar skel))
	    (recurse (cdr skel)))))
    out))

(define (set-skeleton-matrix! skeleton tag matrix)
  "Modify transformation for the bodypart."
  (set-car! (cdr (assq-ref (skeleton-tag-alist skeleton) tag))
	    matrix))

(define (set-skeleton-distance! skeleton tag distance)
  "Modify distance for the bodypart."
  (set-car! (assq-ref (skeleton-tag-alist skeleton) tag)
	    distance))

(define (get-skeleton-matrix skeleton tag)
  "Return matrix associated with the tag."
  (cadr (assq-ref (skeleton-tag-alist skeleton) tag)))

(define (get-skeleton-distance skeleton tag)
  "Return distance associated with the tag."
  (car (assq-ref (skeleton-tag-alist skeleton) tag)))

(define (draw-skeleton skeleton cylinder)
  "Create list of primitives that renders into the skeleton. Cylinder
should be cylinder aligned with Z axis, with Z from 0 to 1. It serves
as the template."
  (let ((tags (skeleton-tag-alist skeleton))
	(systems (skeleton-csystems-alist skeleton)))
    (map (lambda (x)
	   (let* ((tag (car x))
		  (matrix (cdr x))
		  (distance (car (assq-ref tags tag)))
		  (cyl1 (copy-primitive cylinder)))
	     (transform-primitive! cyl1
				   (m* matrix
				       (scale (dvector 1. 1. distance))))
	     cyl1))
	 systems)))

(define (with-skeleton-coordinates skeleton thunk)
  "This calls thunk with 3 functions:
 coord
   just transform the point to the tag's referent system
 coord-relative
   transform the point relative to length
 coord-axis-relative
   transform the point but squish length (along only that axis)"
  (let ((systems (skeleton-csystems-alist skeleton))
	(tags (skeleton-tag-alist skeleton)))
    (thunk
     (lambda (tag vector)
       (m* (assq-ref sytems tag) vector))
     (lambda (tag vector)
       (m* (assq-ref systems tag) (vscale (/ 1.0 (cadar (ainq tags tag))) vector)))
     (lambda (tag vector)
       (m* (assq-ref systems tag) (dvector (array-ref vector 0)
					   (array-ref vector 1)
					   (/ (array-ref vector 2)
					      (cadar (ainq tags tag)))))))))

(define (eval-blob components point)
  "Eval the blob at point."
  (apply +
	 (map (lambda (component)
		(let* ((position (m* (cdr component) point))
		       (r (vdot position position)))
		  (* (car component) (exp (- r)))))
	      components)))

(define (eval-blob-gradient components point)
  "Eval the blob gradient at point."
  (apply v+
	 (map (lambda (component)
		(let* ((position (m* (cdr component) point))
		       (r (vdot position position)))
		  (vscale (* (car component) -2 (exp (- r))) position)))
	      components)))

(define (blob-boundary components point)
  "Use gradient method to progress from a given point to the blob boundary."
  (let ((val (- (eval-blob components point) 0.367879441171442)))
    (if (< (abs val) 0.00001)
	point
	(blob-boundary components
		       (let ((grad (eval-blob-gradient components point)))
			 (v- point
			     (vscale (/ val (sqrt (vdot grad grad))) grad)))))))

(define (blob-midpoint blob)
  (lambda (p1 p2)
    (let* ((x (v+ p1 p2)))
      (blob-boundary blob x))))

