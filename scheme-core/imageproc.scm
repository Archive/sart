(use-modules (ice-9 common-list))

(define (list-insert l index value)
  (if (> index 0)
      (cons (car l) (list-insert (cdr l) (- index 1) value))
      (cons value l)))

(define (list-delete l index)
  (if (> index 0)
      (cons (car l) (list-delete (cdr l) (- index 1)))
      (cdr l)))

(define (remove elt l)
  (if (equal? elt (car l))
      (cdr l)
      (cons (car l) (remove elt (cdr l)))))

(define (make-from-shape proto a)
  (apply make-uniform-array (cons proto (array-shape a))))

(define (array-map-complex funct a)
  (let ((b (make-from-shape +i a)))
    (array-map! b funct a)
    b))

(define (array-map-real funct a)
  (let ((b (make-from-shape 1/3 a)))
    (array-map! b funct a)
    b))

(define (array-complexify a) (array-map-complex identity a))
(define (array-clone a) (array-map-real identity a))
(define (array-abs a) (array-map-real abs a))
(define (array-real-part a) (array-map-real real-part a))
(define (array-imag-part a) (array-map-real imag-part a))

(define (array-add! a b) (array-map! a + a b))
(define (array-sub! a b) (array-map! a - a b))
(define (array-mul! a b) (array-map! a * a b))
(define (array-div! a b) (array-map! a / a b))

(define (array-mask! a b mask) (array-map! a
				(lambda (p q r)
				  (+ (* p r)
				     (* q (- 1.0 r)))) a b mask))

;;; spec is ((start end) (start end) ...) list
(define (index-subset a . spec)
  (apply make-shared-array
	 `(,a
	   ,list
	   ,@spec)))

;;; expand indices, and fill the gaps with fill value. index-subset
;;; is a special case of this call. It creates a new array (so it's
;;; not equivalent to index-subset
(define (index-expand a fill . spec)
  (let ((b (apply make-uniform-array (cons 1/3 spec)))
	(shape (array-shape a))
	(index-valid
	 (lambda (x)
	   (every (lambda (p)
		    (<= (car p) (cadr p)))
		  x)))
	(index-intersect
	 (lambda (x y)
	   (map (lambda (p q)
		  (list (max (car p) (car q))
			(min (cadr p) (cadr q))))
		x y))))
    (array-fill! b fill)
    (let ((ind (index-intersect shape spec)))
      (if (index-valid ind)
	    (array-copy! (apply index-subset (cons a ind))
			 (apply index-subset (cons b ind)))))
      
    b))

(define (array-fft! a dir)
  (for i 0 (array-rank a)
       (array-for-each (lambda (x) (fft! x dir))
		       (apply enclose-array i)))
  a)

(define (gaussian-filter n radius scale)
  (set! radius (/ -1 radius radius))
  (let ((a (make-uniform-array 1/3 (list (- n) n) (list (- n) n))))
    (array-index-map! a (lambda (x y)
			  (* scale (exp (* (+ (* x x) (* y y))
					   radius)))))
    a))

(define (ring-filter n radius scale)
  (set! radius (/ -1 radius radius))
  (let ((a (make-uniform-array 1/3 (list (- n) n) (list (- n) n))))
    (array-index-map! a (lambda (x y)
			  (let ((r (+ (* x x) (* y y))))
			    (* scale r (exp (* r radius))))))
    a))

;;; pipe-style image procesing - for the rendering pipe

(define (array-slices array index)
  (apply enclose-array
	 (cons array (list-delete (list-0-to-n-1 (array-rank array)) index))))

(define (convolve-periodic a kernel)
  (let ((b (apply make-uniform-array (cons 1/3 (array-shape a)))))
    (array-index-map! b
     (lambda x
       (array-dot-adjust kernel a 0 x)))
    b))

(define (array-to-pipe array index out)
  (array-for-each out
		  (array-slices a index))
  (out #f))

(define (pipe-to-array array index)
  (let ((cnt 0)
	(a (array-slices array index)))
    (lambda (slice . rest)
      (array-copy! slice (array-ref a cnt))
      (set! cnt (1+ cnt)))))

(define (convolve-array! out-array filter array . channel-spec)
  (let ((filter-channel
	 ;; filter a single channel
	 (lambda (out-channel in-channel)
	   (array-index-map! (lambda x
			       (array-dot-adjust filter in-channel 1 x))
			     out-channel))))
    (if (null? channel-spec)
	(filter-channel out-array array)
	(array-for-each
	 (apply enclose-array (cons array channel-spec))
	 (apply enclose-array (cons out-array channel-spec))))))

(define (pipe-convolve output-row index filter out)
  (let* ((f (array-shape filter))
	 (shape-index (list-ref f index))
	 (filter-width (- (cadr shape-index) (car shape-index) -1))
	 (current-input-line 0)
	 (current-output-line 0)
	 (buffer-input-line 0)
	 (buffer-output-line 0)
	 (default-buffer-line (car shape-index))
	 (first-to-output (cadr shape-index))
	 (buffer (let ((t (array-shape output-row)))
		   (set-car! (nthcdr index t) (list 0 filter-width))
		   (apply make-uniform-array (cons 1/3 t))))
	 (buffer-rows rows (array-slices buffer index))
	 (output-row-extended
	  (apply make-shared-array
		 `(,(lambda (x) (list-delete x index))
		   ,output-row
		   ,@(list-insert (array-shape output-row index 1)))))
	 (convolve-row
	  (lambda (row)
	    (array-index-map! output-row-extended
			      (lambda x
				(set-car! (list-cdr-ref x index) row)
				(array-dot-adjust filter buffer 1 x))))))
    (lambda (slice . rest)
      (if slice
	  (begin
	    (if (= buffer-input-line (- filter-width 1))
		(for i 0 (- filter-width 1)
		     (array-copy! (array-ref buffer-rows (1+ i))
				  (array-ref buffer-rows i))))
	    (array-copy! slice (array-ref buffer-row buffer-input-line))
	    (if (< buffer-input-line (- filter-width 1))
		(set! buffer-input-line (1+ buffer-input-line))
		(begin
		  ;; buffer is filled at this point
		  (while (<= buffer-output-line default-buffer-line)
			 (convolve-row buffer-output-line)
			 (apply out (cons output-row rest))
			 (set! buffer-output-line (1+ buffer-output-line)))
		  (set! buffer-output-line (- buffer-output-line 1)))))
	  (begin
	    ;; the slice is #f - flush the thing
	    (while (< buffer-output-line filter-width)
		   (convolve-row buffer-output-line)
		   (apply out (cons output-row rest))
		   (set! buffer-output-line (1+ buffer-output-line))))))))

(define (pipe-convolve-forward input-row index filter out . filter-proc)
  (let* ((f (array-shape filter))
	 (shape-index (list-ref f index))
	 (filter-width (- (cadr shape-index) (car shape-index) -1))
	 (current-input-line 0)
	 (current-output-line 0)
	 (buffer-input-line 0)
	 (buffer-output-line 0)
	 (default-buffer-line (- (car shape-index)))
	 (first-to-output (cadr shape-index))
	 (buffer (let ((t (array-shape input-row)))
		   (set-car! (list-cdr-ref t index) (list 0 filter-width))
		   (apply make-uniform-array (cons 1/3 t))))
	 (buffer-rows (array-slices buffer index))
	 (accumulate-row
	  (if (null? filter-proc)
	      (lambda (slice row)
		(array-index-map! 
		 slice
		 (lambda x
		   (let* ((y (apply array-ref (cons slice x))))
		     (set! x (list-insert x index row))
		     (scale-accumulate-array! filter buffer y 1 x)
		     y))))
	      (lambda (slice row)
		(array-index-map! 
		 slice
		 (lambda x
		   (let* ((y (apply array-ref (cons slice x))))
		     (set! x (list-insert x index row))
		     (scale-accumulate-array! ((car filter-proc) y)
					      buffer y 1 x)
		     y)))))))
    (array-fill! buffer 0.0)
    (lambda (slice . rest)
      (if slice
	  (begin
	    (accumulate-row slice buffer-input-line)
	    (if (< buffer-input-line default-buffer-line)
		(set! buffer-input-line (1+ buffer-input-line))
		(begin
		  (apply out (cons (array-ref buffer-rows 0) rest))
		  (for i 0 (- filter-width 1)
		       (array-copy! (array-ref buffer-rows (1+ i))
				    (array-ref buffer-rows i)))
		  (array-fill! (array-ref buffer-rows (- filter-width 1))
			       0.0))))
	  (begin
	    ;; the slice is #f - flush the thing
	    (while (< buffer-output-line default-buffer-line)
		   (apply out (cons (array-ref buffer-rows
					       buffer-output-line)
				    rest))
		   (set! buffer-output-line (1+ buffer-output-line))))))))

(define wavelet-rtd (make-record-type "wavelet"
				      '(low-wavelet
					high-wavelet
					low-scaling
					high-scaling
					offsets
					edge-type))) ;0 = periodic, etc

(define make-wavelet (record-constructor wavelet-rtd))
(define low-wavelet (record-accessor wavelet-rtd 'low-wavelet))
(define high-wavelet (record-accessor wavelet-rtd 'high-wavelet))
(define low-scaling (record-accessor wavelet-rtd 'low-scaling))
(define high-scaling (record-accessor wavelet-rtd 'high-scaling))
(define offsets (record-accessor wavelet-rtd 'offsets))
(define edge-type (record-accessor wavelet-rtd 'edge-type))

(define (wavelet-analyze! arr wavelet ind)
  (let* ((shape (array-shape arr))
	 (l (list-0-to-n-1 (length shape)))
	 (low-offs (array-ref (offsets wavelet) 0))
	 (high-offs (array-ref (offsets wavelet) 1))
	 (lowpass (low-wavelet wavelet))
	 (highpass (high-wavelet wavelet))
	 (edge (edge-type wavelet))
	 (p (list-ref shape ind))
	 (size (- 1 (apply - p)))
	 (x (make-uniform-vector size 1/3))
	 (size-half (quotient size 2))
	 (newarr (apply enclose-array (cons arr (remove ind l)))))
    (array-for-each
     (lambda (a)
       (for j 0 size-half
	    (array-set! x
			(array-dot-adjust lowpass a (list (- (* 2 j) edge low-offs)))
			j)
	    (array-set! x
			(array-dot-adjust highpass a (list (- (* 2 j) edge high-offs)))
			(+ size-half j)))
       (array-copy! x a))
     newarr)))

(define (wavelet-synthetize! arr wavelet ind)
  (let* ((shape (array-shape arr))
	 (l (list-0-to-n-1 (length shape)))
	 (low-offs (array-ref (offsets wavelet) 2))
	 (high-offs (array-ref (offsets wavelet) 3))
	 (lowpass (low-scaling wavelet))
	 (highpass (high-scaling wavelet))
	 (edge (edge-type wavelet))
	 (p (list-ref shape ind))
	 (size (- 1 (apply - p)))
	 (size-half (quotient size 2))
	 (x (make-uniform-vector size 1/3))
	 (y (make-uniform-vector size 1/3))
	 (x1 (make-uniform-vector size 1/3))
	 (y1 (make-uniform-vector size 1/3))
	 (x-even (make-shared-array x (lambda (i) (list (* i 2))) size-half))
	 (x-odd (make-shared-array x (lambda (i) (list (1+ (* i 2)))) size-half))
	 (y-even (make-shared-array y (lambda (i) (list (* i 2))) size-half))
	 (y-odd (make-shared-array y (lambda (i) (list (1+ (* i 2)))) size-half))
	 (newarr (apply enclose-array (cons arr (remove ind l)))))
    (array-fill! x-odd 0.0)
    (array-fill! y-odd 0.0)
    (array-for-each
     (lambda (a)
       (array-copy! (make-shared-array a (lambda (i) (list i)) size-half) x-even)
       (array-copy! (make-shared-array a (lambda (i) (list (+ i size-half))) size-half) y-even)
       (for j 0 size
	    (array-set! x1 (array-dot-adjust lowpass x edge (list (- j low-offs))) j)
	    (array-set! y1 (array-dot-adjust highpass y edge (list (- j high-offs))) j))
       (array-map! a + x1 y1))
     newarr)))

(define (wavelet-transform-forward! a wavelet ind)
  (let* ((s (array-shape a))
	 (srest (nthcdr ind s)))
    (if (< (apply - (car srest)) 0)
	(begin
	  (wavelet-analyze! a wavelet ind)
	  (set-car! srest (/ (1+ (cadar srest)) 2))
	  (wavelet-transform-forward!
	   (apply make-shared-array `(,a ,identity ,@s))
	   wavelet ind)))))

(define (wavelet-transform-backward! a wavelet ind)
  (let* ((s (array-shape a))
	 (srest (nthcdr ind s))
	 (n (1+ (cadar srest)))
	 (k 1))
    (let loop ()
      (if (< k n)
	  (begin
	    (set! k (* 2 k))
	    (set-car! srest k)
	    (wavelet-synthetize!
	     (apply make-shared-array `(,a ,identity ,@s))
	     wavelet ind)
	    (loop))))))

(define (expand-wavelet filt filt-tilde offsets edge-type)
  (let* ((low (vscale 1.0 filt))
	 (high (vscale 1.0 filt-tilde))
	 (low1 (vscale 1.0 filt-tilde))
	 (high1 (vscale 1.0 filt))
	 (n 1.0))
    (for i 0 (uniform-vector-length filt)
	 (array-set! high1 (* n (array-ref filt i)) i)
	 (set! n (- n)))
    (set! n -1.0)
    (for i 0 (uniform-vector-length filt-tilde)
	 (let ((j (- (uniform-vector-length filt-tilde) i 1)))
	   (array-set! low1 (array-ref filt j) i)
	   (array-set! high (* n (array-ref filt-tilde j)) i))
	 (set! n (- n)))
    (make-wavelet low high low1 high1 offsets edge-type)))

(define wavelet-haar
  (let ((s2 (sqrt 0.5)))
    (let ((v (dvector s2 s2)))
      (expand-wavelet v v #e(0 0 1 1) 0))))

(define wavelet-daubechies-4
  (let ((s2 (sqrt 2.0))
	(s3 (sqrt 3.0)))
    (let ((v (dvector
	      (* s2 (+ 1 s3) 1/8)
	      (* s2 (+ 3 s3) 1/8)
	      (* s2 (- 3 s3) 1/8)
	      (* s2 (- 1 s3) 1/8))))
      (expand-wavelet v v #e(1 1 2 2) 0))))

(define wavelet-daubechies-6
  (let ((v #i(0.332670552950
	      0.806891509311
	      0.459877502118
	      -0.135011020010
	      -0.085441273882
	      0.035226291882)))
    (expand-wavelet v v #e(1 3 4 2) 0)))
