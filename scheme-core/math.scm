;;; transformations

(define x-axis (dvector 1. 0. 0.))
(define y-axis (dvector 0. 1. 0.))
(define z-axis (dvector 0. 0. 1.))

(define (translate x)
  "Create translation matrix for 3-vector x"
  (dmatrix 4 4
	   1.0 0.0 0.0 (array-ref x 0)
	   0.0 1.0 0.0 (array-ref x 1)
	   0.0 0.0 1.0 (array-ref x 2)
	   0.0 0.0 0.0 1.0))

(define (rotate-z fi)
  "Create a matrix that rotates about z-axis by fi radians."
  (let ((s (sin fi))
	(c (cos fi)))
    (dmatrix 4 4
	     c s 0.0 0.0
	     (- s) c 0.0 0.0
	     0.0 0.0 1.0 0.0
	     0.0 0.0 0.0 1.0)))

(define (rotate-y fi)
  "Create a matrix that rotates about y-axis by fi radians."
  (let ((s (sin fi))
	(c (cos fi)))
    (dmatrix 4 4
	     c 0.0 s 0.0
	     0.0 1.0 0.0 0.0
	     (- s) 0.0 c 0.0
	     0.0 0.0 0.0 1.0)))

(define (rotate-x fi)
  "Create a matrix that rotates about x-axis by fi radians."
  (let ((s (sin fi))
	(c (cos fi)))
    (dmatrix 4 4
	     1.0 0.0 0.0 0.0
	     0.0 c s 0.0
	     0.0 (- s) c 0.0
	     0.0 0.0 0.0 1.0)))

(defmacro for (var lower upper . body)
  "Loop body when var ranges from lower to upper-1."
  `(do ((,var ,lower (1+ ,var)))
       ((>= ,var ,upper))
     ,@body))

(defmacro for-step (var lower upper step . body)
  "Loop body when var ranges from lower to upper-1, each time increasing by step."
  `(do ((,var ,lower (+ ,var ,step)))
       ((>= ,var ,upper))
     ,@body))

(define (list-0-to-n-1 n)
  "Create a list of numbers from 0 to n-1, where n is the argument."
  (do ((i 0 (1+ i))
       (l '()))
      ((>= i n)
       (reverse l))
    (set! l (cons i l))))

(define (scale x)
  "Create scaling matrix, each of the components of the vector x denote scaling aling the appropriate axis."
  (dmatrix 4 4
	   (array-ref x 0) 0.0 0.0 0.0
	   0.0 (array-ref x 1) 0.0 0.0
	   0.0 0.0 (array-ref x 2) 0.0
	   0.0 0.0 0.0 1.0))

(define (unif-scale x)
  "Create uniform scale matrix, using real argument x."
  (dmatrix 4 4
	   x 0.0 0.0 0.0
	   0.0 x 0.0 0.0
	   0.0 0.0 x 0.0
	   0.0 0.0 0.0 1.0))

(define ident-mat (scale #i(1. 1. 1.)))
(define identity (lambda (x) x))
(define nop (lambda () #f))

(define pi 3.14159265358979)
(define pi2 (* pi 2))
(define deg->rad (/ pi 180.0))
(define rad->deg (/ 1 deg->rad))

(define (tensor x y)
  "Tensor product of the vectors x and y. It's the operator A defined by Ap = <x, p>y."
  (dmatrix 4 4
	   (* (array-ref x 0) (array-ref y 0)) (* (array-ref x 1) (array-ref y 0)) (* (array-ref x 2) (array-ref y 0)) 0.0
	   (* (array-ref x 0) (array-ref y 1)) (* (array-ref x 1) (array-ref y 1)) (* (array-ref x 2) (array-ref y 1)) 0.0
	   (* (array-ref x 0) (array-ref y 2)) (* (array-ref x 1) (array-ref y 2)) (* (array-ref x 2) (array-ref y 2)) 0.0
	   0.0 0.0 0.0 1.0))

(define (skew x)
  "Skew vector of x. It's operator defined by Ap = x cross p."
  (dmatrix 4 4
	   0. (array-ref x 2) (- (array-ref x 1)) 0.0
	   (- (array-ref x 0)) 0. (array-ref x 2) 0.0
	   (array-ref x 1) (- (array-ref x 0)) 0. 0.0
	   0.0 0.0 0.0 1.0))

(define (rotate-axis x fi)
  "Create matrix of rotation by fi radians around the axis given by vector x."
  (let ((s (sin fi))
	(c (cos fi)))
    (m+ (mscale s (skew x)) (mscalar c ident))))

(define (rotate-x-deg x) "Rotate around x axis by fi degrees." (rotate-x (* deg->rad x)))
(define (rotate-y-deg x) "Rotate around y axis by fi degrees." (rotate-y (* deg->rad x)))
(define (rotate-z-deg x) "Rotate around z axis by fi degrees." (rotate-z (* deg->rad x)))
(define (rotate-axis-deg vec x) "Rotate around axis vec by fi degrees." (rotate-axis vec (* deg->rad x)))

(define (orthogonalize v . rest)
  "Project vector v to the subspace orthogonal to the other vectors."
  (define l (vnorm v))
  (for-each (lambda (x)
	      (set! x (vscale (/ 1 (vnorm x)) x))
	      (set! v (v- v (vscale (vdot v x) x))))
	    rest)
  (vscale (/ l (vnorm v)) v))

(define (from-cyl x)
  "Rectangular vector from cylindric coordinate vector."
  (let ((r (array-ref x 0))
	(fi (array-ref x 1)))
    (dvector (* r (cos fi)) (* r (sin fi)) (array-ref x 2))))

(define (to-cyl x)
  "Rectangular vector to cylindric coordinate vector."
  (let ((a (array-ref x 0))
	(b (array-ref x 1)))
    (dvector (vnorm (dvector a b)) (atan b a) (array-ref x 2))))

(define (from-sphere x)
  "Rectangular vector to spherical coordinate vector."
  (let ((r (array-ref x 0))
	(fi (array-ref x 1))
	(psi (array-ref x 2)))
    (define c (* r (cos psi)))
    (dvector (* c (cos fi)) (* c (sin fi)) (* r (sin psi)))))

(define (to-sphere x)
  "Rectangular vector to spherical coordinate vector."
  (let ((a (array-ref x 0))
	(b (array-ref x 1))
	(c (array-ref x 2))
	(r (vnorm x)))
    (let* ((psi (asin (/ c r)))
	   (r1 (* r (cos psi))))
      (dvector r (atan (/ a r1) (/ b r1)) psi))))

(define (find-orthogonal-plane vector)
  "Return (v1 . v2) that form an orthogonal base with the given vector. v1 and v2 will be unit vectors (vector doesn't have to)."
  (let ((x (list #i(1. 0. 0.) #i(0. 1. 0.))))
    (if (> (abs (vdot vector (car x))) 0.999)
	(set-car! x #i(0. 0. 1.))
	(if (> (abs (vdot vector (cadr x))) 0.999)
	    (set-car! (cdr x) #i(0. 0. 1.))))
    (let* ((v1 (orthogonalize (car x) vector))
	   (v2 (orthogonalize (cadr x) vector v1)))
      (cons v1 v2))))

	    

