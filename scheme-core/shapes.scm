
;;; complex surfaces

(define (array-vect a i j)
  (dvector (array-ref a i j 0)
	   (array-ref a i j 1)
	   (array-ref a i j 2)))

(define (triangulate-dots dots material)
  "Dots is a m x n x 3 array of coordinates. This will create planar polygons made of material, in a mxn mesh defined by the coordinates."
  (define shape (array-dimensions dots))
  (define m (car shape))
  (define n (cadr shape))
  (do ((j 0 (1+ j))
       (l ()))
      ((>= j (1- n)) l)
    (do ((i 0 (1+ i)))
	((>= i (1- m)))
      (set! l (cons (make-polygon (list
				   (array-vect dots i j)
				   (array-vect dots (1+ i) j)
				   (array-vect dots i (1+ j)))
				  material) l))
      (set! l (cons (make-polygon (list
				   (array-vect dots (1+ i) j)
				   (array-vect dots i (1+ j))
				   (array-vect dots (1+ i) (1+ j)))
				  material) l)))))


(define (array-move-vect! src dest i j)
  (array-set! dest (array-ref src 0) i j 0)
  (array-set! dest (array-ref src 1) i j 1)
  (array-set! dest (array-ref src 2) i j 2))

(define (toeucl v)
  (dvector (array-ref v 0) (array-ref v 1) (array-ref v 2)))

(define (make-parametric function m n material . smooth)
  "Create parametric surface from function (taking 2 arguments in 0..1 interval, returning a vector), subdividing it into mxn mesh. Smooth parameter, if present, will used to estimate derivative at each node; this may cause the arguments passed to the function to leave 0-1 range and will generate a smooth surface."
  (let ((a (make-uniform-array 1/3 m n 3))
	(m1 (exact->inexact (1- m)))
	(n1 (exact->inexact (1- n))))
    (do ((j 0 (1+ j)))
	((>= j n))
      (do ((i 0 (1+ i)))
	  ((>= i m))
	(array-move-vect! (function (/ i m1) (/ j n1)) a i j)))
    (define b (if (null? smooth)
		  #f
		  (do ((j 0 (1+ j))
		       (d (make-uniform-array 1/3 m n 3))
		       (eps (car smooth))
		       (eps1 (* (car smooth) (car smooth) 1e-8))
		       (eps2 (* (car smooth) 1e-8)))
		      ((>= j n) d)
		    (do ((i 0 (1+ i)))
			((>= i m))
		      (let ((p1 (/ i m1))
			    (q1 (/ j n1)))
			(let retry          ;treat singularities correctly
			    ((p p1)
			     (q q1))
			  (let* ((u (v- (function (+ p eps) q)
					(function (- p eps) q)))
				 (v (v- (function p (+ q eps))
					(function p (- q eps))))
				 (nu (vnorm u))
				 (nv (vnorm v)))
			    (if (or (< nu eps2)
				    (< nv eps2))
				(retry (+ p (* -0.5 eps) (* (random) eps))
				       (+ q (* -0.5 eps) (* (random) eps)))
				(let* ((uv (vcross (vscale (/ 1.0 nu) v)
						   (vscale (/ 1.0 nv) u)))
				       (nuv (vnorm uv)))
				  (if (< nuv eps1)
				      (retry (+ p (* -0.5 eps) (* (random) eps))
					     (+ q (* -0.5 eps) (* (random) eps)))
				      (array-move-vect! (vscale (/ 1.0 nuv) uv) d i j)))))))))))
    (make-tessel a b material)))

(define (make-transpath curve trans-funct m n material . smooth)
  "Take curve (function from interval 0..1 to the set of vectors) and a transformation path (a function that takes 0..1 argument and returns a transformation matrix) and generate mxn mesh, smoothed if the optional smooth argument is present (it should be a derivative delta estimate)."
  (let ((a (make-uniform-array 1/3 m n 3))
	(b (make-array 0 m))
	(t (make-array 0 n))
	(m1 (exact->inexact (1- m)))
	(n1 (exact->inexact (1- n))))
    (array-index-map! b (lambda (i) (curve (/ i m1))))
    (array-index-map! t (lambda (j) (trans-funct (/ j n1))))
    (do ((j 0 (1+ j)))
	((>= j n))
      (do ((i 0 (1+ i)))
	  ((>= i m))
	(array-move-vect! (m* (array-ref t j) (array-ref b i)) a i j)))
    (define n (if (null? smooth)
		  #f
		  (let ((b1 (make-array 0 m))
			(t1 (make-array 0 n))
			(b2 (make-array 0 m))
			(t2 (make-array 0 n))
			(eps (car smooth)))
		    (array-index-map! b1 (lambda (i) (curve (- (/ i m1) eps))))
		    (array-index-map! b2 (lambda (i) (curve (+ (/ i m1) eps))))
		    (array-index-map! t1 (lambda (j) (trans-funct (- (/ j n1) eps))))
		    (array-index-map! t2 (lambda (j) (trans-funct (+ (/ j n1) eps))))
		    (do ((j 0 (1+ j))
			 (d (make-uniform-array 1/3 m n 3)))
			((>= j n) d)
		      (do ((i 0 (1+ i)))
			  ((>= i m))
			(let ((u (v- (m* (array-ref t j) (array-ref b2 i))
				     (m* (array-ref t j) (array-ref b1 i))))
			      (v (v- (m* (array-ref t2 j) (array-ref b i))
				     (m* (array-ref t1 j) (array-ref b i)))))
			  (define uv (vcross (vscale (/ 1.0 (vnorm u)) v)
					     (vscale (/ 1.0 (vnorm v)) u)))
			  (array-move-vect! (vscale (/ 1.0 (vnorm uv)) uv) d i j)))))))
    (make-tessel a n material)))

(define (revolve transformer curve m n material . smooth)
  "Create a surface by revolving the curve using transformer function (which is passed 0 .. 2pi argument. See also: make-transpath."
  (apply make-transpath
	 `(
	   ,curve
	   ,(lambda (x) (transformer (* pi2 x)))
	   ,m ,n ,material
	   ,@smooth)))

(define (extrude curve vec m material . smooth)
  "Extrude a curve by vec, generating a surface. See also: make-traspath."
  (apply make-transpath
	 `(
	   ,curve
	   ,(lambda (x) (translate (vscale x vec)))
	   ,m 2 ,material
	   ,@smooth)))

(define (arc center rad start end)
  "Generate circular arc in X-Y plane, given center, radius, and starting and ending angles (in radians)."
  (let ((delta (- end start)))
    (lambda (x)
      (let ((fi (+ start (* x delta))))
	(v+ center
	    (dvector (* rad (cos fi))
		     (* rad (sin fi))
		     0.0))))))

(define (arc2 start end angle)
  "Generate an arc in X-Y plane, given two endpoints and the angle of the arc. The endpoints must be in the same X-Y plane."
  (let* ((mid (vscale 0.5 (v+ start end)))
	 (norm (vnorm (vcross (dvector 0. 0. 1.) (v- end mid))))
	 (rad (/ (vdistance mid end) (tan (* angle 0.5))))
	 (center (v+ mid (vscale rad norm)))
	 (d (v- start center))
	 (starting-angle (atan2 (array-ref d 1) (array-ref d 0))))
    (arc center rad starting-angle (+ angle starting-angle))))

(define (bezier p1 c1 c2 p2)
  "Create cubic bezier curve, with endpoints and two points in the middle."
  (lambda (t)
    (v+
     (vscale (+ 1 (* t (+ -3 (* t (- 3 t))))) p1)
     (vscale (* 3 t (+ 1 (* t (- t 2)))) c1)
     (vscale (* 3 t t (- 1 t)) c2)
     (vscale (* t t t) p2))))

(define (line start end)
  "Create a line between two points."
  (let ((delta (v- end start)))
    (lambda (x)
      (v+ (vscale x delta) start))))

(define (transform-curve curve trans)
  "Apply a transformation matrix to a curve. Note that as curves are function, this creates an extra level of evaluation (i.e. shouldn't be accumulated in a loop)."
  (lambda (x)
    (m* trans (curve x))))

(define (join-curves curve1 curve2 param)
  "Create a new curve which will be concatenation of two curves, behaving like curve1 when its parameter is 0..param and like curve2 when its parameter is param..1."
  (let ((p1 (- 1.0 param)))
    (lambda (x)
      (if (< x param)
	  (curve1 (/ x param))
	  (curve2 (/ (- x param) p1))))))

(define (interpolate-curves curve1 curve2 value)
  "Create interpolated curve between two curves."
  (lambda (x)
    (v+ (vscale (- 1.0 value) (curve1 x))
	(vscale value (curve2 x)))))

(define (make-trans-array trans n)
  "Create a non-typed array that contains n powers of transformation matrix trans."
  (let ((d (make-array 0 n))
	(m ident-mat))
    (array-index-map! d
		      (lambda (i)
			(let ((m1 m))
			  (set! m (m* trans m))
			  m1)))
    d))

(define (curve-array curve trans n)
  "Create n copies of a curve, each shifted by trans from the previous. This will all be treated as a single curve."
  (let ((n1 (/ 1.0 n))
	(t1 (make-trans-array trans n)))
    (lambda (x)
      (let* ((int (floor (* x n)))
	     (frac (- x int)))
	(m* (array-ref (inexact->exact int)) (curve frac))))))

(define (torus r1 r2 m n material . smooth)
  "Create torus, possibly smoothed."
  (apply make-parametric (append (list (lambda (x y)
					 (let ((x1 (* x pi2))
					       (y1 (* y pi2)))
					   (define q (+ r1 (* r2 (cos y1))))
					   (dvector
					    (* q (cos x1))
					    (* q (sin x1))
					    (* r2 (sin y1)))))
				       m n material)
				 smooth)))

(define (cylinder radius height m mat)
  "Create cylinder."
  (make-parametric (lambda (x y)
		     (from-cyl (dvector radius (* x pi2) (* y height))))
		   m 1 material (* radius 0.001)))

(define (matrix-path start step n)
  "Create a list of n matrices, starting with start, and multiplying it by step each time."
  (let loop ((m start)
	     (k 0)
	     (l '()))
    (if (>= k n)
	(reverse l)
	(loop (m* step m) (1+ k) (cons m l)))))

(define (curve->polygon curve material n)
  "Make a polygon by connecting together n uniform intervals of the (hopefully) closed curve."
  (define step (/ 1.0 n))
  (let loop ((k 0)
	     (x 0.0)
	     (l '()))
    (if (>= k n)
	(apply make-polygon (list l material))
	(loop (1+ k) (+ x step) (cons (curve x) l)))))

(define (closed-prism curve vector n-sides material)
  "Create a non-smoothed prism with its closing polygons."
  ;; This returns a *list* of primitives, rather than a single
  ;; primitive. Handle with care.
  (list (curve->polygon curve material n-sides)
	(curve->polygon (transform-curve (translate vector) curve)
			material n-sides)
	(extrude curve vector n-sides material)))

(define (make-box matrix mat)
  (map
   (lambda (x) (transform-primitive! x matrix) x)
   (list
    (make-polygon (list #i(0. 0. 0.) #i(0. 1. 0.) #i(1. 1. 0.) #i(1. 0. 0.)) mat)
    (make-polygon (list #i(0. 0. 0.) #i(0. 0. 1.) #i(1. 0. 1.) #i(1. 0. 0.)) mat)
    (make-polygon (list #i(0. 0. 0.) #i(0. 1. 0.) #i(0. 1. 1.) #i(0. 0. 1.)) mat)
    (make-polygon (list #i(0. 0. 1.) #i(0. 1. 1.) #i(1. 1. 1.) #i(1. 0. 1.)) mat)
    (make-polygon (list #i(0. 1. 0.) #i(0. 1. 1.) #i(1. 1. 1.) #i(1. 1. 0.)) mat)
    (make-polygon (list #i(1. 0. 0.) #i(1. 1. 0.) #i(1. 1. 1.) #i(1. 0. 1.)) mat))))

; Some convenience functions

(define (csg* . l) "CSG intersection with void material." (csg-intersection l #f))
(define (csg+ . l) "CSG union with void material." (csg-union l #f))
(define (csg- main . l) "CSG difference with void material." (csg-minus main l #f))
(define (csg . l) "CSG container with void material." (csg-container l #f))

;;;FINISH: debug gravity

(define (lparse string initial-matrix forward . functions)
"The L-parser. The argumants are: String with syntax explained below, the initial matrix (giving overall transform), the function used for forward call (for example, cyl-link) and extra functions and custom transforms.

String syntax:
   pqr = rotations (deg)
   s   = scale (%)
   a-e = functions
   A-E = custom transformations
   ()  = group
   t   = translate forward (units)
   m   = move (without drawing) (units)
   f   = draw forward
   g   = gravity (%)"

  (define (get-number-param start default)
    (let* ((basic #f)
	   (plus #f)
	   (i
	    (let loop ((i (1+ start)))
	      (if (= (string-length string) i)
		  i
		  (if (let ((c (string-ref string i)))
			(or (char-numeric? c)
			    (eq? #\. c)))
		      (loop (1+ i))
		      (if (eq? (string-ref string i) #\+)
			  (begin
			    (set! plus (1+ i))
			    (loop (1+ i)))
			  i))))))
      ;; here comes the actual calculation
      (let ((x
	     (if (> i (1+ start))
		 (if plus
		     (+ (exact->inexact (string->number (substring string (1+ start) (1- plus))))
			(* (random) (exact->inexact (string->number (substring string plus i)))))
		     (exact->inexact (string->number (substring string (1+ start) i))))
		 default)))
	(cons i x))))
  (define temp
    (let expand ((s 0)
		 (cursor initial-matrix)
		 (matrix initial-matrix)
		 (term #f))
      (if (= (string-length string) s)
	  (if term
	      (begin
		(display "Terminator ')' expected in L-expression.\n")
		(abort 0))
	      (cons s '()))
	  (case (string-ref string s)
	    ((#\p)
	     (let ((x (get-number-param s 10.0)))
	       (expand (car x) cursor
		       (m* matrix (rotate-x (* deg->rad (cdr x)))) term)))
	    ((#\q)
	     (let ((x (get-number-param s 10.0)))
	       (expand (car x) cursor
		       (m* matrix (rotate-y (* deg->rad (cdr x)))) term)))
	    ((#\r)
	     (let ((x (get-number-param s 10.0)))
	       (expand (car x) cursor
		       (m* matrix (rotate-z (* deg->rad (cdr x)))) term)))
	    ((#\s)
	     (let ((x (get-number-param s 120.0)))
	       (expand (car x) cursor
		       (m* matrix (unif-scale (* 0.01 (cdr x)))) term)))
	    ((#\a #\b #\c #\d #\e)
	     (let ((x ((list-ref functions (- (char->integer (string-ref string s)) (char->integer #\a))) matrix))
		   (y (expand (1+ s) cursor matrix term)))
	       (cons (car y) (append x (cdr y)))))
	    ((#\A #\B #\C #\D #\E)
	     (expand (1+ s) cursor ((list-ref functions (- (char->integer (string-ref string s)) (char->integer #\A))) matrix) term))
	    ((#\()
	     (let* ((y (expand (1+ s) cursor matrix #\) ))
		    (z (expand (car y) cursor matrix term)))
	       (cons (car z) (append (cdr y) (cdr z)))))
	    ((#\))
	     (if (char=? term #\))
		 (cons (1+ s) '())
		 (begin
		   (display "Mismatched ')' in L-expression.\n")
		   (abort))))
	    ((#\t)
	     (let* ((x (get-number-param s 10.0)))
	       (expand (car x) cursor (m* matrix (translate (dvector 0. 0. (cdr x)))) term)))
	    ((#\f)
	     (let* ((x (forward cursor matrix))
		    (y (expand (1+ s) matrix matrix term)))
	       (cons (car y) (append x (cdr y)))))
	    ((#\m)
	     (expand (1+ s) matrix matrix term))
	    ((#\g)
	     (let* ((x (get-number-param s 90.0))
		    (v1 (v- (m* matrix z-axis) (m* matrix (dvector 0. 0. 0.))))
		    (v2 (vcross v1 z-axis))
		    (l1 (vnorm v1))
		    (l2 (vnorm v2))
		    (angle (acos (/ (vdot v1 z-axis) (- l1)))))
		    (expand (car x) cursor (m* matrix (rotate-axis (/ v2 l2) (* angle (- 1.0 (* 0.01 (cdr x)))))) term)))))))

  ;; We're done, now just to discard the string pointer
  (cdr temp))

(define (cyl-link start end n material)
  "Create a smoothed cylinder whose base is transformed by start and its other by end."
  (let* ((angle (/ pi2 n))
	 (next (lambda (i fi)
		 (if (= i n) 0.0 (+ fi angle))))
	 (spoint (lambda (fi)
		   (m* start (dvector (cos fi) (sin fi) 0.0))))
	 (epoint (lambda (fi)
		   (m* end (dvector (cos fi) (sin fi) 0.0)))))
    (do ((i 0 (1+ i))
	 (fi 0.0 (+ fi angle))
	 (l '() `(,(make-polygon (list
				  (spoint fi)
				  (epoint fi)
				  (spoint (next i fi))) material)
		  ,(make-polygon (list
				  (spoint (next i fi))
				  (epoint fi)
				  (epoint (next i fi))) material)
		  ,@l)))
	((> i n)
	 l))))

(define (triangular-loop func v1 v2 v3 n output)
  "Call (output x) for points in the interpolated triangle"
  (set! n (1- n))
  (let ((delta1 (vscale (/ 1.0 n) (v- v2 v1)))
	(delta2 (vscale (/ 1.0 n) (v- v3 v2))))
    (do ((i 0 (1+ i))
	 (vert-line v1 (v+ vert-line delta1)))
	((> i n))
      (do ((j 0 (1+ j))
	   (v vert-line (v+ v delta2)))
	  ((> j i))
	(output (func v))))))

(define (hyper v1 v2 v3 n1 n2 n3 func n mat)
  "Create a hypertextured patch, given 3 vertices, normal, and the displacement map func."
  (let ((l (make-uniform-vector (/ (* n (1+ n)) 2) 1/3))
	(i 0)
	(arr (make-uniform-array 1/3 6 3)))
    (triangular-loop func v1 v2 v3 n
		     (lambda (x)
		       (array-set! l x i)
		       (set! i (1+ i))))
    (do ((i 0 (1+ i)))
	((>= i 3))
      (array-set! arr (array-ref v1 i) 0 i)
      (array-set! arr (array-ref v2 i) 1 i)
      (array-set! arr (array-ref v3 i) 2 i)
      (array-set! arr (array-ref n1 i) 3 i)
      (array-set! arr (array-ref n2 i) 4 i)
      (array-set! arr (array-ref n3 i) 5 i))
    (make-hyper l arr mat)))

(define (make-nurbs nurbs m n material . smooth)
  "Create possibly smoothed NURBS surface. Nurbs is a 3-element vector, consisting of a control point array and 2 knot vectors."
  (let ((knot-u (vector-ref n 1))
	(knot-v (vector-ref n 2)))
    (let* ((min-knot-u (vector-ref knot-u 0))
	   (min-knot-v (vector-ref knot-v 0))
	   (div-knot-u (- (vector-ref knot-u (- (vector-length knot-u) 1))
			  min-knot-u))
	   (div-knot-v (- (vector-ref knot-v (- (vector-length knot-v) 1))
			  min-knot-v)))
      (apply make-parametric
	     `(,(lambda (u v) (eval-nurbs nurbs
					  (+ min-knot-u (* div-knot-u u))
					  (+ min-knot-v (* div-knot-v v))))
	       ,m ,n ,material ,@smooth)))))

;;; this is initializer for the sphere primitive. Don't change this
;;; variable unless you're an author of SART.

(set! $sphere-primitive #f)

(define (ensure-zbuffered-sphere)
  "This generates the $sphere-primitive if needed. Don't call this."
  (if (not $sphere-primitive)
      (set! $sphere-primitive
	    (make-parametric (lambda (u1 v1)
			       (let ((u (+ 1e-6 (* (- pi 2e-6) u1)))
				     (v (* pi2 v1)))
				 (let ((su (sin u))
				       (cu (cos u))
				       (sv (sin v))
				       (cv (cos v)))
				   (dvector (* cv su) (* sv su) cu))))
			     16 32 #f 0.0001))))

